'use strict';

var gulp = require('gulp');
var gutil = require('gulp-util');
var prefix = require('gulp-autoprefixer');
var minifyCss = require('gulp-minify-css');
var usemin = require('gulp-usemin');
var uglify = require('gulp-uglify');
var less = require('gulp-less');
var htmlmin = require('gulp-htmlmin');
var imagemin = require('gulp-imagemin');
var ngAnnotate = require('gulp-ng-annotate');
var ngtemplate = require('gulp-ng-templates');
var ngConstant = require('gulp-ng-constant-fork');
var jshint = require('gulp-jshint');
var rev = require('gulp-rev');
var proxy = require('proxy-middleware');
var es = require('event-stream');
var flatten = require('gulp-flatten');
var del = require('del');
var path = require('path');
var url = require('url');
var wiredep = require('wiredep').stream;
var fs = require('fs');
var runSequence = require('run-sequence') ;
var browserSync = require('browser-sync');
var inject = require('gulp-inject');
var angularFilesort = require('gulp-angular-filesort');
var util = require('util')

var karma = require('gulp-karma')({configFile: 'src/main/webapp/test/karma.conf.js'});

var argv = require('yargs').argv;
var profileConfig = {};
var env = argv.dev ? 'dev' : argv.staging ? 'staging' : argv.demo ? 'demo' : argv.prod ? 'prod' : 'war';

setReplacements(env);

function setReplacements(env) {
  console.log('RUNNING GULP ENV:', env);
  switch (env) {
    case 'dev':
      // dev
      profileConfig.targetApi = 'http://localhost:9000/';
      profileConfig.apiPath = 'api/';
      profileConfig.minify = true;
      break;
    case 'demo':
      // demo environment
      profileConfig.targetApi = 'http://127.0.0.1:8080/';
      profileConfig.apiPath = 'api/';
      profileConfig.minify = true;
      break;
    case 'prod':
      profileConfig.targetApi = 'http://minis.mpn.gov.rs/registri/';
      profileConfig.apiPath = 'api/';
      profileConfig.minify = true;
      break;
    case 'staging':
      profileConfig.targetApi = 'http://testminis.uns.ac.rs/';
      profileConfig.apiPath = 'api/';
      profileConfig.minify = true;
      break;
    default:
      // default local machine
      profileConfig.targetApi = '/';
      profileConfig.apiPath = 'api/';
      profileConfig.minify = true;
  }
}

var appConfig = {
  app: 'src/main/webapp/app/',
  appRoot: 'src/main/webapp/',
  dist: 'src/main/webapp/dist/',
  test: 'src/main/webapp/test/',
  tmp: 'src/main/webapp/.tmp/',
  importPath: 'src/main/webapp/bower_components',
  less: 'src/main/webapp/app/assets/less/',
  port: 9000,
  apiPort: 8080,
  liveReloadPort: 35729
};

var endsWith = function (str, suffix) {
  return str.indexOf('/', str.length - suffix.length) !== -1;
};

var parseString = require('xml2js').parseString;
var parseVersionFromPomXml = function() {
  var version;
  var pomXml = fs.readFileSync('pom.xml', 'utf8');
  parseString(pomXml, function (err, result) {
    version = result.project.version[0];
  });
  return version;
};

gulp.task('clean', ['clean:tmp'], function (cb) {
  return del([appConfig.dist], cb);
});

gulp.task('clean:tmp', function (cb) {
  return del([appConfig.tmp], cb);
});

gulp.task('test', function() {
  return 0;
});

gulp.task('copy', function() {
  gulp.src(appConfig.appRoot + 'i18n/**').
  pipe(gulp.dest(appConfig.dist + 'i18n/'));

  gulp.src([
    appConfig.app + 'assets/fonts/font-awesome/fonts/**',
    appConfig.app + 'assets/fonts/glyphicons/fonts/**'
  ]).
  pipe(gulp.dest(appConfig.dist + 'assets/fonts/'));

  return gulp.src(appConfig.appRoot + '*.{ico,png,txt,htaccess,html}')
    .pipe(gulp.dest(appConfig.dist));
});

gulp.task('images', function() {
  return gulp.src(appConfig.app + 'assets/images/**').
    pipe(imagemin({optimizationLevel: 5})).
    pipe(gulp.dest(appConfig.dist + 'assets/images')).
    pipe(browserSync.reload({stream: true}));
});

gulp.task('less', function () {
  return gulp.src(appConfig.less + '**/AdminLTE.less')
    .pipe(less({paths: [path.join(appConfig.app, 'assets/less', 'assets/less/bootstrap')]}))
    .pipe(gulp.dest(appConfig.app + 'assets/styles'));
});

gulp.task('styles', ['less'], function() {
  return gulp.src(appConfig.app + 'assets/styles/**/*.css').
    pipe(gulp.dest(appConfig.tmp)).
    pipe(browserSync.reload({stream: true}));
});

gulp.task('proxy', function(cb) {
  var baseUri = profileConfig.targetApi;
  // Routes to proxy to the backend. Routes ending with a / will setup
  // a redirect so that if accessed without a trailing slash, will
  // redirect. This is required for some endpoints for proxy-middleware
  // to correctly handle them.
  var proxyRoutes = [
  '/api',
  '/health',
  '/configprops',
  '/v2/api-docs',
  '/swagger-ui',
  '/configuration/security',
  '/configuration/ui',
  '/swagger-resources',
  '/metrics',
  '/websocket/tracker',
  '/dump'
  ];

  var requireTrailingSlash = proxyRoutes.filter(function (r) {
      return endsWith(r, '/');
    }).map(function (r) {
      // Strip trailing slash so we can use the route to match requests
      // with non trailing slash
      return r.substr(0, r.length - 1);
    });

  var proxies = [
      // Ensure trailing slash in routes that require it
      function (req, res, next) {
        requireTrailingSlash.forEach(function(route){
          if (url.parse(req.url).path === route) {
            res.statusCode = 301;
            res.setHeader('Location', route + '/');
            res.end();
          }
        });

        next();
      }
    ].concat(
      // Build a list of proxies for routes: [route1_proxy, route2_proxy, ...]
      proxyRoutes.map(function (r) {
        var options = url.parse('http://localhost:8080' + r);
        options.route = r;
        return proxy(options);
      }));

  browserSync({
    open: false,
    port: appConfig.port,
    server: {
      baseDir: appConfig.appRoot,
      middleware: proxies
    }
  });

  return cb();
});

gulp.task('serve', function(cb) {
  return runSequence('wiredep:test','build', 'proxy', 'watch', cb);
});

gulp.task('watch', function() {
  gulp.watch('bower.json', ['wiredep']);
  gulp.watch(['gulpfile.js', 'pom.xml'], ['ngconstant']);
  gulp.watch(appConfig.less + '**/*.less', ['styles']);
  gulp.watch(appConfig.app + 'assets/images/**', ['images']);
  gulp.watch(appConfig.app + '**/*.html', ['reload:templates']);
  gulp.watch([appConfig.appRoot + '*.html', appConfig.app + 'src/**/*.js', appConfig.app + 'i18n/**']).on('change', browserSync.reload);
});

gulp.task('reload:templates', function(cb) {
  return runSequence('writeTemplates', function() {
    browserSync.reload();
    cb();
  });
});

gulp.task('wiredep', ['wiredep:test', 'wiredep:app']);

gulp.task('wiredep:app', ['writeTemplates'], function () {
  return gulp.src(appConfig.appRoot + 'index.html')
  // Inject bower components
  .pipe(wiredep({
    exclude: [/angular-i18n/],
    fileTypes: {
      html: {
        block: /(([ \t]*)<!--\s*bower:*(\S*)\s*-->)(\n|\r|.)*?(<!--\s*endbower\s*-->)/gi,
        detect: {
          js: /<script.*src=['"]([^'"]+)/gi,
          css: /<link.*href=['"]([^'"]+)/gi
        },
        replace: {
          js: '<script src="{{filePath}}"></script>',
          css: '<link rel="stylesheet" href="{{filePath}}" />'
        }
      }
    }
  }))
  // Inject local scripts
  .pipe(inject(gulp.src(
    [
      appConfig.app + 'src/**/*.js',
      appConfig.appRoot + '.tmp/**/*.js'
    ]).pipe(angularFilesort()), {relative: true}))
  .pipe(gulp.dest(appConfig.appRoot));
});

gulp.task('wiredep:test', function () {
  return gulp.src(appConfig.appRoot + 'karma.conf.js')
  .pipe(wiredep({
    exclude: [/angular-i18n/, /angular-scenario/],
      ignorePath: /\.\.\/\.\.\//, // remove ../../ from paths of injected javascripts
      devDependencies: true,
      fileTypes: {
        js: {
          block: /(([\s\t]*)\/\/\s*bower:*(\S*))(\n|\r|.)*?(\/\/\s*endbower)/gi,
          detect: {
            js: /'(.*\.js)'/gi
          },
          replace: {
            js: '\'{{filePath}}\','
          }
        }
      }
    }))
  .pipe(gulp.dest('src/test/javascript'));
});

gulp.task('build', function (cb) {
  return runSequence('clean', 'copy', 'ngconstant', 'wiredep:app', 'usemin', cb);
});

function getJSCommands() {
  if (profileConfig.minify) {
    return [
      ngAnnotate(),
      uglify(),
      'concat',
      rev()
    ];
  } else {
    return [
      ngAnnotate(),
      'concat',
      rev()
    ];
  }
}

function getTemplateStream() {
  return gulp.src([appConfig.app + '**/*.html', '!' + appConfig.app + 'src/theme{,/**}', '!' + appConfig.app + '*.html', '!' + appConfig.app + 'assets/**/*.html'])
    .pipe(htmlmin({collapseWhitespace: true, collapseBooleanAttributes: true, removeComments: true,
      removeCommentsFromCDATA: true, removeOptionalTags: true}))
    .pipe(ngtemplate({module: 'emMinisTemplates', standalone: true, filename: 'templates.js'}))
  }

gulp.task('writeTemplates', function() {
  return getTemplateStream().pipe(gulp.dest(appConfig.tmp + 'templates/'));
});

gulp.task('minify', function () {
  return gulp.src([appConfig.appRoot + 'index.html']).
  pipe(usemin({
    minisCss: [
      prefix.apply(),
      minifyCss(),  // Replace relative paths for static resources with absolute path with root
      'concat', // Needs to be present for minifyCss root option to work
      rev()
    ],
    css: [
      prefix.apply(),
      minifyCss(),  // Replace relative paths for static resources with absolute path with root
      'concat', // Needs to be present for minifyCss root option to work
      rev()
    ],
    js: getJSCommands(),
    minisJS: getJSCommands()
  })).
  pipe(gulp.dest(appConfig.dist));
});

gulp.task('usemin', function(cb) {
  return runSequence('images', 'styles', 'writeTemplates', 'minify', cb);
});

gulp.task('ngconstant', function() {
  return ngConstant({
    dest: 'config-constants.js',
    name: 'emMinisConfiguration',
    stream: true,
    deps:   false,
    noFile: true,
    space: '  ',
    interpolate: /\{%=(.+?)%\}/g,
    template: 'var mod = angular.module(\'<%- moduleName %>\'<% if (deps) { %>, <%= JSON.stringify(deps) %><% } %>);\n\n' +
      '<% constants.forEach(function(constant) { %>' +
      '  mod.constant(\'<%- constant.name %>\', <%= constant.value %>);\n' +
      '<% }) %>',
    wrap: '/* jshint quotmark: false */\n(function() {\n' +
      '  "use strict";\n  // DO NOT EDIT THIS FILE, EDIT THE GULP TASK NGCONSTANT SETTINGS INSTEAD WHICH GENERATES THIS FILE,\n' +
      '  // OR THE src/main/resources/config/front_profiles/*.json FILES\n' +
      '  {%= __ngModule %}\n})();\n',
    constants: {
      HOST: profileConfig.targetApi,
      API_PATH: profileConfig.apiPath,
      VERSION: parseVersionFromPomXml()
    }
  })
  .pipe(gulp.dest(appConfig.app + 'src/configuration/services/'));
});

gulp.task('jshint', function() {
  return gulp.src(['gulpfile.js', appConfig.app + 'src/**/*.js'])
  .pipe(jshint())
  .pipe(jshint.reporter('jshint-stylish'))
  .pipe(jscs({configPath: '.jscsrc'}));
});

gulp.task('server', ['serve'], function () {
  gutil.log('The `server` task has been deprecated. Use `gulp serve` to start a server');
});

gulp.task('default', function() {
  runSequence('serve');
});
