package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.Role;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Role entity.
 */
public interface RoleSearchRepository extends ElasticsearchRepository<Role, Long> {
}
