package rs.ac.uns.ftn.informatika.minis.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Objects;

/**
 *
 */
@Entity
@Table(name = "position")
@Document(indexName = "position")
public class Position implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "oz")
    private String oz;

    @ManyToOne
    private Language language;

    @OneToMany(mappedBy = "position")
    @JsonIgnore
    private Set<PersonInstitution> personInstitutions = new HashSet<>();
    
    @OneToMany(mappedBy = "position")
    @JsonIgnore
    private Set<PersonPosition> personPosition = new HashSet<>();

    @OneToMany(mappedBy = "position")
    @JsonIgnore
    private Set<PositionNameTranslations> nameTranslations = new HashSet<>();

    public Long getId() {
        return id;
    }

    public String getOz() {
        return oz;
    }

    public void setOz(String oz) {
        this.oz = oz;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<PersonPosition> getPersonPosition() {
        return personPosition;
    }

    public void setPersonPosition(Set<PersonPosition> personPosition) {
        this.personPosition = personPosition;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Set<PersonInstitution> getPersonInstitutions() {
        return personInstitutions;
    }

    public void setPersonInstitutions(Set<PersonInstitution> personInstitutions) {
        this.personInstitutions = personInstitutions;
    }

    public Set<PositionNameTranslations> getNameTranslations() {
        return nameTranslations;
    }

    public void setNameTranslations(Set<PositionNameTranslations> positionNameTranslationss) {
        this.nameTranslations = positionNameTranslationss;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Position position = (Position) o;

        if (!Objects.equals(id, position.id)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Position{"
                + "id=" + id
                + ", name='" + name + "'"
                + ", description='" + description + "'"
                + '}';
    }
}
