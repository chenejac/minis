package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.InstitutionStatusNameTranslations;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the InstitutionStatusNameTranslations entity.
 */
public interface InstitutionStatusNameTranslationsRepository extends JpaRepository<InstitutionStatusNameTranslations,Long> {

}
