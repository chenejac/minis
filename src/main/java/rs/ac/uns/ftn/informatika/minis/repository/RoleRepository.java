package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.Role;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Role entity.
 */
public interface RoleRepository extends JpaRepository<Role,Long> {

    @Query("select distinct role from Role role left join fetch role.privilages")
    List<Role> findAllWithEagerRelationships();

    @Query("select role from Role role left join fetch role.privilages where role.id =:id")
    Role findOneWithEagerRelationships(@Param("id") Long id);

}
