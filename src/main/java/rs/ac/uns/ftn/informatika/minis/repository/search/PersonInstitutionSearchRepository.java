package rs.ac.uns.ftn.informatika.minis.repository.search;

import java.util.List;
import rs.ac.uns.ftn.informatika.minis.domain.PersonInstitution;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the PersonInstitution entity.
 */
public interface PersonInstitutionSearchRepository extends ElasticsearchRepository<PersonInstitution, Long> {
    List<PersonInstitution> findByPersonFirstNameOrPersonLastNameOrInstitutionId(String name, String lastName, Long id);
}
