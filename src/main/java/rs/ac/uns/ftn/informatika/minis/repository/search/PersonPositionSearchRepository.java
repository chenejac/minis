/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.uns.ftn.informatika.minis.repository.search;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import rs.ac.uns.ftn.informatika.minis.domain.PersonPosition;

/**
 *
 * @author milan
 */
public interface PersonPositionSearchRepository extends ElasticsearchRepository<PersonPosition, Long>  {
    
}
