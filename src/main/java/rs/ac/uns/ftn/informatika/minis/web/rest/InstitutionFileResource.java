package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.InstitutionFile;
import rs.ac.uns.ftn.informatika.minis.repository.InstitutionFileRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.InstitutionFileSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing InstitutionFile.
 */
@RestController
@RequestMapping("/api")
public class InstitutionFileResource {

    private final Logger log = LoggerFactory.getLogger(InstitutionFileResource.class);

    @Inject
    private InstitutionFileRepository institutionFileRepository;

    @Inject
    private InstitutionFileSearchRepository institutionFileSearchRepository;

    /**
     * POST  /institutionFiles -> Create a new institutionFile.
     */
    @RequestMapping(value = "/institutionFiles",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstitutionFile> createInstitutionFile(@RequestBody InstitutionFile institutionFile) throws URISyntaxException {
        log.debug("REST request to save InstitutionFile : {}", institutionFile);
        if (institutionFile.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new institutionFile cannot already have an ID").body(null);
        }
        InstitutionFile result = institutionFileRepository.save(institutionFile);
        institutionFileSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/institutionFiles/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("institutionFile", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /institutionFiles -> Updates an existing institutionFile.
     */
    @RequestMapping(value = "/institutionFiles",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstitutionFile> updateInstitutionFile(@RequestBody InstitutionFile institutionFile) throws URISyntaxException {
        log.debug("REST request to update InstitutionFile : {}", institutionFile);
        if (institutionFile.getId() == null) {
            return createInstitutionFile(institutionFile);
        }
        InstitutionFile result = institutionFileRepository.save(institutionFile);
        institutionFileSearchRepository.save(institutionFile);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("institutionFile", institutionFile.getId().toString()))
                .body(result);
    }

    /**
     * GET  /institutionFiles -> get all the institutionFiles.
     */
    @RequestMapping(value = "/institutionFiles",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<InstitutionFile> getAllInstitutionFiles() {
        log.debug("REST request to get all InstitutionFiles");
        return institutionFileRepository.findAll();
    }

    /**
     * GET  /institutionFiles/:id -> get the "id" institutionFile.
     */
    @RequestMapping(value = "/institutionFiles/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstitutionFile> getInstitutionFile(@PathVariable Long id) {
        log.debug("REST request to get InstitutionFile : {}", id);
        return Optional.ofNullable(institutionFileRepository.findOne(id))
            .map(institutionFile -> new ResponseEntity<>(
                institutionFile,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /institutionFiles/:id -> delete the "id" institutionFile.
     */
    @RequestMapping(value = "/institutionFiles/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteInstitutionFile(@PathVariable Long id) {
        log.debug("REST request to delete InstitutionFile : {}", id);
        institutionFileRepository.delete(id);
        institutionFileSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("institutionFile", id.toString())).build();
    }

    /**
     * SEARCH  /_search/institutionFiles/:query -> search for the institutionFile corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/institutionFiles/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<InstitutionFile> searchInstitutionFiles(@PathVariable String query) {
        return StreamSupport
            .stream(institutionFileSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
