package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.Position;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Position entity.
 */
public interface PositionSearchRepository extends ElasticsearchRepository<Position, Long> {
}
