package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.SchemeNameTranslations;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the SchemeNameTranslations entity.
 */
public interface SchemeNameTranslationsSearchRepository extends ElasticsearchRepository<SchemeNameTranslations, Long> {
}
