package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.InstitutionStatus;
import rs.ac.uns.ftn.informatika.minis.repository.InstitutionStatusRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.InstitutionStatusSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

/**
 * REST controller for managing InstitutionStatus.
 */
@RestController
@RequestMapping("/api")
public class InstitutionStatusResource {

    private final Logger log = LoggerFactory.getLogger(InstitutionStatusResource.class);

    @Inject
    private InstitutionStatusRepository institutionStatusRepository;

    @Inject
    private InstitutionStatusSearchRepository institutionStatusSearchRepository;

    /**
     * POST /institutionStatuss -> Create a new institutionStatus.
     */
    @RequestMapping(value = "/institutionStatuss",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstitutionStatus> createInstitutionStatus(@RequestBody InstitutionStatus institutionStatus) throws URISyntaxException {
        log.debug("REST request to save InstitutionStatus : {}", institutionStatus);
        if (institutionStatus.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new institutionStatus cannot already have an ID").body(null);
        }
        InstitutionStatus result = institutionStatusRepository.save(institutionStatus);
        institutionStatusSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/institutionStatuss/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("institutionStatus", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT /institutionStatuss -> Updates an existing institutionStatus.
     */
    @RequestMapping(value = "/institutionStatuss",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstitutionStatus> updateInstitutionStatus(@RequestBody InstitutionStatus institutionStatus) throws URISyntaxException {
        log.debug("REST request to update InstitutionStatus : {}", institutionStatus);
        if (institutionStatus.getId() == null) {
            return createInstitutionStatus(institutionStatus);
        }
        InstitutionStatus result = institutionStatusRepository.save(institutionStatus);
        institutionStatusSearchRepository.save(institutionStatus);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("institutionStatus", institutionStatus.getId().toString()))
                .body(result);
    }

    /**
     * GET /institutionStatuss -> get all the institutionStatuss.
     */
    @RequestMapping(value = "/institutionStatuss",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Object> getAllInstitutionStatuss(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
            @RequestParam(value = "pageSize", defaultValue = "20") int pageSize) {
        log.debug("REST request to get all institutionStatuss");
        if (pageSize < 1) {
            String message = "pageSize can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        if (pageNum < 1) {
            String message = "pageNum can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        PageRequest pageRequest = new PageRequest(pageNum - 1, pageSize);
        Page page = institutionStatusRepository.findAll(pageRequest);
        return ResponseEntity.ok().body(page);
    }

    /**
     * GET /institutionStatuss/:id -> get the "id" institutionStatus.
     */
    @RequestMapping(value = "/institutionStatuss/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstitutionStatus> getInstitutionStatus(@PathVariable Long id) {
        log.debug("REST request to get InstitutionStatus : {}", id);
        return Optional.ofNullable(institutionStatusRepository.findOne(id))
                .map(institutionStatus -> new ResponseEntity<>(
                                institutionStatus,
                                HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE /institutionStatuss/:id -> delete the "id" institutionStatus.
     */
    @RequestMapping(value = "/institutionStatuss/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteInstitutionStatus(@PathVariable Long id) {
        log.debug("REST request to delete InstitutionStatus : {}", id);
        institutionStatusRepository.delete(id);
        institutionStatusSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("institutionStatus", id.toString())).build();
    }

    /**
     * SEARCH /_search/institutionStatuss/:query -> search for the
     * institutionStatus corresponding to the query.
     */
    @RequestMapping(value = "/_search/institutionStatuss/{query}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<InstitutionStatus> searchInstitutionStatuss(@PathVariable String query) {
        return StreamSupport
                .stream(institutionStatusSearchRepository.search(queryString(query)).spliterator(), false)
                .collect(Collectors.toList());
    }
}
