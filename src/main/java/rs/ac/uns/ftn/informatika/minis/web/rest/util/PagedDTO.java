/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.uns.ftn.informatika.minis.web.rest.util;

import java.util.function.Function;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import rs.ac.uns.ftn.informatika.minis.domain.AbstractAuditingEntity;

/**
 *
 * @author stefan
 * @param <T> DTO type that the content is being converted to.
 */
public class PagedDTO<T> extends PageImpl<T>{

    public <R extends AbstractAuditingEntity> PagedDTO(Page<R> pageable, Function<R, T> transform) {
        super(pageable.getContent()
                .stream()
                .map(transform)
                .collect(Collectors.toList()),
                null,
                pageable.getTotalElements());
    }
}
