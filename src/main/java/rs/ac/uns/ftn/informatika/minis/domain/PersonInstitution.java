package rs.ac.uns.ftn.informatika.minis.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import rs.ac.uns.ftn.informatika.minis.domain.util.CustomDateTimeDeserializer;
import rs.ac.uns.ftn.informatika.minis.domain.util.CustomDateTimeSerializer;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.NestedField;

/**
 * A PersonInstitution.
 */
@Entity
@Table(name = "person_institution")
@Document(indexName="personinstitution")
public class PersonInstitution implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @Column(name = "start_date")
    private DateTime startDate;
    
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @Column(name = "end_date")
    private DateTime endDate;
    
    @Column(name = "description")
    private String description;
    
    @Column(name = "research_area")
    private String researchArea;

    @ManyToOne
    private Function function;

    @ManyToOne
    private Institution institution;
    
    @Column(name = "employment_percentage")
    private Integer employmentPercentage;
    
    @Column(name = "max_month_engagement")
    private Integer maxMonthEngagement;

    @ManyToOne
    private Position position;

    @ManyToOne
    private Person person;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(DateTime startDate) {
        this.startDate = startDate;
    }

    public DateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(DateTime endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResearchArea() {
        return researchArea;
    }

    public void setResearchArea(String researchArea) {
        this.researchArea = researchArea;
    }

    public Function getFunction() {
        return function;
    }

    public void setFunction(Function function) {
        this.function = function;
    }

    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    public Integer getEmploymentPercentage() {
        return employmentPercentage;
    }

    public void setEmploymentPercentage(Integer employmentPercentage) {
        this.employmentPercentage = employmentPercentage;
    }

    public Integer getMaxMonthEngagement() {
        return maxMonthEngagement;
    }

    public void setMaxMonthEngagement(Integer maxMonthEngagement) {
        this.maxMonthEngagement = maxMonthEngagement;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PersonInstitution personInstitution = (PersonInstitution) o;

        if ( ! Objects.equals(id, personInstitution.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PersonInstitution{" +
                "id=" + id +
                ", startDate='" + startDate + "'" +
                ", endDate='" + endDate + "'" +
                ", employmentPercentage='" + employmentPercentage + "'" +
                ", maxMonthEgagement='" + maxMonthEngagement + "'" +
                ", description='" + description + "'" +
                ", researchArea='" + researchArea + "'" +
                '}';
    }
}
