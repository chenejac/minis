package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.PersonType;
import rs.ac.uns.ftn.informatika.minis.repository.PersonTypeRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PersonTypeSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

/**
 * REST controller for managing PersonType.
 */
@RestController
@RequestMapping("/api")
public class PersonTypeResource {

    private final Logger log = LoggerFactory.getLogger(PersonTypeResource.class);

    @Inject
    private PersonTypeRepository personTypeRepository;

    @Inject
    private PersonTypeSearchRepository personTypeSearchRepository;

    /**
     * POST /personTypes -> Create a new personType.
     */
    @RequestMapping(value = "/personTypes",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonType> createPersonType(@RequestBody PersonType personType) throws URISyntaxException {
        log.debug("REST request to save PersonType : {}", personType);
        if (personType.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new personType cannot already have an ID").body(null);
        }
        PersonType result = personTypeRepository.save(personType);
        personTypeSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/personTypes/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("personType", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT /personTypes -> Updates an existing personType.
     */
    @RequestMapping(value = "/personTypes",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonType> updatePersonType(@RequestBody PersonType personType) throws URISyntaxException {
        log.debug("REST request to update PersonType : {}", personType);
        if (personType.getId() == null) {
            return createPersonType(personType);
        }
        PersonType result = personTypeRepository.save(personType);
        personTypeSearchRepository.save(personType);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("personType", personType.getId().toString()))
                .body(result);
    }

    /**
     * GET /personTypes -> get all the personTypes.
     */
    @RequestMapping(value = "/personTypes",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Page<PersonType>> getAllPersonTypes(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
            @RequestParam(value = "pageSize", defaultValue = "20") int pageSize) {
        log.debug("REST request to get all PersonTypes");
        if (pageSize < 1) {
            String message = "pageSize can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        if (pageNum < 1) {
            String message = "pageNum can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        PageRequest pageRequest = new PageRequest(pageNum - 1, pageSize);
        Page page = personTypeRepository.findAll(pageRequest);
        return ResponseEntity.ok().body(page);
    }
    
    /**
     * GET /personTypes/:id -> get the "id" personType.
     */
    @RequestMapping(value = "/personTypes/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonType> getPersonType(@PathVariable Long id) {
        log.debug("REST request to get PersonType : {}", id);
        return Optional.ofNullable(personTypeRepository.findOne(id))
                .map(personType -> new ResponseEntity<>(
                                personType,
                                HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE /personTypes/:id -> delete the "id" personType.
     */
    @RequestMapping(value = "/personTypes/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePersonType(@PathVariable Long id) {
        log.debug("REST request to delete PersonType : {}", id);
        personTypeRepository.delete(id);
        personTypeSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("personType", id.toString())).build();
    }

    /**
     * SEARCH /_search/personTypes/:query -> search for the personType
     * corresponding to the query.
     */
    @RequestMapping(value = "/_search/personTypes/{query}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PersonType> searchPersonTypes(@PathVariable String query) {
        return StreamSupport
                .stream(personTypeSearchRepository.search(queryString(query)).spliterator(), false)
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/_search/personTypes/byName/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getPersonTypeByName(@RequestParam(value = "name") String name) {
        log.debug("REST request to get personType with name : {}", name);
        return Optional.ofNullable(personTypeRepository.findPersonTypeByName(name))
                .map(pt -> new ResponseEntity<>(
                                pt,
                                HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    
    
}
