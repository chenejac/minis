package rs.ac.uns.ftn.informatika.minis.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Category.
 */
@Entity
@Table(name = "category")
@Document(indexName="category")
public class Category implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    
    @Column(name = "name")
    private String name;
    
    @Column(name = "description")
    private String description;

    @ManyToOne
    private Language language;

    @OneToMany(mappedBy = "category")
    @JsonIgnore
    private Set<PersonCategory> persons = new HashSet<>();

    @OneToMany(mappedBy = "category")
    @JsonIgnore
    private Set<CategoryNameTranslations> nameTranslationss = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Set<PersonCategory> getPersons() {
        return persons;
    }

    public void setPersons(Set<PersonCategory> personCategorys) {
        this.persons = personCategorys;
    }

    public Set<CategoryNameTranslations> getNameTranslationss() {
        return nameTranslationss;
    }

    public void setNameTranslationss(Set<CategoryNameTranslations> categoryNameTranslationss) {
        this.nameTranslationss = categoryNameTranslationss;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Category category = (Category) o;

        if ( ! Objects.equals(id, category.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + "'" +
                ", description='" + description + "'" +
                '}';
    }
}
