package rs.ac.uns.ftn.informatika.minis.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Language.
 */
@Entity
@Table(name = "language")
@Document(indexName="language")
public class Language implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    
    @Column(name = "code")
    private String code;
    
    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<InstitutionKeywordsTranslations> institutionKeywordsTranslations = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<Institution> institutions = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<PersonKeywordsTranslations> personKeywordsTranslations = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<InstitutionNameTranslations> institutionNameTranslations = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<Person> persons = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<PersonBibliographyTranslations> personBibliographyTranslations = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<ClassNameTranslations> classNameTranslations = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<SchemeNameTranslations> schemeNameTranslationss = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<Classification> classifications = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<Scheme> schemes = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<CategoryNameTranslations> categoryNameTranslations = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<PositionNameTranslations> positionNameTranslations = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<Position> positions = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<Category> categories = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<FunctionNameTranslations> functionNameTranslations = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<Function> functions = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<PersonStatusNameTranslations> personStatusNameTranslations = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<PersonStatus> personStatuses = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<UserAccount> users = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<FounderNameTranslations> founderNameTranslations = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<ResearchAreaNameTranslations> researchAreaNameTranslations = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<ResearchArea> researchAreas = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<Activity> activities = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<ActivityNameTranslations> activityNameTranslations = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<InstitutionType> institutionTypes = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<InstitutionTypeNameTranslations> institutionTypeNameTranslations = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<InstitutionStatus> institutionStatuses = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<InstitutionStatusNameTranslations> institutionStatusNameTranslations = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<PersonType> personTypes = new HashSet<>();

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private Set<PersonTypeNameTranslations> personTypeNameTranslations = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<InstitutionKeywordsTranslations> getInstitutionKeywordsTranslations() {
        return institutionKeywordsTranslations;
    }

    public void setInstitutionKeywordsTranslations(Set<InstitutionKeywordsTranslations> institutionKeywordsTranslationss) {
        this.institutionKeywordsTranslations = institutionKeywordsTranslationss;
    }

    public Set<Institution> getInstitutions() {
        return institutions;
    }

    public void setInstitutions(Set<Institution> institutions) {
        this.institutions = institutions;
    }

    public Set<PersonKeywordsTranslations> getPersonKeywordsTranslations() {
        return personKeywordsTranslations;
    }

    public void setPersonKeywordsTranslations(Set<PersonKeywordsTranslations> personKeywordsTranslationss) {
        this.personKeywordsTranslations = personKeywordsTranslationss;
    }

    public Set<InstitutionNameTranslations> getInstitutionNameTranslations() {
        return institutionNameTranslations;
    }

    public void setInstitutionNameTranslations(Set<InstitutionNameTranslations> institutionNameTranslationss) {
        this.institutionNameTranslations = institutionNameTranslationss;
    }

    public Set<Person> getPersons() {
        return persons;
    }

    public void setPersons(Set<Person> persons) {
        this.persons = persons;
    }

    public Set<PersonBibliographyTranslations> getPersonBibliographyTranslations() {
        return personBibliographyTranslations;
    }

    public void setPersonBibliographyTranslations(Set<PersonBibliographyTranslations> personBibliographyTranslationss) {
        this.personBibliographyTranslations = personBibliographyTranslationss;
    }

    public Set<ClassNameTranslations> getClassNameTranslations() {
        return classNameTranslations;
    }

    public void setClassNameTranslations(Set<ClassNameTranslations> classNameTranslationss) {
        this.classNameTranslations = classNameTranslationss;
    }

    public Set<SchemeNameTranslations> getSchemeNameTranslationss() {
        return schemeNameTranslationss;
    }

    public void setSchemeNameTranslationss(Set<SchemeNameTranslations> schemeNameTranslationss) {
        this.schemeNameTranslationss = schemeNameTranslationss;
    }

    public Set<Classification> getClassifications() {
        return classifications;
    }

    public void setClassifications(Set<Classification> classifications) {
        this.classifications = classifications;
    }

    public Set<Scheme> getSchemes() {
        return schemes;
    }

    public void setSchemes(Set<Scheme> schemes) {
        this.schemes = schemes;
    }

    public Set<CategoryNameTranslations> getCategoryNameTranslations() {
        return categoryNameTranslations;
    }

    public void setCategoryNameTranslations(Set<CategoryNameTranslations> categoryNameTranslationss) {
        this.categoryNameTranslations = categoryNameTranslationss;
    }

    public Set<PositionNameTranslations> getPositionNameTranslations() {
        return positionNameTranslations;
    }

    public void setPositionNameTranslations(Set<PositionNameTranslations> positionNameTranslationss) {
        this.positionNameTranslations = positionNameTranslationss;
    }

    public Set<Position> getPositions() {
        return positions;
    }

    public void setPositions(Set<Position> positions) {
        this.positions = positions;
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public void setCategories(Set<Category> categorys) {
        this.categories = categorys;
    }

    public Set<FunctionNameTranslations> getFunctionNameTranslations() {
        return functionNameTranslations;
    }

    public void setFunctionNameTranslations(Set<FunctionNameTranslations> functionNameTranslationss) {
        this.functionNameTranslations = functionNameTranslationss;
    }

    public Set<Function> getFunctions() {
        return functions;
    }

    public void setFunctions(Set<Function> functions) {
        this.functions = functions;
    }

    public Set<PersonStatusNameTranslations> getPersonStatusNameTranslations() {
        return personStatusNameTranslations;
    }

    public void setPersonStatusNameTranslations(Set<PersonStatusNameTranslations> personStatusNameTranslationss) {
        this.personStatusNameTranslations = personStatusNameTranslationss;
    }

    public Set<PersonStatus> getPersonStatuses() {
        return personStatuses;
    }

    public void setPersonStatuses(Set<PersonStatus> personStatuss) {
        this.personStatuses = personStatuss;
    }

    public Set<UserAccount> getUsers() {
        return users;
    }

    public void setUsers(Set<UserAccount> userAccounts) {
        this.users = userAccounts;
    }

    public Set<FounderNameTranslations> getFounderNameTranslations() {
        return founderNameTranslations;
    }

    public void setFounderNameTranslations(Set<FounderNameTranslations> founderNameTranslationss) {
        this.founderNameTranslations = founderNameTranslationss;
    }

    public Set<ResearchAreaNameTranslations> getResearchAreaNameTranslations() {
        return researchAreaNameTranslations;
    }

    public void setResearchAreaNameTranslations(Set<ResearchAreaNameTranslations> researchAreaNameTranslationss) {
        this.researchAreaNameTranslations = researchAreaNameTranslationss;
    }

    public Set<ResearchArea> getResearchAreas() {
        return researchAreas;
    }

    public void setResearchAreas(Set<ResearchArea> researchAreas) {
        this.researchAreas = researchAreas;
    }

    public Set<Activity> getActivities() {
        return activities;
    }

    public void setActivities(Set<Activity> activitys) {
        this.activities = activitys;
    }

    public Set<ActivityNameTranslations> getActivityNameTranslations() {
        return activityNameTranslations;
    }

    public void setActivityNameTranslations(Set<ActivityNameTranslations> activityNameTranslationss) {
        this.activityNameTranslations = activityNameTranslationss;
    }

    public Set<InstitutionType> getInstitutionTypes() {
        return institutionTypes;
    }

    public void setInstitutionTypes(Set<InstitutionType> institutionTypes) {
        this.institutionTypes = institutionTypes;
    }

    public Set<InstitutionTypeNameTranslations> getInstitutionTypeNameTranslations() {
        return institutionTypeNameTranslations;
    }

    public void setInstitutionTypeNameTranslations(Set<InstitutionTypeNameTranslations> institutionTypeNameTranslationss) {
        this.institutionTypeNameTranslations = institutionTypeNameTranslationss;
    }

    public Set<InstitutionStatus> getInstitutionStatuses() {
        return institutionStatuses;
    }

    public void setInstitutionStatuses(Set<InstitutionStatus> institutionStatuss) {
        this.institutionStatuses = institutionStatuss;
    }

    public Set<InstitutionStatusNameTranslations> getInstitutionStatusNameTranslations() {
        return institutionStatusNameTranslations;
    }

    public void setInstitutionStatusNameTranslations(Set<InstitutionStatusNameTranslations> institutionStatusNameTranslationss) {
        this.institutionStatusNameTranslations = institutionStatusNameTranslationss;
    }

    public Set<PersonType> getPersonTypes() {
        return personTypes;
    }

    public void setPersonTypes(Set<PersonType> personTypes) {
        this.personTypes = personTypes;
    }

    public Set<PersonTypeNameTranslations> getPersonTypeNameTranslations() {
        return personTypeNameTranslations;
    }

    public void setPersonTypeNameTranslations(Set<PersonTypeNameTranslations> personTypeNameTranslationss) {
        this.personTypeNameTranslations = personTypeNameTranslationss;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Language language = (Language) o;

        if ( ! Objects.equals(id, language.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Language{" +
                "id=" + id +
                ", code='" + code + "'" +
                ", name='" + name + "'" +
                '}';
    }
}
