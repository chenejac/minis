package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.InstitutionTypeNameTranslations;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the InstitutionTypeNameTranslations entity.
 */
public interface InstitutionTypeNameTranslationsSearchRepository extends ElasticsearchRepository<InstitutionTypeNameTranslations, Long> {
}
