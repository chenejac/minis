/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.uns.ftn.informatika.minis.web.rest.errors;

import java.io.Serializable;
import java.util.List;
import org.springframework.http.HttpStatus;

/**
 * Exception class that standardizes data errors that occur through the back-end
 * app.
 * Error content should be a string, or a list of strings. Any other serializable
 * data can be returned, though should be avoided, unless clearly stated otherwise,
 * because of client-side compatibility.
 * 
 * Usage:
 *  Instead of returning errors in headers or by any other means,
 *  throw the exception that will return an Error DTO with the specified HTTP
 *  status.
 * 
 *  The exception can be thrown constructed as is. It also supports the build pattern.
 *  Starting with {@link DataExceptionBuilder}, error fields can be appended and updated.
 *  The class also supports error building with the use of {@link DataException#errorType(java.lang.String) }.{@link DataException#errorMessage(java.io.Serializable) }
 *  chains.
 * 
 * Example 1:
 *  DataExceptionBuilder.nonFieldError("Error message one.").nonFieldError("Error message two.");
 * Output 1:
 *  { non_field_errors: ["Error message one", "Error message two."] }
 * 
 * Example 2:
 *  DataExceptionBuilder.error("name", "Name related error message")
 *                      .error("jmbg", "Jmbg related error message")
 *                      .nonFieldError("Non field error message");
 * Output 2:
 *  {
 *    name: ["Name related error message"],
 *    jmbg: ["Jmbg related error message"],
 *    non_field_error: ["Non field error message"]
 *  }
 * 
 * Example 3:
 *  DataExceptionBuilder.errorType("name")
 *                          .errorMessage("Name error one")
 *                          .errorMessage("Name error two")
 *                      .errorType("jmbg")
 *                          .errorMessage("Jmbg error one");
 * Output 4:
 *  {
 *    name: ["Name error one", "Name error two"],
 *    jmbg: ["Jmbg error one"]
 *  }
 * 
 * The default HTTP staus returned is BAD_REQUEST 400
 * 
 * Exception handling is done in the {@link ExceptionTranslator} controller
 * advisor.
 * 
 * @author stefan
 */
public class DataException extends RuntimeException{
    
    private final MinisDataErrorDTO errors;
    private String currentErrorType;
    private HttpStatus httpStatus;
    
    public DataException() {
       this.httpStatus = HttpStatus.BAD_REQUEST;
       this.errors = new MinisDataErrorDTO();
    }
    
    public DataException(String errorType, List<Serializable> errors) {
        this();
        this.errors.putAllErrors(errorType, errors);
    }
    
    public DataException(String errorType, Serializable error) {
        this();
        this.errors.putError(errorType, error);
    }
    
    public DataException status(HttpStatus status) {
        this.httpStatus = status;
        return this;
    }
    
    /**
     * Adds the passed value to the non_field_error field in the Data Error.
     * @param error
     * A serializable object that will be set to the non_field_error
     * field.
     * @return
     * This DataException object, to further the build chain.
     */
    public DataException nonFieldError(List<Serializable> errors) {
        this.errors.putAllErrors(ErrorConstants.NON_FIELD_ERROR ,errors);
        return this;
    }
    
    /**
     * Look at {@link DataException#nonFieldError(java.util.List) }
     * Single parameter function.
     * @param error
     * A serializable object that will be set to the non_field_error
     * field.
     * @return
     * This DataException object, to further the build chain.
     */
    public DataException nonFieldError(Serializable error) {
        this.errors.putError(ErrorConstants.NON_FIELD_ERROR ,error);
        return this;
    }
    
    /**
     * Sets an arbitrary error for the built Data Error.
     * @param errorType
     * String that determines the error key in the JSON object returned to
     * the client.
     * @param error
     * Error data to be added to the specified array in the created Data Error.
     * @return
     * This DataException object, to further the build chain.
     */
    public DataException error(String errorType, Serializable error) {
        this.errors.putError(errorType, error);
        return this;
    }
    
    /**
     * Look at {@link DataException#error(java.lang.String, java.io.Serializable) }.
     * @param errorType
     * String that determines the error key in the JSON object returned to
     * the client.
     * @param errors
     * List of error data to be added to the specified array in the created Data Error.
     * @return 
     * This DataException object, to further the build chain.
     */
    public DataException error(String errorType, List<Serializable> errors) {
        this.errors.putAllErrors(errorType, errors);
        return this;
    }
    
    /**
     * Starts a chain for the errorType().errorMessage() construct.
     * @param errorType
     * Error type, that all subsequent calls to {@link DataException#errorMessage(java.io.Serializable) }
     * will refer to.
     * @return 
     * This DataException object, to further the build chain.
     */
    public DataException errorType(String errorType) {
        this.currentErrorType = errorType;
        return this;
    }
    
    /**
     * Adds error data to the current error field, set by the {@link DataException#errorType(java.lang.String) }
     * method.
     * Be sure to set the error type first.
     * @param error
     * Error data to be set.
     * @return 
     * This DataException object, to further the build chain.
     */
    public DataException errorMessage(Serializable error) {
        if(this.currentErrorType == null)
            return null;
        this.errors.putError(this.currentErrorType, error);
        return this;
    }
    
    public MinisDataErrorDTO getErrorDTO() {
        return this.errors;
    }
    
    public HttpStatus getHttpStatus() {
        return this.httpStatus;
    }
    
    @Override
    public synchronized Throwable fillInStackTrace() {
        // Prevent stack trace logging.
        return this;
    }
    
    /**
     * Builder class for {@link DataException}
     */
    public static class DataExceptionBuilder {
        /**
         * Creates a {@link DataException} instance, and sets a non_field_error
         * for the built Data Error.
         * @param error
         * List of serializable objects that will be set to the non_field_error
         * field.
         * @return
         * Newly created DataException object.
         */
        public static DataException nonFieldError(List<Serializable> error) {
            return new DataException(ErrorConstants.NON_FIELD_ERROR, error);
        }
        
        /**
         * Look at {@link DataExceptionBuilder#nonFieldError(java.util.List) }
         * Single parameter function.
         * @param error
         * A serializable object that will be set to the non_field_error
         * field.
         * @return
         * Newly created DataException object.
         */
        public static DataException nonFieldError(Serializable error) {
            return new DataException(ErrorConstants.NON_FIELD_ERROR, error);
        }
        
        /**
         * Creates a {@link DataException} instance, and sets an arbitrary
         * error for the built Data Error.
         * @param errorType
         * String that determines the error key in the JSON object returned to
         * the client.
         * @param error
         * Error data to be added to the specified array in the created Data Error.
         * @return
         * Newly created DataException object.
         */
        public static DataException error(String errorType, Serializable error) {
            return new DataException(errorType, error);
        }
        
        /**
         * Look at {@link DataExceptionBuilder#error(java.lang.String, java.io.Serializable) }
         * Multiple parameter (list) function.
         * @param errorType
         * String that determines the error key in the JSON object returned to
         * the client.
         * @param error
         * Error data to be added to the specified array in the created Data Error.
         * @return
         * Newly created DataException object.
         */
        public static DataException error(String errorType, List<Serializable> error) {
            return new DataException(errorType, error);
        }
        
        /**
         * Initiates a {@link DataException} object and starts a chain for the
         * errorType().errorMessage() construct.
         * @param errorType
         * Error type, that all subsequent calls to {@link DataException#errorMessage(java.io.Serializable) }
         * will refer to.
         * @return
         * Newly created DataException object.
         */
        public static DataException errorType(String errorType){
            DataException retVal = new DataException();
            return retVal.errorType(errorType);
        }
    }
}
