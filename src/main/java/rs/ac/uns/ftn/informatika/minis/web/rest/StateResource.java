/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.uns.ftn.informatika.minis.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.uns.ftn.informatika.minis.domain.State;
import rs.ac.uns.ftn.informatika.minis.repository.StateRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;

/**
 * @author Igor Milanovic REST controller for managing State.
 */
@RestController
@RequestMapping("/api")
public class StateResource {

    private final Logger log = LoggerFactory.getLogger(StateResource.class);

    @Inject
    private StateRepository stateRepository;

    /**
     * POST /states -> Create a new state.
     */
    @RequestMapping(value = "/states",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<State> createState(@RequestBody State state) throws URISyntaxException {
        log.debug("REST request to save state : {}", state);
        if (state.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new state cannot already have an ID").body(null);
        }
        State result = stateRepository.save(state);
        return ResponseEntity.created(new URI("/api/states/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("state", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT /states -> Updates an existing state.
     */
    @RequestMapping(value = "/states",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<State> updateState(@RequestBody State state) throws URISyntaxException {
        log.debug("REST request to update State : {}", state);
        if (state.getId() == null) {
            return createState(state);
        }
        State result = stateRepository.save(state);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("state", state.getId().toString()))
                .body(result);
    }

    /**
     * GET /states -> get all the states.
     */
    @RequestMapping(value = "/states",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<State>> getAllStates(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
            @RequestParam(value = "pageSize", defaultValue = "20") int pageSize) {

        log.debug("REST request to get all states");
        if (pageSize < 1) {
            String message = "pageSize can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        if (pageNum < 1) {
            String message = "pageNum can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        PageRequest pageRequest = new PageRequest(pageNum - 1, pageSize);
        Page page = stateRepository.findAll(pageRequest);
        return ResponseEntity.ok().body(page);
    }

    /**
     * GET /states/:id -> get the "id" state.
     */
    @RequestMapping(value = "/states/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<State> getState(@PathVariable Long id) {
        log.debug("REST request to get State : {}", id);
        return Optional.ofNullable(stateRepository.findOne(id))
                .map(state -> new ResponseEntity<>(
                                state,
                                HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE /states/:id -> delete the "id" state.
     */
    @RequestMapping(value = "/states/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> deleteState(@PathVariable Long id) {
        log.debug("REST request to delete State : {}", id);
        stateRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("state", id.toString())).build();
    }

    /**
     * GET /_search/states/byName?name={name} -> get state by name.
     */
    @RequestMapping(value = "/_search/states/byName/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getStateByName(@RequestParam(value = "name") String name) {
        log.info("REST request to get state with name : {}", name);
        return Optional.ofNullable(stateRepository.findStateByName(name))
                .map(state -> new ResponseEntity<>(
                                state,
                                HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
