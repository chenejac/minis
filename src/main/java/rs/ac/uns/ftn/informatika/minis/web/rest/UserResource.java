package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.Authority;
import rs.ac.uns.ftn.informatika.minis.domain.User;
import rs.ac.uns.ftn.informatika.minis.repository.AuthorityRepository;
import rs.ac.uns.ftn.informatika.minis.repository.UserRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.UserSearchRepository;
import rs.ac.uns.ftn.informatika.minis.security.AuthoritiesConstants;
import rs.ac.uns.ftn.informatika.minis.service.UserService;
import rs.ac.uns.ftn.informatika.minis.web.rest.dto.ManagedUserDTO;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.apache.commons.lang.StringUtils;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import rs.ac.uns.ftn.informatika.minis.domain.Institution;
import rs.ac.uns.ftn.informatika.minis.domain.Person;
import rs.ac.uns.ftn.informatika.minis.repository.InstitutionRepository;
import rs.ac.uns.ftn.informatika.minis.repository.PersonRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.dto.UserDTO;
import rs.ac.uns.ftn.informatika.minis.web.rest.dto.UserListDTO;
import rs.ac.uns.ftn.informatika.minis.web.rest.errors.ErrorConstants;
import rs.ac.uns.ftn.informatika.minis.web.rest.errors.ErrorDTO;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.PagedDTO;

/**
 * REST controller for managing users.
 *
 * <p>This class accesses the User entity, and needs to fetch its collection of authorities.</p>
 * <p>
 * For a normal use-case, it would be better to have an eager relationship between User and Authority,
 * and send everything to the client side: there would be no DTO, a lot less code, and an outer-join
 * which would be good for performance.
 * </p>
 * <p>
 * We use a DTO for 3 reasons:
 * <ul>
 * <li>We want to keep a lazy association between the user and the authorities, because people will
 * quite often do relationships with the user, and we don't want them to get the authorities all
 * the time for nothing (for performance reasons). This is the #1 goal: we should not impact our users'
 * application because of this use-case.</li>
 * <li> Not having an outer join causes n+1 requests to the database. This is not a real issue as
 * we have by default a second-level cache. This means on the first HTTP call we do the n+1 requests,
 * but then all authorities come from the cache, so in fact it's much better than doing an outer join
 * (which will get lots of data from the database, for each HTTP call).</li>
 * <li> As this manages users, for security reasons, we'd rather have a DTO layer.</li>
 * </p>
 * <p>Another option would be to have a specific JPA entity graph to handle this case.</p>
 */
@RestController
@RequestMapping("/api")
public class UserResource {

    private final Logger log = LoggerFactory.getLogger(UserResource.class);

    @Inject
    private UserRepository userRepository;

    @Inject
    private AuthorityRepository authorityRepository;

    @Inject
    private UserService userService;

    @Inject
    private UserSearchRepository userSearchRepository;
    
    @Inject
    private InstitutionRepository institutionRepository;
    
    @Inject
    private PersonRepository personRepository;     

    /**
     * POST  /users -> Create a new user.
     */
    @RequestMapping(value = "/users",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.ADMIN)
    public ResponseEntity<User> createUser(@RequestBody User user) throws URISyntaxException {
        log.debug("REST request to save User : {}", user);
        if (user.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new user cannot already have an ID").body(null);
        }
        User result = userRepository.save(user);
        return ResponseEntity.created(new URI("/api/users/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("user", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /users -> Updates an existing User.
     */
    @RequestMapping(value = "/users",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @Secured(AuthoritiesConstants.ADMIN)
    public ResponseEntity<ManagedUserDTO> updateUser(@RequestBody ManagedUserDTO managedUserDTO) throws URISyntaxException {
        log.debug("REST request to update User : {}", managedUserDTO);
        return Optional.of(userRepository
            .findOne(managedUserDTO.getId()))
            .map(user -> {
                user.setLogin(managedUserDTO.getLogin());
                user.setFirstName(managedUserDTO.getFirstName());
                user.setLastName(managedUserDTO.getLastName());
                user.setEmail(managedUserDTO.getEmail());
                user.setActivated(managedUserDTO.isActivated());
                user.setLangKey(managedUserDTO.getLangKey());
                Institution institution = null;
                if(managedUserDTO.getInstitutionId() != null) {
                    institution = institutionRepository.findOne(managedUserDTO.getInstitutionId());
                }
                user.setInstitution(institution);
                Person person = null;
                if(managedUserDTO.getPersonId() != null) {
                    person = personRepository.findOne(managedUserDTO.getPersonId());
                }
                user.setPerson(person);
                Set<Authority> authorities = user.getAuthorities();
                authorities.clear();
                managedUserDTO.getAuthorities().stream().forEach(
                    authority -> authorities.add(authorityRepository.findOne(authority))
                );
                authorities.add(authorityRepository.findOne("ROLE_USER"));
                return ResponseEntity.ok()
                    .headers(HeaderUtil.createEntityUpdateAlert("user", managedUserDTO.getLogin()))
                    .body(new ManagedUserDTO(userRepository
                        .findOne(managedUserDTO.getId())));
            })
            .orElseGet(() -> new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
    }
    
    /**
     * 
     * PATCH /users/{id}/password -> force update user password
     */
    @RequestMapping(value = "/users/{userId}/password",
        method = RequestMethod.PATCH,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @Secured(AuthoritiesConstants.ADMIN)
    public ResponseEntity updatePassword(@PathVariable Long userId, @RequestBody String password) {
        if(!checkPasswordLength(password)) {
            ErrorDTO error = new ErrorDTO(ErrorConstants.ERR_VALIDATION);
            error.add("user", "password", "Lozinka mora imati između 5 i 100 karaktera.");
            return ResponseEntity
                    .badRequest()
                    .body(error);
        }
        return userService.forceChangePassword(password, userId);
    }
    
    /**
     * GET  /users -> get all users.
     */
    @RequestMapping(value = "/users",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @Secured(AuthoritiesConstants.ADMIN)
    public ResponseEntity<PagedDTO<ManagedUserDTO>> getAllUsers(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                                @RequestParam(value = "pageSize", defaultValue = "20") int pageSize,
                                                @RequestParam(value = "sortBy", defaultValue = "lastName") String sortBy,
                                                @RequestParam(value = "orderBy", defaultValue = "asc") String orderBy)
        throws URISyntaxException {
        log.debug("REST request to get all users");
        if(pageSize < 1)
        {
            String message = "pageSize can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        if(pageNum < 1)
        {
            String message = "pageNum can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        PagedDTO<ManagedUserDTO> page = new PagedDTO<ManagedUserDTO>(userService.getAllUsers(pageNum, pageSize, sortBy, orderBy), (User user) -> new ManagedUserDTO(user));
        
        return ResponseEntity.ok().body(page);
    }

    /**
     * GET  /users/:id -> get the "login" user.
     */
    @RequestMapping(value = "/users/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserListDTO> getUser(@PathVariable Long id) {
        log.debug("REST request to get User : {}", id);
        User user = userService.getUserWithAuthorities(id);
        ResponseEntity<UserListDTO> retVal = null;
        if (user != null) {
            retVal = new ResponseEntity<>(new UserListDTO(user), HttpStatus.OK);
        } else {
            retVal = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return retVal;
    }

    /**
     * SEARCH  /_search/users/:query -> search for the User corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/users/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<User> search(@PathVariable String query, @RequestParam(required = false) String[] fields) {
        QueryStringQueryBuilder qs = queryString(query);
        for(String field : fields) {
            qs.field(field);
        }
        return StreamSupport
            .stream(userSearchRepository.search(qs).spliterator(), false)
            .collect(Collectors.toList());
    }
    
    private boolean checkPasswordLength(String password) {
        return (!StringUtils.isEmpty(password)
                && password.length() >= UserDTO.PASSWORD_MIN_LENGTH
                && password.length() <= UserDTO.PASSWORD_MAX_LENGTH);
    }
    
}
