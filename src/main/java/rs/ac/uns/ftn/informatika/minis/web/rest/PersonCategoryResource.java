package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.PersonCategory;
import rs.ac.uns.ftn.informatika.minis.repository.PersonCategoryRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PersonCategorySearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PersonCategory.
 */
@RestController
@RequestMapping("/api")
public class PersonCategoryResource {

    private final Logger log = LoggerFactory.getLogger(PersonCategoryResource.class);

    @Inject
    private PersonCategoryRepository personCategoryRepository;

    @Inject
    private PersonCategorySearchRepository personCategorySearchRepository;

    /**
     * POST  /personCategorys -> Create a new personCategory.
     */
    @RequestMapping(value = "/personCategorys",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonCategory> createPersonCategory(@RequestBody PersonCategory personCategory) throws URISyntaxException {
        log.debug("REST request to save PersonCategory : {}", personCategory);
        if (personCategory.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new personCategory cannot already have an ID").body(null);
        }
        PersonCategory result = personCategoryRepository.save(personCategory);
        personCategorySearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/personCategorys/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("personCategory", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /personCategorys -> Updates an existing personCategory.
     */
    @RequestMapping(value = "/personCategorys",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonCategory> updatePersonCategory(@RequestBody PersonCategory personCategory) throws URISyntaxException {
        log.debug("REST request to update PersonCategory : {}", personCategory);
        if (personCategory.getId() == null) {
            return createPersonCategory(personCategory);
        }
        PersonCategory result = personCategoryRepository.save(personCategory);
        personCategorySearchRepository.save(personCategory);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("personCategory", personCategory.getId().toString()))
                .body(result);
    }

    /**
     * GET  /personCategorys -> get all the personCategorys.
     */
    @RequestMapping(value = "/personCategorys",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PersonCategory> getAllPersonCategorys() {
        log.debug("REST request to get all PersonCategorys");
        return personCategoryRepository.findAll();
    }

    /**
     * GET  /personCategorys/:id -> get the "id" personCategory.
     */
    @RequestMapping(value = "/personCategorys/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonCategory> getPersonCategory(@PathVariable Long id) {
        log.debug("REST request to get PersonCategory : {}", id);
        return Optional.ofNullable(personCategoryRepository.findOne(id))
            .map(personCategory -> new ResponseEntity<>(
                personCategory,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /personCategorys/:id -> delete the "id" personCategory.
     */
    @RequestMapping(value = "/personCategorys/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePersonCategory(@PathVariable Long id) {
        log.debug("REST request to delete PersonCategory : {}", id);
        personCategoryRepository.delete(id);
        personCategorySearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("personCategory", id.toString())).build();
    }

    /**
     * SEARCH  /_search/personCategorys/:query -> search for the personCategory corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/personCategorys/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PersonCategory> searchPersonCategorys(@PathVariable String query) {
        return StreamSupport
            .stream(personCategorySearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
    
    
    /**
     * GET  /personPositions/:id/for_person -> get person positions for "id" person.
     */
    @RequestMapping(value = "/personCategorys/{id}/for_person",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PersonCategory> getPositionsForPerson(@PathVariable Long id) {
        List<PersonCategory> categories = personCategoryRepository.findForPerson(id);
        return categories;
    }
}
