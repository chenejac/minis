package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.PersonInstitution;
import org.springframework.data.jpa.repository.*;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import rs.ac.uns.ftn.informatika.minis.domain.Institution;
import rs.ac.uns.ftn.informatika.minis.domain.Person;

/**
 * Spring Data JPA repository for the PersonInstitution entity.
 */
public interface PersonInstitutionRepository extends JpaRepository<PersonInstitution,Long> {

    Page<PersonInstitution> findPersonInstitutionByPerson(Person person, Pageable pageable);
    Page<PersonInstitution> findPersonInstitutionByInstitution(Institution institution, Pageable pageable);
    List<PersonInstitution> findPersonInstitutionByPerson(Person person);
    List<PersonInstitution> findPersonInstitutionByInstitution(Institution institution);
    
    List<PersonInstitution> findByPersonIdInAndInstitutionIdIn(List<Long> personIds, List<Long> institutionIds);
    List<PersonInstitution> findByPersonIdIn(List<Long> personIds);
    List<PersonInstitution> findByInstitutionIdIn(List<Long> institutionIds);
}
