package rs.ac.uns.ftn.informatika.minis.service;

import java.util.Collections;
import rs.ac.uns.ftn.informatika.minis.domain.Authority;
import rs.ac.uns.ftn.informatika.minis.domain.User;
import rs.ac.uns.ftn.informatika.minis.repository.AuthorityRepository;
import rs.ac.uns.ftn.informatika.minis.repository.UserRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.UserSearchRepository;
import rs.ac.uns.ftn.informatika.minis.security.SecurityUtils;
import rs.ac.uns.ftn.informatika.minis.service.util.RandomUtil;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import rs.ac.uns.ftn.informatika.minis.domain.Institution;
import rs.ac.uns.ftn.informatika.minis.domain.Person;
import rs.ac.uns.ftn.informatika.minis.repository.InstitutionRepository;
import rs.ac.uns.ftn.informatika.minis.repository.PersonRepository;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    @Inject
    private PasswordEncoder passwordEncoder;

    @Inject
    private UserRepository userRepository;

    @Inject
    private UserSearchRepository userSearchRepository;

    @Inject
    private AuthorityRepository authorityRepository;
    
    @Inject
    private InstitutionRepository institutionRepository;
    
    @Inject
    private PersonRepository personRepostitory;

    public Optional<User> activateRegistration(String key) {
        log.debug("Activating user for activation key {}", key);
        userRepository.findOneByActivationKey(key)
            .map(user -> {
                // activate given user for the registration key.
                user.setActivated(true);
                user.setActivationKey(null);
                userRepository.save(user);
                userSearchRepository.save(user);
                log.debug("Activated user: {}", user);
                return user;
            });
        return Optional.empty();
    }

    public Optional<User> completePasswordReset(String newPassword, String key) {
       log.debug("Reset user password for reset key {}", key);

       return userRepository.findOneByResetKey(key)
           .filter(user -> {
               DateTime oneDayAgo = DateTime.now().minusHours(24);
               return user.getResetDate().isAfter(oneDayAgo.toInstant().getMillis());
           })
           .map(user -> {
               user.setPassword(passwordEncoder.encode(newPassword));
               user.setResetKey(null);
               user.setResetDate(null);
               userRepository.save(user);
               return user;
           });
    }

    public Optional<User> requestPasswordReset(String mail) {
       return userRepository.findOneByEmail(mail)
           .filter(user -> user.getActivated() == true)
           .map(user -> {
               user.setResetKey(RandomUtil.generateResetKey());
               user.setResetDate(DateTime.now());
               userRepository.save(user);
               return user;
           });
    }

    public User createUserInformation(String login, String password, String firstName, String lastName, String email,
                                      String langKey, Set<String> authoritiesManual, Long personId, Long institutionId) {

        User newUser = new User();
        Authority authority = authorityRepository.findOne("ROLE_USER");
        Set<Authority> authorities = new HashSet<>();
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setLogin(login);
        // new user gets initially a generated password
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(firstName);
        newUser.setLastName(lastName);
        newUser.setEmail(email);
        newUser.setLangKey(langKey);
        // new user is active
        newUser.setActivated(true);
        // new user gets registration key
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        authorities.add(authority);
        if(authoritiesManual != null) {
            authoritiesManual
                    .stream()
                    .forEach((String strAuth) -> authorities.add(authorityRepository.findOne(strAuth)));
        }
        newUser.setAuthorities(authorities);
        
        if(personId != null)
        {
            Person p = personRepostitory.findOne(personId);
            newUser.setPerson(p);
        }
        
        if(institutionId != null)
        {
            Institution i = institutionRepository.getOne(institutionId);
            newUser.setInstitution(i);
        }
        userRepository.save(newUser);
        userSearchRepository.save(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }

    public void updateUserInformation(String firstName, String lastName, String email, String langKey) {
        userRepository.findOneByLogin(SecurityUtils.getCurrentLogin()).ifPresent(u -> {
            u.setFirstName(firstName);
            u.setLastName(lastName);
            u.setEmail(email);
            u.setLangKey(langKey);
            userRepository.save(u);
            userSearchRepository.save(u);
            log.debug("Changed Information for User: {}", u);
        });
    }

    public void changePassword(String newPassword, String oldPassword) {
        userRepository.findOneByLogin(SecurityUtils.getCurrentLogin()).ifPresent(u-> {
            String newEncryptedPassword = passwordEncoder.encode(newPassword);
            String oldEncryptedPassword = passwordEncoder.encode(oldPassword);
            log.debug("oldPassword : " + oldPassword);
            if(!passwordEncoder.matches(oldPassword.trim(), u.getPassword()))
            {
                log.debug("User: {} didn't post good old password.", u); 
                throw new RuntimeException("User didn't post good old password!");
            }
            u.setPassword(newEncryptedPassword);
            userRepository.save(u);
            log.debug("Changed password for User: {}", u);
        });
    }
    
    public ResponseEntity forceChangePassword(String newPassword, Long userId) {
        User user = userRepository.findOne(userId);
        user.setPassword(passwordEncoder.encode(newPassword));
        userRepository.save(user);
        return ResponseEntity.noContent().build();
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthoritiesByLogin(String login) {
        return userRepository.findOneByLogin(login).map(u -> {
            u.getAuthorities().size();
            return u;
        });
    }

    @Transactional(readOnly = true)
    public User getUserWithAuthorities(Long id) {
        User user = userRepository.findOne(id);
        user.getAuthorities().size(); // eagerly load the association
        return user;
    }

    @Transactional(readOnly = true)
    public User getUserWithAuthorities() {
        User user = userRepository.findOneByLogin(SecurityUtils.getCurrentLogin()).get();
        user.getAuthorities().size(); // eagerly load the association
        return user;
    }

    /**
     * Not activated users should be automatically deleted after 3 days.
     * <p/>
     * <p>
     * This is scheduled to get fired everyday, at 01:00 (am).
     * </p>
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void removeNotActivatedUsers() {
        DateTime now = new DateTime();
        List<User> users = userRepository.findAllByActivatedIsFalseAndCreatedDateBefore(now.minusDays(3));
        for (User user : users) {
            log.debug("Deleting not activated user {}", user.getLogin());
            userRepository.delete(user);
            userSearchRepository.delete(user);
        }
    }
    
    public Page<User> getAllUsers(int pageNum, int pageSize, String sortBy, String orderBy){
        Sort sort = new Sort(Sort.Direction.ASC, "lastName");
        if(orderBy.equals("desc")){
            sort = new Sort(Sort.Direction.DESC, sortBy);
        }else if(orderBy.equals("asc")){
            sort = new Sort(Sort.Direction.ASC, sortBy);
        }
        PageRequest pageRequest = new PageRequest(pageNum - 1, pageSize, sort);
        List<User> usersUnsorted = userRepository.findAll();
        List<User> usersSorted = this.sort(usersUnsorted, sortBy, orderBy);
        List<User> users = this.paginate(usersSorted, pageSize , pageNum);
        Page page = new PageImpl(users, pageRequest, usersSorted.size());
            return page;
    }
    
    
    private List<User> sort(List<User> users, String sortBy, String orderBy) {
        if(sortBy.equals("firstName")){
            if(orderBy.equals("asc")){
                Collections.sort(users, (p1, p2) -> {
                    String name1 = "";
                    String name2 = "";
                    if(p1.getFirstName() != null){
                        name1 = p1.getFirstName();
                    }
                    if(p2.getFirstName() != null){
                        name2 = p2.getFirstName();
                    }
                    return name1.compareTo(name2);
                });
            }else if(orderBy.equals("desc"))
                Collections.sort(users, (p1, p2) -> {
                    String name1 = "";
                    String name2 = "";
                    if(p1.getFirstName() != null){
                        name1 = p1.getFirstName();
                    }
                    if(p2.getFirstName() != null){
                        name2 = p2.getFirstName();
                    }
                    return name2.compareTo(name1);
                });
        }else if(sortBy.equals("lastName")){
            if(orderBy.equals("asc")){
                Collections.sort(users, (p1, p2) -> {
                    String name1 = "";
                    String name2 = "";
                    if(p1.getFirstName() != null){
                        name1 = p1.getLastName();
                    }
                    if(p2.getFirstName() != null){
                        name2 = p2.getLastName();
                    }
                    return name1.compareTo(name2);
                });
            }else if(orderBy.equals("desc")){
                Collections.sort(users, (p1, p2) -> {
                    String name1 = "";
                    String name2 = "";
                    if(p1.getFirstName() != null){
                        name1 = p1.getLastName();
                    }
                    if(p2.getFirstName() != null){
                        name2 = p2.getLastName();
                    }
                    return name2.compareTo(name1);
                });
            }
        }else if(sortBy.equals("login")){
            if(orderBy.equals("asc")){
                Collections.sort(users, (p1, p2) -> {
                    String name1 = "";
                    String name2 = "";
                    if(p1.getLogin() != null){
                        name1 = p1.getLogin();
                    }
                    if(p2.getLogin() != null){
                        name2 = p2.getLogin();
                    }
                    return name1.compareTo(name2);
                });
            }else if(orderBy.equals("desc")){
                Collections.sort(users, (p1, p2) -> {
                    String name1 = "";
                    String name2 = "";
                    if(p1.getLogin() != null){
                        name1 = p1.getLogin();
                    }
                    if(p2.getLogin() != null){
                        name2 = p2.getLogin();
                    }
                    return name2.compareTo(name1);
                });
            }
        }else if(sortBy.equals("authorities")){
            if(orderBy.equals("asc")){
                Collections.sort(users, (p1, p2) -> {
                    String role1 = "";
                    String role2 = "";
                    if(p1.getAuthorities().size() > 1 && ((Authority)p1.getAuthorities().toArray()[1]).getName() != "ROLE_USER"){
                        role1 = ((Authority)p1.getAuthorities().toArray()[1]).getName();
                    }else if(p1.getAuthorities().toArray()[0] != null && ((Authority)p1.getAuthorities().toArray()[0]).getName() != "ROLE_USER"){
                        role1 = ((Authority)p1.getAuthorities().toArray()[0]).getName();
                    }
                    if(p2.getAuthorities().size() > 1 && ((Authority)p2.getAuthorities().toArray()[1]).getName() != "ROLE_USER"){
                        role2 = ((Authority)p2.getAuthorities().toArray()[1]).getName();
                    }else if(p2.getAuthorities().toArray()[0] != null && ((Authority)p2.getAuthorities().toArray()[0]).getName() != "ROLE_USER"){
                        role2 = ((Authority)p2.getAuthorities().toArray()[0]).getName();
                    }
                    return role1.compareTo(role2);
                });
            }else if(orderBy.equals("desc")){
                Collections.sort(users, (p1, p2) -> {
                    String role1 = "";
                    String role2 = "";
                    if(p1.getAuthorities().size() > 1 && ((Authority)p1.getAuthorities().toArray()[1]).getName() != "ROLE_USER"){
                        role1 = ((Authority)p1.getAuthorities().toArray()[1]).getName();
                    }else if(p1.getAuthorities().toArray()[0] != null && ((Authority)p1.getAuthorities().toArray()[0]).getName() != "ROLE_USER"){
                        role1 = ((Authority)p1.getAuthorities().toArray()[0]).getName();
                    }
                    if(p2.getAuthorities().size() > 1 && ((Authority)p2.getAuthorities().toArray()[1]).getName() != "ROLE_USER"){
                        role2 = ((Authority)p2.getAuthorities().toArray()[1]).getName();
                    }else if(p2.getAuthorities().toArray()[0] != null && ((Authority)p2.getAuthorities().toArray()[0]).getName() != "ROLE_USER"){
                        role2 = ((Authority)p2.getAuthorities().toArray()[0]).getName();
                    }
                    return role2.compareTo(role1);
                });
            }
        }
        return users;
    }
    
    private List<User> paginate(List<User> users, int pageSize, int pageNum) {
        int firstIndex = (pageNum-1)*pageSize;
        int lastIndex = pageNum*pageSize - 1;
        if(lastIndex > users.size()){
            lastIndex = users.size();
        }
        return users.subList(firstIndex, lastIndex);
    }
}
