package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.Institution;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Institution entity.
 */
public interface InstitutionSearchRepository extends ElasticsearchRepository<Institution, Long> {
}
