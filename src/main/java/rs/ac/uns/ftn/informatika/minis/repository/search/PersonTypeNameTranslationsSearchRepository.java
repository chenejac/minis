package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.PersonTypeNameTranslations;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the PersonTypeNameTranslations entity.
 */
public interface PersonTypeNameTranslationsSearchRepository extends ElasticsearchRepository<PersonTypeNameTranslations, Long> {
}
