package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.Privilege;
import rs.ac.uns.ftn.informatika.minis.repository.PrivilegeRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PrivilegeSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Privilege.
 */
@RestController
@RequestMapping("/api")
public class PrivilegeResource {

    private final Logger log = LoggerFactory.getLogger(PrivilegeResource.class);

    @Inject
    private PrivilegeRepository privilegeRepository;

    @Inject
    private PrivilegeSearchRepository privilegeSearchRepository;

    /**
     * POST  /privileges -> Create a new privilege.
     */
    @RequestMapping(value = "/privileges",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Privilege> createPrivilege(@RequestBody Privilege privilege) throws URISyntaxException {
        log.debug("REST request to save Privilege : {}", privilege);
        if (privilege.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new privilege cannot already have an ID").body(null);
        }
        Privilege result = privilegeRepository.save(privilege);
        privilegeSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/privileges/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("privilege", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /privileges -> Updates an existing privilege.
     */
    @RequestMapping(value = "/privileges",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Privilege> updatePrivilege(@RequestBody Privilege privilege) throws URISyntaxException {
        log.debug("REST request to update Privilege : {}", privilege);
        if (privilege.getId() == null) {
            return createPrivilege(privilege);
        }
        Privilege result = privilegeRepository.save(privilege);
        privilegeSearchRepository.save(privilege);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("privilege", privilege.getId().toString()))
                .body(result);
    }

    /**
     * GET  /privileges -> get all the privileges.
     */
    @RequestMapping(value = "/privileges",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Privilege> getAllPrivileges() {
        log.debug("REST request to get all Privileges");
        return privilegeRepository.findAll();
    }

    /**
     * GET  /privileges/:id -> get the "id" privilege.
     */
    @RequestMapping(value = "/privileges/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Privilege> getPrivilege(@PathVariable Long id) {
        log.debug("REST request to get Privilege : {}", id);
        return Optional.ofNullable(privilegeRepository.findOne(id))
            .map(privilege -> new ResponseEntity<>(
                privilege,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /privileges/:id -> delete the "id" privilege.
     */
    @RequestMapping(value = "/privileges/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePrivilege(@PathVariable Long id) {
        log.debug("REST request to delete Privilege : {}", id);
        privilegeRepository.delete(id);
        privilegeSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("privilege", id.toString())).build();
    }

    /**
     * SEARCH  /_search/privileges/:query -> search for the privilege corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/privileges/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Privilege> searchPrivileges(@PathVariable String query) {
        return StreamSupport
            .stream(privilegeSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
