package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.PersonBibliographyTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.PersonBibliographyTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PersonBibliographyTranslationsSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PersonBibliographyTranslations.
 */
@RestController
@RequestMapping("/api")
public class PersonBibliographyTranslationsResource {

    private final Logger log = LoggerFactory.getLogger(PersonBibliographyTranslationsResource.class);

    @Inject
    private PersonBibliographyTranslationsRepository personBibliographyTranslationsRepository;

    @Inject
    private PersonBibliographyTranslationsSearchRepository personBibliographyTranslationsSearchRepository;

    /**
     * POST  /personBibliographyTranslationss -> Create a new personBibliographyTranslations.
     */
    @RequestMapping(value = "/personBibliographyTranslationss",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonBibliographyTranslations> createPersonBibliographyTranslations(@RequestBody PersonBibliographyTranslations personBibliographyTranslations) throws URISyntaxException {
        log.debug("REST request to save PersonBibliographyTranslations : {}", personBibliographyTranslations);
        if (personBibliographyTranslations.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new personBibliographyTranslations cannot already have an ID").body(null);
        }
        PersonBibliographyTranslations result = personBibliographyTranslationsRepository.save(personBibliographyTranslations);
        personBibliographyTranslationsSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/personBibliographyTranslationss/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("personBibliographyTranslations", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /personBibliographyTranslationss -> Updates an existing personBibliographyTranslations.
     */
    @RequestMapping(value = "/personBibliographyTranslationss",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonBibliographyTranslations> updatePersonBibliographyTranslations(@RequestBody PersonBibliographyTranslations personBibliographyTranslations) throws URISyntaxException {
        log.debug("REST request to update PersonBibliographyTranslations : {}", personBibliographyTranslations);
        if (personBibliographyTranslations.getId() == null) {
            return createPersonBibliographyTranslations(personBibliographyTranslations);
        }
        PersonBibliographyTranslations result = personBibliographyTranslationsRepository.save(personBibliographyTranslations);
        personBibliographyTranslationsSearchRepository.save(personBibliographyTranslations);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("personBibliographyTranslations", personBibliographyTranslations.getId().toString()))
                .body(result);
    }

    /**
     * GET  /personBibliographyTranslationss -> get all the personBibliographyTranslationss.
     */
    @RequestMapping(value = "/personBibliographyTranslationss",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PersonBibliographyTranslations> getAllPersonBibliographyTranslationss() {
        log.debug("REST request to get all PersonBibliographyTranslationss");
        return personBibliographyTranslationsRepository.findAll();
    }

    /**
     * GET  /personBibliographyTranslationss/:id -> get the "id" personBibliographyTranslations.
     */
    @RequestMapping(value = "/personBibliographyTranslationss/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonBibliographyTranslations> getPersonBibliographyTranslations(@PathVariable Long id) {
        log.debug("REST request to get PersonBibliographyTranslations : {}", id);
        return Optional.ofNullable(personBibliographyTranslationsRepository.findOne(id))
            .map(personBibliographyTranslations -> new ResponseEntity<>(
                personBibliographyTranslations,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /personBibliographyTranslationss/:id -> delete the "id" personBibliographyTranslations.
     */
    @RequestMapping(value = "/personBibliographyTranslationss/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePersonBibliographyTranslations(@PathVariable Long id) {
        log.debug("REST request to delete PersonBibliographyTranslations : {}", id);
        personBibliographyTranslationsRepository.delete(id);
        personBibliographyTranslationsSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("personBibliographyTranslations", id.toString())).build();
    }

    /**
     * SEARCH  /_search/personBibliographyTranslationss/:query -> search for the personBibliographyTranslations corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/personBibliographyTranslationss/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PersonBibliographyTranslations> searchPersonBibliographyTranslationss(@PathVariable String query) {
        return StreamSupport
            .stream(personBibliographyTranslationsSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
