package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.PersonKeywordsTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.PersonKeywordsTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PersonKeywordsTranslationsSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PersonKeywordsTranslations.
 */
@RestController
@RequestMapping("/api")
public class PersonKeywordsTranslationsResource {

    private final Logger log = LoggerFactory.getLogger(PersonKeywordsTranslationsResource.class);

    @Inject
    private PersonKeywordsTranslationsRepository personKeywordsTranslationsRepository;

    @Inject
    private PersonKeywordsTranslationsSearchRepository personKeywordsTranslationsSearchRepository;

    /**
     * POST  /personKeywordsTranslationss -> Create a new personKeywordsTranslations.
     */
    @RequestMapping(value = "/personKeywordsTranslationss",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonKeywordsTranslations> createPersonKeywordsTranslations(@RequestBody PersonKeywordsTranslations personKeywordsTranslations) throws URISyntaxException {
        log.debug("REST request to save PersonKeywordsTranslations : {}", personKeywordsTranslations);
        if (personKeywordsTranslations.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new personKeywordsTranslations cannot already have an ID").body(null);
        }
        PersonKeywordsTranslations result = personKeywordsTranslationsRepository.save(personKeywordsTranslations);
        personKeywordsTranslationsSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/personKeywordsTranslationss/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("personKeywordsTranslations", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /personKeywordsTranslationss -> Updates an existing personKeywordsTranslations.
     */
    @RequestMapping(value = "/personKeywordsTranslationss",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonKeywordsTranslations> updatePersonKeywordsTranslations(@RequestBody PersonKeywordsTranslations personKeywordsTranslations) throws URISyntaxException {
        log.debug("REST request to update PersonKeywordsTranslations : {}", personKeywordsTranslations);
        if (personKeywordsTranslations.getId() == null) {
            return createPersonKeywordsTranslations(personKeywordsTranslations);
        }
        PersonKeywordsTranslations result = personKeywordsTranslationsRepository.save(personKeywordsTranslations);
        personKeywordsTranslationsSearchRepository.save(personKeywordsTranslations);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("personKeywordsTranslations", personKeywordsTranslations.getId().toString()))
                .body(result);
    }

    /**
     * GET  /personKeywordsTranslationss -> get all the personKeywordsTranslationss.
     */
    @RequestMapping(value = "/personKeywordsTranslationss",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PersonKeywordsTranslations> getAllPersonKeywordsTranslationss() {
        log.debug("REST request to get all PersonKeywordsTranslationss");
        return personKeywordsTranslationsRepository.findAll();
    }

    /**
     * GET  /personKeywordsTranslationss/:id -> get the "id" personKeywordsTranslations.
     */
    @RequestMapping(value = "/personKeywordsTranslationss/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonKeywordsTranslations> getPersonKeywordsTranslations(@PathVariable Long id) {
        log.debug("REST request to get PersonKeywordsTranslations : {}", id);
        return Optional.ofNullable(personKeywordsTranslationsRepository.findOne(id))
            .map(personKeywordsTranslations -> new ResponseEntity<>(
                personKeywordsTranslations,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /personKeywordsTranslationss/:id -> delete the "id" personKeywordsTranslations.
     */
    @RequestMapping(value = "/personKeywordsTranslationss/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePersonKeywordsTranslations(@PathVariable Long id) {
        log.debug("REST request to delete PersonKeywordsTranslations : {}", id);
        personKeywordsTranslationsRepository.delete(id);
        personKeywordsTranslationsSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("personKeywordsTranslations", id.toString())).build();
    }

    /**
     * SEARCH  /_search/personKeywordsTranslationss/:query -> search for the personKeywordsTranslations corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/personKeywordsTranslationss/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PersonKeywordsTranslations> searchPersonKeywordsTranslationss(@PathVariable String query) {
        return StreamSupport
            .stream(personKeywordsTranslationsSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
