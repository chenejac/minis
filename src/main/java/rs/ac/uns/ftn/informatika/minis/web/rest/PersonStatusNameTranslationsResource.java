package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.PersonStatusNameTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.PersonStatusNameTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PersonStatusNameTranslationsSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PersonStatusNameTranslations.
 */
@RestController
@RequestMapping("/api")
public class PersonStatusNameTranslationsResource {

    private final Logger log = LoggerFactory.getLogger(PersonStatusNameTranslationsResource.class);

    @Inject
    private PersonStatusNameTranslationsRepository personStatusNameTranslationsRepository;

    @Inject
    private PersonStatusNameTranslationsSearchRepository personStatusNameTranslationsSearchRepository;

    /**
     * POST  /personStatusNameTranslationss -> Create a new personStatusNameTranslations.
     */
    @RequestMapping(value = "/personStatusNameTranslationss",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonStatusNameTranslations> createPersonStatusNameTranslations(@RequestBody PersonStatusNameTranslations personStatusNameTranslations) throws URISyntaxException {
        log.debug("REST request to save PersonStatusNameTranslations : {}", personStatusNameTranslations);
        if (personStatusNameTranslations.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new personStatusNameTranslations cannot already have an ID").body(null);
        }
        PersonStatusNameTranslations result = personStatusNameTranslationsRepository.save(personStatusNameTranslations);
        personStatusNameTranslationsSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/personStatusNameTranslationss/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("personStatusNameTranslations", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /personStatusNameTranslationss -> Updates an existing personStatusNameTranslations.
     */
    @RequestMapping(value = "/personStatusNameTranslationss",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonStatusNameTranslations> updatePersonStatusNameTranslations(@RequestBody PersonStatusNameTranslations personStatusNameTranslations) throws URISyntaxException {
        log.debug("REST request to update PersonStatusNameTranslations : {}", personStatusNameTranslations);
        if (personStatusNameTranslations.getId() == null) {
            return createPersonStatusNameTranslations(personStatusNameTranslations);
        }
        PersonStatusNameTranslations result = personStatusNameTranslationsRepository.save(personStatusNameTranslations);
        personStatusNameTranslationsSearchRepository.save(personStatusNameTranslations);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("personStatusNameTranslations", personStatusNameTranslations.getId().toString()))
                .body(result);
    }

    /**
     * GET  /personStatusNameTranslationss -> get all the personStatusNameTranslationss.
     */
    @RequestMapping(value = "/personStatusNameTranslationss",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PersonStatusNameTranslations> getAllPersonStatusNameTranslationss() {
        log.debug("REST request to get all PersonStatusNameTranslationss");
        return personStatusNameTranslationsRepository.findAll();
    }

    /**
     * GET  /personStatusNameTranslationss/:id -> get the "id" personStatusNameTranslations.
     */
    @RequestMapping(value = "/personStatusNameTranslationss/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonStatusNameTranslations> getPersonStatusNameTranslations(@PathVariable Long id) {
        log.debug("REST request to get PersonStatusNameTranslations : {}", id);
        return Optional.ofNullable(personStatusNameTranslationsRepository.findOne(id))
            .map(personStatusNameTranslations -> new ResponseEntity<>(
                personStatusNameTranslations,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /personStatusNameTranslationss/:id -> delete the "id" personStatusNameTranslations.
     */
    @RequestMapping(value = "/personStatusNameTranslationss/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePersonStatusNameTranslations(@PathVariable Long id) {
        log.debug("REST request to delete PersonStatusNameTranslations : {}", id);
        personStatusNameTranslationsRepository.delete(id);
        personStatusNameTranslationsSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("personStatusNameTranslations", id.toString())).build();
    }

    /**
     * SEARCH  /_search/personStatusNameTranslationss/:query -> search for the personStatusNameTranslations corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/personStatusNameTranslationss/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PersonStatusNameTranslations> searchPersonStatusNameTranslationss(@PathVariable String query) {
        return StreamSupport
            .stream(personStatusNameTranslationsSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
