package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.InstitutionNameTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.InstitutionNameTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.InstitutionNameTranslationsSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing InstitutionNameTranslations.
 */
@RestController
@RequestMapping("/api")
public class InstitutionNameTranslationsResource {

    private final Logger log = LoggerFactory.getLogger(InstitutionNameTranslationsResource.class);

    @Inject
    private InstitutionNameTranslationsRepository institutionNameTranslationsRepository;

    @Inject
    private InstitutionNameTranslationsSearchRepository institutionNameTranslationsSearchRepository;

    /**
     * POST  /institutionNameTranslationss -> Create a new institutionNameTranslations.
     */
    @RequestMapping(value = "/institutionNameTranslationss",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstitutionNameTranslations> createInstitutionNameTranslations(@RequestBody InstitutionNameTranslations institutionNameTranslations) throws URISyntaxException {
        log.debug("REST request to save InstitutionNameTranslations : {}", institutionNameTranslations);
        if (institutionNameTranslations.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new institutionNameTranslations cannot already have an ID").body(null);
        }
        InstitutionNameTranslations result = institutionNameTranslationsRepository.save(institutionNameTranslations);
        institutionNameTranslationsSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/institutionNameTranslationss/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("institutionNameTranslations", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /institutionNameTranslationss -> Updates an existing institutionNameTranslations.
     */
    @RequestMapping(value = "/institutionNameTranslationss",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstitutionNameTranslations> updateInstitutionNameTranslations(@RequestBody InstitutionNameTranslations institutionNameTranslations) throws URISyntaxException {
        log.debug("REST request to update InstitutionNameTranslations : {}", institutionNameTranslations);
        if (institutionNameTranslations.getId() == null) {
            return createInstitutionNameTranslations(institutionNameTranslations);
        }
        InstitutionNameTranslations result = institutionNameTranslationsRepository.save(institutionNameTranslations);
        institutionNameTranslationsSearchRepository.save(institutionNameTranslations);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("institutionNameTranslations", institutionNameTranslations.getId().toString()))
                .body(result);
    }

    /**
     * GET  /institutionNameTranslationss -> get all the institutionNameTranslationss.
     */
    @RequestMapping(value = "/institutionNameTranslationss",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<InstitutionNameTranslations> getAllInstitutionNameTranslationss() {
        log.debug("REST request to get all InstitutionNameTranslationss");
        return institutionNameTranslationsRepository.findAll();
    }

    /**
     * GET  /institutionNameTranslationss/:id -> get the "id" institutionNameTranslations.
     */
    @RequestMapping(value = "/institutionNameTranslationss/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstitutionNameTranslations> getInstitutionNameTranslations(@PathVariable Long id) {
        log.debug("REST request to get InstitutionNameTranslations : {}", id);
        return Optional.ofNullable(institutionNameTranslationsRepository.findOne(id))
            .map(institutionNameTranslations -> new ResponseEntity<>(
                institutionNameTranslations,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /institutionNameTranslationss/:id -> delete the "id" institutionNameTranslations.
     */
    @RequestMapping(value = "/institutionNameTranslationss/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteInstitutionNameTranslations(@PathVariable Long id) {
        log.debug("REST request to delete InstitutionNameTranslations : {}", id);
        institutionNameTranslationsRepository.delete(id);
        institutionNameTranslationsSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("institutionNameTranslations", id.toString())).build();
    }

    /**
     * SEARCH  /_search/institutionNameTranslationss/:query -> search for the institutionNameTranslations corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/institutionNameTranslationss/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<InstitutionNameTranslations> searchInstitutionNameTranslationss(@PathVariable String query) {
        return StreamSupport
            .stream(institutionNameTranslationsSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
