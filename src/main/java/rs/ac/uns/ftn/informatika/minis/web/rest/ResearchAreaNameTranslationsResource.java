package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.ResearchAreaNameTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.ResearchAreaNameTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.ResearchAreaNameTranslationsSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ResearchAreaNameTranslations.
 */
@RestController
@RequestMapping("/api")
public class ResearchAreaNameTranslationsResource {

    private final Logger log = LoggerFactory.getLogger(ResearchAreaNameTranslationsResource.class);

    @Inject
    private ResearchAreaNameTranslationsRepository researchAreaNameTranslationsRepository;

    @Inject
    private ResearchAreaNameTranslationsSearchRepository researchAreaNameTranslationsSearchRepository;

    /**
     * POST  /researchAreaNameTranslationss -> Create a new researchAreaNameTranslations.
     */
    @RequestMapping(value = "/researchAreaNameTranslationss",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ResearchAreaNameTranslations> createResearchAreaNameTranslations(@RequestBody ResearchAreaNameTranslations researchAreaNameTranslations) throws URISyntaxException {
        log.debug("REST request to save ResearchAreaNameTranslations : {}", researchAreaNameTranslations);
        if (researchAreaNameTranslations.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new researchAreaNameTranslations cannot already have an ID").body(null);
        }
        ResearchAreaNameTranslations result = researchAreaNameTranslationsRepository.save(researchAreaNameTranslations);
        researchAreaNameTranslationsSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/researchAreaNameTranslationss/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("researchAreaNameTranslations", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /researchAreaNameTranslationss -> Updates an existing researchAreaNameTranslations.
     */
    @RequestMapping(value = "/researchAreaNameTranslationss",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ResearchAreaNameTranslations> updateResearchAreaNameTranslations(@RequestBody ResearchAreaNameTranslations researchAreaNameTranslations) throws URISyntaxException {
        log.debug("REST request to update ResearchAreaNameTranslations : {}", researchAreaNameTranslations);
        if (researchAreaNameTranslations.getId() == null) {
            return createResearchAreaNameTranslations(researchAreaNameTranslations);
        }
        ResearchAreaNameTranslations result = researchAreaNameTranslationsRepository.save(researchAreaNameTranslations);
        researchAreaNameTranslationsSearchRepository.save(researchAreaNameTranslations);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("researchAreaNameTranslations", researchAreaNameTranslations.getId().toString()))
                .body(result);
    }

    /**
     * GET  /researchAreaNameTranslationss -> get all the researchAreaNameTranslationss.
     */
    @RequestMapping(value = "/researchAreaNameTranslationss",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ResearchAreaNameTranslations> getAllResearchAreaNameTranslationss() {
        log.debug("REST request to get all ResearchAreaNameTranslationss");
        return researchAreaNameTranslationsRepository.findAll();
    }

    /**
     * GET  /researchAreaNameTranslationss/:id -> get the "id" researchAreaNameTranslations.
     */
    @RequestMapping(value = "/researchAreaNameTranslationss/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ResearchAreaNameTranslations> getResearchAreaNameTranslations(@PathVariable Long id) {
        log.debug("REST request to get ResearchAreaNameTranslations : {}", id);
        return Optional.ofNullable(researchAreaNameTranslationsRepository.findOne(id))
            .map(researchAreaNameTranslations -> new ResponseEntity<>(
                researchAreaNameTranslations,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /researchAreaNameTranslationss/:id -> delete the "id" researchAreaNameTranslations.
     */
    @RequestMapping(value = "/researchAreaNameTranslationss/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteResearchAreaNameTranslations(@PathVariable Long id) {
        log.debug("REST request to delete ResearchAreaNameTranslations : {}", id);
        researchAreaNameTranslationsRepository.delete(id);
        researchAreaNameTranslationsSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("researchAreaNameTranslations", id.toString())).build();
    }

    /**
     * SEARCH  /_search/researchAreaNameTranslationss/:query -> search for the researchAreaNameTranslations corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/researchAreaNameTranslationss/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ResearchAreaNameTranslations> searchResearchAreaNameTranslationss(@PathVariable String query) {
        return StreamSupport
            .stream(researchAreaNameTranslationsSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
