/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.uns.ftn.informatika.minis.web.rest.dto;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import rs.ac.uns.ftn.informatika.minis.domain.Language;
import rs.ac.uns.ftn.informatika.minis.domain.Position;

/**
 *
 * @author stefan
 */
public class PositionDTO {
    private Long id;
    private String name;
    private String description;
    private String oz;
    private Language language;
    private Set<PositionNameTranslationsDTO> nameTranslations = new HashSet<>();

    public PositionDTO(Long id, String name, String description, String oz, Language language) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.oz = oz;
        this.language = language;
    }
    
    public PositionDTO(Position position) {
        this.id = position.getId();
        this.name = position.getName();
        this.description = position.getDescription();
        this.oz = position.getOz();
        this.language = position.getLanguage();
        if (position.getNameTranslations() != null) {
            this.nameTranslations = position.getNameTranslations().stream()
                .map(nameTranslation -> new PositionNameTranslationsDTO(nameTranslation))
                .collect(Collectors.toSet());
        } else {
            this.nameTranslations = null;
        }
    }
    
    public Long getId() {
        return id;
    }

    public String getOz() {
        return oz;
    }

    public void setOz(String oz) {
        this.oz = oz;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Set<PositionNameTranslationsDTO> getNameTranslations() {
        return nameTranslations;
    }

    public void setNameTranslations(Set<PositionNameTranslationsDTO> positionNameTranslationss) {
        this.nameTranslations = positionNameTranslationss;
    }

    @Override
    public String toString() {
        return "Position{"
                + "id=" + id
                + ", name='" + name + "'"
                + ", description='" + description + "'"
                + '}';
    }
}
