package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.InstitutionKeywordsTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.InstitutionKeywordsTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.InstitutionKeywordsTranslationsSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing InstitutionKeywordsTranslations.
 */
@RestController
@RequestMapping("/api")
public class InstitutionKeywordsTranslationsResource {

    private final Logger log = LoggerFactory.getLogger(InstitutionKeywordsTranslationsResource.class);

    @Inject
    private InstitutionKeywordsTranslationsRepository institutionKeywordsTranslationsRepository;

    @Inject
    private InstitutionKeywordsTranslationsSearchRepository institutionKeywordsTranslationsSearchRepository;

    /**
     * POST  /institutionKeywordsTranslationss -> Create a new institutionKeywordsTranslations.
     */
    @RequestMapping(value = "/institutionKeywordsTranslationss",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstitutionKeywordsTranslations> createInstitutionKeywordsTranslations(@RequestBody InstitutionKeywordsTranslations institutionKeywordsTranslations) throws URISyntaxException {
        log.debug("REST request to save InstitutionKeywordsTranslations : {}", institutionKeywordsTranslations);
        if (institutionKeywordsTranslations.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new institutionKeywordsTranslations cannot already have an ID").body(null);
        }
        InstitutionKeywordsTranslations result = institutionKeywordsTranslationsRepository.save(institutionKeywordsTranslations);
        institutionKeywordsTranslationsSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/institutionKeywordsTranslationss/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("institutionKeywordsTranslations", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /institutionKeywordsTranslationss -> Updates an existing institutionKeywordsTranslations.
     */
    @RequestMapping(value = "/institutionKeywordsTranslationss",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstitutionKeywordsTranslations> updateInstitutionKeywordsTranslations(@RequestBody InstitutionKeywordsTranslations institutionKeywordsTranslations) throws URISyntaxException {
        log.debug("REST request to update InstitutionKeywordsTranslations : {}", institutionKeywordsTranslations);
        if (institutionKeywordsTranslations.getId() == null) {
            return createInstitutionKeywordsTranslations(institutionKeywordsTranslations);
        }
        InstitutionKeywordsTranslations result = institutionKeywordsTranslationsRepository.save(institutionKeywordsTranslations);
        institutionKeywordsTranslationsSearchRepository.save(institutionKeywordsTranslations);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("institutionKeywordsTranslations", institutionKeywordsTranslations.getId().toString()))
                .body(result);
    }

    /**
     * GET  /institutionKeywordsTranslationss -> get all the institutionKeywordsTranslationss.
     */
    @RequestMapping(value = "/institutionKeywordsTranslationss",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<InstitutionKeywordsTranslations> getAllInstitutionKeywordsTranslationss() {
        log.debug("REST request to get all InstitutionKeywordsTranslationss");
        return institutionKeywordsTranslationsRepository.findAll();
    }

    /**
     * GET  /institutionKeywordsTranslationss/:id -> get the "id" institutionKeywordsTranslations.
     */
    @RequestMapping(value = "/institutionKeywordsTranslationss/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstitutionKeywordsTranslations> getInstitutionKeywordsTranslations(@PathVariable Long id) {
        log.debug("REST request to get InstitutionKeywordsTranslations : {}", id);
        return Optional.ofNullable(institutionKeywordsTranslationsRepository.findOne(id))
            .map(institutionKeywordsTranslations -> new ResponseEntity<>(
                institutionKeywordsTranslations,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /institutionKeywordsTranslationss/:id -> delete the "id" institutionKeywordsTranslations.
     */
    @RequestMapping(value = "/institutionKeywordsTranslationss/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteInstitutionKeywordsTranslations(@PathVariable Long id) {
        log.debug("REST request to delete InstitutionKeywordsTranslations : {}", id);
        institutionKeywordsTranslationsRepository.delete(id);
        institutionKeywordsTranslationsSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("institutionKeywordsTranslations", id.toString())).build();
    }

    /**
     * SEARCH  /_search/institutionKeywordsTranslationss/:query -> search for the institutionKeywordsTranslations corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/institutionKeywordsTranslationss/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<InstitutionKeywordsTranslations> searchInstitutionKeywordsTranslationss(@PathVariable String query) {
        return StreamSupport
            .stream(institutionKeywordsTranslationsSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
