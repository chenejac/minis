package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.CategoryNameTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.CategoryNameTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.CategoryNameTranslationsSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing CategoryNameTranslations.
 */
@RestController
@RequestMapping("/api")
public class CategoryNameTranslationsResource {

    private final Logger log = LoggerFactory.getLogger(CategoryNameTranslationsResource.class);

    @Inject
    private CategoryNameTranslationsRepository categoryNameTranslationsRepository;

    @Inject
    private CategoryNameTranslationsSearchRepository categoryNameTranslationsSearchRepository;

    /**
     * POST  /categoryNameTranslationss -> Create a new categoryNameTranslations.
     */
    @RequestMapping(value = "/categoryNameTranslationss",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CategoryNameTranslations> createCategoryNameTranslations(@RequestBody CategoryNameTranslations categoryNameTranslations) throws URISyntaxException {
        log.debug("REST request to save CategoryNameTranslations : {}", categoryNameTranslations);
        if (categoryNameTranslations.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new categoryNameTranslations cannot already have an ID").body(null);
        }
        CategoryNameTranslations result = categoryNameTranslationsRepository.save(categoryNameTranslations);
        categoryNameTranslationsSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/categoryNameTranslationss/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("categoryNameTranslations", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /categoryNameTranslationss -> Updates an existing categoryNameTranslations.
     */
    @RequestMapping(value = "/categoryNameTranslationss",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CategoryNameTranslations> updateCategoryNameTranslations(@RequestBody CategoryNameTranslations categoryNameTranslations) throws URISyntaxException {
        log.debug("REST request to update CategoryNameTranslations : {}", categoryNameTranslations);
        if (categoryNameTranslations.getId() == null) {
            return createCategoryNameTranslations(categoryNameTranslations);
        }
        CategoryNameTranslations result = categoryNameTranslationsRepository.save(categoryNameTranslations);
        categoryNameTranslationsSearchRepository.save(categoryNameTranslations);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("categoryNameTranslations", categoryNameTranslations.getId().toString()))
                .body(result);
    }

    /**
     * GET  /categoryNameTranslationss -> get all the categoryNameTranslationss.
     */
    @RequestMapping(value = "/categoryNameTranslationss",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<CategoryNameTranslations> getAllCategoryNameTranslationss() {
        log.debug("REST request to get all CategoryNameTranslationss");
        return categoryNameTranslationsRepository.findAll();
    }

    /**
     * GET  /categoryNameTranslationss/:id -> get the "id" categoryNameTranslations.
     */
    @RequestMapping(value = "/categoryNameTranslationss/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CategoryNameTranslations> getCategoryNameTranslations(@PathVariable Long id) {
        log.debug("REST request to get CategoryNameTranslations : {}", id);
        return Optional.ofNullable(categoryNameTranslationsRepository.findOne(id))
            .map(categoryNameTranslations -> new ResponseEntity<>(
                categoryNameTranslations,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /categoryNameTranslationss/:id -> delete the "id" categoryNameTranslations.
     */
    @RequestMapping(value = "/categoryNameTranslationss/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteCategoryNameTranslations(@PathVariable Long id) {
        log.debug("REST request to delete CategoryNameTranslations : {}", id);
        categoryNameTranslationsRepository.delete(id);
        categoryNameTranslationsSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("categoryNameTranslations", id.toString())).build();
    }

    /**
     * SEARCH  /_search/categoryNameTranslationss/:query -> search for the categoryNameTranslations corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/categoryNameTranslationss/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<CategoryNameTranslations> searchCategoryNameTranslationss(@PathVariable String query) {
        return StreamSupport
            .stream(categoryNameTranslationsSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
