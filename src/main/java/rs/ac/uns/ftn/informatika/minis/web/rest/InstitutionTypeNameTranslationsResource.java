package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.InstitutionTypeNameTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.InstitutionTypeNameTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.InstitutionTypeNameTranslationsSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing InstitutionTypeNameTranslations.
 */
@RestController
@RequestMapping("/api")
public class InstitutionTypeNameTranslationsResource {

    private final Logger log = LoggerFactory.getLogger(InstitutionTypeNameTranslationsResource.class);

    @Inject
    private InstitutionTypeNameTranslationsRepository institutionTypeNameTranslationsRepository;

    @Inject
    private InstitutionTypeNameTranslationsSearchRepository institutionTypeNameTranslationsSearchRepository;

    /**
     * POST  /institutionTypeNameTranslationss -> Create a new institutionTypeNameTranslations.
     */
    @RequestMapping(value = "/institutionTypeNameTranslationss",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstitutionTypeNameTranslations> createInstitutionTypeNameTranslations(@RequestBody InstitutionTypeNameTranslations institutionTypeNameTranslations) throws URISyntaxException {
        log.debug("REST request to save InstitutionTypeNameTranslations : {}", institutionTypeNameTranslations);
        if (institutionTypeNameTranslations.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new institutionTypeNameTranslations cannot already have an ID").body(null);
        }
        InstitutionTypeNameTranslations result = institutionTypeNameTranslationsRepository.save(institutionTypeNameTranslations);
        institutionTypeNameTranslationsSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/institutionTypeNameTranslationss/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("institutionTypeNameTranslations", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /institutionTypeNameTranslationss -> Updates an existing institutionTypeNameTranslations.
     */
    @RequestMapping(value = "/institutionTypeNameTranslationss",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstitutionTypeNameTranslations> updateInstitutionTypeNameTranslations(@RequestBody InstitutionTypeNameTranslations institutionTypeNameTranslations) throws URISyntaxException {
        log.debug("REST request to update InstitutionTypeNameTranslations : {}", institutionTypeNameTranslations);
        if (institutionTypeNameTranslations.getId() == null) {
            return createInstitutionTypeNameTranslations(institutionTypeNameTranslations);
        }
        InstitutionTypeNameTranslations result = institutionTypeNameTranslationsRepository.save(institutionTypeNameTranslations);
        institutionTypeNameTranslationsSearchRepository.save(institutionTypeNameTranslations);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("institutionTypeNameTranslations", institutionTypeNameTranslations.getId().toString()))
                .body(result);
    }

    /**
     * GET  /institutionTypeNameTranslationss -> get all the institutionTypeNameTranslationss.
     */
    @RequestMapping(value = "/institutionTypeNameTranslationss",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<InstitutionTypeNameTranslations> getAllInstitutionTypeNameTranslationss() {
        log.debug("REST request to get all InstitutionTypeNameTranslationss");
        return institutionTypeNameTranslationsRepository.findAll();
    }

    /**
     * GET  /institutionTypeNameTranslationss/:id -> get the "id" institutionTypeNameTranslations.
     */
    @RequestMapping(value = "/institutionTypeNameTranslationss/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstitutionTypeNameTranslations> getInstitutionTypeNameTranslations(@PathVariable Long id) {
        log.debug("REST request to get InstitutionTypeNameTranslations : {}", id);
        return Optional.ofNullable(institutionTypeNameTranslationsRepository.findOne(id))
            .map(institutionTypeNameTranslations -> new ResponseEntity<>(
                institutionTypeNameTranslations,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /institutionTypeNameTranslationss/:id -> delete the "id" institutionTypeNameTranslations.
     */
    @RequestMapping(value = "/institutionTypeNameTranslationss/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteInstitutionTypeNameTranslations(@PathVariable Long id) {
        log.debug("REST request to delete InstitutionTypeNameTranslations : {}", id);
        institutionTypeNameTranslationsRepository.delete(id);
        institutionTypeNameTranslationsSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("institutionTypeNameTranslations", id.toString())).build();
    }

    /**
     * SEARCH  /_search/institutionTypeNameTranslationss/:query -> search for the institutionTypeNameTranslations corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/institutionTypeNameTranslationss/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<InstitutionTypeNameTranslations> searchInstitutionTypeNameTranslationss(@PathVariable String query) {
        return StreamSupport
            .stream(institutionTypeNameTranslationsSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
