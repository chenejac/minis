package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.ActivityNameTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.ActivityNameTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.ActivityNameTranslationsSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ActivityNameTranslations.
 */
@RestController
@RequestMapping("/api")
public class ActivityNameTranslationsResource {

    private final Logger log = LoggerFactory.getLogger(ActivityNameTranslationsResource.class);

    @Inject
    private ActivityNameTranslationsRepository activityNameTranslationsRepository;

    @Inject
    private ActivityNameTranslationsSearchRepository activityNameTranslationsSearchRepository;

    /**
     * POST  /activityNameTranslationss -> Create a new activityNameTranslations.
     */
    @RequestMapping(value = "/activityNameTranslationss",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ActivityNameTranslations> createActivityNameTranslations(@RequestBody ActivityNameTranslations activityNameTranslations) throws URISyntaxException {
        log.debug("REST request to save ActivityNameTranslations : {}", activityNameTranslations);
        if (activityNameTranslations.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new activityNameTranslations cannot already have an ID").body(null);
        }
        ActivityNameTranslations result = activityNameTranslationsRepository.save(activityNameTranslations);
        activityNameTranslationsSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/activityNameTranslationss/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("activityNameTranslations", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /activityNameTranslationss -> Updates an existing activityNameTranslations.
     */
    @RequestMapping(value = "/activityNameTranslationss",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ActivityNameTranslations> updateActivityNameTranslations(@RequestBody ActivityNameTranslations activityNameTranslations) throws URISyntaxException {
        log.debug("REST request to update ActivityNameTranslations : {}", activityNameTranslations);
        if (activityNameTranslations.getId() == null) {
            return createActivityNameTranslations(activityNameTranslations);
        }
        ActivityNameTranslations result = activityNameTranslationsRepository.save(activityNameTranslations);
        activityNameTranslationsSearchRepository.save(activityNameTranslations);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("activityNameTranslations", activityNameTranslations.getId().toString()))
                .body(result);
    }

    /**
     * GET  /activityNameTranslationss -> get all the activityNameTranslationss.
     */
    @RequestMapping(value = "/activityNameTranslationss",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ActivityNameTranslations> getAllActivityNameTranslationss() {
        log.debug("REST request to get all ActivityNameTranslationss");
        return activityNameTranslationsRepository.findAll();
    }

    /**
     * GET  /activityNameTranslationss/:id -> get the "id" activityNameTranslations.
     */
    @RequestMapping(value = "/activityNameTranslationss/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ActivityNameTranslations> getActivityNameTranslations(@PathVariable Long id) {
        log.debug("REST request to get ActivityNameTranslations : {}", id);
        return Optional.ofNullable(activityNameTranslationsRepository.findOne(id))
            .map(activityNameTranslations -> new ResponseEntity<>(
                activityNameTranslations,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /activityNameTranslationss/:id -> delete the "id" activityNameTranslations.
     */
    @RequestMapping(value = "/activityNameTranslationss/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteActivityNameTranslations(@PathVariable Long id) {
        log.debug("REST request to delete ActivityNameTranslations : {}", id);
        activityNameTranslationsRepository.delete(id);
        activityNameTranslationsSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("activityNameTranslations", id.toString())).build();
    }

    /**
     * SEARCH  /_search/activityNameTranslationss/:query -> search for the activityNameTranslations corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/activityNameTranslationss/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ActivityNameTranslations> searchActivityNameTranslationss(@PathVariable String query) {
        return StreamSupport
            .stream(activityNameTranslationsSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
