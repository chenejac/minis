package rs.ac.uns.ftn.informatika.minis.service;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.*;
import rs.ac.uns.ftn.informatika.minis.repository.*;
import rs.ac.uns.ftn.informatika.minis.repository.search.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.io.Serializable;

@Service
public class ElasticsearchIndexService {

    private final Logger log = LoggerFactory.getLogger(ElasticsearchIndexService.class);

    @Inject
    private ActivityRepository activityRepository;

    @Inject
    private ActivitySearchRepository activitySearchRepository;

    @Inject
    private ActivityNameTranslationsRepository activityNameTranslationsRepository;

    @Inject
    private ActivityNameTranslationsSearchRepository activityNameTranslationsSearchRepository;

    @Inject
    private CategoryRepository categoryRepository;

    @Inject
    private CategorySearchRepository categorySearchRepository;

    @Inject
    private CategoryNameTranslationsRepository categoryNameTranslationsRepository;

    @Inject
    private CategoryNameTranslationsSearchRepository categoryNameTranslationsSearchRepository;

    @Inject
    private ClassNameTranslationsRepository classNameTranslationsRepository;

    @Inject
    private ClassNameTranslationsSearchRepository classNameTranslationsSearchRepository;

    @Inject
    private ClassificationRepository classificationRepository;

    @Inject
    private ClassificationSearchRepository classificationSearchRepository;

    @Inject
    private FounderNameTranslationsRepository founderNameTranslationsRepository;

    @Inject
    private FounderNameTranslationsSearchRepository founderNameTranslationsSearchRepository;

    @Inject
    private FunctionRepository functionRepository;

    @Inject
    private FunctionSearchRepository functionSearchRepository;

    @Inject
    private FunctionNameTranslationsRepository functionNameTranslationsRepository;

    @Inject
    private FunctionNameTranslationsSearchRepository functionNameTranslationsSearchRepository;

    @Inject
    private InstitutionRepository institutionRepository;

    @Inject
    private InstitutionSearchRepository institutionSearchRepository;

    @Inject
    private InstitutionFileRepository institutionFileRepository;

    @Inject
    private InstitutionFileSearchRepository institutionFileSearchRepository;

    @Inject
    private InstitutionKeywordsTranslationsRepository institutionKeywordsTranslationsRepository;

    @Inject
    private InstitutionKeywordsTranslationsSearchRepository institutionKeywordsTranslationsSearchRepository;

    @Inject
    private InstitutionNameTranslationsRepository institutionNameTranslationsRepository;

    @Inject
    private InstitutionNameTranslationsSearchRepository institutionNameTranslationsSearchRepository;

    @Inject
    private InstitutionStatusRepository institutionStatusRepository;

    @Inject
    private InstitutionStatusSearchRepository institutionStatusSearchRepository;

    @Inject
    private InstitutionStatusNameTranslationsRepository institutionStatusNameTranslationsRepository;

    @Inject
    private InstitutionStatusNameTranslationsSearchRepository institutionStatusNameTranslationsSearchRepository;

    @Inject
    private InstitutionTypeRepository institutionTypeRepository;

    @Inject
    private InstitutionTypeSearchRepository institutionTypeSearchRepository;

    @Inject
    private InstitutionTypeNameTranslationsRepository institutionTypeNameTranslationsRepository;

    @Inject
    private InstitutionTypeNameTranslationsSearchRepository institutionTypeNameTranslationsSearchRepository;

    @Inject
    private LanguageRepository languageRepository;

    @Inject
    private LanguageSearchRepository languageSearchRepository;

    @Inject
    private PersonRepository personRepository;

    @Inject
    private PersonSearchRepository personSearchRepository;

    @Inject
    private PersonBibliographyTranslationsRepository personBibliographyTranslationsRepository;

    @Inject
    private PersonBibliographyTranslationsSearchRepository personBibliographyTranslationsSearchRepository;

    @Inject
    private PersonCategoryRepository personCategoryRepository;

    @Inject
    private PersonCategorySearchRepository personCategorySearchRepository;

    @Inject
    private PersonFileRepository personFileRepository;

    @Inject
    private PersonFileSearchRepository personFileSearchRepository;

    @Inject
    private PersonInstitutionRepository personInstitutionRepository;

    @Inject
    private PersonInstitutionSearchRepository personInstitutionSearchRepository;

    @Inject
    private PersonKeywordsTranslationsRepository personKeywordsTranslationsRepository;

    @Inject
    private PersonKeywordsTranslationsSearchRepository personKeywordsTranslationsSearchRepository;

    @Inject
    private PersonNameRepository personNameRepository;

    @Inject
    private PersonNameSearchRepository personNameSearchRepository;

    @Inject
    private PersonStatusRepository personStatusRepository;

    @Inject
    private PersonStatusSearchRepository personStatusSearchRepository;

    @Inject
    private PersonStatusNameTranslationsRepository personStatusNameTranslationsRepository;

    @Inject
    private PersonStatusNameTranslationsSearchRepository personStatusNameTranslationsSearchRepository;

    @Inject
    private PersonTypeRepository personTypeRepository;

    @Inject
    private PersonTypeSearchRepository personTypeSearchRepository;

    @Inject
    private PersonTypeNameTranslationsRepository personTypeNameTranslationsRepository;

    @Inject
    private PersonTypeNameTranslationsSearchRepository personTypeNameTranslationsSearchRepository;

    @Inject
    private PositionRepository positionRepository;

    @Inject
    private PositionSearchRepository positionSearchRepository;

    @Inject
    private PositionNameTranslationsRepository positionNameTranslationsRepository;

    @Inject
    private PositionNameTranslationsSearchRepository positionNameTranslationsSearchRepository;

    @Inject
    private PrivilegeRepository privilegeRepository;

    @Inject
    private PrivilegeSearchRepository privilegeSearchRepository;

    @Inject
    private ResearchAreaRepository researchAreaRepository;

    @Inject
    private ResearchAreaSearchRepository researchAreaSearchRepository;

    @Inject
    private ResearchAreaNameTranslationsRepository researchAreaNameTranslationsRepository;

    @Inject
    private ResearchAreaNameTranslationsSearchRepository researchAreaNameTranslationsSearchRepository;

    @Inject
    private RoleRepository roleRepository;

    @Inject
    private RoleSearchRepository roleSearchRepository;

    @Inject
    private SchemeRepository schemeRepository;

    @Inject
    private SchemeSearchRepository schemeSearchRepository;

    @Inject
    private SchemeNameTranslationsRepository schemeNameTranslationsRepository;

    @Inject
    private SchemeNameTranslationsSearchRepository schemeNameTranslationsSearchRepository;

    @Inject
    private TitleInstitutionRepository titleInstitutionRepository;

    @Inject
    private TitleInstitutionSearchRepository titleInstitutionSearchRepository;

    @Inject
    private UserAccountRepository userAccountRepository;

    @Inject
    private UserAccountSearchRepository userAccountSearchRepository;

    @Inject
    private UserGroupRepository userGroupRepository;

    @Inject
    private UserGroupSearchRepository userGroupSearchRepository;

    @Inject
    private UserRepository userRepository;

    @Inject
    private UserSearchRepository userSearchRepository;
    
    @Inject
    private StateRepository stateRepository;
    
    @Inject
    private StateSearchRepository stateSearchRepository;
    
    @Inject
    private CityRepository cityRepository;
    
    @Inject
    private CitySearchRepository citySearchRepository;
    
    @Inject
    private TownShipRepository townShipRepository;
    
    @Inject
    private TownShipSearchRepository townShipSearchRepository;

    @Inject
    private ElasticsearchTemplate elasticsearchTemplate;

    @Timed
    @Transactional
    public void reindexAll() {
        reindexForClass(Activity.class, activityRepository, activitySearchRepository);
        reindexForClass(ActivityNameTranslations.class, activityNameTranslationsRepository, activityNameTranslationsSearchRepository);
        reindexForClass(Category.class, categoryRepository, categorySearchRepository);
        reindexForClass(CategoryNameTranslations.class, categoryNameTranslationsRepository, categoryNameTranslationsSearchRepository);
        reindexForClass(ClassNameTranslations.class, classNameTranslationsRepository, classNameTranslationsSearchRepository);
        reindexForClass(Classification.class, classificationRepository, classificationSearchRepository);
        reindexForClass(FounderNameTranslations.class, founderNameTranslationsRepository, founderNameTranslationsSearchRepository);
        reindexForClass(Function.class, functionRepository, functionSearchRepository);
        reindexForClass(FunctionNameTranslations.class, functionNameTranslationsRepository, functionNameTranslationsSearchRepository);
        reindexForClass(Institution.class, institutionRepository, institutionSearchRepository);
        reindexForClass(InstitutionFile.class, institutionFileRepository, institutionFileSearchRepository);
        reindexForClass(InstitutionKeywordsTranslations.class, institutionKeywordsTranslationsRepository, institutionKeywordsTranslationsSearchRepository);
        reindexForClass(InstitutionNameTranslations.class, institutionNameTranslationsRepository, institutionNameTranslationsSearchRepository);
        reindexForClass(InstitutionStatus.class, institutionStatusRepository, institutionStatusSearchRepository);
        reindexForClass(InstitutionStatusNameTranslations.class, institutionStatusNameTranslationsRepository, institutionStatusNameTranslationsSearchRepository);
        reindexForClass(InstitutionType.class, institutionTypeRepository, institutionTypeSearchRepository);
        reindexForClass(InstitutionTypeNameTranslations.class, institutionTypeNameTranslationsRepository, institutionTypeNameTranslationsSearchRepository);
        reindexForClass(Language.class, languageRepository, languageSearchRepository);
        reindexForClass(Person.class, personRepository, personSearchRepository);
        reindexForClass(PersonBibliographyTranslations.class, personBibliographyTranslationsRepository, personBibliographyTranslationsSearchRepository);
        reindexForClass(PersonCategory.class, personCategoryRepository, personCategorySearchRepository);
        reindexForClass(PersonFile.class, personFileRepository, personFileSearchRepository);
        reindexForClass(PersonInstitution.class, personInstitutionRepository, personInstitutionSearchRepository);
        reindexForClass(PersonKeywordsTranslations.class, personKeywordsTranslationsRepository, personKeywordsTranslationsSearchRepository);
        reindexForClass(PersonName.class, personNameRepository, personNameSearchRepository);
        reindexForClass(PersonStatus.class, personStatusRepository, personStatusSearchRepository);
        reindexForClass(PersonStatusNameTranslations.class, personStatusNameTranslationsRepository, personStatusNameTranslationsSearchRepository);
        reindexForClass(PersonType.class, personTypeRepository, personTypeSearchRepository);
        reindexForClass(PersonTypeNameTranslations.class, personTypeNameTranslationsRepository, personTypeNameTranslationsSearchRepository);
        reindexForClass(Position.class, positionRepository, positionSearchRepository);
        reindexForClass(PositionNameTranslations.class, positionNameTranslationsRepository, positionNameTranslationsSearchRepository);
        reindexForClass(Privilege.class, privilegeRepository, privilegeSearchRepository);
        reindexForClass(ResearchArea.class, researchAreaRepository, researchAreaSearchRepository);
        reindexForClass(ResearchAreaNameTranslations.class, researchAreaNameTranslationsRepository, researchAreaNameTranslationsSearchRepository);
        reindexForClass(Role.class, roleRepository, roleSearchRepository);
        reindexForClass(Scheme.class, schemeRepository, schemeSearchRepository);
        reindexForClass(SchemeNameTranslations.class, schemeNameTranslationsRepository, schemeNameTranslationsSearchRepository);
        reindexForClass(TitleInstitution.class, titleInstitutionRepository, titleInstitutionSearchRepository);
        reindexForClass(UserAccount.class, userAccountRepository, userAccountSearchRepository);
        reindexForClass(UserGroup.class, userGroupRepository, userGroupSearchRepository);
        reindexForClass(User.class, userRepository, userSearchRepository);
        reindexForClass(State.class, stateRepository, stateSearchRepository);
        reindexForClass(City.class, cityRepository, citySearchRepository);
        reindexForClass(TownShip.class, townShipRepository, townShipSearchRepository);

        log.info("Elasticsearch: Successfully performed reindexing");
    }

    @Transactional
    private <T extends Serializable> void reindexForClass(Class<T> entityClass, JpaRepository<T, Long> jpaRepository,
                                                          ElasticsearchRepository<T, Long> elasticsearchRepository) {
        elasticsearchTemplate.deleteIndex(entityClass);
        elasticsearchTemplate.createIndex(entityClass);
        elasticsearchTemplate.putMapping(entityClass);
        if (jpaRepository.count() > 0) {
            elasticsearchRepository.save(jpaRepository.findAll());
        }
        log.info("Elasticsearch: Indexed all rows for " + entityClass.getSimpleName());
    }
}
