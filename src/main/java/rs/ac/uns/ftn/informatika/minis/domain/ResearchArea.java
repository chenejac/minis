package rs.ac.uns.ftn.informatika.minis.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A ResearchArea.
 */
@Entity
@Table(name = "research_area")
@Document(indexName="researcharea")
public class ResearchArea implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    
    @Column(name = "name")
    private String name;
    
    @Column(name = "description")
    private String description;

    @ManyToOne
    private Language language;

    @OneToMany(mappedBy = "researchArea")
    @JsonIgnore
    private Set<ResearchAreaNameTranslations> nameTranslations = new HashSet<>();

    @ManyToMany(mappedBy = "researchAreas")
    @JsonIgnore
    private Set<Institution> insitutions = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Set<ResearchAreaNameTranslations> getNameTranslations() {
        return nameTranslations;
    }

    public void setNameTranslations(Set<ResearchAreaNameTranslations> researchAreaNameTranslationss) {
        this.nameTranslations = researchAreaNameTranslationss;
    }

    public Set<Institution> getInsitutions() {
        return insitutions;
    }

    public void setInsitutions(Set<Institution> institutions) {
        this.insitutions = institutions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ResearchArea researchArea = (ResearchArea) o;

        if ( ! Objects.equals(id, researchArea.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ResearchArea{" +
                "id=" + id +
                ", name='" + name + "'" +
                ", description='" + description + "'" +
                '}';
    }
}
