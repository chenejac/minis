package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.CategoryNameTranslations;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the CategoryNameTranslations entity.
 */
public interface CategoryNameTranslationsRepository extends JpaRepository<CategoryNameTranslations,Long> {

}
