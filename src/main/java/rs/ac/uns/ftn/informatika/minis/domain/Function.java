package rs.ac.uns.ftn.informatika.minis.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Objects;

/**
 * A Function.
 */
@Entity
@Table(name = "function")
@Document(indexName = "function")
public class Function implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "oz")
    private String oz;

    @ManyToOne
    private Language language;

    @OneToMany(mappedBy = "function")
    @JsonIgnore
    private Set<PersonInstitution> personInstitutionss = new HashSet<>();

    @OneToMany(mappedBy = "function")
    @JsonIgnore
    private Set<FunctionNameTranslations> nameTranslations = new HashSet<>();

    public Long getId() {
        return id;
    }

    public String getOz() {
        return oz;
    }

    public void setOzs(String oz) {
        this.oz = oz;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Set<PersonInstitution> getPersonInstitutionss() {
        return personInstitutionss;
    }

    public void setPersonInstitutionss(Set<PersonInstitution> personInstitutions) {
        this.personInstitutionss = personInstitutions;
    }

    public Set<FunctionNameTranslations> getNameTranslations() {
        return nameTranslations;
    }

    public void setNameTranslations(Set<FunctionNameTranslations> functionNameTranslationss) {
        this.nameTranslations = functionNameTranslationss;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Function function = (Function) o;

        if (!Objects.equals(id, function.id)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Function{"
                + "id=" + id
                + ", name='" + name + "'"
                + ", description='" + description + "'"
                + '}';
    }
}
