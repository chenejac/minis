/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.uns.ftn.informatika.minis.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.uns.ftn.informatika.minis.domain.TownShip;
import rs.ac.uns.ftn.informatika.minis.repository.TownShipRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;

/**
 * @author Igor Milanovic REST controller for managing TownShip.
 */
@RestController
@RequestMapping("/api")
public class TownShipResource {

    private final Logger log = LoggerFactory.getLogger(TownShipResource.class);

    @Inject
    private TownShipRepository townShipRepository;

    /**
     * POST /townShips -> Create a new TownShip.
     */
    @RequestMapping(value = "/townShips",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TownShip> createTownShip(@RequestBody TownShip townShip) throws URISyntaxException {
        log.debug("REST request to save townShip : {}", townShip);
        if (townShip.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new townShip cannot already have an ID").body(null);
        }
        TownShip result = townShipRepository.save(townShip);
        return ResponseEntity.created(new URI("/api/townShips/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("townShip", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT /townShips -> Updates an existing TownShip.
     */
    @RequestMapping(value = "/townShips",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TownShip> updateTownShip(@RequestBody TownShip townShip) throws URISyntaxException {
        log.debug("REST request to update townShip : {}", townShip);
        if (townShip.getId() == null) {
            return createTownShip(townShip);
        }
        TownShip result = townShipRepository.save(townShip);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("townShip", townShip.getId().toString()))
                .body(result);
    }

    /**
     * GET /townShips -> get all the TownShips.
     */
    @RequestMapping(value = "/townShips",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<TownShip>> getAllTownShips(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
            @RequestParam(value = "pageSize", defaultValue = "20") int pageSize) {
        log.debug("REST request to get all townShips");
        if (pageSize < 1) {
            String message = "pageSize can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        if (pageNum < 1) {
            String message = "pageNum can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        PageRequest pageRequest = new PageRequest(pageNum - 1, pageSize);
        Page page = townShipRepository.findAll(pageRequest);
        return ResponseEntity.ok().body(page);
    }

    /**
     * GET /townShips/:id -> get the "id" TownShip.
     */
    @RequestMapping(value = "/townShips/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TownShip> getTownShip(@PathVariable Long id) {
        log.debug("REST request to get townShip : {}", id);
        return Optional.ofNullable(townShipRepository.findOne(id))
                .map(townShip -> new ResponseEntity<>(
                                townShip,
                                HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE /townShips/:id -> delete the "id" TownShip.
     */
    @RequestMapping(value = "/townShips/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> deleteTownShip(@PathVariable Long id) {
        log.debug("REST request to delete townShip : {}", id);
        townShipRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("townShip", id.toString())).build();
    }
}
