package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import java.io.IOException;
import rs.ac.uns.ftn.informatika.minis.domain.Person;
import rs.ac.uns.ftn.informatika.minis.repository.PersonRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PersonSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.elasticsearch.index.query.QueryStringQueryBuilder;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import rs.ac.uns.ftn.informatika.minis.config.Constants;
import rs.ac.uns.ftn.informatika.minis.domain.PersonInstitution;
import rs.ac.uns.ftn.informatika.minis.repository.search.PersonInstitutionSearchRepository;
import rs.ac.uns.ftn.informatika.minis.service.PersonService;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.LatCyrUtils;

/**
 * REST controller for managing Person.
 */
@RestController
@RequestMapping("/api")
public class PersonResource {

    private final Logger log = LoggerFactory.getLogger(PersonResource.class);

    @Inject
    private PersonRepository personRepository;

    @Inject
    private PersonService personService;
    
    @Inject
    private PersonSearchRepository personSearchRepository;
    
    @Inject
    private PersonInstitutionSearchRepository personInstitutionSearchRepository;

    /**
     * POST /persons -> Create a new person.
     */
    @RequestMapping(value = "/persons",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Person> createPerson(@RequestBody Person person) throws URISyntaxException {
        log.debug("REST request to save Person : {}", person);
        if (person.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new person cannot already have an ID").body(null);
        }
        Person result = personService.save(person);
        personSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/persons/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("person", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT /persons -> Updates an existing person.
     */
    @RequestMapping(value = "/persons",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Person> updatePerson(@RequestBody Person person) throws URISyntaxException {
        log.debug("REST request to update Person : {}", person);
        if (person.getId() == null) {
            return createPerson(person);
        }
        Person result = personService.save(person);
        personSearchRepository.save(person);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("person", person.getId().toString()))
                .body(result);
    }

    /**
     * GET /persons -> get all the persons.
     */
    @RequestMapping(value = "/persons",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Page<Person>> getAllPersons(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
            @RequestParam(value = "pageSize", defaultValue = "20") int pageSize,
            @RequestParam(value = "migrated", defaultValue = "true") boolean migrated,
            @RequestParam(value = "changed", defaultValue = "true") boolean changed,
            @RequestParam(value = "approved", defaultValue = "true") boolean approved,
            @RequestParam(value = "sortBy", defaultValue = "lastName") String sortBy,
            @RequestParam(value = "orderBy", defaultValue = "asc") String orderBy) {
        
        log.debug("REST request to get all Persons");
        if (pageSize < 1) {
            String message = "pageSize can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        if(pageNum < 1)
        {
            String message = "pageNum can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }

        Page retVal = personService.getAllPersons(pageNum, pageSize, migrated, changed, approved, sortBy, orderBy);
        if(retVal != null){
            return ResponseEntity.ok().body(retVal);
        }else{
            String message = "Logged in user doesn`t belong in any institution.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
    }

    /**
     * GET /persons/:id -> get the "id" person.
     */
    @RequestMapping(value = "/persons/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Person> getPerson(@PathVariable Long id) {
        log.debug("REST request to get Person : {}", id);
        return Optional.ofNullable(personRepository.findOneWithEagerRelationships(id))
                .map(person -> new ResponseEntity<>(
                                person,
                                HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE /persons/:id -> delete the "id" person.
     */
    @RequestMapping(value = "/persons/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePerson(@PathVariable Long id) {
        log.debug("REST request to delete Person : {}", id);
        Person p = personRepository.findOne(id);
        Long uID = p.getUser().getId();
        personRepository.delete(id);
        personSearchRepository.delete(id);
        //userRepostiry.delete(uID);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("person", id.toString())).build();
    }

    /**
     * SEARCH /_search/persons/:query -> search for the person corresponding to
     * the query.
     */
    @RequestMapping(value = "/_search/persons/{query}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Person> searchPersons(@PathVariable String query, @RequestParam(required = false) String[] fields) {
        if(fields == null) {
            fields = new String[] {"firstName", "lastName", "middleName", "jmbg"};
        }
        Pageable page = new PageRequest(0, Constants.DEFAULT_PAGE_SIZE);
        QueryStringQueryBuilder qs = queryString(transformQueryLatin(query));
        for(String field : fields) {
            qs.field(field);
        }
        return StreamSupport
                .stream(personSearchRepository.search(qs, page).spliterator(), false)
                .collect(Collectors.toList());
    }
    
    @RequestMapping(value = "/_search/personsByInstitution/{query}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Person> searchPersonsLimited(@PathVariable String query) {  
        QueryStringQueryBuilder qs = queryString(transformQueryLatin(query));
        
        Pageable page = new PageRequest(0, Constants.DEFAULT_PAGE_SIZE);
        
        // Fetch both assigned researchers and researchers with no assignments to a specific institution.
        Iterable<PersonInstitution> personInstitutions = personInstitutionSearchRepository.search(qs, page);
        Iterable<Person> persons = personSearchRepository.search(qs, page);
        
        // Load all found persons into the personsList list.
        List<Person> personsList = StreamSupport.stream(persons.spliterator(), false).collect(Collectors.toList());
        StreamSupport
                .stream(personInstitutions.spliterator(), false)
                .forEach((PersonInstitution personInstitution)-> personsList.add(personInstitution.getPerson()));
        
        // Get distinct person objects and return a list as the response.
        return StreamSupport
                .stream(personsList.spliterator(), false)
                .sorted((person1, person2) -> Long.compare(person1.getId(), person2.getId()))
                .distinct()
                .collect(Collectors.toList());
    }
    
    private String transformQueryLatin(String query) {
        // For query format +(<name>* <name>)
        // The following action will transform the string to +(<lower case latin name>* <name>) and will only do so for
        // what is in the parentesis regardless of the rest of the query string.
        String retVal = query;
        int parenQueryIdxStart = query.lastIndexOf('(');
        if (parenQueryIdxStart != -1) {
            String parenQueryString = query.substring(parenQueryIdxStart + 1, query.indexOf('*', parenQueryIdxStart));
            String latinParenQueryString = LatCyrUtils.toLatinUnaccented(parenQueryString.toLowerCase())
                    .replace("dj", "d");
            retVal = query.substring(0, parenQueryIdxStart + 1) + latinParenQueryString + "* " + parenQueryString + ")";
        }
        return retVal;
    } 

    @RequestMapping(value = "/_search/persons/byJmbg/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getPersonByJmbg(@RequestParam(value = "jmbg") String jmbg) {
        log.debug("REST request to get person with jmbg: {}", jmbg);
        Person p = personRepository.findPersonByJmbg(jmbg);
        return Optional.ofNullable(personRepository.findPersonByJmbg(jmbg))
                .map(per -> new ResponseEntity<>(
                                per,
                                HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    
    @RequestMapping(value = "/persons/xls", method = RequestMethod.GET)
    public ResponseEntity<?> getPersonsInXLS(@RequestParam(value = "migrated", defaultValue = "true") boolean migrated,
                        @RequestParam(value = "changed", defaultValue = "true") boolean changed,
                        @RequestParam(value = "approved", defaultValue = "true") boolean approved,
                        @RequestParam(value = "sortBy", defaultValue = "lastName") String sortBy,
                        @RequestParam(value = "orderBy", defaultValue = "asc") String orderBy) {
        
            log.debug("REST request to get persons in XLSX");
            HttpHeaders responseHeaders = new HttpHeaders();
            byte[] retVal;
            try {
                retVal = personService.getPersonsInXLS(migrated, changed, approved, sortBy, orderBy);
                log.debug("Document created");
                responseHeaders.setContentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
                responseHeaders.setContentDispositionFormData("attachment", "persons.xlsx");
                return new ResponseEntity<>(retVal,responseHeaders, HttpStatus.OK);
            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(PersonResource.class.getName()).log(Level.SEVERE, null, ex);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
            
    }

}
