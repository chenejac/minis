package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.InstitutionType;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the InstitutionType entity.
 */
public interface InstitutionTypeRepository extends JpaRepository<InstitutionType,Long> {
    
    public InstitutionType findInstitutionTypeByName(String name);

}
