package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.PersonStatusNameTranslations;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the PersonStatusNameTranslations entity.
 */
public interface PersonStatusNameTranslationsRepository extends JpaRepository<PersonStatusNameTranslations,Long> {

}
