/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.uns.ftn.informatika.minis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.informatika.minis.domain.State;

/**
 *
 * @author Igor Milanovic
 */
public interface StateRepository extends JpaRepository<State, Long> {
    
    public State findStateByName(String name);

}
