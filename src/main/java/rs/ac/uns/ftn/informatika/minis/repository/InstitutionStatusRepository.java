package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.InstitutionStatus;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the InstitutionStatus entity.
 */
public interface InstitutionStatusRepository extends JpaRepository<InstitutionStatus,Long> {

}
