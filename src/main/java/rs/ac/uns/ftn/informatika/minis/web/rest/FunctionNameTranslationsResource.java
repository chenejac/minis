package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.FunctionNameTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.FunctionNameTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.FunctionNameTranslationsSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing FunctionNameTranslations.
 */
@RestController
@RequestMapping("/api")
public class FunctionNameTranslationsResource {

    private final Logger log = LoggerFactory.getLogger(FunctionNameTranslationsResource.class);

    @Inject
    private FunctionNameTranslationsRepository functionNameTranslationsRepository;

    @Inject
    private FunctionNameTranslationsSearchRepository functionNameTranslationsSearchRepository;

    /**
     * POST  /functionNameTranslationss -> Create a new functionNameTranslations.
     */
    @RequestMapping(value = "/functionNameTranslationss",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FunctionNameTranslations> createFunctionNameTranslations(@RequestBody FunctionNameTranslations functionNameTranslations) throws URISyntaxException {
        log.debug("REST request to save FunctionNameTranslations : {}", functionNameTranslations);
        if (functionNameTranslations.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new functionNameTranslations cannot already have an ID").body(null);
        }
        FunctionNameTranslations result = functionNameTranslationsRepository.save(functionNameTranslations);
        functionNameTranslationsSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/functionNameTranslationss/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("functionNameTranslations", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /functionNameTranslationss -> Updates an existing functionNameTranslations.
     */
    @RequestMapping(value = "/functionNameTranslationss",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FunctionNameTranslations> updateFunctionNameTranslations(@RequestBody FunctionNameTranslations functionNameTranslations) throws URISyntaxException {
        log.debug("REST request to update FunctionNameTranslations : {}", functionNameTranslations);
        if (functionNameTranslations.getId() == null) {
            return createFunctionNameTranslations(functionNameTranslations);
        }
        FunctionNameTranslations result = functionNameTranslationsRepository.save(functionNameTranslations);
        functionNameTranslationsSearchRepository.save(functionNameTranslations);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("functionNameTranslations", functionNameTranslations.getId().toString()))
                .body(result);
    }

    /**
     * GET  /functionNameTranslationss -> get all the functionNameTranslationss.
     */
    @RequestMapping(value = "/functionNameTranslationss",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<FunctionNameTranslations> getAllFunctionNameTranslationss() {
        log.debug("REST request to get all FunctionNameTranslationss");
        return functionNameTranslationsRepository.findAll();
    }

    /**
     * GET  /functionNameTranslationss/:id -> get the "id" functionNameTranslations.
     */
    @RequestMapping(value = "/functionNameTranslationss/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FunctionNameTranslations> getFunctionNameTranslations(@PathVariable Long id) {
        log.debug("REST request to get FunctionNameTranslations : {}", id);
        return Optional.ofNullable(functionNameTranslationsRepository.findOne(id))
            .map(functionNameTranslations -> new ResponseEntity<>(
                functionNameTranslations,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /functionNameTranslationss/:id -> delete the "id" functionNameTranslations.
     */
    @RequestMapping(value = "/functionNameTranslationss/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteFunctionNameTranslations(@PathVariable Long id) {
        log.debug("REST request to delete FunctionNameTranslations : {}", id);
        functionNameTranslationsRepository.delete(id);
        functionNameTranslationsSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("functionNameTranslations", id.toString())).build();
    }

    /**
     * SEARCH  /_search/functionNameTranslationss/:query -> search for the functionNameTranslations corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/functionNameTranslationss/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<FunctionNameTranslations> searchFunctionNameTranslationss(@PathVariable String query) {
        return StreamSupport
            .stream(functionNameTranslationsSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
