package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.ResearchArea;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ResearchArea entity.
 */
public interface ResearchAreaRepository extends JpaRepository<ResearchArea,Long> {

}
