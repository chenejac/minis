package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.SchemeNameTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.SchemeNameTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.SchemeNameTranslationsSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SchemeNameTranslations.
 */
@RestController
@RequestMapping("/api")
public class SchemeNameTranslationsResource {

    private final Logger log = LoggerFactory.getLogger(SchemeNameTranslationsResource.class);

    @Inject
    private SchemeNameTranslationsRepository schemeNameTranslationsRepository;

    @Inject
    private SchemeNameTranslationsSearchRepository schemeNameTranslationsSearchRepository;

    /**
     * POST  /schemeNameTranslationss -> Create a new schemeNameTranslations.
     */
    @RequestMapping(value = "/schemeNameTranslationss",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SchemeNameTranslations> createSchemeNameTranslations(@RequestBody SchemeNameTranslations schemeNameTranslations) throws URISyntaxException {
        log.debug("REST request to save SchemeNameTranslations : {}", schemeNameTranslations);
        if (schemeNameTranslations.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new schemeNameTranslations cannot already have an ID").body(null);
        }
        SchemeNameTranslations result = schemeNameTranslationsRepository.save(schemeNameTranslations);
        schemeNameTranslationsSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/schemeNameTranslationss/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("schemeNameTranslations", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /schemeNameTranslationss -> Updates an existing schemeNameTranslations.
     */
    @RequestMapping(value = "/schemeNameTranslationss",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SchemeNameTranslations> updateSchemeNameTranslations(@RequestBody SchemeNameTranslations schemeNameTranslations) throws URISyntaxException {
        log.debug("REST request to update SchemeNameTranslations : {}", schemeNameTranslations);
        if (schemeNameTranslations.getId() == null) {
            return createSchemeNameTranslations(schemeNameTranslations);
        }
        SchemeNameTranslations result = schemeNameTranslationsRepository.save(schemeNameTranslations);
        schemeNameTranslationsSearchRepository.save(schemeNameTranslations);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("schemeNameTranslations", schemeNameTranslations.getId().toString()))
                .body(result);
    }

    /**
     * GET  /schemeNameTranslationss -> get all the schemeNameTranslationss.
     */
    @RequestMapping(value = "/schemeNameTranslationss",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<SchemeNameTranslations> getAllSchemeNameTranslationss() {
        log.debug("REST request to get all SchemeNameTranslationss");
        return schemeNameTranslationsRepository.findAll();
    }

    /**
     * GET  /schemeNameTranslationss/:id -> get the "id" schemeNameTranslations.
     */
    @RequestMapping(value = "/schemeNameTranslationss/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SchemeNameTranslations> getSchemeNameTranslations(@PathVariable Long id) {
        log.debug("REST request to get SchemeNameTranslations : {}", id);
        return Optional.ofNullable(schemeNameTranslationsRepository.findOne(id))
            .map(schemeNameTranslations -> new ResponseEntity<>(
                schemeNameTranslations,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /schemeNameTranslationss/:id -> delete the "id" schemeNameTranslations.
     */
    @RequestMapping(value = "/schemeNameTranslationss/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteSchemeNameTranslations(@PathVariable Long id) {
        log.debug("REST request to delete SchemeNameTranslations : {}", id);
        schemeNameTranslationsRepository.delete(id);
        schemeNameTranslationsSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("schemeNameTranslations", id.toString())).build();
    }

    /**
     * SEARCH  /_search/schemeNameTranslationss/:query -> search for the schemeNameTranslations corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/schemeNameTranslationss/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<SchemeNameTranslations> searchSchemeNameTranslationss(@PathVariable String query) {
        return StreamSupport
            .stream(schemeNameTranslationsSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
