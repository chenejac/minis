package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.PersonTypeNameTranslations;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the PersonTypeNameTranslations entity.
 */
public interface PersonTypeNameTranslationsRepository extends JpaRepository<PersonTypeNameTranslations,Long> {

}
