package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.Classification;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Classification entity.
 */
public interface ClassificationSearchRepository extends ElasticsearchRepository<Classification, Long> {
}
