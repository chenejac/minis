package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.InstitutionNameTranslations;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the InstitutionNameTranslations entity.
 */
public interface InstitutionNameTranslationsSearchRepository extends ElasticsearchRepository<InstitutionNameTranslations, Long> {
}
