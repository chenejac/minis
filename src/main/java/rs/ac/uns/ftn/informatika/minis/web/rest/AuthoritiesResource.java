/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.uns.ftn.informatika.minis.domain.Authority;
import rs.ac.uns.ftn.informatika.minis.domain.User;
import rs.ac.uns.ftn.informatika.minis.security.AuthoritiesConstants;
import rs.ac.uns.ftn.informatika.minis.service.UserService;

/**
 *
 * @author stefan
 */
@RestController
@RequestMapping("/api")
public class AuthoritiesResource {
    private final Logger log = LoggerFactory.getLogger(AuthoritiesResource.class);
    
    @Inject
    private UserService userService;

    /**
     * GET  /authorities -> Fetch possible authorities that the logged user is allowed to set.
     */
    @RequestMapping(value = "/authorities",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @Secured(AuthoritiesConstants.ADMIN) // For now, possible extenstion for an Institution Administrator later
    public ResponseEntity<Map<String,List<String>>> getAuthorities() throws URISyntaxException {
        Map<String, List<String>> retVal = new HashMap();
        List<String> authoritiesList;
        User currentUser = userService.getUserWithAuthorities();
        if(currentUser.getAuthorities().contains(new Authority(AuthoritiesConstants.ADMIN))) {
            log.debug("Fetching ADMIN authorized creation authorities for: User {}:" + currentUser);
            authoritiesList = Arrays.asList(AuthoritiesConstants.CREATE_AUTHORITIES_ADMIN);
        } else if(currentUser.getAuthorities().contains(new Authority(AuthoritiesConstants.INSTITUTION_ADMIN))){
            log.debug("Fetching INSTITUTION_ADMIN authorized creation authorities for: User {}:" + currentUser);
            authoritiesList = Arrays.asList(AuthoritiesConstants.CREATE_AUTHORITIES_INSTITUTION_ADMIN);
        } else {
            log.debug("Fetching USER authorized creation authorities for: User {}:" + currentUser);
            authoritiesList = new ArrayList();
        }
        retVal.put("authorities", authoritiesList);
        return ResponseEntity.ok().body(retVal);
    }
}
