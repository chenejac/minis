package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.PersonName;
import rs.ac.uns.ftn.informatika.minis.repository.PersonNameRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PersonNameSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PersonName.
 */
@RestController
@RequestMapping("/api")
public class PersonNameResource {

    private final Logger log = LoggerFactory.getLogger(PersonNameResource.class);

    @Inject
    private PersonNameRepository personNameRepository;

    @Inject
    private PersonNameSearchRepository personNameSearchRepository;

    /**
     * POST  /personNames -> Create a new personName.
     */
    @RequestMapping(value = "/personNames",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonName> createPersonName(@RequestBody PersonName personName) throws URISyntaxException {
        log.debug("REST request to save PersonName : {}", personName);
        if (personName.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new personName cannot already have an ID").body(null);
        }
        PersonName result = personNameRepository.save(personName);
        personNameSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/personNames/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("personName", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /personNames -> Updates an existing personName.
     */
    @RequestMapping(value = "/personNames",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonName> updatePersonName(@RequestBody PersonName personName) throws URISyntaxException {
        log.debug("REST request to update PersonName : {}", personName);
        if (personName.getId() == null) {
            return createPersonName(personName);
        }
        PersonName result = personNameRepository.save(personName);
        personNameSearchRepository.save(personName);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("personName", personName.getId().toString()))
                .body(result);
    }

    /**
     * GET  /personNames -> get all the personNames.
     */
    @RequestMapping(value = "/personNames",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PersonName> getAllPersonNames() {
        log.debug("REST request to get all PersonNames");
        return personNameRepository.findAll();
    }

    /**
     * GET  /personNames/:id -> get the "id" personName.
     */
    @RequestMapping(value = "/personNames/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonName> getPersonName(@PathVariable Long id) {
        log.debug("REST request to get PersonName : {}", id);
        return Optional.ofNullable(personNameRepository.findOne(id))
            .map(personName -> new ResponseEntity<>(
                personName,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /personNames/:id -> delete the "id" personName.
     */
    @RequestMapping(value = "/personNames/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePersonName(@PathVariable Long id) {
        log.debug("REST request to delete PersonName : {}", id);
        personNameRepository.delete(id);
        personNameSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("personName", id.toString())).build();
    }

    /**
     * SEARCH  /_search/personNames/:query -> search for the personName corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/personNames/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PersonName> searchPersonNames(@PathVariable String query) {
        return StreamSupport
            .stream(personNameSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
