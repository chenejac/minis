package rs.ac.uns.ftn.informatika.minis.domain;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * 
 */
@Entity
@Table(name = "person_bibliography_translations")
@Document(indexName="personbibliographytranslations")
public class PersonBibliographyTranslations implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    
    @Column(name = "bibliography")
    private String bibliography;
    
    @Column(name = "trans_type")
    private String transType;

    @ManyToOne
    private Language language;

    @ManyToOne
    private Person person;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBibliography() {
        return bibliography;
    }

    public void setBibliography(String bibliography) {
        this.bibliography = bibliography;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PersonBibliographyTranslations personBibliographyTranslations = (PersonBibliographyTranslations) o;

        if ( ! Objects.equals(id, personBibliographyTranslations.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PersonBibliographyTranslations{" +
                "id=" + id +
                ", bibliography='" + bibliography + "'" +
                ", transType='" + transType + "'" +
                '}';
    }
}
