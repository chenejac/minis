package rs.ac.uns.ftn.informatika.minis.config;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import rs.ac.uns.ftn.informatika.minis.domain.util.CustomDateTimeDeserializer;
import rs.ac.uns.ftn.informatika.minis.domain.util.CustomDateTimeSerializer;
import rs.ac.uns.ftn.informatika.minis.domain.util.CustomLocalDateSerializer;
import rs.ac.uns.ftn.informatika.minis.domain.util.ISO8601LocalDateDeserializer;
import com.fasterxml.jackson.datatype.joda.JodaModule;

@Configuration
public class JacksonConfiguration {

    @Bean
    public JodaModule jacksonJodaModule() {
        JodaModule module = new JodaModule();
        module.addSerializer(DateTime.class, new CustomDateTimeSerializer());
        module.addDeserializer(DateTime.class, new CustomDateTimeDeserializer());
        module.addSerializer(LocalDate.class, new CustomLocalDateSerializer());
        module.addDeserializer(LocalDate.class, new ISO8601LocalDateDeserializer());
        return module;
    }
}
