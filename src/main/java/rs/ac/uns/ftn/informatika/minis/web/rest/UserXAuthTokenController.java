package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.security.xauth.TokenProvider;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.logging.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import rs.ac.uns.ftn.informatika.minis.security.xauth.Token;
import rs.ac.uns.ftn.informatika.minis.web.rest.errors.ErrorDTO;

@RestController
@RequestMapping("/api")
public class UserXAuthTokenController {

    private static final Logger LOG = Logger.getLogger(UserXAuthTokenController.class.getName());

    @Inject
    private TokenProvider tokenProvider;

    @Inject
    private AuthenticationManager authenticationManager;

    @Inject
    private UserDetailsService userDetailsService;

    @RequestMapping(value = "/authenticate",
            method = RequestMethod.POST)
    @Timed
    public ResponseEntity<?> authorize(@RequestParam String username, @RequestParam String password) {
        try {
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            Authentication authentication = this.authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            UserDetails details = this.userDetailsService.loadUserByUsername(username);
            return new ResponseEntity<Token>(tokenProvider.createToken(details), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<ErrorDTO>(new ErrorDTO(ex.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }
}
