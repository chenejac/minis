package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.UserAccount;
import rs.ac.uns.ftn.informatika.minis.repository.UserAccountRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.UserAccountSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing UserAccount.
 */
@RestController
@RequestMapping("/api")
public class UserAccountResource {

    private final Logger log = LoggerFactory.getLogger(UserAccountResource.class);

    @Inject
    private UserAccountRepository userAccountRepository;

    @Inject
    private UserAccountSearchRepository userAccountSearchRepository;

    /**
     * POST  /userAccounts -> Create a new userAccount.
     */
    @RequestMapping(value = "/userAccounts",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserAccount> createUserAccount(@RequestBody UserAccount userAccount) throws URISyntaxException {
        log.debug("REST request to save UserAccount : {}", userAccount);
        if (userAccount.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new userAccount cannot already have an ID").body(null);
        }
        UserAccount result = userAccountRepository.save(userAccount);
        userAccountSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/userAccounts/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("userAccount", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /userAccounts -> Updates an existing userAccount.
     */
    @RequestMapping(value = "/userAccounts",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserAccount> updateUserAccount(@RequestBody UserAccount userAccount) throws URISyntaxException {
        log.debug("REST request to update UserAccount : {}", userAccount);
        if (userAccount.getId() == null) {
            return createUserAccount(userAccount);
        }
        UserAccount result = userAccountRepository.save(userAccount);
        userAccountSearchRepository.save(userAccount);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("userAccount", userAccount.getId().toString()))
                .body(result);
    }

    /**
     * GET  /userAccounts -> get all the userAccounts.
     */
    @RequestMapping(value = "/userAccounts",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<UserAccount> getAllUserAccounts() {
        log.debug("REST request to get all UserAccounts");
        return userAccountRepository.findAll();
    }

    /**
     * GET  /userAccounts/:id -> get the "id" userAccount.
     */
    @RequestMapping(value = "/userAccounts/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserAccount> getUserAccount(@PathVariable Long id) {
        log.debug("REST request to get UserAccount : {}", id);
        return Optional.ofNullable(userAccountRepository.findOne(id))
            .map(userAccount -> new ResponseEntity<>(
                userAccount,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /userAccounts/:id -> delete the "id" userAccount.
     */
    @RequestMapping(value = "/userAccounts/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteUserAccount(@PathVariable Long id) {
        log.debug("REST request to delete UserAccount : {}", id);
        userAccountRepository.delete(id);
        userAccountSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("userAccount", id.toString())).build();
    }

    /**
     * SEARCH  /_search/userAccounts/:query -> search for the userAccount corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/userAccounts/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<UserAccount> searchUserAccounts(@PathVariable String query) {
        return StreamSupport
            .stream(userAccountSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
