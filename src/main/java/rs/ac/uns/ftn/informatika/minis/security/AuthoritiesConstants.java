package rs.ac.uns.ftn.informatika.minis.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";
    
    public static final String INSTITUTION_ADMIN = "ROLE_INSTITUTION_ADMIN";
    
    public static final String RESEARCHER = "ROLE_RESEARCHER";

    public static final String USER = "ROLE_USER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";
    
    public static final String[] CREATE_AUTHORITIES_ADMIN = new String[]{
        ADMIN, INSTITUTION_ADMIN, RESEARCHER, USER
    };
    
    public static final String[] CREATE_AUTHORITIES_INSTITUTION_ADMIN = new String[]{
        RESEARCHER, USER
    };

    private AuthoritiesConstants() {
    }
}
