package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.Scheme;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Scheme entity.
 */
public interface SchemeRepository extends JpaRepository<Scheme,Long> {

}
