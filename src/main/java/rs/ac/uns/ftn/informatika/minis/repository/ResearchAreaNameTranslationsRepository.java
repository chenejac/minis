package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.ResearchAreaNameTranslations;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ResearchAreaNameTranslations entity.
 */
public interface ResearchAreaNameTranslationsRepository extends JpaRepository<ResearchAreaNameTranslations,Long> {

}
