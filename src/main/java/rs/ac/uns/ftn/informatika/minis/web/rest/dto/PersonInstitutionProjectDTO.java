/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.uns.ftn.informatika.minis.web.rest.dto;

import java.util.List;

/**
 *
 * @author stefan
 */
public class PersonInstitutionProjectDTO {
    private List<Long> personIds;
    private List<Long> institutionIds;

    public List<Long> getPersonIds() {
        return personIds;
    }

    public void setPersonIds(List<Long> personIds) {
        this.personIds = personIds;
    }

    public List<Long> getInstitutionIds() {
        return institutionIds;
    }

    public void setInstitutionIds(List<Long> institutionIds) {
        this.institutionIds = institutionIds;
    }
    
    
}
