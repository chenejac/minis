package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.ClassNameTranslations;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ClassNameTranslations entity.
 */
public interface ClassNameTranslationsRepository extends JpaRepository<ClassNameTranslations,Long> {

}
