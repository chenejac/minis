package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.FunctionNameTranslations;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the FunctionNameTranslations entity.
 */
public interface FunctionNameTranslationsRepository extends JpaRepository<FunctionNameTranslations,Long> {

}
