package rs.ac.uns.ftn.informatika.minis.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import rs.ac.uns.ftn.informatika.minis.domain.util.CustomLocalDateSerializer;
import rs.ac.uns.ftn.informatika.minis.domain.util.ISO8601LocalDateDeserializer;
import rs.ac.uns.ftn.informatika.minis.domain.util.CustomDateTimeDeserializer;
import rs.ac.uns.ftn.informatika.minis.domain.util.CustomDateTimeSerializer;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.joda.time.DateTime;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.NestedField;

/**
 * 
 */
@Entity
@Table(name = "person")
@Document(indexName="person")
public class Person implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    
    @Column(name = "first_name")
    @Field(type = FieldType.String)
    private String firstName;
    
    @Column(name = "last_name")
    @Field(type = FieldType.String)
    private String lastName;
    
    @Column(name = "middle_name")
    @Field(type = FieldType.String)
    private String middleName;
    
    @Column(name = "title")
    private String title;
    
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    @JsonSerialize(using = CustomLocalDateSerializer.class)
    @JsonDeserialize(using = ISO8601LocalDateDeserializer.class)
    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;
    
    @Column(name = "place_of_birth")
    private String placeOfBirth;
    
    @Column(name = "township_of_birth")
    private String townShipOfBirth;
    
    @Column(name = "state")
    private String state;
    
    @Column(name = "state_of_residence")
    private String stateOfResidence;
    
    @Column(name = "township_of_residence")
    private String townShipOfResidence;
    
    @Column(name = "address")
    private String address;
    
    @Column(name = "city")
    private String city;
    
    @Column(name = "bibliography")
    private String bibliography;
    
    @Column(name = "keywords")
    private String keywords;
    
    @Column(name = "gender")
    private String gender;
    
    @Column(name = "uri")
    private String uri;
    
    @Column(name = "email")
    private String email;
    
    @Column(name = "orcid")
    private String orcid;
    
    @Column(name = "jmbg", unique = true)
    private String jmbg;
    
    @Column(name = "phones")
    private String phones;
    
    @Column(name = "research_areas")
    private String researchAreas;
    
    @Column(name = "mntrn")
    private String mntrn;
    
    @Column(name = "note")
    private String note;
    
    @Column(name = "creator")
    private String creator;
    
    @Column(name = "modifier")
    private String modifier;
    
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @Column(name = "creation_date")
    private DateTime creationDate;
    
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @Column(name = "last_modification_date")
    private DateTime lastModificationDate;
    
    @Column(name = "record_status")
    private Integer recordStatus;
    
    @Column(name = "already_registered")
    private Boolean alreadyRegistered;

    @ManyToOne
    private Language language;

    @ManyToOne
    private PersonStatus personStatus;

    @OneToMany(mappedBy = "person")
    @JsonIgnore
    private Set<PersonName> otherNames = new HashSet<>();

    @OneToMany(mappedBy = "person")
    @JsonIgnore
    private Set<TitleInstitution> titleInstitutions = new HashSet<>();

    @OneToMany(mappedBy = "person")
    @JsonIgnore
    private Set<UserAccount> users = new HashSet<>();

    @OneToMany(mappedBy = "person")
    @JsonIgnore
    private Set<PersonCategory> categories = new HashSet<>();

    @OneToMany(mappedBy = "person")
    @JsonIgnore()
    private Set<PersonInstitution> institutions = new HashSet<>();

    @OneToMany(mappedBy = "person")
    @JsonIgnore
    private Set<PersonFile> files = new HashSet<>();

    @OneToMany(mappedBy = "person")
    @JsonIgnore
    private Set<PersonBibliographyTranslations> bibliographyTranslations = new HashSet<>();

    @OneToMany(mappedBy = "person")
    @JsonIgnore
    private Set<PersonKeywordsTranslations> keywordsTranslations = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "person_classification",
               joinColumns = @JoinColumn(name="persons_id", referencedColumnName="ID"),
               inverseJoinColumns = @JoinColumn(name="classifications_id", referencedColumnName="ID"))
    private Set<Classification> classifications = new HashSet<>();

    @OneToMany(mappedBy = "person")
    @JsonIgnore
    private Set<PersonPosition> personPosition = new HashSet<>();
    
    @ManyToOne
    private PersonType type;
    
    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    @PreUpdate
    public void preUpdate() {
        lastModificationDate = new DateTime();
    }

    @PrePersist
    public void prePersist() {
        DateTime now = new DateTime();
        creationDate = now;
        lastModificationDate = now;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    
    public String getTitle() {
        return title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    public String getStateOfResidence() {
        return stateOfResidence;
    }

    public String getTownShipOfBirth() {
        return townShipOfBirth;
    }

    public String getTownShipOfResidence() {
        return townShipOfResidence;
    }

    public void setStateOfResidence(String stateOfResidence) {
        this.stateOfResidence = stateOfResidence;
    }

    public void setTownShipOfBirth(String townShipOfBirth) {
        this.townShipOfBirth = townShipOfBirth;
    }

    public void setTownShipOfResidence(String townShipOfResidence) {
        this.townShipOfResidence = townShipOfResidence;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBibliography() {
        return bibliography;
    }

    public void setBibliography(String bibliography) {
        this.bibliography = bibliography;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOrcid() {
        return orcid;
    }

    public void setOrcid(String orcid) {
        this.orcid = orcid;
    }

    public String getJmbg() {
        return jmbg;
    }

    public void setJmbg(String jmbg) {
        this.jmbg = jmbg;
    }

    public String getPhones() {
        return phones;
    }

    public void setPhones(String phones) {
        this.phones = phones;
    }

    public String getResearchAreas() {
        return researchAreas;
    }

    public void setResearchAreas(String researchAreas) {
        this.researchAreas = researchAreas;
    }

    public String getMntrn() {
        return mntrn;
    }

    public void setMntrn(String mntrn) {
        this.mntrn = mntrn;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public DateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(DateTime creationDate) {
        this.creationDate = creationDate;
    }

    public DateTime getLastModificationDate() {
        return lastModificationDate;
    }

    public void setLastModificationDate(DateTime lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    public Integer getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(Integer recordStatus) {
        this.recordStatus = recordStatus;
    }

    public Boolean getAlreadyRegistered() {
        return alreadyRegistered;
    }

    public void setAlreadyRegistered(Boolean alreadyRegistered) {
        this.alreadyRegistered = alreadyRegistered;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public PersonStatus getPersonStatus() {
        return personStatus;
    }

    public void setPersonStatus(PersonStatus personStatus) {
        this.personStatus = personStatus;
    }

    public Set<PersonName> getOtherNames() {
        return otherNames;
    }

    public void setOtherNames(Set<PersonName> personNames) {
        this.otherNames = personNames;
    }

    public Set<TitleInstitution> getTitleInstitutions() {
        return titleInstitutions;
    }

    public void setTitleInstitutions(Set<TitleInstitution> titleInstitutions) {
        this.titleInstitutions = titleInstitutions;
    }

    public Set<UserAccount> getUsers() {
        return users;
    }

    public void setUsers(Set<UserAccount> userAccounts) {
        this.users = userAccounts;
    }

    public Set<PersonCategory> getCategories() {
        return categories;
    }

    public void setCategories(Set<PersonCategory> personCategorys) {
        this.categories = personCategorys;
    }

    public Set<PersonInstitution> getInstitutions() {
        return institutions;
    }

    public void setInstitutions(Set<PersonInstitution> personInstitutions) {
        this.institutions = personInstitutions;
    }

    public Set<PersonFile> getFiles() {
        return files;
    }

    public void setFiles(Set<PersonFile> personFiles) {
        this.files = personFiles;
    }

    public Set<PersonBibliographyTranslations> getBibliographyTranslations() {
        return bibliographyTranslations;
    }

    public void setBibliographyTranslations(Set<PersonBibliographyTranslations> personBibliographyTranslationss) {
        this.bibliographyTranslations = personBibliographyTranslationss;
    }

    public Set<PersonKeywordsTranslations> getKeywordsTranslations() {
        return keywordsTranslations;
    }

    public void setKeywordsTranslations(Set<PersonKeywordsTranslations> personKeywordsTranslationss) {
        this.keywordsTranslations = personKeywordsTranslationss;
    }

    public Set<Classification> getClassifications() {
        return classifications;
    }

    public void setClassifications(Set<Classification> classifications) {
        this.classifications = classifications;
    }

    public PersonType getType() {
        return type;
    }

    public void setType(PersonType personType) {
        this.type = personType;
    }

    public Set<PersonPosition> getPersonPosition() {
        return personPosition;
    }

    public void setPersonPosition(Set<PersonPosition> personPosition) {
        this.personPosition = personPosition;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Person person = (Person) o;

        if ( ! Objects.equals(id, person.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", firstName='" + firstName + "'" +
                ", lastName='" + lastName + "'" +
                ", middleName='" + middleName + "'" +
                ", title='" + title + "'" +
                ", dateOfBirth='" + dateOfBirth + "'" +
                ", placeOfBirth='" + placeOfBirth + "'" +
                ", state='" + state + "'" +
                ", address='" + address + "'" +
                ", city='" + city + "'" +
                ", bibliography='" + bibliography + "'" +
                ", keywords='" + keywords + "'" +
                ", gender='" + gender + "'" +
                ", uri='" + uri + "'" +
                ", email='" + email + "'" +
                ", orcid='" + orcid + "'" +
                ", jmbg='" + jmbg + "'" +
                ", phones='" + phones + "'" +
                ", researchAreas='" + researchAreas + "'" +
                ", mntrn='" + mntrn + "'" +
                ", note='" + note + "'" +
                ", creator='" + creator + "'" +
                ", modifier='" + modifier + "'" +
                ", creationDate='" + creationDate + "'" +
                ", lastModificationDate='" + lastModificationDate + "'" +
                ", recordStatus='" + recordStatus + "'" +
                ", alreadyRegistered='" + alreadyRegistered + "'" +
                '}';
    }
}
