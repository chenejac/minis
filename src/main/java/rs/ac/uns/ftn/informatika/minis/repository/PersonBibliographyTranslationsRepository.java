package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.PersonBibliographyTranslations;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the PersonBibliographyTranslations entity.
 */
public interface PersonBibliographyTranslationsRepository extends JpaRepository<PersonBibliographyTranslations,Long> {

}
