/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.uns.ftn.informatika.minis.config;

import org.hibernate.dialect.MySQL5InnoDBDialect;

/**
 *
 * @author Igor Milanovic
 */
public class CyrillicMysqlDialect extends MySQL5InnoDBDialect {

    @Override
    public String getTableTypeString() {
        return " ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
    }

}
