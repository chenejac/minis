package rs.ac.uns.ftn.informatika.minis.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Classification.
 */
@Entity
@Table(name = "classification")
@Document(indexName="classification")
public class Classification implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    
    @Column(name = "class_id")
    private String classId;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "description")
    private String description;

    @ManyToOne
    private Language language;

    @OneToMany(mappedBy = "classification")
    @JsonIgnore
    private Set<ClassNameTranslations> nameTranslations = new HashSet<>();

    @ManyToMany(mappedBy = "classifications")
    @JsonIgnore
    private Set<Institution> institutions = new HashSet<>();

    @ManyToOne
    private Scheme scheme;

    @ManyToMany(mappedBy = "classifications")
    @JsonIgnore
    private Set<Person> persons = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Set<ClassNameTranslations> getNameTranslations() {
        return nameTranslations;
    }

    public void setNameTranslations(Set<ClassNameTranslations> classNameTranslationss) {
        this.nameTranslations = classNameTranslationss;
    }

    public Set<Institution> getInstitutions() {
        return institutions;
    }

    public void setInstitutions(Set<Institution> institutions) {
        this.institutions = institutions;
    }

    public Scheme getScheme() {
        return scheme;
    }

    public void setScheme(Scheme scheme) {
        this.scheme = scheme;
    }

    public Set<Person> getPersons() {
        return persons;
    }

    public void setPersons(Set<Person> persons) {
        this.persons = persons;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Classification classification = (Classification) o;

        if ( ! Objects.equals(id, classification.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Classification{" +
                "id=" + id +
                ", classId='" + classId + "'" +
                ", name='" + name + "'" +
                ", description='" + description + "'" +
                '}';
    }
}
