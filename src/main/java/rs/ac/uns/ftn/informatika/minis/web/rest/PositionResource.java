package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.Position;
import rs.ac.uns.ftn.informatika.minis.repository.PositionRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PositionSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import rs.ac.uns.ftn.informatika.minis.domain.Function;

/**
 * REST controller for managing Position.
 */
@RestController
@RequestMapping("/api")
public class PositionResource {

    private final Logger log = LoggerFactory.getLogger(PositionResource.class);

    @Inject
    private PositionRepository positionRepository;

    @Inject
    private PositionSearchRepository positionSearchRepository;

    /**
     * POST /positions -> Create a new position.
     */
    @RequestMapping(value = "/positions",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Position> createPosition(@RequestBody Position position) throws URISyntaxException {
        log.debug("REST request to save Position : {}", position);
        if (position.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new position cannot already have an ID").body(null);
        }
        Position result = positionRepository.save(position);
        positionSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/positions/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("position", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT /positions -> Updates an existing position.
     */
    @RequestMapping(value = "/positions",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Position> updatePosition(@RequestBody Position position) throws URISyntaxException {
        log.debug("REST request to update Position : {}", position);
        if (position.getId() == null) {
            return createPosition(position);
        }
        Position result = positionRepository.save(position);
        positionSearchRepository.save(position);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("position", position.getId().toString()))
                .body(result);
    }

    /**
     * GET /positions -> get all the positions.
     */
    @RequestMapping(value = "/positions",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Page<Position>> getAllPositions(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
            @RequestParam(value = "pageSize", defaultValue = "20") int pageSize) {
        log.debug("REST request to get all Positions");
        if (pageSize < 1) {
            String message = "pageSize can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        if (pageNum < 1) {
            String message = "pageNum can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        PageRequest pageRequest = new PageRequest(pageNum - 1, pageSize);
        Page page = positionRepository.findAll(pageRequest);
        return ResponseEntity.ok().body(page);
    }

    /**
     * GET /positions/:id -> get the "id" position.
     */
    @RequestMapping(value = "/positions/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Position> getPosition(@PathVariable Long id) {
        log.debug("REST request to get Position : {}", id);
        return Optional.ofNullable(positionRepository.findOne(id))
                .map(position -> new ResponseEntity<>(
                                position,
                                HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE /positions/:id -> delete the "id" position.
     */
    @RequestMapping(value = "/positions/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePosition(@PathVariable Long id) {
        log.debug("REST request to delete Position : {}", id);
        positionRepository.delete(id);
        positionSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("position", id.toString())).build();
    }

    /**
     * SEARCH /_search/positions/:query -> search for the position corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/positions/{query}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Position> searchPositions(@PathVariable String query) {
        return StreamSupport
                .stream(positionSearchRepository.search(queryString(query)).spliterator(), false)
                .collect(Collectors.toList());
    }

    /**
     * GET /_search/positions/byName?name={name} -> get position by name.
     */
    @RequestMapping(value = "/_search/positions/byName/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getPositionByName(@RequestParam(value = "name") String name) {
        log.info("REST request to get position with name : {}", name);
        return Optional.ofNullable(positionRepository.findPositionByName(name))
                .map(pos -> new ResponseEntity<>(
                                pos,
                                HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    // TODO : Popraviti ovu brlju
    @RequestMapping(value = "/_search/positions/byOz/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getPositionByOz(@RequestParam(value = "oz") String oz) {
        log.info("REST request to get position with oz : {}", oz);
        List<String> input = new ArrayList<String>();
        if (oz.contains(",")) {
            input = Arrays.asList(oz.split(","));
        } else {
            input.add(oz);
        }
        for (Position p : positionRepository.findAll()) {
            for (String i : input) {
                i = i.trim();
                log.debug("" + p.getOz());
                if (p.getOz().contains(",")) {
                    for (String s2 : Arrays.asList(p.getOz().split(","))) {
                        s2 = s2.trim();
                        if (s2.equals(i)) {
                            return ResponseEntity.ok(p);
                        }
                    }
                } else {
                    if (i.equals(p.getOz())) {
                        return ResponseEntity.ok(p);
                    }
                }
            }
        }

        return ResponseEntity.notFound().build();
    }
}
