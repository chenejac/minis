package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.Function;
import rs.ac.uns.ftn.informatika.minis.repository.FunctionRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.FunctionSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

/**
 * REST controller for managing Function.
 */
@RestController
@RequestMapping("/api")
public class FunctionResource {

    private final Logger log = LoggerFactory.getLogger(FunctionResource.class);

    @Inject
    private FunctionRepository functionRepository;

    @Inject
    private FunctionSearchRepository functionSearchRepository;

    /**
     * POST /functions -> Create a new function.
     */
    @RequestMapping(value = "/functions",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Function> createFunction(@RequestBody Function function) throws URISyntaxException {
        log.debug("REST request to save Function : {}", function);
        if (function.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new function cannot already have an ID").body(null);
        }
        Function result = functionRepository.save(function);
        functionSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/functions/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("function", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT /functions -> Updates an existing function.
     */
    @RequestMapping(value = "/functions",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Function> updateFunction(@RequestBody Function function) throws URISyntaxException {
        log.debug("REST request to update Function : {}", function);
        if (function.getId() == null) {
            return createFunction(function);
        }
        Function result = functionRepository.save(function);
        functionSearchRepository.save(function);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("function", function.getId().toString()))
                .body(result);
    }

    /**
     * GET /functions -> get all the functions.
     */
    @RequestMapping(value = "/functions",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Page<Function>> getAllFunctions(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                                @RequestParam(value = "pageSize", defaultValue = "20") int pageSize) {
        log.debug("REST request to get all Functions");
        if(pageSize < 1)
        {
            String message = "pageSize can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        if(pageNum < 1)
        {
            String message = "pageNum can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        PageRequest pageRequest = new PageRequest(pageNum - 1, pageSize);
        Page page = functionRepository.findAll(pageRequest);
        return ResponseEntity.ok().body(page);
    }

    /**
     * GET /functions/:id -> get the "id" function.
     */
    @RequestMapping(value = "/functions/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Function> getFunction(@PathVariable Long id) {
        log.debug("REST request to get Function : {}", id);
        return Optional.ofNullable(functionRepository.findOne(id))
                .map(function -> new ResponseEntity<>(
                                function,
                                HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE /functions/:id -> delete the "id" function.
     */
    @RequestMapping(value = "/functions/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteFunction(@PathVariable Long id) {
        log.debug("REST request to delete Function : {}", id);
        functionRepository.delete(id);
        functionSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("function", id.toString())).build();
    }

    /**
     * SEARCH /_search/functions/:query -> search for the function corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/functions/{query}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Function> searchFunctions(@PathVariable String query) {
        return StreamSupport
                .stream(functionSearchRepository.search(queryString(query)).spliterator(), false)
                .collect(Collectors.toList());
    }

    // TODO : Popraviti ovu brlju
    @RequestMapping(value = "/_search/functions/byOz/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getFunctionByName(@RequestParam(value = "oz") String oz) {
        log.info("REST request to get function with oz : {}", oz);
        
        List<String> input = new ArrayList<String>();
        if (oz.contains(",")) {
            input = Arrays.asList(oz.split(","));
        } else {
            input.add(oz);
        }
        for (Function f : functionRepository.findAll()) {
            for (String i : input) {
                i = i.trim();
                log.debug("" + f.getOz());
                if (f.getOz().contains(",")) {
                    for (String s2 : Arrays.asList(f.getOz().split(","))) {
                        s2 = s2.trim();
                        if (s2.equals(i)) {
                            return ResponseEntity.ok(f);
                        }
                    }
                } else {
                    if (i.equals(f.getOz())) {
                        return ResponseEntity.ok(f);
                    }
                }
            }
        }

        return ResponseEntity.notFound().build();
    }
}
