package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.PersonStatusNameTranslations;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the PersonStatusNameTranslations entity.
 */
public interface PersonStatusNameTranslationsSearchRepository extends ElasticsearchRepository<PersonStatusNameTranslations, Long> {
}
