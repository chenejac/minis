package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.FounderNameTranslations;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the FounderNameTranslations entity.
 */
public interface FounderNameTranslationsSearchRepository extends ElasticsearchRepository<FounderNameTranslations, Long> {
}
