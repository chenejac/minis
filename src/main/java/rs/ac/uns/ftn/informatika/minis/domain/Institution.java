package rs.ac.uns.ftn.informatika.minis.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import rs.ac.uns.ftn.informatika.minis.domain.util.CustomLocalDateSerializer;
import rs.ac.uns.ftn.informatika.minis.domain.util.ISO8601LocalDateDeserializer;
import rs.ac.uns.ftn.informatika.minis.domain.util.CustomDateTimeDeserializer;
import rs.ac.uns.ftn.informatika.minis.domain.util.CustomDateTimeSerializer;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.joda.time.DateTime;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 *
 */
@Entity
@Table(name = "institution")
@Document(indexName = "institution")
public class Institution implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Field(type = FieldType.Long, store = true)
    private Long id;

    @Column(name = "name")
    private String name;
    
    @Column(name = "name_en")
    private String nameEn;

    @Column(name = "address")
    private String address;

    @Column(name = "place")
    private String place;

    @Column(name = "postal_code")
    private Integer postalCode;

    @Column(name = "acro")
    private String acro;

    @Column(name = "keywords")
    private String keywords;

    @Column(name = "uri")
    private String uri;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    @Column(name = "pib")
    private String pib;

    @Column(name = "maticni_broj")
    private String maticniBroj;

    @Column(name = "account")
    private String account;

    @Column(name = "ecrisid")
    private Integer ecrisid;

    @Column(name = "nio_id")
    private Integer nioID;

    @Column(name = "mntr_id")
    private String mntrID;

    @Column(name = "orcid")
    private String orcid;

    @Column(name = "rescript_number")
    private String rescriptNumber;
    
    @Column(name = "ownership_structure")
    private String ownershipStructure;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    @JsonSerialize(using = CustomLocalDateSerializer.class)
    @JsonDeserialize(using = ISO8601LocalDateDeserializer.class)
    @Column(name = "date")
    private LocalDate date;

    @Column(name = "founder")
    private String founder;

    @Column(name = "note")
    private String note;

    @Column(name = "accreditation_number")
    private String accreditationNumber;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    @JsonSerialize(using = CustomLocalDateSerializer.class)
    @JsonDeserialize(using = ISO8601LocalDateDeserializer.class)
    @Column(name = "accreditation_date")
    private LocalDate accreditationDate;

    @Column(name = "accreditation_note")
    private String accreditationNote;

    @Column(name = "creator")
    private String creator;

    @Column(name = "modifier")
    private String modifier;
    
    @Column(name = "town_ship_text")
    private String townShipText;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @Column(name = "creation_date")
    private DateTime creationDate;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @Column(name = "last_modification_date")
    private DateTime lastModificationDate;

    @Column(name = "record_status")
    private Integer recordStatus;

    @ManyToOne
    private Language language;

    @ManyToOne
    private InstitutionType type;

    @ManyToOne
    private InstitutionStatus institutionStatus;

    @OneToMany(mappedBy = "supetInstitution")
    @JsonIgnore
    private Set<Institution> subInstitutions = new HashSet<>();

    @ManyToOne
    private Institution supetInstitution;

    @OneToMany(mappedBy = "institution")
    @JsonIgnore
    private Set<TitleInstitution> titleInstitutions = new HashSet<>();

    @OneToMany(mappedBy = "institution")
    @JsonIgnore
    private Set<UserAccount> users = new HashSet<>();

    @OneToMany(mappedBy = "institution")
    @JsonIgnore
    private Set<PersonInstitution> persons = new HashSet<>();

    @OneToMany(mappedBy = "institution")
    @JsonIgnore
    private Set<InstitutionKeywordsTranslations> keywordsTranslations = new HashSet<>();

    @OneToMany(mappedBy = "institution")
    @JsonIgnore
    private Set<InstitutionNameTranslations> nameTranslations = new HashSet<>();

    @OneToMany(mappedBy = "institution")
    @JsonIgnore
    private Set<InstitutionFile> files = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "institution_activitie",
            joinColumns = @JoinColumn(name = "institutions_id", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "activities_id", referencedColumnName = "ID"))
    private Set<Activity> activities = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "institution_research_area",
            joinColumns = @JoinColumn(name = "institutions_id", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "research_areas_id", referencedColumnName = "ID"))
    private Set<ResearchArea> researchAreas = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "institution_classification",
            joinColumns = @JoinColumn(name = "institutions_id", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "classifications_id", referencedColumnName = "ID"))
    private Set<Classification> classifications = new HashSet<>();

    @ManyToOne
    private State state;

    @ManyToOne
    private City city;

    @ManyToOne
    private TownShip townShip;

    @PrePersist
    public void prePersist() {
        DateTime now = new DateTime();
        creationDate = now;
    }
    
    @PreUpdate
    public void preUpdate() {
        DateTime now = new DateTime();
        lastModificationDate = now;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getNameEn() {
        return nameEn;
    }
    
    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }
    
    public String getTownShipText() {
        return townShipText;
    }

    public void setTownShipText(String townShipText) {
        this.townShipText = townShipText;
    }

    public Integer getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(Integer postalCode) {
        this.postalCode = postalCode;
    }

    public String getAcro() {
        return acro;
    }

    public void setAcro(String acro) {
        this.acro = acro;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPib() {
        return pib;
    }

    public void setPib(String pib) {
        this.pib = pib;
    }

    public String getMaticniBroj() {
        return maticniBroj;
    }

    public void setMaticniBroj(String maticniBroj) {
        this.maticniBroj = maticniBroj;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Integer getEcrisid() {
        return ecrisid;
    }

    public void setEcrisid(Integer ecrisid) {
        this.ecrisid = ecrisid;
    }

    public Integer getNioID() {
        return nioID;
    }

    public void setNioID(Integer nioID) {
        this.nioID = nioID;
    }

    public String getMntrID() {
        return mntrID;
    }

    public void setMntrID(String mntrID) {
        this.mntrID = mntrID;
    }

    public String getOrcid() {
        return orcid;
    }

    public void setOrcid(String orcid) {
        this.orcid = orcid;
    }

    public String getRescriptNumber() {
        return rescriptNumber;
    }

    public void setRescriptNumber(String rescriptNumber) {
        this.rescriptNumber = rescriptNumber;
    }
    
    public String getOwnershipStructure() {
        return ownershipStructure;
    }

    public void setOwnershipStructure(String ownershipStructure) {
        this.ownershipStructure = ownershipStructure;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getFounder() {
        return founder;
    }

    public void setFounder(String founder) {
        this.founder = founder;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getAccreditationNumber() {
        return accreditationNumber;
    }

    public void setAccreditationNumber(String accreditationNumber) {
        this.accreditationNumber = accreditationNumber;
    }

    public LocalDate getAccreditationDate() {
        return accreditationDate;
    }

    public void setAccreditationDate(LocalDate accreditationDate) {
        this.accreditationDate = accreditationDate;
    }

    public String getAccreditationNote() {
        return accreditationNote;
    }

    public void setAccreditationNote(String accreditationNote) {
        this.accreditationNote = accreditationNote;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public DateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(DateTime creationDate) {
        this.creationDate = creationDate;
    }

    public DateTime getLastModificationDate() {
        return lastModificationDate;
    }

    public void setLastModificationDate(DateTime lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    public Integer getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(Integer recordStatus) {
        this.recordStatus = recordStatus;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public InstitutionType getType() {
        return type;
    }

    public void setType(InstitutionType institutionType) {
        this.type = institutionType;
    }

    public InstitutionStatus getInstitutionStatus() {
        return institutionStatus;
    }

    public void setInstitutionStatus(InstitutionStatus institutionStatus) {
        this.institutionStatus = institutionStatus;
    }

    public Set<Institution> getSubInstitutions() {
        return subInstitutions;
    }

    public void setSubInstitutions(Set<Institution> institutions) {
        this.subInstitutions = institutions;
    }

    public Institution getSupetInstitution() {
        return supetInstitution;
    }

    public void setSupetInstitution(Institution institution) {
        this.supetInstitution = institution;
    }

    public Set<TitleInstitution> getTitleInstitutions() {
        return titleInstitutions;
    }

    public void setTitleInstitutions(Set<TitleInstitution> titleInstitutions) {
        this.titleInstitutions = titleInstitutions;
    }

    public Set<UserAccount> getUsers() {
        return users;
    }

    public void setUsers(Set<UserAccount> userAccounts) {
        this.users = userAccounts;
    }

    public Set<PersonInstitution> getPersons() {
        return persons;
    }

    public void setPersons(Set<PersonInstitution> personInstitutions) {
        this.persons = personInstitutions;
    }

    public Set<InstitutionKeywordsTranslations> getKeywordsTranslations() {
        return keywordsTranslations;
    }

    public void setKeywordsTranslations(Set<InstitutionKeywordsTranslations> institutionKeywordsTranslationss) {
        this.keywordsTranslations = institutionKeywordsTranslationss;
    }

    public Set<InstitutionNameTranslations> getNameTranslations() {
        return nameTranslations;
    }

    public void setNameTranslations(Set<InstitutionNameTranslations> institutionNameTranslationss) {
        this.nameTranslations = institutionNameTranslationss;
    }

    public Set<InstitutionFile> getFiles() {
        return files;
    }

    public void setFiles(Set<InstitutionFile> institutionFiles) {
        this.files = institutionFiles;
    }

    public Set<Activity> getActivities() {
        return activities;
    }

    public void setActivities(Set<Activity> activitys) {
        this.activities = activitys;
    }

    public Set<ResearchArea> getResearchAreas() {
        return researchAreas;
    }

    public void setResearchAreas(Set<ResearchArea> researchAreas) {
        this.researchAreas = researchAreas;
    }

    public Set<Classification> getClassifications() {
        return classifications;
    }

    public void setClassifications(Set<Classification> classifications) {
        this.classifications = classifications;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public TownShip getTownShip() {
        return townShip;
    }

    public void setTownShip(TownShip townShip) {
        this.townShip = townShip;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Institution institution = (Institution) o;

        if (!Objects.equals(id, institution.id)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Institution{"
                + "id=" + id
                + ", name='" + name + "'"
                + ", engName='" + nameEn + "'"
                + ", address='" + address + "'"
                + ", place='" + place + "'"
                + ", postalCode='" + postalCode + "'"
                + ", acro='" + acro + "'"
                + ", keywords='" + keywords + "'"
                + ", uri='" + uri + "'"
                + ", email='" + email + "'"
                + ", phone='" + phone + "'"
                + ", pib='" + pib + "'"
                + ", maticniBroj='" + maticniBroj + "'"
                + ", account='" + account + "'"
                + ", ecrisid='" + ecrisid + "'"
                + ", nioID='" + nioID + "'"
                + ", mntrID='" + mntrID + "'"
                + ", orcid='" + orcid + "'"
                + ", rescriptNumber='" + rescriptNumber + "'"
                + ", date='" + date + "'"
                + ", founder='" + founder + "'"
                + ", note='" + note + "'"
                + ", accreditationNumber='" + accreditationNumber + "'"
                + ", accreditationDate='" + accreditationDate + "'"
                + ", accreditationNote='" + accreditationNote + "'"
                + ", creator='" + creator + "'"
                + ", modifier='" + modifier + "'"
                + ", creationDate='" + creationDate + "'"
                + ", lastModificationDate='" + lastModificationDate + "'"
                + ", recordStatus='" + recordStatus + "'"
                + '}';
    }
}
