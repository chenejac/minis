package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.Privilege;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Privilege entity.
 */
public interface PrivilegeRepository extends JpaRepository<Privilege,Long> {

}
