package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.PersonCategory;
import org.springframework.data.jpa.repository.*;

import java.util.List;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the PersonCategory entity.
 */
public interface PersonCategoryRepository extends JpaRepository<PersonCategory,Long> {

    @Query("select personCategory from PersonCategory personCategory where personCategory.person.id =:id")
    List<PersonCategory> findForPerson(@Param("id") Long id);
    
}
