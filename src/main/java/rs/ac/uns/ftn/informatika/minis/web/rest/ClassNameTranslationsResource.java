package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.ClassNameTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.ClassNameTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.ClassNameTranslationsSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ClassNameTranslations.
 */
@RestController
@RequestMapping("/api")
public class ClassNameTranslationsResource {

    private final Logger log = LoggerFactory.getLogger(ClassNameTranslationsResource.class);

    @Inject
    private ClassNameTranslationsRepository classNameTranslationsRepository;

    @Inject
    private ClassNameTranslationsSearchRepository classNameTranslationsSearchRepository;

    /**
     * POST  /classNameTranslationss -> Create a new classNameTranslations.
     */
    @RequestMapping(value = "/classNameTranslationss",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ClassNameTranslations> createClassNameTranslations(@RequestBody ClassNameTranslations classNameTranslations) throws URISyntaxException {
        log.debug("REST request to save ClassNameTranslations : {}", classNameTranslations);
        if (classNameTranslations.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new classNameTranslations cannot already have an ID").body(null);
        }
        ClassNameTranslations result = classNameTranslationsRepository.save(classNameTranslations);
        classNameTranslationsSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/classNameTranslationss/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("classNameTranslations", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /classNameTranslationss -> Updates an existing classNameTranslations.
     */
    @RequestMapping(value = "/classNameTranslationss",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ClassNameTranslations> updateClassNameTranslations(@RequestBody ClassNameTranslations classNameTranslations) throws URISyntaxException {
        log.debug("REST request to update ClassNameTranslations : {}", classNameTranslations);
        if (classNameTranslations.getId() == null) {
            return createClassNameTranslations(classNameTranslations);
        }
        ClassNameTranslations result = classNameTranslationsRepository.save(classNameTranslations);
        classNameTranslationsSearchRepository.save(classNameTranslations);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("classNameTranslations", classNameTranslations.getId().toString()))
                .body(result);
    }

    /**
     * GET  /classNameTranslationss -> get all the classNameTranslationss.
     */
    @RequestMapping(value = "/classNameTranslationss",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ClassNameTranslations> getAllClassNameTranslationss() {
        log.debug("REST request to get all ClassNameTranslationss");
        return classNameTranslationsRepository.findAll();
    }

    /**
     * GET  /classNameTranslationss/:id -> get the "id" classNameTranslations.
     */
    @RequestMapping(value = "/classNameTranslationss/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ClassNameTranslations> getClassNameTranslations(@PathVariable Long id) {
        log.debug("REST request to get ClassNameTranslations : {}", id);
        return Optional.ofNullable(classNameTranslationsRepository.findOne(id))
            .map(classNameTranslations -> new ResponseEntity<>(
                classNameTranslations,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /classNameTranslationss/:id -> delete the "id" classNameTranslations.
     */
    @RequestMapping(value = "/classNameTranslationss/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteClassNameTranslations(@PathVariable Long id) {
        log.debug("REST request to delete ClassNameTranslations : {}", id);
        classNameTranslationsRepository.delete(id);
        classNameTranslationsSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("classNameTranslations", id.toString())).build();
    }

    /**
     * SEARCH  /_search/classNameTranslationss/:query -> search for the classNameTranslations corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/classNameTranslationss/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ClassNameTranslations> searchClassNameTranslationss(@PathVariable String query) {
        return StreamSupport
            .stream(classNameTranslationsSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
