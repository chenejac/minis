package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.Classification;
import rs.ac.uns.ftn.informatika.minis.repository.ClassificationRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.ClassificationSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Classification.
 */
@RestController
@RequestMapping("/api")
public class ClassificationResource {

    private final Logger log = LoggerFactory.getLogger(ClassificationResource.class);

    @Inject
    private ClassificationRepository classificationRepository;

    @Inject
    private ClassificationSearchRepository classificationSearchRepository;

    /**
     * POST  /classifications -> Create a new classification.
     */
    @RequestMapping(value = "/classifications",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Classification> createClassification(@RequestBody Classification classification) throws URISyntaxException {
        log.debug("REST request to save Classification : {}", classification);
        if (classification.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new classification cannot already have an ID").body(null);
        }
        Classification result = classificationRepository.save(classification);
        classificationSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/classifications/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("classification", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /classifications -> Updates an existing classification.
     */
    @RequestMapping(value = "/classifications",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Classification> updateClassification(@RequestBody Classification classification) throws URISyntaxException {
        log.debug("REST request to update Classification : {}", classification);
        if (classification.getId() == null) {
            return createClassification(classification);
        }
        Classification result = classificationRepository.save(classification);
        classificationSearchRepository.save(classification);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("classification", classification.getId().toString()))
                .body(result);
    }

    /**
     * GET  /classifications -> get all the classifications.
     */
    @RequestMapping(value = "/classifications",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Classification> getAllClassifications() {
        log.debug("REST request to get all Classifications");
        return classificationRepository.findAll();
    }

    /**
     * GET  /classifications/:id -> get the "id" classification.
     */
    @RequestMapping(value = "/classifications/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Classification> getClassification(@PathVariable Long id) {
        log.debug("REST request to get Classification : {}", id);
        return Optional.ofNullable(classificationRepository.findOne(id))
            .map(classification -> new ResponseEntity<>(
                classification,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /classifications/:id -> delete the "id" classification.
     */
    @RequestMapping(value = "/classifications/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteClassification(@PathVariable Long id) {
        log.debug("REST request to delete Classification : {}", id);
        classificationRepository.delete(id);
        classificationSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("classification", id.toString())).build();
    }

    /**
     * SEARCH  /_search/classifications/:query -> search for the classification corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/classifications/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Classification> searchClassifications(@PathVariable String query) {
        return StreamSupport
            .stream(classificationSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
