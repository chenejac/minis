/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.uns.ftn.informatika.minis.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import rs.ac.uns.ftn.informatika.minis.domain.PersonPosition;

/**
 *
 */
public interface PersonPositionRepository extends JpaRepository<PersonPosition, Long>{
    
    @Query("select personPosition from PersonPosition personPosition where personPosition.person.id =:id")
    List<PersonPosition> findForPerson(@Param("id") Long id);
    
}
