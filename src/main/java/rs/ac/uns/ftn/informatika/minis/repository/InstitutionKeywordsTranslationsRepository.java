package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.InstitutionKeywordsTranslations;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the InstitutionKeywordsTranslations entity.
 */
public interface InstitutionKeywordsTranslationsRepository extends JpaRepository<InstitutionKeywordsTranslations,Long> {

}
