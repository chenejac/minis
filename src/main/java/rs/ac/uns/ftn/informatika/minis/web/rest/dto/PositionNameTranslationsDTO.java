/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.uns.ftn.informatika.minis.web.rest.dto;

import rs.ac.uns.ftn.informatika.minis.domain.Language;
import rs.ac.uns.ftn.informatika.minis.domain.PositionNameTranslations;

/**
 *
 * @author stefan
 */
public class PositionNameTranslationsDTO {
    private Long id;
    private String name;
    private String transType;
    private Language language;

    public PositionNameTranslationsDTO(Long id, String name, String transType, Language language) {
        this.id = id;
        this.name = name;
        this.transType = transType;
        this.language = language;
    }
    
    public PositionNameTranslationsDTO(PositionNameTranslations pnt) {
        this.id = pnt.getId();
        this.name = pnt.getName();
        this.transType = pnt.getTransType();
        this.language = pnt.getLanguage();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return "PositionNameTranslations{" +
                "id=" + id +
                ", name='" + name + "'" +
                ", transType='" + transType + "'" +
                '}';
    }
}
