package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.SchemeNameTranslations;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the SchemeNameTranslations entity.
 */
public interface SchemeNameTranslationsRepository extends JpaRepository<SchemeNameTranslations,Long> {

}
