package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.PersonFile;
import rs.ac.uns.ftn.informatika.minis.repository.PersonFileRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PersonFileSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PersonFile.
 */
@RestController
@RequestMapping("/api")
public class PersonFileResource {

    private final Logger log = LoggerFactory.getLogger(PersonFileResource.class);

    @Inject
    private PersonFileRepository personFileRepository;

    @Inject
    private PersonFileSearchRepository personFileSearchRepository;

    /**
     * POST  /personFiles -> Create a new personFile.
     */
    @RequestMapping(value = "/personFiles",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonFile> createPersonFile(@RequestBody PersonFile personFile) throws URISyntaxException {
        log.debug("REST request to save PersonFile : {}", personFile);
        if (personFile.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new personFile cannot already have an ID").body(null);
        }
        PersonFile result = personFileRepository.save(personFile);
        personFileSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/personFiles/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("personFile", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /personFiles -> Updates an existing personFile.
     */
    @RequestMapping(value = "/personFiles",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonFile> updatePersonFile(@RequestBody PersonFile personFile) throws URISyntaxException {
        log.debug("REST request to update PersonFile : {}", personFile);
        if (personFile.getId() == null) {
            return createPersonFile(personFile);
        }
        PersonFile result = personFileRepository.save(personFile);
        personFileSearchRepository.save(personFile);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("personFile", personFile.getId().toString()))
                .body(result);
    }

    /**
     * GET  /personFiles -> get all the personFiles.
     */
    @RequestMapping(value = "/personFiles",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PersonFile> getAllPersonFiles() {
        log.debug("REST request to get all PersonFiles");
        return personFileRepository.findAll();
    }

    /**
     * GET  /personFiles/:id -> get the "id" personFile.
     */
    @RequestMapping(value = "/personFiles/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonFile> getPersonFile(@PathVariable Long id) {
        log.debug("REST request to get PersonFile : {}", id);
        return Optional.ofNullable(personFileRepository.findOne(id))
            .map(personFile -> new ResponseEntity<>(
                personFile,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /personFiles/:id -> delete the "id" personFile.
     */
    @RequestMapping(value = "/personFiles/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePersonFile(@PathVariable Long id) {
        log.debug("REST request to delete PersonFile : {}", id);
        personFileRepository.delete(id);
        personFileSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("personFile", id.toString())).build();
    }

    /**
     * SEARCH  /_search/personFiles/:query -> search for the personFile corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/personFiles/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PersonFile> searchPersonFiles(@PathVariable String query) {
        return StreamSupport
            .stream(personFileSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
