package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.PositionNameTranslations;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the PositionNameTranslations entity.
 */
public interface PositionNameTranslationsSearchRepository extends ElasticsearchRepository<PositionNameTranslations, Long> {
}
