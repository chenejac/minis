/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.inject.Inject;
import static org.elasticsearch.index.query.QueryBuilders.queryString;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.uns.ftn.informatika.minis.domain.Institution;
import rs.ac.uns.ftn.informatika.minis.domain.Person;
import rs.ac.uns.ftn.informatika.minis.domain.PersonInstitution;
import rs.ac.uns.ftn.informatika.minis.repository.InstitutionRepository;
import rs.ac.uns.ftn.informatika.minis.repository.PersonInstitutionRepository;
import rs.ac.uns.ftn.informatika.minis.repository.PersonRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.InstitutionSearchRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PersonSearchRepository;
import rs.ac.uns.ftn.informatika.minis.service.InstitutionService;
import rs.ac.uns.ftn.informatika.minis.service.PersonService;
import rs.ac.uns.ftn.informatika.minis.web.rest.dto.PersonInstitutionDTO;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.LatCyrUtils;
import rs.ac.uns.ftn.informatika.minis.web.rest.dto.PersonInstitutionProjectDTO;
import rs.ac.uns.ftn.informatika.minis.web.rest.errors.DataException;

/**
 *
 * @author Milan Deket
 */
@RestController
@RequestMapping("/api/project")
public class ProjectResource {
    private final Logger log = LoggerFactory.getLogger(ProjectResource.class);
    
    @Inject
    private PersonRepository personRepository;

    @Inject
    private PersonService personService;
    
    @Inject
    private InstitutionRepository institutionRepository;
    
    @Inject
    private InstitutionService institutionService;
    
    @Inject
    private PersonInstitutionRepository personInstitutionRepository;
    
    @Inject
    private PersonSearchRepository personSearchRepository;
    
    @Inject
    private InstitutionSearchRepository institutionSearchRepository;
    
    /**
     * GET /project/person/:id -> get the "id" person.
     */
    @RequestMapping(value = "/person/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Person> getPerson(@PathVariable Long id) {
        log.debug("REST request to get Person : {}", id);
        return Optional.ofNullable(personRepository.findOne(id))
                .map(position -> new ResponseEntity<>(
                                position,
                                HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    
    /**
     * GET /project/person gets all persons with requested id's.
     */
    @RequestMapping(value = "/person",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Map<Long,Person>> getPersons(@RequestBody List<Long> ids) {
        log.debug("REST request to get Persons by ids");
        return Optional.ofNullable(personService.getPersonsByIds(ids))
                .map(position -> new ResponseEntity<>(
                                position,
                                HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    
    /**
     * GET /project/institution/:id -> get the "id" institution.
     */
    @RequestMapping(value = "/institution/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Institution> getInstitution(@PathVariable Long id) {
        log.debug("REST request to get Institution : {}", id);
        return Optional.ofNullable(institutionRepository.findOne(id))
                .map(position -> new ResponseEntity<>(
                                position,
                                HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    
    /**
     * GET /project/institution gets all institutions with requested id's.
     */
    @RequestMapping(value = "/institution",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Map<Long,Institution>> getInstitutions(@RequestBody List<Long> ids) {
        log.debug("REST request to get Persons by ids");
        return Optional.ofNullable(institutionService.getInstitutionsByIds(ids))
                .map(position -> new ResponseEntity<>(
                                position,
                                HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    
    /**
     * POST /project/personInstitutions/ gets all person institutions where person.id in person_ids and institution.id in institution_ids
     */
    @RequestMapping(value = "/personInstitutions",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<PersonInstitution>> getPersonInstitutions(@RequestBody(required = true) PersonInstitutionProjectDTO pipDTO) {
        log.debug("REST request to get PersonInstitutions by ids");
        List<PersonInstitution> retVal;
        boolean hasPersons = false, hasInstitutions = false;
        if (pipDTO.getPersonIds() != null && pipDTO.getPersonIds().size() > 0) {
            hasPersons = true;
        }
        if (pipDTO.getInstitutionIds() != null && pipDTO.getInstitutionIds().size() > 0) {
            hasInstitutions = true;
        }
        
        if (hasPersons && hasInstitutions) {
            retVal = personInstitutionRepository.findByPersonIdInAndInstitutionIdIn(pipDTO.getPersonIds(), pipDTO.getInstitutionIds());
        } else if (hasPersons) {
            retVal = personInstitutionRepository.findByPersonIdIn(pipDTO.getPersonIds());
        } else if (hasInstitutions) {
            retVal = personInstitutionRepository.findByInstitutionIdIn(pipDTO.getInstitutionIds());
        } else {
            throw DataException.DataExceptionBuilder.nonFieldError("You have to provide a non empty array for at least one of the following fields: personIds, institutionIds");
        }
        
        return new ResponseEntity(retVal, HttpStatus.OK);
    }
    
    /**
     * GET /project/personInstitution/:id -> get the "id" personInstitution.
     */
    @RequestMapping(value = "/personInstitution/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    public ResponseEntity<List<PersonInstitutionDTO>> getPersonInstitution(@PathVariable Long id) {
        log.debug("REST request to get PersonInstitution : {}", id);
        Person person = personRepository.findOne(id);
        if(person != null){
            return Optional.ofNullable(personInstitutionRepository.findPersonInstitutionByPerson(person))
                    .map(personInstitutions -> personInstitutions.stream()
                        .map(personInstitution -> new PersonInstitutionDTO(personInstitution))
                        .collect(Collectors.toList()))
                    .map(personInstitutions -> new ResponseEntity<>(
                                    personInstitutions,
                                    HttpStatus.OK))
                    .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    
    /**
     * SEARCH /_search/persons/:query -> search for the person corresponding to
     * the query.
     */
    @RequestMapping(value = "/_search/persons/{query}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Person> searchPersons(@PathVariable String query) {
        if(query == null || query.isEmpty()){
            return new ArrayList<>();
        }
        QueryStringQueryBuilder qs = queryString(transformQueryLatin(query));
        String[] fields = new String[] {"firstName", "lastName", "middleName", "jmbg"};
        for(String field : fields) {
            qs.field(field);
        }
        return StreamSupport
                .stream(personSearchRepository.search(qs).spliterator(), false)
                .collect(Collectors.toList());
    }
    
    private String transformQueryLatin(String query) {
        String retVal = "+";
        String[] parts = query.split(" +");
        int i = 0;
        for (; i < parts.length - 1; ++i) {
            retVal +=  parts[i] + " +";
        }
        String parenQueryString = parts[i];
        String latinParenQueryString = LatCyrUtils.toLatinUnaccented(parenQueryString.toLowerCase())
                .replace("dj", "d");
        retVal += " (" + latinParenQueryString + "* " + parenQueryString + ")";
        return retVal;
    } 
    
    /**
     * SEARCH  /_search/institutions/:query -> search for the institution corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/institutions/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Institution> searchInstitutions(@PathVariable String query) {
        if(query == null || query.isEmpty()){
            return new ArrayList<>();
        }
        QueryStringQueryBuilder qs = queryString(transformQueryLatin(query));
        String[] fields = new String[] {"name"};
        for(String field : fields) {
            qs.field(field);
        }
        return StreamSupport
            .stream(institutionSearchRepository.search(qs).spliterator(), false)
            .collect(Collectors.toList());
    }
    
}
