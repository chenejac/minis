package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.InstitutionFile;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the InstitutionFile entity.
 */
public interface InstitutionFileRepository extends JpaRepository<InstitutionFile,Long> {

}
