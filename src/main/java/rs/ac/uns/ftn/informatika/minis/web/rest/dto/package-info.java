/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package rs.ac.uns.ftn.informatika.minis.web.rest.dto;
