package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.InstitutionKeywordsTranslations;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the InstitutionKeywordsTranslations entity.
 */
public interface InstitutionKeywordsTranslationsSearchRepository extends ElasticsearchRepository<InstitutionKeywordsTranslations, Long> {
}
