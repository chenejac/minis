/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.uns.ftn.informatika.minis.web.rest.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.DateTime;
import rs.ac.uns.ftn.informatika.minis.domain.Function;
import rs.ac.uns.ftn.informatika.minis.domain.Institution;
import rs.ac.uns.ftn.informatika.minis.domain.Person;
import rs.ac.uns.ftn.informatika.minis.domain.PersonInstitution;
import rs.ac.uns.ftn.informatika.minis.domain.util.CustomDateTimeDeserializer;
import rs.ac.uns.ftn.informatika.minis.domain.util.CustomDateTimeSerializer;

/**
 *
 * @author stefan
 */
public class PersonInstitutionDTO {
    private Long id;
    
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    private DateTime startDate;
    
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    private DateTime endDate;
    
    private String description;
    
    private String researchArea;

    private Function function;

    private Institution institution;
    
    private Integer employmentPercentage;
    
    private Integer maxMonthEngagement;

    private PositionDTO position;

    private Person person;

    public PersonInstitutionDTO(Long id, DateTime startDate, DateTime endDate, String description, String researchArea, Function function, Institution institution, Integer employmentPercentage, Integer maxMonthEngagement, PositionDTO position, Person person) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.description = description;
        this.researchArea = researchArea;
        this.function = function;
        this.institution = institution;
        this.employmentPercentage = employmentPercentage;
        this.maxMonthEngagement = maxMonthEngagement;
        this.position = position;
        this.person = person;
    }
    
    public PersonInstitutionDTO(PersonInstitution personInstitution) {
        this.id = personInstitution.getId();
        this.startDate = personInstitution.getStartDate();
        this.endDate = personInstitution.getEndDate();
        this.description = personInstitution.getDescription();
        this.researchArea = personInstitution.getResearchArea();
        this.function = personInstitution.getFunction();
        this.institution = personInstitution.getInstitution();
        this.employmentPercentage = personInstitution.getEmploymentPercentage();
        this.maxMonthEngagement = personInstitution.getMaxMonthEngagement();
        if (personInstitution.getPosition() != null) {
            this.position = new PositionDTO(personInstitution.getPosition());
        } else {
            this.position = null;
        }
        this.person = personInstitution.getPerson();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(DateTime startDate) {
        this.startDate = startDate;
    }

    public DateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(DateTime endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResearchArea() {
        return researchArea;
    }

    public void setResearchArea(String researchArea) {
        this.researchArea = researchArea;
    }

    public Function getFunction() {
        return function;
    }

    public void setFunction(Function function) {
        this.function = function;
    }

    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    public Integer getEmploymentPercentage() {
        return employmentPercentage;
    }

    public void setEmploymentPercentage(Integer employmentPercentage) {
        this.employmentPercentage = employmentPercentage;
    }

    public Integer getMaxMonthEngagement() {
        return maxMonthEngagement;
    }

    public void setMaxMonthEngagement(Integer maxMonthEngagement) {
        this.maxMonthEngagement = maxMonthEngagement;
    }

    public PositionDTO getPosition() {
        return position;
    }

    public void setPosition(PositionDTO position) {
        this.position = position;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public String toString() {
        return "PersonInstitution{" +
                "id=" + id +
                ", startDate='" + startDate + "'" +
                ", endDate='" + endDate + "'" +
                ", employmentPercentage='" + employmentPercentage + "'" +
                ", maxMonthEgagement='" + maxMonthEngagement + "'" +
                ", description='" + description + "'" +
                ", researchArea='" + researchArea + "'" +
                '}';
    }
}
