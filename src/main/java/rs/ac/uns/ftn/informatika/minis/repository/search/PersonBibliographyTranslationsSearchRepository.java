package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.PersonBibliographyTranslations;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the PersonBibliographyTranslations entity.
 */
public interface PersonBibliographyTranslationsSearchRepository extends ElasticsearchRepository<PersonBibliographyTranslations, Long> {
}
