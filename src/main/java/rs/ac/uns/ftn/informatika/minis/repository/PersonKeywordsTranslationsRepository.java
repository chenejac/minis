package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.PersonKeywordsTranslations;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the PersonKeywordsTranslations entity.
 */
public interface PersonKeywordsTranslationsRepository extends JpaRepository<PersonKeywordsTranslations,Long> {

}
