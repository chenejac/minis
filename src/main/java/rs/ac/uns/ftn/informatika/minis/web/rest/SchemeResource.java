package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.Scheme;
import rs.ac.uns.ftn.informatika.minis.repository.SchemeRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.SchemeSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Scheme.
 */
@RestController
@RequestMapping("/api")
public class SchemeResource {

    private final Logger log = LoggerFactory.getLogger(SchemeResource.class);

    @Inject
    private SchemeRepository schemeRepository;

    @Inject
    private SchemeSearchRepository schemeSearchRepository;

    /**
     * POST  /schemes -> Create a new scheme.
     */
    @RequestMapping(value = "/schemes",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Scheme> createScheme(@RequestBody Scheme scheme) throws URISyntaxException {
        log.debug("REST request to save Scheme : {}", scheme);
        if (scheme.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new scheme cannot already have an ID").body(null);
        }
        Scheme result = schemeRepository.save(scheme);
        schemeSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/schemes/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("scheme", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /schemes -> Updates an existing scheme.
     */
    @RequestMapping(value = "/schemes",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Scheme> updateScheme(@RequestBody Scheme scheme) throws URISyntaxException {
        log.debug("REST request to update Scheme : {}", scheme);
        if (scheme.getId() == null) {
            return createScheme(scheme);
        }
        Scheme result = schemeRepository.save(scheme);
        schemeSearchRepository.save(scheme);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("scheme", scheme.getId().toString()))
                .body(result);
    }

    /**
     * GET  /schemes -> get all the schemes.
     */
    @RequestMapping(value = "/schemes",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Scheme> getAllSchemes() {
        log.debug("REST request to get all Schemes");
        return schemeRepository.findAll();
    }

    /**
     * GET  /schemes/:id -> get the "id" scheme.
     */
    @RequestMapping(value = "/schemes/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Scheme> getScheme(@PathVariable Long id) {
        log.debug("REST request to get Scheme : {}", id);
        return Optional.ofNullable(schemeRepository.findOne(id))
            .map(scheme -> new ResponseEntity<>(
                scheme,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /schemes/:id -> delete the "id" scheme.
     */
    @RequestMapping(value = "/schemes/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteScheme(@PathVariable Long id) {
        log.debug("REST request to delete Scheme : {}", id);
        schemeRepository.delete(id);
        schemeSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("scheme", id.toString())).build();
    }

    /**
     * SEARCH  /_search/schemes/:query -> search for the scheme corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/schemes/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Scheme> searchSchemes(@PathVariable String query) {
        return StreamSupport
            .stream(schemeSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
