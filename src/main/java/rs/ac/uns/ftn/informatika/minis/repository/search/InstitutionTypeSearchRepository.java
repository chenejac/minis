package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.InstitutionType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the InstitutionType entity.
 */
public interface InstitutionTypeSearchRepository extends ElasticsearchRepository<InstitutionType, Long> {
}
