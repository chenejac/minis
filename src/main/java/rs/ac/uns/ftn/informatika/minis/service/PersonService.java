/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rs.ac.uns.ftn.informatika.minis.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.informatika.minis.domain.Authority;
import rs.ac.uns.ftn.informatika.minis.domain.Institution;
import rs.ac.uns.ftn.informatika.minis.domain.Person;
import rs.ac.uns.ftn.informatika.minis.domain.PersonInstitution;
import rs.ac.uns.ftn.informatika.minis.domain.User;
import rs.ac.uns.ftn.informatika.minis.repository.PersonInstitutionRepository;
import rs.ac.uns.ftn.informatika.minis.repository.PersonRepository;
import rs.ac.uns.ftn.informatika.minis.repository.UserRepository;
import rs.ac.uns.ftn.informatika.minis.security.AuthoritiesConstants;
import rs.ac.uns.ftn.informatika.minis.security.SecurityUtils;

/**
 *
 * @author Milan Deket
 *
 * Service class for managing persons.
 */
@Service
@Transactional
public class PersonService {
    
    private final Logger log = LoggerFactory.getLogger(PersonService.class);
    
    @Inject
    private PersonRepository personRepository;
    
    @Inject
    private PersonInstitutionRepository personInstitutionRepository;
    
    @Inject
    private UserRepository userRepository;

    
    public Person save(Person person){
        userRepository.findOneByLogin(SecurityUtils.getCurrentLogin()).ifPresent(u-> {
            boolean admin = false;
            boolean institutionAdmin = false;
            for(Authority auth : u.getAuthorities()){
                if(auth.getName().equals("ROLE_ADMIN")){
                    admin = true;
                    break;
                }else if(auth.getName().equals("ROLE_INSTITUTION_ADMIN")){
                    institutionAdmin = true;
                }
            }
            if(u.getPerson() != null){
                if(person.getId() == null){
                    person.setCreator(u.getPerson().getFirstName() + " " + u.getPerson().getLastName());
                }else{
                    person.setModifier(u.getPerson().getFirstName() + " " + u.getPerson().getLastName());
                }       
            }
            if(admin){
                person.setRecordStatus(3);
            }else if(institutionAdmin){
                person.setRecordStatus(2);
            }
            personRepository.save(person);
        });
        return person;
    }
    
    public Page<Person> getAllPersons(int pageNum, int pageSize, boolean migrated, boolean changed, boolean approved, String sortBy, String orderBy){
        Sort sort = new Sort(Sort.Direction.ASC, "lastName");
        if(orderBy.equals("desc")){
            sort = new Sort(Sort.Direction.DESC, sortBy);
        }else if(orderBy.equals("asc")){
            sort = new Sort(Sort.Direction.ASC, sortBy);
        }
        PageRequest pageRequest = new PageRequest(pageNum - 1, pageSize, sort);
        Optional<User> user = userRepository.findOneByLogin(SecurityUtils.getCurrentLogin());
        Integer paramMigrated = null;
        Integer paramChanged = null;
        Integer paramApproved = null;
        List<Integer> recordStatus = new ArrayList<>();
        if(migrated){
            paramMigrated = 1;
            recordStatus.add(1);
        }
        if(changed){
            paramChanged = 2;
            recordStatus.add(2);
        }
        if(approved){
            paramApproved = 3;
            recordStatus.add(3);
        }
        
        for(Authority auth : user.get().getAuthorities()){
            if(auth.getName().equals(AuthoritiesConstants.ADMIN)){
                Page<Person> persons = personRepository.findByRecordStatusIn(recordStatus, pageRequest);
                return persons;
            }
        }
        
        if(user.get().getInstitution() != null){
            Institution inst = user.get().getInstitution();
            List<Person> personsUnsorted = personRepository.searchForPersonsByRecordStatusAndInstitutionIdWithouPagination(inst.getId(), paramMigrated, paramChanged, paramApproved);
            List<Person> personsSorted = this.sort(personsUnsorted, sortBy, orderBy);
            List<Person> persons = this.paginate(personsSorted, pageSize , pageNum);
            Page page = new PageImpl(persons, pageRequest, personsSorted.size());
            return page;
        }else{
            String message = "Logged in user doesn`t belong in any institution.";
            log.info(message);
            return null;
        }
    }

    private List<Person> sort(List<Person> persons, String sortBy, String orderBy) {
        if(sortBy.equals("firstName")){
            if(orderBy.equals("asc")){
                Collections.sort(persons, (p1, p2) -> p1.getFirstName().compareTo(p2.getFirstName()));
            }else if(orderBy.equals("desc")){
                Collections.sort(persons, (p1, p2) -> p2.getFirstName().compareTo(p1.getFirstName()));
            }
        }else if(sortBy.equals("lastName")){
            if(orderBy.equals("asc")){
                Collections.sort(persons, (p1, p2) -> p1.getLastName().compareTo(p2.getLastName()));
            }else if(orderBy.equals("desc")){
                Collections.sort(persons, (p1, p2) -> p2.getLastName().compareTo(p1.getLastName()));
            }
        }else if(sortBy.equals("dateOfBirth")){
            if(orderBy.equals("asc")){
                Collections.sort(persons, (p1, p2) -> {
                    Long date1 = Long.MIN_VALUE;
                    Long date2 = Long.MIN_VALUE;
                    if(p1.getDateOfBirth() != null ){
                        date1 = p1.getDateOfBirth().toDate().getTime();
                    }
                    if(p2.getDateOfBirth() != null ){
                        date2 = p2.getDateOfBirth().toDate().getTime();
                    }
                    return date1.compareTo(date2);
                });
            }else if(orderBy.equals("desc")){
                Collections.sort(persons, (p1, p2) -> {
                    Long date1 = Long.MIN_VALUE;
                    Long date2 = Long.MIN_VALUE;
                    if(p1.getDateOfBirth() != null ){
                        date1 = p1.getDateOfBirth().toDate().getTime();
                    }
                    if(p2.getDateOfBirth() != null ){
                        date2 = p2.getDateOfBirth().toDate().getTime();
                    }
                    return date2.compareTo(date1);
                });
            }
        }
        return persons;
    }
    
    private List<Person> paginate(List<Person> persons, int pageSize, int pageNum) {
        int firstIndex = (pageNum-1)*pageSize;
        int lastIndex = pageNum*pageSize;
        if(lastIndex > persons.size()){
            lastIndex = persons.size();
        }
        return persons.subList(firstIndex, lastIndex);
    }
    
    public byte[] getPersonsInXLS(boolean migrated, boolean changed, boolean approved, String sortBy, String orderBy) throws IOException{
        
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Istrazivaci");
        sheet.setColumnWidth(0, 3000);
        sheet.setColumnWidth(1, 3000);
        sheet.setColumnWidth(2, 4500);
        int rownum = 0;
        XSSFRow row = sheet.createRow(rownum++);
        List<Person> persons = this.getAllPersons(1, (int) personRepository.count(), migrated, changed, approved, sortBy, orderBy).getContent();
        if(persons != null && persons.size() > 0){
            XSSFCell headerNameCell = row.createCell(0);
            headerNameCell.setCellValue("Ime");
            XSSFCell headerLastNameCell = row.createCell(1);
            headerLastNameCell.setCellValue("Prezime");
            XSSFCell headerDateCell = row.createCell(2);
            headerDateCell.setCellValue("Datum rodjenja");
            for(Person person : persons){
                int cellnum = 0;
                row = sheet.createRow(rownum++);
                XSSFCell nameCell = row.createCell(cellnum++);
                nameCell.setCellValue(person.getFirstName());
                XSSFCell lastNameCell = row.createCell(cellnum++);
                lastNameCell.setCellValue(person.getLastName());
                XSSFCell dateCell = row.createCell(cellnum++);
                if(person.getDateOfBirth()!=null)
                    dateCell.setCellValue(person.getDateOfBirth().toString("dd.MM.yyyy."));
                else 
                    dateCell.setCellValue("");
            }
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            bos.close();
            return bos.toByteArray();
        }else{
            row = sheet.createRow(rownum++);
            XSSFCell cell = row.createCell(0);
            cell.setCellValue("Ulogovani korisnik ne pripada nijednoj instituciji");
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            bos.close();
            return bos.toByteArray();
        }
    }
      
    public Map<Long, Person> getPersonsByIds(List<Long> ids){
        List<Person> persons = personRepository.findByIdIn(ids);
        return persons.stream().collect(Collectors.toMap((person) -> person.getId(),(person) -> person));
        
    }
}
