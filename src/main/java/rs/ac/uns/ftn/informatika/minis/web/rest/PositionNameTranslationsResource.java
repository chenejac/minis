package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.PositionNameTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.PositionNameTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PositionNameTranslationsSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PositionNameTranslations.
 */
@RestController
@RequestMapping("/api")
public class PositionNameTranslationsResource {

    private final Logger log = LoggerFactory.getLogger(PositionNameTranslationsResource.class);

    @Inject
    private PositionNameTranslationsRepository positionNameTranslationsRepository;

    @Inject
    private PositionNameTranslationsSearchRepository positionNameTranslationsSearchRepository;

    /**
     * POST  /positionNameTranslationss -> Create a new positionNameTranslations.
     */
    @RequestMapping(value = "/positionNameTranslationss",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PositionNameTranslations> createPositionNameTranslations(@RequestBody PositionNameTranslations positionNameTranslations) throws URISyntaxException {
        log.debug("REST request to save PositionNameTranslations : {}", positionNameTranslations);
        if (positionNameTranslations.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new positionNameTranslations cannot already have an ID").body(null);
        }
        PositionNameTranslations result = positionNameTranslationsRepository.save(positionNameTranslations);
        positionNameTranslationsSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/positionNameTranslationss/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("positionNameTranslations", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /positionNameTranslationss -> Updates an existing positionNameTranslations.
     */
    @RequestMapping(value = "/positionNameTranslationss",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PositionNameTranslations> updatePositionNameTranslations(@RequestBody PositionNameTranslations positionNameTranslations) throws URISyntaxException {
        log.debug("REST request to update PositionNameTranslations : {}", positionNameTranslations);
        if (positionNameTranslations.getId() == null) {
            return createPositionNameTranslations(positionNameTranslations);
        }
        PositionNameTranslations result = positionNameTranslationsRepository.save(positionNameTranslations);
        positionNameTranslationsSearchRepository.save(positionNameTranslations);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("positionNameTranslations", positionNameTranslations.getId().toString()))
                .body(result);
    }

    /**
     * GET  /positionNameTranslationss -> get all the positionNameTranslationss.
     */
    @RequestMapping(value = "/positionNameTranslationss",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PositionNameTranslations> getAllPositionNameTranslationss() {
        log.debug("REST request to get all PositionNameTranslationss");
        return positionNameTranslationsRepository.findAll();
    }

    /**
     * GET  /positionNameTranslationss/:id -> get the "id" positionNameTranslations.
     */
    @RequestMapping(value = "/positionNameTranslationss/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PositionNameTranslations> getPositionNameTranslations(@PathVariable Long id) {
        log.debug("REST request to get PositionNameTranslations : {}", id);
        return Optional.ofNullable(positionNameTranslationsRepository.findOne(id))
            .map(positionNameTranslations -> new ResponseEntity<>(
                positionNameTranslations,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /positionNameTranslationss/:id -> delete the "id" positionNameTranslations.
     */
    @RequestMapping(value = "/positionNameTranslationss/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePositionNameTranslations(@PathVariable Long id) {
        log.debug("REST request to delete PositionNameTranslations : {}", id);
        positionNameTranslationsRepository.delete(id);
        positionNameTranslationsSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("positionNameTranslations", id.toString())).build();
    }

    /**
     * SEARCH  /_search/positionNameTranslationss/:query -> search for the positionNameTranslations corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/positionNameTranslationss/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PositionNameTranslations> searchPositionNameTranslationss(@PathVariable String query) {
        return StreamSupport
            .stream(positionNameTranslationsSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
