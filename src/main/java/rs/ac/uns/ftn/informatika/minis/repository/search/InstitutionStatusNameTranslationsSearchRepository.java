package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.InstitutionStatusNameTranslations;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the InstitutionStatusNameTranslations entity.
 */
public interface InstitutionStatusNameTranslationsSearchRepository extends ElasticsearchRepository<InstitutionStatusNameTranslations, Long> {
}
