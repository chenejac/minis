package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.InstitutionType;
import rs.ac.uns.ftn.informatika.minis.repository.InstitutionTypeRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.InstitutionTypeSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

/**
 * REST controller for managing InstitutionType.
 */
@RestController
@RequestMapping("/api")
public class InstitutionTypeResource {

    private final Logger log = LoggerFactory.getLogger(InstitutionTypeResource.class);

    @Inject
    private InstitutionTypeRepository institutionTypeRepository;

    @Inject
    private InstitutionTypeSearchRepository institutionTypeSearchRepository;

    /**
     * POST /institutionTypes -> Create a new institutionType.
     */
    @RequestMapping(value = "/institutionTypes",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstitutionType> createInstitutionType(@RequestBody InstitutionType institutionType) throws URISyntaxException {
        log.debug("REST request to save InstitutionType : {}", institutionType);
        if (institutionType.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new institutionType cannot already have an ID").body(null);
        }
        InstitutionType result = institutionTypeRepository.save(institutionType);
        institutionTypeSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/institutionTypes/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("institutionType", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT /institutionTypes -> Updates an existing institutionType.
     */
    @RequestMapping(value = "/institutionTypes",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstitutionType> updateInstitutionType(@RequestBody InstitutionType institutionType) throws URISyntaxException {
        log.debug("REST request to update InstitutionType : {}", institutionType);
        if (institutionType.getId() == null) {
            return createInstitutionType(institutionType);
        }
        InstitutionType result = institutionTypeRepository.save(institutionType);
        institutionTypeSearchRepository.save(institutionType);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("institutionType", institutionType.getId().toString()))
                .body(result);
    }

    /**
     * GET /institutionTypes -> get all the institutionTypes.
     */
    @RequestMapping(value = "/institutionTypes",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<?> getAllInstitutionTypes(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
            @RequestParam(value = "pageSize", defaultValue = "20") int pageSize) {
        log.debug("REST request to get all institutionTypes");
        if (pageSize < 1) {
            String message = "pageSize can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        if (pageNum < 1) {
            String message = "pageNum can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        PageRequest pageRequest = new PageRequest(pageNum - 1, pageSize);
        Page page = institutionTypeRepository.findAll(pageRequest);
        return ResponseEntity.ok().body(page);
    }

    /**
     * GET /institutionTypes/:id -> get the "id" institutionType.
     */
    @RequestMapping(value = "/institutionTypes/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstitutionType> getInstitutionType(@PathVariable Long id) {
        log.debug("REST request to get InstitutionType : {}", id);
        return Optional.ofNullable(institutionTypeRepository.findOne(id))
                .map(institutionType -> new ResponseEntity<>(
                                institutionType,
                                HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE /institutionTypes/:id -> delete the "id" institutionType.
     */
    @RequestMapping(value = "/institutionTypes/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteInstitutionType(@PathVariable Long id) {
        log.debug("REST request to delete InstitutionType : {}", id);
        institutionTypeRepository.delete(id);
        institutionTypeSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("institutionType", id.toString())).build();
    }

    /**
     * SEARCH /_search/institutionTypes/:query -> search for the institutionType
     * corresponding to the query.
     */
    @RequestMapping(value = "/_search/institutionTypes/{query}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<InstitutionType> searchInstitutionTypes(@PathVariable String query) {
        return StreamSupport
                .stream(institutionTypeSearchRepository.search(queryString(query)).spliterator(), false)
                .collect(Collectors.toList());
    }

    /**
     * GET /_search/institutionTypes/byName?name={zip} -> get institutionType by
     * name.
     */
    @RequestMapping(value = "/_search/institutionTypes/byName/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getInstitutionTypeByZip(@RequestParam(value = "name") String name) {
        log.info("REST request to get institutionType with name : {}", name);
        return Optional.ofNullable(institutionTypeRepository.findInstitutionTypeByName(name))
                .map(inst -> new ResponseEntity<>(
                                inst,
                                HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
