package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.UserGroup;
import rs.ac.uns.ftn.informatika.minis.repository.UserGroupRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.UserGroupSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing UserGroup.
 */
@RestController
@RequestMapping("/api")
public class UserGroupResource {

    private final Logger log = LoggerFactory.getLogger(UserGroupResource.class);

    @Inject
    private UserGroupRepository userGroupRepository;

    @Inject
    private UserGroupSearchRepository userGroupSearchRepository;

    /**
     * POST  /userGroups -> Create a new userGroup.
     */
    @RequestMapping(value = "/userGroups",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserGroup> createUserGroup(@RequestBody UserGroup userGroup) throws URISyntaxException {
        log.debug("REST request to save UserGroup : {}", userGroup);
        if (userGroup.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new userGroup cannot already have an ID").body(null);
        }
        UserGroup result = userGroupRepository.save(userGroup);
        userGroupSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/userGroups/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("userGroup", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /userGroups -> Updates an existing userGroup.
     */
    @RequestMapping(value = "/userGroups",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserGroup> updateUserGroup(@RequestBody UserGroup userGroup) throws URISyntaxException {
        log.debug("REST request to update UserGroup : {}", userGroup);
        if (userGroup.getId() == null) {
            return createUserGroup(userGroup);
        }
        UserGroup result = userGroupRepository.save(userGroup);
        userGroupSearchRepository.save(userGroup);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("userGroup", userGroup.getId().toString()))
                .body(result);
    }

    /**
     * GET  /userGroups -> get all the userGroups.
     */
    @RequestMapping(value = "/userGroups",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<UserGroup> getAllUserGroups() {
        log.debug("REST request to get all UserGroups");
        return userGroupRepository.findAllWithEagerRelationships();
    }

    /**
     * GET  /userGroups/:id -> get the "id" userGroup.
     */
    @RequestMapping(value = "/userGroups/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserGroup> getUserGroup(@PathVariable Long id) {
        log.debug("REST request to get UserGroup : {}", id);
        return Optional.ofNullable(userGroupRepository.findOneWithEagerRelationships(id))
            .map(userGroup -> new ResponseEntity<>(
                userGroup,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /userGroups/:id -> delete the "id" userGroup.
     */
    @RequestMapping(value = "/userGroups/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteUserGroup(@PathVariable Long id) {
        log.debug("REST request to delete UserGroup : {}", id);
        userGroupRepository.delete(id);
        userGroupSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("userGroup", id.toString())).build();
    }

    /**
     * SEARCH  /_search/userGroups/:query -> search for the userGroup corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/userGroups/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<UserGroup> searchUserGroups(@PathVariable String query) {
        return StreamSupport
            .stream(userGroupSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
