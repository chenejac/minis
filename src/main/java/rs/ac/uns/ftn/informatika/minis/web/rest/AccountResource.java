package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.User;
import rs.ac.uns.ftn.informatika.minis.repository.UserRepository;
import rs.ac.uns.ftn.informatika.minis.security.SecurityUtils;
import rs.ac.uns.ftn.informatika.minis.service.MailService;
import rs.ac.uns.ftn.informatika.minis.service.UserService;
import rs.ac.uns.ftn.informatika.minis.web.rest.dto.KeyAndPasswordDTO;
import rs.ac.uns.ftn.informatika.minis.web.rest.dto.UserDTO;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;
import rs.ac.uns.ftn.informatika.minis.service.util.RandomUtil;
import rs.ac.uns.ftn.informatika.minis.web.rest.dto.OldAndNewPasswordDTO;

/**
 * REST controller for managing the current user's account.
 */
@RestController
@RequestMapping("/api")
public class AccountResource {

    private final Logger log = LoggerFactory.getLogger(AccountResource.class);

    @Inject
    private UserRepository userRepository;

    @Inject
    private UserService userService;

    @Inject
    private MailService mailService;

    /**
     * POST /register -> register the user.
     */
    @RequestMapping(value = "/register",
            method = RequestMethod.POST,
            produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    public ResponseEntity<?> registerAccount(@Valid @RequestBody UserDTO userDTO, HttpServletRequest request) {
        return userRepository.findOneByLogin(userDTO.getLogin())
                .map(user -> ResponseEntity.badRequest().body("Korisničko ime već postoji."))
                .orElseGet(() -> userRepository.findOneByEmail(userDTO.getEmail())
                        .map(user -> ResponseEntity.badRequest().body("E-Mail nalog je već u upotrebi."))
                        .orElseGet(() -> {
                            String password = RandomUtil.generatePassword();
                            User user = userService.createUserInformation(userDTO.getLogin(), password,
                                    userDTO.getFirstName(), userDTO.getLastName(), userDTO.getEmail().toLowerCase(),
                                    userDTO.getLangKey(), userDTO.getAuthorities(), userDTO.getPersonId(), userDTO.getInstitutionId());
                            String baseUrl = request.getScheme() + // "http"
                            "://" + // "://"
                            request.getServerName() + // "myhost"
                            ":" + // ":"
                            request.getServerPort();               // "80"

                            mailService.sendPasswordToEmail(user, baseUrl, password);
                            return new ResponseEntity<>(HttpStatus.CREATED);
                        })
                );
    }

    /**
     * GET /activate -> activate the registered user.
     */
    @RequestMapping(value = "/activate",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<String> activateAccount(@RequestParam(value = "key") String key) {
        return Optional.ofNullable(userService.activateRegistration(key))
                .map(user -> new ResponseEntity<String>(HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    /**
     * GET /authenticate -> check if the user is authenticated, and return its
     * login.
     */
    @RequestMapping(value = "/authenticate",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public String isAuthenticated(HttpServletRequest request) {
        log.debug("REST request to check if the current user is authenticated");
        return request.getRemoteUser();
    }

    /**
     * GET /account -> get the current user.
     */
    @RequestMapping(value = "/account",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserDTO> getAccount() {
        return Optional.ofNullable(userService.getUserWithAuthorities())
                .map(user -> new ResponseEntity<>(new UserDTO(user), HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    /**
     * POST /account -> update the current user information.
     */
    @RequestMapping(value = "/account",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<String> saveAccount(@RequestBody UserDTO userDTO) {
        return userRepository
                .findOneByLogin(userDTO.getLogin())
                .filter(u -> u.getLogin().equals(SecurityUtils.getCurrentLogin()))
                .map(u -> {
                    userService.updateUserInformation(userDTO.getFirstName(), userDTO.getLastName(), userDTO.getEmail(),
                            userDTO.getLangKey());
                    return new ResponseEntity<String>(HttpStatus.OK);
                })
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    /**
     * POST /change_password -> changes the current user's password
     */
    @RequestMapping(value = "/account/change_password",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<?> changePassword(@RequestBody OldAndNewPasswordDTO data) {
        //log.info("old : " + oldPassword + " new : " + newPassword + " ex : " + !checkPasswordLength(newPassword));
        if (!checkPasswordLength(data.getNewPassword())) {
            return new ResponseEntity<>("{\"status\":\"NEW_BAD_FORMED\"}", HttpStatus.BAD_REQUEST);
        }
        try {
            userService.changePassword(data.getNewPassword(), data.getOldPassword());
        } catch (RuntimeException ex) {
            return new ResponseEntity<>("{\"status\":\"OLD_BAD_FORMED\"}", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("{\"status\":\"OK\"}", HttpStatus.OK);
    }

    @RequestMapping(value = "/account/reset_password/init",
            method = RequestMethod.POST,
            produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    public ResponseEntity<?> requestPasswordReset(@RequestBody String mail, HttpServletRequest request) {
        String[] mailParsed = mail.split("=");

        if (mailParsed[1] != null && !mailParsed[1].equals("")) {
            mail = mailParsed[1].replaceFirst("%40", "@");
            return userService.requestPasswordReset(mail)
                    .map(user -> {
                        String baseUrl = request.getScheme()
                        + "://"
                        + request.getServerName()
                        + ":"
                        + request.getServerPort();
                        mailService.sendPasswordResetMail(user, baseUrl);
                        return new ResponseEntity<>("e-mail was sent", HttpStatus.OK);
                    }).orElse(new ResponseEntity<>("e-mail address not registered", HttpStatus.BAD_REQUEST));
        } else {
            return new ResponseEntity<>("e-mail address not registered", HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/account/reset_password/finish",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<String> finishPasswordReset(@RequestBody KeyAndPasswordDTO keyAndPassword) {
        if (!checkPasswordLength(keyAndPassword.getNewPassword())) {
            return new ResponseEntity<>("Incorrect password", HttpStatus.BAD_REQUEST);
        }
        return userService.completePasswordReset(keyAndPassword.getNewPassword(), keyAndPassword.getKey())
                .map(user -> new ResponseEntity<String>(HttpStatus.OK)).orElse(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    private boolean checkPasswordLength(String password) {
        return (!StringUtils.isEmpty(password)
                && password.length() >= UserDTO.PASSWORD_MIN_LENGTH
                && password.length() <= UserDTO.PASSWORD_MAX_LENGTH);
    }

    @RequestMapping(value = "/account/new_password",
            method = RequestMethod.POST,
            produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    public ResponseEntity<?> requestNewPassword(@RequestBody String mail, HttpServletRequest request) {
        String[] mailParsed = mail.split("=");

        Random rn = new Random();
        Integer nesto = rn.nextInt(999999 - 100000 + 1) + 100000;

        if (mailParsed[1] != null && !mailParsed[1].equals("")) {
            mail = mailParsed[1].replaceFirst("%40", "@");
            return userRepository.findOneByEmail(mail).map(user -> {
                userService.forceChangePassword(nesto.toString(), user.getId());
                mailService.sendNewPasswordMail(user, nesto.toString());
                return new ResponseEntity<>("e-mail was sent", HttpStatus.OK);
            }).orElse(new ResponseEntity<>("e-mail address not registered", HttpStatus.BAD_REQUEST));
        } else {
            return new ResponseEntity<>("e-mail address not registered", HttpStatus.BAD_REQUEST);
        }

    }

}
