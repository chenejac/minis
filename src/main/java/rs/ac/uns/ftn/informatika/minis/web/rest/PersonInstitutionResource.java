package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.PersonInstitution;
import rs.ac.uns.ftn.informatika.minis.repository.PersonInstitutionRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PersonInstitutionSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import rs.ac.uns.ftn.informatika.minis.domain.Institution;
import rs.ac.uns.ftn.informatika.minis.domain.Person;
import rs.ac.uns.ftn.informatika.minis.repository.InstitutionRepository;
import rs.ac.uns.ftn.informatika.minis.repository.PersonRepository;

/**
 * REST controller for managing PersonInstitution.
 */
@RestController
@RequestMapping("/api")
public class PersonInstitutionResource {

    private final Logger log = LoggerFactory.getLogger(PersonInstitutionResource.class);

    @Inject
    private PersonInstitutionRepository personInstitutionRepository;

    @Inject
    private PersonInstitutionSearchRepository personInstitutionSearchRepository;

    @Inject
    private PersonRepository personRepository;
    
    @Inject
    private InstitutionRepository institutionRepository;
    /**
     * POST  /personInstitutions -> Create a new personInstitution.
     */
    @RequestMapping(value = "/personInstitutions",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonInstitution> createPersonInstitution(@RequestBody PersonInstitution personInstitution) throws URISyntaxException {
        log.debug("REST request to save PersonInstitution : {}", personInstitution);
        if (personInstitution.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new personInstitution cannot already have an ID").body(null);
        }
        PersonInstitution result = personInstitutionRepository.save(personInstitution);
        personInstitutionSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/personInstitutions/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("personInstitution", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /personInstitutions -> Updates an existing personInstitution.
     */
    @RequestMapping(value = "/personInstitutions",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonInstitution> updatePersonInstitution(@RequestBody PersonInstitution personInstitution) throws URISyntaxException {
        log.debug("REST request to update PersonInstitution : {}", personInstitution);
        if (personInstitution.getId() == null) {
            return createPersonInstitution(personInstitution);
        }
        PersonInstitution result = personInstitutionRepository.save(personInstitution);
        personInstitutionSearchRepository.save(personInstitution);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("personInstitution", personInstitution.getId().toString()))
                .body(result);
    }

    /**
     * GET  /personInstitutions -> get all the personInstitutions.
     */
    @RequestMapping(value = "/personInstitutions",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PersonInstitution> getAllPersonInstitutions() {
        log.debug("REST request to get all PersonInstitutions");
        return personInstitutionRepository.findAll();
    }

    /**
     * GET  /personInstitutions/:id -> get the "id" personInstitution.
     */
    @RequestMapping(value = "/personInstitutions/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonInstitution> getPersonInstitution(@PathVariable Long id) {
        log.debug("REST request to get PersonInstitution : {}", id);
        return Optional.ofNullable(personInstitutionRepository.findOne(id))
            .map(personInstitution -> new ResponseEntity<>(
                personInstitution,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /personInstitutions/:id -> delete the "id" personInstitution.
     */
    @RequestMapping(value = "/personInstitutions/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePersonInstitution(@PathVariable Long id) {
        log.debug("REST request to delete PersonInstitution : {}", id);
        personInstitutionRepository.delete(id);
        personInstitutionSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("personInstitution", id.toString())).build();
    }

    /**
     * SEARCH  /_search/personInstitutions/:query -> search for the personInstitution corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/personInstitutions/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PersonInstitution> searchPersonInstitutions(@PathVariable String query) {
        return StreamSupport
            .stream(personInstitutionSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
    
    @RequestMapping(value = "/personsInstitutionByPerson",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Page<Institution>> getPersonInstitutionsForPerson(@RequestParam(value = "person") Long personId, 
                                                @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                                @RequestParam(value = "pageSize", defaultValue = "20") int pageSize) {
        if(personId==null)
        {
            String message = "Person id is required.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        if(pageSize < 1)
        {
            String message = "pageSize can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        if(pageNum < 1)
        {
            String message = "pageNum can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        Person person = personRepository.findOneWithEagerRelationships(personId);
        PageRequest pageReq = new PageRequest(pageNum - 1, pageSize);
        Page page = personInstitutionRepository.findPersonInstitutionByPerson(person, pageReq);
        return ResponseEntity.ok().body(page);
    }
    
    @RequestMapping(value = "/personsInstitutionByInstitution",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Page<Institution>> getPersonInstitutionsForInstitution(@RequestParam(value = "institution") Long institutionId, 
                                                @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                                @RequestParam(value = "pageSize", defaultValue = "20") int pageSize) {
        if(institutionId==null)
        {
            String message = "Institution id is required.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        if(pageSize < 1)
        {
            String message = "pageSize can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        if(pageNum < 1)
        {
            String message = "pageNum can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        Institution institution = institutionRepository.findOne(institutionId);
        PageRequest pageReq = new PageRequest(pageNum - 1, pageSize);
        Page page = personInstitutionRepository.findPersonInstitutionByInstitution(institution, pageReq);
        return ResponseEntity.ok().body(page);
    }
}
