package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.CategoryNameTranslations;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the CategoryNameTranslations entity.
 */
public interface CategoryNameTranslationsSearchRepository extends ElasticsearchRepository<CategoryNameTranslations, Long> {
}
