/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.uns.ftn.informatika.minis.web.rest.errors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The visibility of this DTO is intentionally made for package only,
 * and it should be only accessed through a DataException instance.
 * @author stefan
 */
class MinisDataErrorDTO extends HashMap<String, List<Serializable>>{

    public void putError(String errorType, Serializable errorData) {
        List<Serializable> currentErrors = this.getOrDefault(errorType, new ArrayList());
        currentErrors.add(errorData);
        this.put(errorType, currentErrors);
    }
    
    public void putAllErrors(String errorType, List<Serializable> errorData) {
        List<Serializable> currentErrors = this.getOrDefault(errorType, new ArrayList());
        currentErrors.addAll(errorData);
        this.put(errorType, currentErrors);
    }
}
