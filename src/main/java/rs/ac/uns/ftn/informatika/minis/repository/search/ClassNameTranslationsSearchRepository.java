package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.ClassNameTranslations;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the ClassNameTranslations entity.
 */
public interface ClassNameTranslationsSearchRepository extends ElasticsearchRepository<ClassNameTranslations, Long> {
}
