package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.PersonStatus;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the PersonStatus entity.
 */
public interface PersonStatusRepository extends JpaRepository<PersonStatus,Long> {

}
