package rs.ac.uns.ftn.informatika.minis.domain;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A InstitutionStatusNameTranslations.
 */
@Entity
@Table(name = "institution_status_name_translations")
@Document(indexName="institutionstatusnametranslations")
public class InstitutionStatusNameTranslations implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    
    @Column(name = "name")
    private String name;
    
    @Column(name = "trans_type")
    private String transType;

    @ManyToOne
    private Language language;

    @ManyToOne
    private InstitutionStatus institutionStatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public InstitutionStatus getInstitutionStatus() {
        return institutionStatus;
    }

    public void setInstitutionStatus(InstitutionStatus institutionStatus) {
        this.institutionStatus = institutionStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        InstitutionStatusNameTranslations institutionStatusNameTranslations = (InstitutionStatusNameTranslations) o;

        if ( ! Objects.equals(id, institutionStatusNameTranslations.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "InstitutionStatusNameTranslations{" +
                "id=" + id +
                ", name='" + name + "'" +
                ", transType='" + transType + "'" +
                '}';
    }
}
