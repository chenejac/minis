/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.uns.ftn.informatika.minis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.informatika.minis.domain.City;

/**
 *
 * @author Igor Milanovic
 */
public interface CityRepository extends JpaRepository<City, Long> {
    
    public City findCityByName(String name);
    
    public City findCityByZipCode(Long zip);

}
