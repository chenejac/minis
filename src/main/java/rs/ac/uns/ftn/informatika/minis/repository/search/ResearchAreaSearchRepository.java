package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.ResearchArea;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the ResearchArea entity.
 */
public interface ResearchAreaSearchRepository extends ElasticsearchRepository<ResearchArea, Long> {
}
