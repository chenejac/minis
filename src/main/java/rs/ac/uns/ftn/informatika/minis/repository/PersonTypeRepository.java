package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.PersonType;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the PersonType entity.
 */
public interface PersonTypeRepository extends JpaRepository<PersonType,Long> {
    
    public PersonType findPersonTypeByName(String name);

}
