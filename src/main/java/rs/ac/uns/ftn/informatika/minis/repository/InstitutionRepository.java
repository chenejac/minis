package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.Institution;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the Institution entity.
 */
public interface InstitutionRepository extends JpaRepository<Institution, Long> {

    @Query("select distinct institution from Institution institution left join fetch institution.activities left join fetch institution.researchAreas left join fetch institution.classifications")
    List<Institution> findAllWithEagerRelationships();

    @Query("select institution from Institution institution left join fetch institution.activities left join fetch institution.researchAreas left join fetch institution.classifications where institution.id =:id")
    Institution findOneWithEagerRelationships(@Param("id") Long id);

    public Page<Institution> findByRecordStatusIn(List<Integer> migrated, Pageable pageable);
    
    public Page<Institution> findByIdAndRecordStatusIn(Long id, List<Integer> migrated, Pageable pageable);
    
    public List<Institution> findByRecordStatusIn(List<Integer> migrated);
    
    public List<Institution> findByIdAndRecordStatusIn(Long id, List<Integer> migrated);
    
    public Page<Institution> findByIdInAndRecordStatusIn(List<Long> ids, List<Integer> migrated, Pageable pageable);
    
    public Institution findInstitutionByNioID(Integer nioId);
    
    public List<Institution> findByIdIn(List<Long> ids);

}
