package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.Function;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Function entity.
 */
public interface FunctionRepository extends JpaRepository<Function, Long> {

    public Function findFunctionByName(String name);
    
    public Function findFunctionByOzContaining(String oz);

}
