package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.PersonCategory;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the PersonCategory entity.
 */
public interface PersonCategorySearchRepository extends ElasticsearchRepository<PersonCategory, Long> {
}
