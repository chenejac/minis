package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.Scheme;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Scheme entity.
 */
public interface SchemeSearchRepository extends ElasticsearchRepository<Scheme, Long> {
}
