/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.uns.ftn.informatika.minis.web.rest.dto;
import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;
import rs.ac.uns.ftn.informatika.minis.domain.Authority;
import rs.ac.uns.ftn.informatika.minis.domain.Institution;
import rs.ac.uns.ftn.informatika.minis.domain.Person;
import rs.ac.uns.ftn.informatika.minis.domain.User;

/**
 *
 * @author stefan
 */
public class UserListDTO{
    @Pattern(regexp = "^[a-z0-9]*$")
    @NotNull
    @Size(min = 1, max = 50)
    private String login;

    @Size(max = 50)
    private String firstName;

    @Size(max = 50)
    private String lastName;

    @Email
    @Size(min = 5, max = 100)
    private String email;

    private boolean activated = false;

    private Set<String> authorities;

    private Long personId;

    private Long institutionId;
    
    private Long id;

    public UserListDTO() {
    }

    public UserListDTO(User user) {
        this(user.getId(), user.getLogin(), user.getFirstName(), user.getLastName(),
                user.getEmail(), user.getActivated(),
                user.getAuthorities().stream().map(Authority::getName)
                .collect(Collectors.toSet()), user.getPerson(),
                user.getInstitution());
    }

    public UserListDTO(Long id, String login, String firstName, String lastName,
            String email, boolean activated, Set<String> authorities,
            Person person, Institution institute) {

        this.id = id;
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.activated = activated;
        this.authorities = authorities;
        if(person != null) {
            this.personId = person.getId();
        }
        if(institute != null) {
            this.institutionId = institute.getId();
        }
    }

    public String getLogin() {
        return login;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public boolean isActivated() {
        return activated;
    }

    public Set<String> getAuthorities() {
        return authorities;
    }

    public Long getPersonId() {
        return personId;
    }

    public Long getInstitutionId() {
        return institutionId;
    }
    
    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "UserDTO{"
                + "login='" + login + '\''
                + ", firstName='" + firstName + '\''
                + ", lastName='" + lastName + '\''
                + ", email='" + email + '\''
                + ", activated=" + activated
                + ", authorities=" + authorities
                + ", personId=" + personId
                + ", institutionId=" + institutionId
                + '}';
    }
}
