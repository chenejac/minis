package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.TitleInstitution;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the TitleInstitution entity.
 */
public interface TitleInstitutionRepository extends JpaRepository<TitleInstitution,Long> {

}
