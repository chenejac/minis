package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.PersonKeywordsTranslations;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the PersonKeywordsTranslations entity.
 */
public interface PersonKeywordsTranslationsSearchRepository extends ElasticsearchRepository<PersonKeywordsTranslations, Long> {
}
