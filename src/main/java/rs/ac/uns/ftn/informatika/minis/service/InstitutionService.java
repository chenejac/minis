/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rs.ac.uns.ftn.informatika.minis.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import rs.ac.uns.ftn.informatika.minis.domain.Authority;
import rs.ac.uns.ftn.informatika.minis.domain.Institution;
import rs.ac.uns.ftn.informatika.minis.domain.PersonInstitution;
import rs.ac.uns.ftn.informatika.minis.domain.User;
import rs.ac.uns.ftn.informatika.minis.repository.InstitutionRepository;
import rs.ac.uns.ftn.informatika.minis.repository.PersonInstitutionRepository;
import rs.ac.uns.ftn.informatika.minis.repository.UserRepository;
import rs.ac.uns.ftn.informatika.minis.security.AuthoritiesConstants;
import rs.ac.uns.ftn.informatika.minis.security.SecurityUtils;
import rs.ac.uns.ftn.informatika.minis.web.rest.errors.DataException.DataExceptionBuilder;

/**
 *
 * @author Milan Deket
 * 
 * Service class for managing institutions.
 */
@Service
@Transactional
public class InstitutionService {
    
    private final Logger log = LoggerFactory.getLogger(InstitutionService.class);
    
    @Inject
    private UserRepository userRepository;
    
    @Inject
    private InstitutionRepository institutionRepository;
    
    @Inject
    private PersonInstitutionRepository personInstitutionRepository;
    
    public Institution save(Institution institution){
        userRepository.findOneByLogin(SecurityUtils.getCurrentLogin()).ifPresent(u-> {
            boolean admin = false;
            boolean institutionAdmin = false;
            for(Authority auth : u.getAuthorities()){
                if(auth.getName().equals("ROLE_ADMIN")){
                    admin = true;
                    break;
                }else if(auth.getName().equals("ROLE_INSTITUTION_ADMIN")){
                    institutionAdmin = true;
                }
            }
            if(u.getPerson() != null){
                if(institution.getId() == null){
                    institution.setCreator(u.getPerson().getFirstName() + " " + u.getPerson().getLastName());
                }else{
                    institution.setModifier(u.getPerson().getFirstName() + " " + u.getPerson().getLastName());
                }                
            }
            if(admin){
                institution.setRecordStatus(3);
            }else if(institutionAdmin){
                institution.setRecordStatus(2);
            }
            institutionRepository.save(institution);
        });
        return institution;
    }
    
    
    public Page<Institution> getAllInstitutions(int pageNum, int pageSize, boolean migrated, boolean changed, boolean approved, String sortBy, String orderBy){
        if(orderBy == null) {
            orderBy = "desc";
        }
        if(sortBy == null) {
            sortBy = "name";
        }
        Sort sort =  new Sort(Direction.fromString(orderBy), sortBy);
        
        PageRequest pageRequest = new PageRequest(pageNum - 1, pageSize, sort);
        User user = userRepository.findOneByLogin(SecurityUtils.getCurrentLogin()).get();
        
        List<Integer> recordStatus = new ArrayList<>();
        if(migrated){
            recordStatus.add(1);
        }
        if(changed){
            recordStatus.add(2);
        }
        if(approved){
            recordStatus.add(3);
        }
        
        if(user.hasAuthority(AuthoritiesConstants.ADMIN)) {
            return institutionRepository.findByRecordStatusIn(recordStatus, pageRequest);
        }
        
        if(user.hasAuthority(AuthoritiesConstants.INSTITUTION_ADMIN)) {
            // Get all personInstitution objects related to the logged Person
            List<PersonInstitution> personInstitutions = personInstitutionRepository.findPersonInstitutionByPerson(user.getPerson());
            if(personInstitutions != null && personInstitutions.size() > 0){
                // Get all insitution ids from found personInstitutions
                List<Long> institutionIds = personInstitutions
                        .stream()
                        .map(personInstitution -> personInstitution.getInstitution().getId())
                        .collect(Collectors.toList());
                // Lookup by ID-IN and recordStatus-IN
                Page<Institution> institutions = institutionRepository.findByIdInAndRecordStatusIn(institutionIds, recordStatus, pageRequest);
                return institutions;
            }
        }
        
        throw DataExceptionBuilder.nonFieldError("Logged person is not assigned to an institution.");
    }
    
    public byte[] getInstitutionsInXLS(boolean migrated, boolean changed, boolean approved, String sortBy, String orderBy) throws IOException{
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Institucije");
        sheet.setColumnWidth(0, 3000);
        sheet.setColumnWidth(1, 3000);
        sheet.setColumnWidth(2, 4500);
        int rownum = 0;
        XSSFRow row = sheet.createRow(rownum++);
        List<Institution> institutions = this.getAllInstitutions(1, institutionRepository.findAll().size(),migrated, changed, approved, sortBy, orderBy).getContent();
        if(institutions != null && institutions.size() > 0){
            XSSFCell headerNameCell = row.createCell(0);
            headerNameCell.setCellValue("Naziv");
            XSSFCell headerLastNameCell = row.createCell(1);
            headerLastNameCell.setCellValue("Mesto");
            XSSFCell headerDateCell = row.createCell(2);
            headerDateCell.setCellValue("Zvanična web adresa");
            for(Institution institution : institutions){
                int cellnum = 0;
                row = sheet.createRow(rownum++);
                XSSFCell nameCell = row.createCell(cellnum++);
                nameCell.setCellValue(institution.getName());
                XSSFCell cityCell = row.createCell(cellnum++);
                if(institution.getCity() != null){
                    cityCell.setCellValue(institution.getCity().getName());
                }else{
                    cityCell.setCellValue("");
                }
                XSSFCell uriCell = row.createCell(cellnum++);
                uriCell.setCellValue(institution.getUri());
            }
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            bos.close();
            return bos.toByteArray();
        }else{
            row = sheet.createRow(rownum++);
            XSSFCell cell = row.createCell(0);
            cell.setCellValue("Ulogovani korisnik ne pripada nijednoj instituciji");
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            bos.close();
            return bos.toByteArray();
        }
    }
   
    public Map<Long, Institution> getInstitutionsByIds(List<Long> ids){
        List<Institution> institutions = institutionRepository.findByIdIn(ids);
        return institutions.stream().collect(Collectors.toMap((institution) -> institution.getId(),(institution) -> institution));
        
    }

    
}
