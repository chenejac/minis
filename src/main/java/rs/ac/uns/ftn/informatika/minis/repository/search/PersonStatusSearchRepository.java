package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.PersonStatus;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the PersonStatus entity.
 */
public interface PersonStatusSearchRepository extends ElasticsearchRepository<PersonStatus, Long> {
}
