package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.ActivityNameTranslations;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ActivityNameTranslations entity.
 */
public interface ActivityNameTranslationsRepository extends JpaRepository<ActivityNameTranslations,Long> {

}
