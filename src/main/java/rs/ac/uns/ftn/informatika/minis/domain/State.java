/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.uns.ftn.informatika.minis.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 *
 * @author Igor Milanovic
 */
@Entity
@Table(name = "state")
@Document(indexName = "state")
public class State implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToOne
    private Language language;
    
    @OneToMany(mappedBy = "state")
    @JsonIgnore
    private Set<City> cities = new HashSet<City>();
    
    @OneToMany(mappedBy = "state")
    @JsonIgnore
    private Set<TownShip> townShips = new HashSet<TownShip>();

    @Column(name = "cities_registered")
    private Boolean citiesRegistered;
    
    public State() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getCitiesRegistered() {
        return citiesRegistered;
    }

    public void setCitiesRegistered(Boolean citiesRegistered) {
        this.citiesRegistered = citiesRegistered;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Set<City> getCities() {
        return cities;
    }

    public void setCities(Set<City> cities) {
        this.cities = cities;
    }

    public Set<TownShip> getTownShips() {
        return townShips;
    }

    public void setTownShips(Set<TownShip> townShips) {
        this.townShips = townShips;
    }
    
    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        State state = (State) o;

        if (!Objects.equals(id, state.id)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "State{" + "id=" + id + ", name=" + name + ", description=" + description + ", language=" + language + '}';
    }

}
