package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.InstitutionStatus;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the InstitutionStatus entity.
 */
public interface InstitutionStatusSearchRepository extends ElasticsearchRepository<InstitutionStatus, Long> {
}
