package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.Privilege;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Privilege entity.
 */
public interface PrivilegeSearchRepository extends ElasticsearchRepository<Privilege, Long> {
}
