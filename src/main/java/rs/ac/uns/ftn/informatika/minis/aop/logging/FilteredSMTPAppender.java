/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rs.ac.uns.ftn.informatika.minis.aop.logging;

import ch.qos.logback.classic.net.SMTPAppender;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.boolex.EventEvaluator;
import ch.qos.logback.core.helpers.CyclicBuffer;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;

/**
 *
 * @author Milan Deket
 */
public class FilteredSMTPAppender extends SMTPAppender{

    private int timeFrame;
    private int maxEMails;
    private int maxBuffer;
    protected long timeFrameMillis;
    protected List<Date> exceptionDates = new ArrayList<Date>();
    
    private final ThreadFactory tf = r -> {
        Thread t = new Thread(r, "ScheduledSMTPAppender Thread");
        t.setDaemon(true); //make daemon or it will prevent your program to exit
        return t;
    };
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1, tf);
    private final List<ILoggingEvent> events = new ArrayList<> ();
    
    public FilteredSMTPAppender() { 
        super(); 
    }
    public FilteredSMTPAppender(EventEvaluator<ILoggingEvent> eventEvaluator) { 
        super(eventEvaluator); 
    }

  
    @Override
    public void start() {
        super.start(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void sendBuffer(CyclicBuffer<ILoggingEvent> cb, ILoggingEvent lastEventObject) {
        cleanTimedoutExceptions();
        if (isSendMailAllowed()){
            events.add(lastEventObject);
            if (events.size() > maxBuffer){
                sendEmail();
            }
            addException();
        }
    }
    
    //needs to be synchronized for thread safety
    private synchronized void sendEmail() {
        try {
            if (events.isEmpty()) {
                return;
            }
            ILoggingEvent lastEvent = events.get(events.size() - 1);
            events.remove(events.size() - 1);
            CyclicBuffer<ILoggingEvent> cb;
            if (events.isEmpty()) {
                cb = new CyclicBuffer<>(1);
            } else {
                cb = new CyclicBuffer<>(events.size());
                events.stream().forEach((e) -> {
                    cb.add(e);
                });
            }
            super.sendBuffer(cb, lastEvent);
            events.clear();
        } catch (Exception e) {
            //Important to have a catch all here or the scheduled task will die
            addError("Error occurred while sending e-mail notification.", e);
        }
    }
    
    public int getTimeFrame() {
      return timeFrame;
    }
    
    public void setTimeFrame(int timeFrame) {
      this.timeFrame = timeFrame;
    }
    
    public int getMaxEMails() {
      return maxEMails;
    }
    
    public void setMaxEMails(int maxEMails) {
      this.maxEMails = maxEMails;
    }

    public int getMaxBuffer() {
        return maxBuffer;
    }

    public void setMaxBuffer(int maxBuffer) {
        this.maxBuffer = maxBuffer;
    }
    
    protected void cleanTimedoutExceptions(){
        Date current = new Date();
        // Remove timedout exceptions
        Iterator<Date> itr = exceptionDates.iterator();
        while (itr.hasNext())
        {
          Date exceptionDate = itr.next();
          if (current.getTime() - exceptionDate.getTime() > (timeFrame * 60 * 1000))
          {
            itr.remove();
          }
          else
          {
            break;
          }
        }
    }
    
    protected void addException(){
      exceptionDates.add(new Date());
    }
    
    protected boolean isSendMailAllowed(){
        return exceptionDates.size() <= maxEMails*maxBuffer;
    }
  
}
