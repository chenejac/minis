package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.PositionNameTranslations;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the PositionNameTranslations entity.
 */
public interface PositionNameTranslationsRepository extends JpaRepository<PositionNameTranslations,Long> {

}
