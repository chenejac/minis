package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.TitleInstitution;
import rs.ac.uns.ftn.informatika.minis.repository.TitleInstitutionRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.TitleInstitutionSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing TitleInstitution.
 */
@RestController
@RequestMapping("/api")
public class TitleInstitutionResource {

    private final Logger log = LoggerFactory.getLogger(TitleInstitutionResource.class);

    @Inject
    private TitleInstitutionRepository titleInstitutionRepository;

    @Inject
    private TitleInstitutionSearchRepository titleInstitutionSearchRepository;

    /**
     * POST  /titleInstitutions -> Create a new titleInstitution.
     */
    @RequestMapping(value = "/titleInstitutions",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TitleInstitution> createTitleInstitution(@RequestBody TitleInstitution titleInstitution) throws URISyntaxException {
        log.debug("REST request to save TitleInstitution : {}", titleInstitution);
        if (titleInstitution.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new titleInstitution cannot already have an ID").body(null);
        }
        TitleInstitution result = titleInstitutionRepository.save(titleInstitution);
        titleInstitutionSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/titleInstitutions/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("titleInstitution", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /titleInstitutions -> Updates an existing titleInstitution.
     */
    @RequestMapping(value = "/titleInstitutions",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TitleInstitution> updateTitleInstitution(@RequestBody TitleInstitution titleInstitution) throws URISyntaxException {
        log.debug("REST request to update TitleInstitution : {}", titleInstitution);
        if (titleInstitution.getId() == null) {
            return createTitleInstitution(titleInstitution);
        }
        TitleInstitution result = titleInstitutionRepository.save(titleInstitution);
        titleInstitutionSearchRepository.save(titleInstitution);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("titleInstitution", titleInstitution.getId().toString()))
                .body(result);
    }

    /**
     * GET  /titleInstitutions -> get all the titleInstitutions.
     */
    @RequestMapping(value = "/titleInstitutions",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<TitleInstitution> getAllTitleInstitutions() {
        log.debug("REST request to get all TitleInstitutions");
        return titleInstitutionRepository.findAll();
    }

    /**
     * GET  /titleInstitutions/:id -> get the "id" titleInstitution.
     */
    @RequestMapping(value = "/titleInstitutions/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TitleInstitution> getTitleInstitution(@PathVariable Long id) {
        log.debug("REST request to get TitleInstitution : {}", id);
        return Optional.ofNullable(titleInstitutionRepository.findOne(id))
            .map(titleInstitution -> new ResponseEntity<>(
                titleInstitution,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /titleInstitutions/:id -> delete the "id" titleInstitution.
     */
    @RequestMapping(value = "/titleInstitutions/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTitleInstitution(@PathVariable Long id) {
        log.debug("REST request to delete TitleInstitution : {}", id);
        titleInstitutionRepository.delete(id);
        titleInstitutionSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("titleInstitution", id.toString())).build();
    }

    /**
     * SEARCH  /_search/titleInstitutions/:query -> search for the titleInstitution corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/titleInstitutions/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<TitleInstitution> searchTitleInstitutions(@PathVariable String query) {
        return StreamSupport
            .stream(titleInstitutionSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
