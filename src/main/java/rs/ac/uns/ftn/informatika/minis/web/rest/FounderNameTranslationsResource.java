package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.FounderNameTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.FounderNameTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.FounderNameTranslationsSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing FounderNameTranslations.
 */
@RestController
@RequestMapping("/api")
public class FounderNameTranslationsResource {

    private final Logger log = LoggerFactory.getLogger(FounderNameTranslationsResource.class);

    @Inject
    private FounderNameTranslationsRepository founderNameTranslationsRepository;

    @Inject
    private FounderNameTranslationsSearchRepository founderNameTranslationsSearchRepository;

    /**
     * POST  /founderNameTranslationss -> Create a new founderNameTranslations.
     */
    @RequestMapping(value = "/founderNameTranslationss",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FounderNameTranslations> createFounderNameTranslations(@RequestBody FounderNameTranslations founderNameTranslations) throws URISyntaxException {
        log.debug("REST request to save FounderNameTranslations : {}", founderNameTranslations);
        if (founderNameTranslations.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new founderNameTranslations cannot already have an ID").body(null);
        }
        FounderNameTranslations result = founderNameTranslationsRepository.save(founderNameTranslations);
        founderNameTranslationsSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/founderNameTranslationss/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("founderNameTranslations", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /founderNameTranslationss -> Updates an existing founderNameTranslations.
     */
    @RequestMapping(value = "/founderNameTranslationss",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FounderNameTranslations> updateFounderNameTranslations(@RequestBody FounderNameTranslations founderNameTranslations) throws URISyntaxException {
        log.debug("REST request to update FounderNameTranslations : {}", founderNameTranslations);
        if (founderNameTranslations.getId() == null) {
            return createFounderNameTranslations(founderNameTranslations);
        }
        FounderNameTranslations result = founderNameTranslationsRepository.save(founderNameTranslations);
        founderNameTranslationsSearchRepository.save(founderNameTranslations);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("founderNameTranslations", founderNameTranslations.getId().toString()))
                .body(result);
    }

    /**
     * GET  /founderNameTranslationss -> get all the founderNameTranslationss.
     */
    @RequestMapping(value = "/founderNameTranslationss",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<FounderNameTranslations> getAllFounderNameTranslationss() {
        log.debug("REST request to get all FounderNameTranslationss");
        return founderNameTranslationsRepository.findAll();
    }

    /**
     * GET  /founderNameTranslationss/:id -> get the "id" founderNameTranslations.
     */
    @RequestMapping(value = "/founderNameTranslationss/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FounderNameTranslations> getFounderNameTranslations(@PathVariable Long id) {
        log.debug("REST request to get FounderNameTranslations : {}", id);
        return Optional.ofNullable(founderNameTranslationsRepository.findOne(id))
            .map(founderNameTranslations -> new ResponseEntity<>(
                founderNameTranslations,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /founderNameTranslationss/:id -> delete the "id" founderNameTranslations.
     */
    @RequestMapping(value = "/founderNameTranslationss/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteFounderNameTranslations(@PathVariable Long id) {
        log.debug("REST request to delete FounderNameTranslations : {}", id);
        founderNameTranslationsRepository.delete(id);
        founderNameTranslationsSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("founderNameTranslations", id.toString())).build();
    }

    /**
     * SEARCH  /_search/founderNameTranslationss/:query -> search for the founderNameTranslations corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/founderNameTranslationss/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<FounderNameTranslations> searchFounderNameTranslationss(@PathVariable String query) {
        return StreamSupport
            .stream(founderNameTranslationsSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
