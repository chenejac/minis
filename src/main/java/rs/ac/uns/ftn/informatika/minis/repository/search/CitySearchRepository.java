/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.uns.ftn.informatika.minis.repository.search;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import rs.ac.uns.ftn.informatika.minis.domain.City;

/**
 *
 * @author stefan
 */
public interface CitySearchRepository extends ElasticsearchRepository<City, Long>{
    
}
