package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import java.io.IOException;
import rs.ac.uns.ftn.informatika.minis.domain.Institution;
import rs.ac.uns.ftn.informatika.minis.repository.search.InstitutionSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import rs.ac.uns.ftn.informatika.minis.config.Constants;
import rs.ac.uns.ftn.informatika.minis.repository.InstitutionRepository;
import rs.ac.uns.ftn.informatika.minis.service.InstitutionService;
import rs.ac.uns.ftn.informatika.minis.web.rest.errors.DataException.DataExceptionBuilder;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.LatCyrUtils;

/**
 * REST controller for managing Institution.
 */
@RestController
@RequestMapping("/api")
public class InstitutionResource {

    private final Logger log = LoggerFactory.getLogger(InstitutionResource.class);

    @Inject
    private InstitutionRepository institutionRepository;

    @Inject
    private InstitutionSearchRepository institutionSearchRepository;

    @Inject
    private InstitutionService institutionService;
    /**
     * POST  /institutions -> Create a new institution.
     */
    @RequestMapping(value = "/institutions",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Institution> createInstitution(@RequestBody Institution institution) throws URISyntaxException {
        log.debug("REST request to save Institution : {}", institution);
        if (institution.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new institution cannot already have an ID").body(null);
        }
        Institution result = institutionService.save(institution);
        institutionSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/institutions/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("institution", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /institutions -> Updates an existing institution.
     */
    @RequestMapping(value = "/institutions",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Institution> updateInstitution(@RequestBody Institution institution) throws URISyntaxException {
        log.debug("REST request to update Institution : {}", institution);
        if (institution.getId() == null) {
            return createInstitution(institution);
        }
        Institution result = institutionService.save(institution);
        institutionSearchRepository.save(institution);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("institution", institution.getId().toString()))
                .body(result);
    }

    /**
     * GET  /institutions -> get all the institutions.
     */
    @RequestMapping(value = "/institutions",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Page<Institution>> getAllInstitutions(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                @RequestParam(value = "pageSize", defaultValue = "20") int pageSize,
                @RequestParam(value = "migrated", defaultValue = "true") boolean migrated,
                @RequestParam(value = "changed", defaultValue = "true") boolean changed,
                @RequestParam(value = "approved", defaultValue = "true") boolean approved,
                @RequestParam(value = "sortBy", defaultValue = "name") String sortBy,
                @RequestParam(value = "orderBy", defaultValue = "asc") String orderBy) {
        
        Page retVal = institutionService.getAllInstitutions(pageNum, pageSize, migrated, changed, approved, sortBy, orderBy);
        return ResponseEntity.ok().body(retVal);
     }
    
    @RequestMapping(value = "/institutions/all",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Institution>> getAllInstitutionsWithoutPagination() {
        
        log.debug("REST request to get all Institutions");
        List<Institution> retVal = institutionRepository.findAll();
        return ResponseEntity.ok().body(retVal);
        
     }


    /**
     * GET  /institutions/:id -> get the "id" institution.
     */
    @RequestMapping(value = "/institutions/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Institution> getInstitution(@PathVariable Long id) {
        log.debug("REST request to get Institution : {}", id);
        return Optional.ofNullable(institutionRepository.findOneWithEagerRelationships(id))
            .map(institution -> new ResponseEntity<>(
                institution,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    

    
    /**
     * DELETE  /institutions/:id -> delete the "id" institution.
     */
    @RequestMapping(value = "/institutions/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteInstitution(@PathVariable Long id) {
        log.debug("REST request to delete Institution : {}", id);
        institutionRepository.delete(id);
        institutionSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("institution", id.toString())).build();
    }

    /**
     * SEARCH  /_search/institutions/:query -> search for the institution corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/institutions/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Institution> searchInstitutions(@PathVariable String query, @RequestParam(required = false) String[] fields) {
        if(fields == null) {
            fields = new String[] {"name"};
        }
        Pageable page = new PageRequest(0, Constants.DEFAULT_PAGE_SIZE);
        QueryStringQueryBuilder qs = queryString(transformQueryLatin(query));
        for(String field : fields) {
            qs.field(field);
        }
        return StreamSupport
            .stream(institutionSearchRepository.search(qs, page).spliterator(), false)
            .collect(Collectors.toList());
    }
    
    private String transformQueryLatin(String query) {
        // For query format +(<name>* <name>)
        // The following action will transform the string to +(<lower case latin name>* <name>) and will only do so for
        // what is in the parentesis regardless of the rest of the query string.
        String retVal = query;
        int parenQueryIdxStart = query.lastIndexOf('(');
        if (parenQueryIdxStart != -1) {
            String parenQueryString = query.substring(parenQueryIdxStart + 1, query.indexOf('*', parenQueryIdxStart));
            String latinParenQueryString = LatCyrUtils.toLatinUnaccented(parenQueryString.toLowerCase())
                    .replace("dj", "d");
            retVal = query.substring(0, parenQueryIdxStart + 1) + latinParenQueryString + "* " + parenQueryString + ")";
        }
        return retVal;
    }
    
    
    @RequestMapping(value = "/institutions/xls", method = RequestMethod.GET)
    public ResponseEntity<?> getInstitutionsInXLS(@RequestParam(value = "migrated", defaultValue = "true") boolean migrated,
                        @RequestParam(value = "changed", defaultValue = "true") boolean changed,
                        @RequestParam(value = "approved", defaultValue = "true") boolean approved,
                        @RequestParam(value = "sortBy", defaultValue = "lastName") String sortBy,
                        @RequestParam(value = "orderBy", defaultValue = "asc") String orderBy) {
        
            log.debug("REST request to get institutions in XLSX");
            HttpHeaders responseHeaders = new HttpHeaders();
            byte[] retVal;
            try {
                retVal = institutionService.getInstitutionsInXLS(migrated, changed, approved, sortBy, orderBy);
                log.debug("Document created");
                responseHeaders.setContentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
                responseHeaders.setContentDispositionFormData("attachment", "institutions.xlsx");
                return new ResponseEntity<>(retVal,responseHeaders, HttpStatus.OK);
            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(InstitutionResource.class.getName()).log(Level.SEVERE, null, ex);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
    }
}
