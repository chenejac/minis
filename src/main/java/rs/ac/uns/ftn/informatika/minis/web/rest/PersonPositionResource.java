/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.inject.Inject;
import static org.elasticsearch.index.query.QueryBuilders.queryString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.uns.ftn.informatika.minis.domain.PersonPosition;
import rs.ac.uns.ftn.informatika.minis.repository.PersonPositionRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PersonPositionSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;

/**
 *
 */
@RestController
@RequestMapping("/api")
public class PersonPositionResource {
private final Logger log = LoggerFactory.getLogger(PersonPositionResource.class);

    @Inject
    private PersonPositionRepository personPositionRepository;

    @Inject
    private PersonPositionSearchRepository personPositionSearchRepository;

    /**
     * POST  /personPositions -> Create a new personPosition.
     */
    @RequestMapping(value = "/personPositions",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonPosition> createPersonPosition(@RequestBody PersonPosition personPosition) throws URISyntaxException {
        log.debug("REST request to save PersonPosition : {}", personPosition);
        if (personPosition.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new personPosition cannot already have an ID").body(null);
        }
        PersonPosition result = personPositionRepository.save(personPosition);
        personPositionSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/personPositions/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("personPosition", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /personPositions -> Updates an existing personPosition.
     */
    @RequestMapping(value = "/personPositions",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonPosition> updatePersonPosition(@RequestBody PersonPosition personPosition) throws URISyntaxException {
        log.debug("REST request to update PersonPosition : {}", personPosition);
        if (personPosition.getId() == null) {
            return createPersonPosition(personPosition);
        }
        PersonPosition result = personPositionRepository.save(personPosition);
        personPositionSearchRepository.save(personPosition);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("personPosition", personPosition.getId().toString()))
                .body(result);
    }

    /**
     * GET  /personPositions -> get all the personPositions.
     */
    @RequestMapping(value = "/personPositions",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Page<PersonPosition>> getAllPersonPositions(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                                @RequestParam(value = "pageSize", defaultValue = "20") int pageSize) {
        log.debug("REST request to get all PersonPositions");
        if(pageSize < 1)
        {
            String message = "pageSize can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        if(pageNum < 1)
        {
            String message = "pageNum can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        PageRequest pageRequest = new PageRequest(pageNum - 1, pageSize);
        Page page = personPositionRepository.findAll(pageRequest);
        return ResponseEntity.ok().body(page);
    }

    /**
     * GET  /personPositions/:id -> get the "id" personPosition.
     */
    @RequestMapping(value = "/personPositions/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonPosition> getPersonPositions(@PathVariable Long id) {
        log.debug("REST request to get PersonPosition : {}", id);
        return Optional.ofNullable(personPositionRepository.findOne(id))
            .map(personPosition -> new ResponseEntity<>(
                personPosition,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /personPositions/:id -> delete the "id" personPosition.
     */
    @RequestMapping(value = "/personPositions/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePersonPosition(@PathVariable Long id) {
        log.debug("REST request to delete PersonPosition : {}", id);
        personPositionRepository.delete(id);
        personPositionSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("personPosition", id.toString())).build();
    }

    /**
     * SEARCH  /_search/personPositions/:query -> search for the personPosition corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/personPositions/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PersonPosition> searchPersonPositions(@PathVariable String query) {
        return StreamSupport
            .stream(personPositionSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
    
    
    /**
     * GET  /personPositions/:id/for_person -> get person positions for "id" person.
     */
    @RequestMapping(value = "/personPositions/{id}/for_person",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PersonPosition> getPositionsForPerson(@PathVariable Long id) {
        List<PersonPosition> positions = personPositionRepository.findForPerson(id);
        return positions;
    }
}
