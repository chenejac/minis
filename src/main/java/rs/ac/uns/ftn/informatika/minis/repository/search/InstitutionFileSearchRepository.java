package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.InstitutionFile;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the InstitutionFile entity.
 */
public interface InstitutionFileSearchRepository extends ElasticsearchRepository<InstitutionFile, Long> {
}
