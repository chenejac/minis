package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.PersonFile;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the PersonFile entity.
 */
public interface PersonFileSearchRepository extends ElasticsearchRepository<PersonFile, Long> {
}
