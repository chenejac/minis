package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.PersonTypeNameTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.PersonTypeNameTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PersonTypeNameTranslationsSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PersonTypeNameTranslations.
 */
@RestController
@RequestMapping("/api")
public class PersonTypeNameTranslationsResource {

    private final Logger log = LoggerFactory.getLogger(PersonTypeNameTranslationsResource.class);

    @Inject
    private PersonTypeNameTranslationsRepository personTypeNameTranslationsRepository;

    @Inject
    private PersonTypeNameTranslationsSearchRepository personTypeNameTranslationsSearchRepository;

    /**
     * POST  /personTypeNameTranslationss -> Create a new personTypeNameTranslations.
     */
    @RequestMapping(value = "/personTypeNameTranslationss",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonTypeNameTranslations> createPersonTypeNameTranslations(@RequestBody PersonTypeNameTranslations personTypeNameTranslations) throws URISyntaxException {
        log.debug("REST request to save PersonTypeNameTranslations : {}", personTypeNameTranslations);
        if (personTypeNameTranslations.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new personTypeNameTranslations cannot already have an ID").body(null);
        }
        PersonTypeNameTranslations result = personTypeNameTranslationsRepository.save(personTypeNameTranslations);
        personTypeNameTranslationsSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/personTypeNameTranslationss/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("personTypeNameTranslations", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /personTypeNameTranslationss -> Updates an existing personTypeNameTranslations.
     */
    @RequestMapping(value = "/personTypeNameTranslationss",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonTypeNameTranslations> updatePersonTypeNameTranslations(@RequestBody PersonTypeNameTranslations personTypeNameTranslations) throws URISyntaxException {
        log.debug("REST request to update PersonTypeNameTranslations : {}", personTypeNameTranslations);
        if (personTypeNameTranslations.getId() == null) {
            return createPersonTypeNameTranslations(personTypeNameTranslations);
        }
        PersonTypeNameTranslations result = personTypeNameTranslationsRepository.save(personTypeNameTranslations);
        personTypeNameTranslationsSearchRepository.save(personTypeNameTranslations);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("personTypeNameTranslations", personTypeNameTranslations.getId().toString()))
                .body(result);
    }

    /**
     * GET  /personTypeNameTranslationss -> get all the personTypeNameTranslationss.
     */
    @RequestMapping(value = "/personTypeNameTranslationss",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PersonTypeNameTranslations> getAllPersonTypeNameTranslationss() {
        log.debug("REST request to get all PersonTypeNameTranslationss");
        return personTypeNameTranslationsRepository.findAll();
    }

    /**
     * GET  /personTypeNameTranslationss/:id -> get the "id" personTypeNameTranslations.
     */
    @RequestMapping(value = "/personTypeNameTranslationss/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonTypeNameTranslations> getPersonTypeNameTranslations(@PathVariable Long id) {
        log.debug("REST request to get PersonTypeNameTranslations : {}", id);
        return Optional.ofNullable(personTypeNameTranslationsRepository.findOne(id))
            .map(personTypeNameTranslations -> new ResponseEntity<>(
                personTypeNameTranslations,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /personTypeNameTranslationss/:id -> delete the "id" personTypeNameTranslations.
     */
    @RequestMapping(value = "/personTypeNameTranslationss/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePersonTypeNameTranslations(@PathVariable Long id) {
        log.debug("REST request to delete PersonTypeNameTranslations : {}", id);
        personTypeNameTranslationsRepository.delete(id);
        personTypeNameTranslationsSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("personTypeNameTranslations", id.toString())).build();
    }

    /**
     * SEARCH  /_search/personTypeNameTranslationss/:query -> search for the personTypeNameTranslations corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/personTypeNameTranslationss/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PersonTypeNameTranslations> searchPersonTypeNameTranslationss(@PathVariable String query) {
        return StreamSupport
            .stream(personTypeNameTranslationsSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
