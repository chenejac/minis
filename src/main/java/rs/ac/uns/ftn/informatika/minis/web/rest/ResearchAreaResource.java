package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.ResearchArea;
import rs.ac.uns.ftn.informatika.minis.repository.ResearchAreaRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.ResearchAreaSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

/**
 * REST controller for managing ResearchArea.
 */
@RestController
@RequestMapping("/api")
public class ResearchAreaResource {

    private final Logger log = LoggerFactory.getLogger(ResearchAreaResource.class);

    @Inject
    private ResearchAreaRepository researchAreaRepository;

    @Inject
    private ResearchAreaSearchRepository researchAreaSearchRepository;

    /**
     * POST  /researchAreas -> Create a new researchArea.
     */
    @RequestMapping(value = "/researchAreas",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ResearchArea> createResearchArea(@RequestBody ResearchArea researchArea) throws URISyntaxException {
        log.debug("REST request to save ResearchArea : {}", researchArea);
        if (researchArea.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new researchArea cannot already have an ID").body(null);
        }
        ResearchArea result = researchAreaRepository.save(researchArea);
        researchAreaSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/researchAreas/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("researchArea", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /researchAreas -> Updates an existing researchArea.
     */
    @RequestMapping(value = "/researchAreas",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ResearchArea> updateResearchArea(@RequestBody ResearchArea researchArea) throws URISyntaxException {
        log.debug("REST request to update ResearchArea : {}", researchArea);
        if (researchArea.getId() == null) {
            return createResearchArea(researchArea);
        }
        ResearchArea result = researchAreaRepository.save(researchArea);
        researchAreaSearchRepository.save(researchArea);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("researchArea", researchArea.getId().toString()))
                .body(result);
    }

    /**
     * GET  /researchAreas -> get all the researchAreas.
     */
    @RequestMapping(value = "/researchAreas",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Page<ResearchArea>> getAllResearchAreas(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                                @RequestParam(value = "pageSize", defaultValue = "20") int pageSize) {
        log.debug("REST request to get all ResearchAreas");
        if(pageSize < 1)
        {
            String message = "pageSize can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        if(pageNum < 1)
        {
            String message = "pageNum can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        PageRequest pageRequest = new PageRequest(pageNum - 1, pageSize);
        Page page = researchAreaRepository.findAll(pageRequest);
        return ResponseEntity.ok().body(page);
    }

    /**
     * GET  /researchAreas/:id -> get the "id" researchArea.
     */
    @RequestMapping(value = "/researchAreas/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ResearchArea> getResearchArea(@PathVariable Long id) {
        log.debug("REST request to get ResearchArea : {}", id);
        return Optional.ofNullable(researchAreaRepository.findOne(id))
            .map(researchArea -> new ResponseEntity<>(
                researchArea,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /researchAreas/:id -> delete the "id" researchArea.
     */
    @RequestMapping(value = "/researchAreas/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteResearchArea(@PathVariable Long id) {
        log.debug("REST request to delete ResearchArea : {}", id);
        researchAreaRepository.delete(id);
        researchAreaSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("researchArea", id.toString())).build();
    }

    /**
     * SEARCH  /_search/researchAreas/:query -> search for the researchArea corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/researchAreas/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ResearchArea> searchResearchAreas(@PathVariable String query) {
        return StreamSupport
            .stream(researchAreaSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
