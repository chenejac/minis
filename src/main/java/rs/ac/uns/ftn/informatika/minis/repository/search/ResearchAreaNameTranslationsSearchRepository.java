package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.ResearchAreaNameTranslations;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the ResearchAreaNameTranslations entity.
 */
public interface ResearchAreaNameTranslationsSearchRepository extends ElasticsearchRepository<ResearchAreaNameTranslations, Long> {
}
