package rs.ac.uns.ftn.informatika.minis.service;

import java.util.Arrays;
import rs.ac.uns.ftn.informatika.minis.domain.User;
import org.apache.commons.lang.CharEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.mail.internet.MimeMessage;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Service for sending e-mails.
 * <p/>
 * <p>
 * We use the @Async annotation to send e-mails asynchronously.
 * </p>
 */
@Service
public class MailService {

    private final Logger log = LoggerFactory.getLogger(MailService.class);

    private static final String DEFAULT_LANGUAGE = "EN";

    @Inject
    private Environment env;

    @Inject
    private JavaMailSenderImpl javaMailSender;

    @Inject
    private MessageSource messageSource;

    @Inject
    private SpringTemplateEngine templateEngine;

    /**
     * System default email address that sends the e-mails.
     */
    private String from;
    private String appRoot;

    @PostConstruct
    public void init() {
        this.from = env.getProperty("jhipster.mail.from");
        this.appRoot = env.getProperty("minisAppRoot");
    }

    @Async
    public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml, String bcc) {
        log.debug("Send e-mail[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
                isMultipart, isHtml, to, subject, content);

        // Send email only when production profile is on, or staging
        if (Arrays.asList(env.getActiveProfiles()).contains("prod") || Arrays.asList(env.getActiveProfiles()).contains("staging")) {
            // Prepare message using a Spring helper
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            try {
                MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
                message.setTo(to);
                message.setFrom(from);
                message.setSubject(subject);
                if (bcc != null) {
                    message.addBcc(bcc);
                }
                message.setText(content, isHtml);
                javaMailSender.send(mimeMessage);
                log.debug("Sent e-mail to User '{}'", to);
            } catch (Exception e) {
                log.warn("E-mail could not be sent to user '{}', exception is: {}", to, e.getMessage());
            }
        }
    }

    @Async
    public void sendActivationEmail(User user, String baseUrl) {
        log.debug("Sending activation e-mail to '{}'", user.getEmail());
        Locale locale = this.getLocaleForUser(user);
        Context context = new Context(locale);
        context.setVariable("user", user);
        context.setVariable("baseUrl", this.appRoot);
        String content = templateEngine.process("activationEmail", context);
        String subject = messageSource.getMessage("email.activation.title", null, locale);
        sendEmail(user.getEmail(), subject, content, false, true, null);
    }

    @Async
    public void sendPasswordResetMail(User user, String baseUrl) {
        log.debug("Sending password reset e-mail to '{}'", user.getEmail());
        Locale locale = this.getLocaleForUser(user);
        Context context = new Context(locale);
        context.setVariable("user", user);
        context.setVariable("baseUrl", this.appRoot);
        context.setVariable("mailFrom", this.from);
        String content = templateEngine.process("passwordResetEmail", context);
        String subject = messageSource.getMessage("email.reset.title", null, locale);
        sendEmail(user.getEmail(), subject, content, false, true, null);
    }

    @Async
    public void sendNewPasswordMail(User user, String password) {
        log.debug("Sending password reset e-mail to '{}'", user.getEmail());
        Locale locale = this.getLocaleForUser(user);
        Context context = new Context(locale);
        context.setVariable("user", user);
        context.setVariable("password", password);
        context.setVariable("baseUrl", this.appRoot);
        String content = templateEngine.process("newPassword", context);
        String subject = messageSource.getMessage("email.newPassword.title", null, locale);
        sendEmail(user.getEmail(), subject, content, false, true, null);
    }

    @Async
    public void sendPasswordToEmail(User user, String baseUrl, String password) {
        log.debug("Sending e-mail with password to '{}'", user.getEmail());
        Locale locale = this.getLocaleForUser(user);
        Context context = new Context(locale);
        context.setVariable("user", user);
        context.setVariable("baseUrl", this.appRoot);
        context.setVariable("password", password);
        context.setVariable("mailFrom", this.from);
        String content = templateEngine.process("passwordInEmail", context);
        String subject = messageSource.getMessage("email.passInEmail.title", null, locale);
        sendEmail(user.getEmail(), subject, content, false, true, this.from);
    }

    private Locale getLocaleForUser(User u) {
        return Locale.forLanguageTag(u.getLangKey() != null ? u.getLangKey() : DEFAULT_LANGUAGE);
    }

}
