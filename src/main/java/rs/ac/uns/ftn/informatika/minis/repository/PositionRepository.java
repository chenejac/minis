package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.Position;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Position entity.
 */
public interface PositionRepository extends JpaRepository<Position,Long> {
    
    public Position findPositionByName(String name);
    
    public Position findPositionByOzContaining(String oz);

}
