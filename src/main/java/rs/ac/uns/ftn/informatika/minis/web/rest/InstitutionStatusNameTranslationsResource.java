package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.InstitutionStatusNameTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.InstitutionStatusNameTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.InstitutionStatusNameTranslationsSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing InstitutionStatusNameTranslations.
 */
@RestController
@RequestMapping("/api")
public class InstitutionStatusNameTranslationsResource {

    private final Logger log = LoggerFactory.getLogger(InstitutionStatusNameTranslationsResource.class);

    @Inject
    private InstitutionStatusNameTranslationsRepository institutionStatusNameTranslationsRepository;

    @Inject
    private InstitutionStatusNameTranslationsSearchRepository institutionStatusNameTranslationsSearchRepository;

    /**
     * POST  /institutionStatusNameTranslationss -> Create a new institutionStatusNameTranslations.
     */
    @RequestMapping(value = "/institutionStatusNameTranslationss",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstitutionStatusNameTranslations> createInstitutionStatusNameTranslations(@RequestBody InstitutionStatusNameTranslations institutionStatusNameTranslations) throws URISyntaxException {
        log.debug("REST request to save InstitutionStatusNameTranslations : {}", institutionStatusNameTranslations);
        if (institutionStatusNameTranslations.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new institutionStatusNameTranslations cannot already have an ID").body(null);
        }
        InstitutionStatusNameTranslations result = institutionStatusNameTranslationsRepository.save(institutionStatusNameTranslations);
        institutionStatusNameTranslationsSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/institutionStatusNameTranslationss/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("institutionStatusNameTranslations", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /institutionStatusNameTranslationss -> Updates an existing institutionStatusNameTranslations.
     */
    @RequestMapping(value = "/institutionStatusNameTranslationss",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstitutionStatusNameTranslations> updateInstitutionStatusNameTranslations(@RequestBody InstitutionStatusNameTranslations institutionStatusNameTranslations) throws URISyntaxException {
        log.debug("REST request to update InstitutionStatusNameTranslations : {}", institutionStatusNameTranslations);
        if (institutionStatusNameTranslations.getId() == null) {
            return createInstitutionStatusNameTranslations(institutionStatusNameTranslations);
        }
        InstitutionStatusNameTranslations result = institutionStatusNameTranslationsRepository.save(institutionStatusNameTranslations);
        institutionStatusNameTranslationsSearchRepository.save(institutionStatusNameTranslations);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("institutionStatusNameTranslations", institutionStatusNameTranslations.getId().toString()))
                .body(result);
    }

    /**
     * GET  /institutionStatusNameTranslationss -> get all the institutionStatusNameTranslationss.
     */
    @RequestMapping(value = "/institutionStatusNameTranslationss",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<InstitutionStatusNameTranslations> getAllInstitutionStatusNameTranslationss() {
        log.debug("REST request to get all InstitutionStatusNameTranslationss");
        return institutionStatusNameTranslationsRepository.findAll();
    }

    /**
     * GET  /institutionStatusNameTranslationss/:id -> get the "id" institutionStatusNameTranslations.
     */
    @RequestMapping(value = "/institutionStatusNameTranslationss/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstitutionStatusNameTranslations> getInstitutionStatusNameTranslations(@PathVariable Long id) {
        log.debug("REST request to get InstitutionStatusNameTranslations : {}", id);
        return Optional.ofNullable(institutionStatusNameTranslationsRepository.findOne(id))
            .map(institutionStatusNameTranslations -> new ResponseEntity<>(
                institutionStatusNameTranslations,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /institutionStatusNameTranslationss/:id -> delete the "id" institutionStatusNameTranslations.
     */
    @RequestMapping(value = "/institutionStatusNameTranslationss/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteInstitutionStatusNameTranslations(@PathVariable Long id) {
        log.debug("REST request to delete InstitutionStatusNameTranslations : {}", id);
        institutionStatusNameTranslationsRepository.delete(id);
        institutionStatusNameTranslationsSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("institutionStatusNameTranslations", id.toString())).build();
    }

    /**
     * SEARCH  /_search/institutionStatusNameTranslationss/:query -> search for the institutionStatusNameTranslations corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/institutionStatusNameTranslationss/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<InstitutionStatusNameTranslations> searchInstitutionStatusNameTranslationss(@PathVariable String query) {
        return StreamSupport
            .stream(institutionStatusNameTranslationsSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
