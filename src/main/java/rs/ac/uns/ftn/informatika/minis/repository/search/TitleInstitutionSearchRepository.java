package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.TitleInstitution;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the TitleInstitution entity.
 */
public interface TitleInstitutionSearchRepository extends ElasticsearchRepository<TitleInstitution, Long> {
}
