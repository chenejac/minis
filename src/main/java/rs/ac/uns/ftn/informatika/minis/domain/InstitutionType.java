package rs.ac.uns.ftn.informatika.minis.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A InstitutionType.
 */
@Entity
@Table(name = "institution_type")
@Document(indexName="institutiontype")
public class InstitutionType implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    
    @Column(name = "name")
    private String name;
    
    @Column(name = "description")
    private String description;

    @ManyToOne
    private Language language;

    @OneToMany(mappedBy = "type")
    @JsonIgnore
    private Set<Institution> institutions = new HashSet<>();

    @OneToMany(mappedBy = "institutionType")
    @JsonIgnore
    private Set<InstitutionTypeNameTranslations> nameTranslations = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Set<Institution> getInstitutions() {
        return institutions;
    }

    public void setInstitutions(Set<Institution> institutions) {
        this.institutions = institutions;
    }

    public Set<InstitutionTypeNameTranslations> getNameTranslations() {
        return nameTranslations;
    }

    public void setNameTranslations(Set<InstitutionTypeNameTranslations> institutionTypeNameTranslationss) {
        this.nameTranslations = institutionTypeNameTranslationss;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        InstitutionType institutionType = (InstitutionType) o;

        if ( ! Objects.equals(id, institutionType.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "InstitutionType{" +
                "id=" + id +
                ", name='" + name + "'" +
                ", description='" + description + "'" +
                '}';
    }
}
