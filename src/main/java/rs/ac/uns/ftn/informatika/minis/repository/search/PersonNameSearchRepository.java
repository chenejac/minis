package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.PersonName;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the PersonName entity.
 */
public interface PersonNameSearchRepository extends ElasticsearchRepository<PersonName, Long> {
}
