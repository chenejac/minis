package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.ActivityNameTranslations;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the ActivityNameTranslations entity.
 */
public interface ActivityNameTranslationsSearchRepository extends ElasticsearchRepository<ActivityNameTranslations, Long> {
}
