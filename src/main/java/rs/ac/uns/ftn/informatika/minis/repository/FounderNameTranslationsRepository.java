package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.FounderNameTranslations;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the FounderNameTranslations entity.
 */
public interface FounderNameTranslationsRepository extends JpaRepository<FounderNameTranslations,Long> {

}
