package rs.ac.uns.ftn.informatika.minis.repository;

import rs.ac.uns.ftn.informatika.minis.domain.Person;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import rs.ac.uns.ftn.informatika.minis.domain.Institution;

/**
 * Spring Data JPA repository for the Person entity.
 */
public interface PersonRepository extends JpaRepository<Person, Long> {

    @Query("select distinct person from Person person left join fetch person.classifications")
    List<Person> findAllWithEagerRelationships();

    @Query("select person from Person person left join fetch person.classifications where person.id =:id")
    Person findOneWithEagerRelationships(@Param("id") Long id);

    public Person findPersonByJmbg(String jmbg);
    
    @Query(value = "select distinct p.* from person p left join person_institution inst on p.id = inst.person_id where inst.institution_id = ?1 and (p.record_status = ?4 or p.record_status = ?5 or p.record_status = ?6) LIMIT ?2 OFFSET ?3", nativeQuery = true)
    public List<Person> searchForPersonsByRecordStatusAndInstitutionId(Long id, int pageSize, int pageNum, Integer migrated, Integer changed, Integer approved);
    
    @Query(value = "select distinct p.* from person p left join person_institution inst on p.id = inst.person_id where inst.institution_id = ?1 and (p.record_status = ?2 or p.record_status = ?3 or p.record_status = ?4)", nativeQuery = true)
    public List<Person> searchForPersonsByRecordStatusAndInstitutionIdWithouPagination(Long id, Integer migrated, Integer changed, Integer approved);
    
    public Page<Person> findByRecordStatusIn(List<Integer> recordStatus, Pageable pageable);
    
    public List<Person> findByRecordStatusIn(List<Integer> recordStatus);
    
    @Query(value = "select distinct count(*) from person p left join person_institution inst on p.id = inst.person_id where inst.institution_id = ?1 and (p.record_status = ?2 or p.record_status = ?3 or p.record_status = ?4)", nativeQuery = true)
    public Integer countForPersonsByRecordStatusAndInstitutionId( Long id, Integer migrated, Integer changed, Integer approved);
    
    public List<Person> findByIdIn(List<Long> ids);
}
