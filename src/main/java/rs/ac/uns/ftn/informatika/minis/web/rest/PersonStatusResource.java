package rs.ac.uns.ftn.informatika.minis.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.ac.uns.ftn.informatika.minis.domain.PersonStatus;
import rs.ac.uns.ftn.informatika.minis.repository.PersonStatusRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PersonStatusSearchRepository;
import rs.ac.uns.ftn.informatika.minis.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

/**
 * REST controller for managing PersonStatus.
 */
@RestController
@RequestMapping("/api")
public class PersonStatusResource {

    private final Logger log = LoggerFactory.getLogger(PersonStatusResource.class);

    @Inject
    private PersonStatusRepository personStatusRepository;

    @Inject
    private PersonStatusSearchRepository personStatusSearchRepository;

    /**
     * POST  /personStatuss -> Create a new personStatus.
     */
    @RequestMapping(value = "/personStatuss",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonStatus> createPersonStatus(@RequestBody PersonStatus personStatus) throws URISyntaxException {
        log.debug("REST request to save PersonStatus : {}", personStatus);
        if (personStatus.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new personStatus cannot already have an ID").body(null);
        }
        PersonStatus result = personStatusRepository.save(personStatus);
        personStatusSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/personStatuss/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("personStatus", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /personStatuss -> Updates an existing personStatus.
     */
    @RequestMapping(value = "/personStatuss",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonStatus> updatePersonStatus(@RequestBody PersonStatus personStatus) throws URISyntaxException {
        log.debug("REST request to update PersonStatus : {}", personStatus);
        if (personStatus.getId() == null) {
            return createPersonStatus(personStatus);
        }
        PersonStatus result = personStatusRepository.save(personStatus);
        personStatusSearchRepository.save(personStatus);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("personStatus", personStatus.getId().toString()))
                .body(result);
    }

    /**
     * GET  /personStatuss -> get all the personStatuss.
     */
    @RequestMapping(value = "/personStatuss",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Page<PersonStatus>> getAllPersonStatuss(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                                @RequestParam(value = "pageSize", defaultValue = "20") int pageSize) {
        log.debug("REST request to get all PersonStatuss");
         if(pageSize < 1)
        {
            String message = "pageSize can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        if(pageNum < 1)
        {
            String message = "pageNum can not be less than one.";
            log.info(message);
            return ResponseEntity.badRequest().header("Failure", message).body(null);
        }
        PageRequest pageRequest = new PageRequest(pageNum - 1, pageSize);
        Page page = personStatusRepository.findAll(pageRequest);
        return ResponseEntity.ok().body(page);
    }

    /**
     * GET  /personStatuss/:id -> get the "id" personStatus.
     */
    @RequestMapping(value = "/personStatuss/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PersonStatus> getPersonStatus(@PathVariable Long id) {
        log.debug("REST request to get PersonStatus : {}", id);
        return Optional.ofNullable(personStatusRepository.findOne(id))
            .map(personStatus -> new ResponseEntity<>(
                personStatus,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /personStatuss/:id -> delete the "id" personStatus.
     */
    @RequestMapping(value = "/personStatuss/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePersonStatus(@PathVariable Long id) {
        log.debug("REST request to delete PersonStatus : {}", id);
        personStatusRepository.delete(id);
        personStatusSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("personStatus", id.toString())).build();
    }

    /**
     * SEARCH  /_search/personStatuss/:query -> search for the personStatus corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/personStatuss/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PersonStatus> searchPersonStatuss(@PathVariable String query) {
        return StreamSupport
            .stream(personStatusSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
