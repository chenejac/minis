package rs.ac.uns.ftn.informatika.minis.repository.search;

import rs.ac.uns.ftn.informatika.minis.domain.FunctionNameTranslations;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the FunctionNameTranslations entity.
 */
public interface FunctionNameTranslationsSearchRepository extends ElasticsearchRepository<FunctionNameTranslations, Long> {
}
