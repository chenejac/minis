(function() {
  'use strict';

  describe('Environment test', function() {
    it('should display login page', function() {
      browser.get('/');
      browser.waitForAngular();
      expect(browser.getCurrentUrl()).toMatch(/login/);
    });
  });
})();
