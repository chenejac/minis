(function() {
  'use strict';

  exports.config = {
    seleniumServerJar: '../../node_modules/protractor/selenium/selenium-server-standalone-2.47.1.jar',
    chromeDriver: '../../node_modules/protractor/selenium/chromedriver',
    specs: [
      '../../test/e2e/environmentVerification.spec.js'
    ],
    capabilities: {
      browserName: 'chrome',
      // uncomment to set up default chrome download folder
      chromeOptions: {
        // Get rid of --ignore-certificate yellow warning
        args: ['--no-sandbox', '--test-type=browser'],
      }
    },
    jasmineNodeOpts: {
      defaultTimeoutInterval: 600000,
      isVerbose: true
    },
    allScriptsTimeout: 360000,
    onPrepare: function() {
      browser.manage().window().setSize(1200, 800);
    }
  };
})();
