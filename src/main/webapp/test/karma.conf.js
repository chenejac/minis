// Karma configuration
// http://karma-runner.github.io/0.12/config/configuration-file.html
// Generated on 2015-03-20 using
// generator-karma 0.9.0
(function() {
  'use strict';

  module.exports = function(config) {

    var buildReportsPath = 'buildreports/';

    config.set({
      // enable / disable watching file and executing tests whenever any file changes
      autoWatch: false,

      // base path, that will be used to resolve files and exclude
      basePath: '../',

      // testing framework to use (jasmine/mocha/qunit/...)
      frameworks: ['mocha'],

      // list of files / patterns to load in the browser
      files: [
        'node_modules/chai/chai.js',
        'node_modules/chai-as-promised/lib/chai-as-promised.js',
        'node_modules/sinon/pkg/sinon.js',
        'node_modules/sinon-chai/lib/sinon-chai.js',

        // bower:js
        'bower_components/modernizr/modernizr.js',
        'bower_components/es5-shim/es5-shim.js',
        'bower_components/jquery/dist/jquery.js',
        'bower_components/angular/angular.js',
        'bower_components/angular-animate/angular-animate.js',
        'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
        'bower_components/angular-cookies/angular-cookies.js',
        'bower_components/angular-poller/angular-poller.min.js',
        'bower_components/raven-js/dist/raven.js',
        'bower_components/angular-raven/angular-raven.js',
        'bower_components/angular-resource/angular-resource.js',
        'bower_components/angular-route/angular-route.js',
        'bower_components/angular-sanitize/angular-sanitize.js',
        'bower_components/angular-touch/angular-touch.js',
        'bower_components/angular-ui-router/release/angular-ui-router.js',
        'bower_components/angular-xeditable/dist/js/xeditable.js',
        'bower_components/bootstrap/dist/js/bootstrap.js',
        'bower_components/json3/lib/json3.js',
        'bower_components/lodash/dist/lodash.compat.js',
        'bower_components/moment/moment.js',
        'bower_components/restangular/dist/restangular.js',
        'bower_components/underscore/underscore.js',
        'bower_components/angular-scenario/angular-scenario.js',
        // endbower
        'bower_components/angular-mocks/angular-mocks.js',


        '.tmp/templates/templates.js',
        'app/src/**/module.js',
        'app/src/**/!(*.spec).js',
        'test/unit/mocks/*.js',
        'test/unit/**/*.spec.js'
      ],

      // list of files / patterns to exclude
      exclude: [

      ],

      preprocessors: {
        '../app/src/**/!(*.spec).js': 'coverage'
      },

      coverageReporter: {
        type : 'html',
        dir : buildReportsPath + 'coverage/'
      },

      htmlReporter: {
        outputFile: buildReportsPath + 'unit_tests/index.html'
      },

      junitReporter: {
        outputFile: buildReportsPath + 'unit_tests/test-results.xml',
        suite: ''
      },

      // web server port
      port: 9876,
      runnerPort: 9100,

      // Start these browsers, currently available:
      // - Chrome
      // - ChromeCanary
      // - Firefox
      // - Opera
      // - Safari (only Mac)
      // - PhantomJS
      // - IE (only Windows)
      browsers: [
        'PhantomJS'
      ],

      // Which plugins to enable
      plugins: [
        'karma-mocha',
        'karma-phantomjs-launcher',
        'karma-coverage',
        'karma-htmlfile-reporter',
        'karma-junit-reporter'
      ],

      colors: true,

      // level of logging
      // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
      logLevel: config.LOG_INFO,

      reporters: ['progress', 'coverage', 'html', 'junit'],

      client: {
        captureConsole: true
      }
    });
  };
})();
