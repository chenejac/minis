(function() {
  'use strict';

  var mod = angular.module('emPlugins');

  mod.factory('pinesNotifications', function() {
    return {
      notify: function(args) {
        var notification = $.pnotify(args);
        notification.notify = notification.pnotify;
        return notification;
      },
      defaults: $.pnotify.defaults
    };
  });

})();
