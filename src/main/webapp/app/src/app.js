(function() {
  'use strict';

  _.mixin({
    freeze: function(obj) {
      Object.freeze(obj);
      _.each(obj, function(prop) {
        if (typeof prop === 'object' && prop !== null && !Object.isFrozen(prop)) {
          _.freeze(prop);
        }
      });
      return obj;
    }
  });

  /**
   * @ngdoc overview
   * @name employiiEmergencyAdminApp
   * @description
   * # employiiEmergencyAdminApp
   *
   * Main module of the application.
   */
  angular
    .module('minisApp', [
      'ngAnimate',
      'ngCookies',
      'ngResource',
      'ngRoute',
      'ngSanitize',
      'ngTouch',
      'ui.router',
      'ui.select2',
      'ui.bootstrap',
      'restangular',
      'xeditable',
      'emguo.poller',
      'theme',
      'emMinisTemplates',
      'emMinisConfiguration',
      'emMinisCommunication',
      'emMinisAuthentication',
      'emMinisAdministration',
      'emErrorHandling',
      'emMinisRouting',
      'emMinisStorage',
      'emMinisInstitutions',
      'emMinisPersons',
      'emPlugins',
      'emAdminUtility',
      'pascalprecht.translate',
      'angularSpinner'
    ]).config(['$httpProvider', function($httpProvider) {
      $httpProvider.defaults.withCredentials = true;
      delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }])
    .config(['pollerConfig', function(pollerConfig) {
      pollerConfig.stopOnStateChange = true;
      pollerConfig.stopOnRouteChange = true;
    }])
    .config(function ($translateProvider) {
      $translateProvider.preferredLanguage('sr_cyr');
      $translateProvider.useStaticFilesLoader({
          prefix: 'app/assets/languages/',
          suffix: '.json'
      });
    })
    .controller('EmAdminController', ['$scope', '$log', '$state', 'errorManager', 'rest', 'profile', '$translate', '$rootScope', '$filter', '$window', 
    function($scope, $log, $state, errorManager, rest, profile, $translate, $rootScope, $filter, $window) {
      
      $scope.$on('profileChanged', errorManager.broadcastListener);
      $scope.$on('profileChanged', function(event, newProfile) {
        $scope.userInfo = newProfile.user;
      });

      if($window.localStorage.getItem('lang_locale')==null || $window.localStorage.getItem('lang_locale')==undefined){
        $window.localStorage.setItem('lang_locale', 'sr_cyr');
        $translate.use('sr_cyr');
        $rootScope.activeLanguage = 'sr_cyr';
      }
      else{
        $translate.use($window.localStorage.getItem('lang_locale'));
        $rootScope.activeLanguage = $window.localStorage.getItem('lang_locale');
      }

      $scope.changeLanguage = function(language){
        $translate.use(language);
        $rootScope.activeLanguage = language;
        $window.localStorage.setItem('lang_locale', language);
        location.reload();
      };
      
      $log.info('Going to the main state');
    }])
    .run(['editableThemes', 'editableOptions', function(editableThemes, editableOptions) {
      editableOptions.theme = 'bs3';
      editableThemes['bs3'].submitTpl =
        '<button type="submit" class="editable-buttons-submit btn btn-success">' +
        '<i class="fa fa-check fa-fw"></i></button>';

      editableThemes['bs3'].cancelTpl =
        '<button type="button" class="editable-buttons-cancel btn btn-danger" ng-click="$form.$cancel()">' +
        '<i class="fa fa-times fa-fw"></i></button>';
    }])
    .run(['$rootScope', 'session', 'errorManager', 'profile', '$state', '$window', 'polling',
      function($rootScope, session, errorManager, profile, $state, $window, polling) {
        $rootScope.logOut = function() {
          polling.clear();
          profile.logOut();
          errorManager.clearUser();
          $state.go('login');
        };

        $rootScope.logOutIfUnauthorized = function(response) {
          if (response.status === 401) { // UNAUTHORIZED
            $rootScope.logOut();
            return true;
          }
          return false;
        };

        angular.element($window).on('storage', function(event) {
          var storageEvent = event.originalEvent || {};
          if (!session.get().loggedIn && storageEvent.key === 'data' &&
              storageEvent.oldValue !== storageEvent.newValue) {
            $rootScope.logOut();
          }
        });

        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState) {
          var userSession = session.get();

          if (!(profile.hasRoleAccess(toState))) {
            event.preventDefault();
            $state.go(fromState || 'login');
            return;
          }

          session.ping();
        });
      }]);
})();
