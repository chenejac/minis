'use strict';

angular.module('emMinisCommunication').factory('polling',
['$q', 'rest', 'poller', 'config', '$log', 'utils',
function($q, rest, poller, config, $log, utils) {

  var defaultPollerOptions = {
    action: 'get',
    delay: config.pollingDelay,
    smart: true,
    catchError: true
  };

  var targets = {};

  var PollingTarget = function(route) {
    this.route = route;
    this.status = null;
    this.dirty = false;
    this.poller = null;
    this.progress = null;
    this.flags = {};
  };

  // expects an array with strings or simple objects
  function toStringMsgs(msgs) {
    return _.reduce(msgs, function(memo, msg) {
      if (_.isString(msg)) {
        memo.push(msg);
      } else {
        // turns object `{'keyA': 'valA', 'keyB': 'valB'}` into array of strings `['keyA: valA', 'keyB: valB']`
        _.each(msg, function(value, key) {
          memo.push(key + ': ' + value);
        });
      }
      return memo;
    }, []);
  }

  function pollerNotify(self, defer, progress) {
    progress = progress || {};
    var p = self.progress = _.isFunction(progress.plain) ? progress.plain() : progress;
    var result = p.result || {};
    self.status = p.status || 'UNKNOWN';
    if (self.status !== config.pollingStatus.pending) {
      self.poller.stop();
      // display any warnings
      p.warnings = toStringMsgs(result.warnings || []);
      if (!_.isEmpty(p.warnings)) {
        $log.warn(p.warnings);
      }

      // default catch for errors
      p.errors = toStringMsgs(result.errors || []);
      if (_.isString(result.error)) {
        p.errors.push(result.error);
      }
      if (self.status === config.pollingStatus.success) {
        defer.resolve(p);
      } else {
        // just error message as result
        if (_.isString(result)) {
          p.errors.push(result);
        }
        // FAILURE with exception
        if (result.exc_type && result.exc_message) {
          p.errors.push(result.exc_type + ': ' + result.exc_message);
        }
        // generic error message if nothing else was extracted
        if (_.isEmpty(p.errors)) {
          p.errors.push('An error occured.');
        }
        defer.reject(p);
      }
    }
  }

  _.extend(PollingTarget.prototype, {

    resolved: function() {
      return this.status === config.pollingStatus.success && !this.dirty &&
        _.isEmpty(utils.getProperty(this.progress, 'errors', []));
    },

    inProgress: function() {
      return !this.dirty && this.poller && this.status === config.pollingStatus.pending;
    },

    wait: function() {
      var defer = $q.defer();
      if (_.isUndefined(this.poller.interval)) {
        this.poller.restart();
      }
      this.poller.promise.then(null, null, pollerNotify.bind(null, this, defer));
      return defer.promise;
    },

    init: function(route, params) {
      this.dirty = false;
      if (this.poller) {
        this.poller.stop();
      }
      var _route = route.replace(/^\//, '');
      this.poller = poller.get(rest.polling(_route), _.extend(defaultPollerOptions, {
        argumentsArray: _.isEmpty(params) ? [] : [params]
      }));
      return this.wait();
    }

  });

  var _polling = {

    clear: function() {
      poller.stopAll();
      targets = {};
    },

    get: function(route, id) {
      var key = route + (id || '');
      if (!targets[key]) {
        targets[key] = new PollingTarget(route);
      }
      return targets[key];
    },

    /**
     * @ngdoc method
     * @name  polling#run
     * @param  {PollingTarget} pollingTarget - existing PollingTarget object or url route for the initial POST request
     * @param {Boolean} forceDirty - boolean flag, true if you want to always start over, false to use any existing
     * progress
     * @param {Object} data - data to send with initial POST request
     * @param {Object} id - PollingTarget id to get the correct one with `get(route, id)`
     * @param {Object} params - params for the initial POST request
     * @return {Promise} Returns a promise that resolves on polling SUCCESS/FAIL.
     * @description
     * Starts polling by sending an initial POST request, and afterwards polls with GET until the server returns
     * a success or fail message.
     */
    run: function(pollingTarget, forceDirty, data, id, params) {
      if (!(pollingTarget instanceof PollingTarget)) {
        if (_.isEmpty(pollingTarget) || !_.isString(pollingTarget)) {
          return $q.reject('Wrong Arguments');
        }
        pollingTarget = _polling.get(pollingTarget, id);
      }
      if (forceDirty) {
        pollingTarget.dirty = true;
      }
      if (pollingTarget.inProgress()) {
        return pollingTarget.wait();
      } else if (pollingTarget.resolved()) {
        return $q.when(_.extend({completed: true}, pollingTarget.progress));
      } else {
        return rest.post(pollingTarget.route, data, null, params)
          .then(function(response) {
            return pollingTarget.init(response.task_status);
          });
      }
    },

    dirtyByFlag: function(group, item) {
      _.each(targets, function(target) {
        var flags = target.flags[group] || {};
        if (flags[item]) {
          target.dirty = true;
        }
      });
    }

  };

  return _polling;

}]);
