(function() {
  'use strict';

  var mod = angular.module('emMinisCommunication');

  mod.config(['RestangularProvider', '$urlRouterProvider', 'HOST', 'API_PATH',
  function(RestangularProvider, $urlRouterProvider, HOST, API_PATH) {
    RestangularProvider.setBaseUrl(HOST + API_PATH);
    RestangularProvider.setRequestInterceptor(function(element, operation) {
      return operation !== 'remove' ? element : null;
    });

    $urlRouterProvider.otherwise('/login');
  }]);

  mod.factory('rest',
  ['$rootScope', '$q', '$log', '$timeout', 'Restangular', 'session', 'config', 'responseHandler',
  function($rootScope, $q, $log, $timeout, Restangular, session, config, responseHandler) {

    var defaultHeaders = {
      'Content-Type': 'application/json;charset=UTF-8'
    };

    function restFactory(headers) {
      return Restangular
        .setErrorInterceptor(responseHandler.restangularErrorInterceptor)
        .withConfig(function(RestangularConfigurer) {
          RestangularConfigurer.setDefaultHeaders(headers || {});
        });
    }
    var noAuth = restFactory;

    function auth(headers) {
      // Set access token in header
      return restFactory(_.extend({'x-auth-token': session.get().token}, headers));
    }

    function polling(url) {
      return auth(defaultHeaders)
        .withConfig(function(RestangularConfigurer) {
          RestangularConfigurer.setBaseUrl(config.api.host);
          RestangularConfigurer.setRequestSuffix('');
        })
        .one(url);
    }

    function getResponse(response) {
      if(typeof response === 'string') {
        return response;
      }
      return response && response.plain();
    }

    var methods = {
      all: function(rest, route, params, headers) {
        function results(response) {
          return response.content;
        }

        return rest(_.extend({}, defaultHeaders, headers)).one(route)
          .get(_.extend({pageNum: 1}, params))
          .then(function(response) {
            response = getResponse(response);
            if (!response.last && response.totalElements > response.content.length) {
              var pageCount = Math.ceil(response.totalElements / response.content.length),
                pages = [$q.when(response.content)], page = 2;

              while (page <= pageCount) {
                pages.push(rest().one(route).get(_.extend({pageNum: page++}, params)).then(results));
              }
              return $q.all(pages);
            } else {
              return [response.content];
            }
          })
          .then(function(pages) {
            pages = _.flatten(pages);
            $log.debug('GET', route, params, pages);
            return pages;
          });
      },

      get: function(rest, route, id, params, headers) {
        return rest(_.extend({}, defaultHeaders, headers)).one(route + (id || ''))
          .get(params)
          .then(function(response) {
            response = getResponse(response);
            $log.debug('GET', route, id, params, response);
            return response;
          });
      },

      post: function(rest, route, data, path, params, headers) {
        var isFile = data instanceof FormData,
          request = rest(!isFile ? _.extend({}, defaultHeaders, headers) : undefined)
          .all(route + (path || ''));

        if (isFile) {
          request = request.withHttpConfig({transformRequest: angular.identity});
        }

        return request.customPOST(
            _.isNull(data) || _.isUndefined(data) ? {} : data,
            '',
            params,
            isFile ? {'Content-Type': undefined} : undefined
          )
          .then(function(response) {
            response = getResponse(response);
            $log.debug(isFile ? 'POST (multipart)' : 'POST', route, path, data, params, response);
            return response;
          });
      },

      put: function(rest, route, id, data, headers) {
        return rest(_.extend({}, defaultHeaders, headers)).all(route)
          .customOperation('put', id, null, null, data)
          .then(function(response) {
            response = getResponse(response);
            $log.debug('PUT', route, id, data, response);
            return response;
          });
      },

      patch: function(rest, route, id, data, headers) {
        return rest(_.extend({}, defaultHeaders, headers)).all(route)
          .customOperation('patch', id, null, null, data)
          .then(function(response) {
            response = getResponse(response);
            $log.debug('PATCH', route, id, data, response);
            return response;
          });
      },

      save: function(rest, route, data, headers) {
        return data.id ?
          this.put(rest, route, null, data, headers) :
          this.post(rest, route, data, headers);
      },

      remove: function(rest, route, id, params, headers) {
        return rest(_.extend({}, defaultHeaders, headers)).one(route + (id || ''))
          .remove(params)
          .then(function() { $log.debug('DELETE', route, id, params); });
      }
    };

    // use partial binding to generate specific rest interface from generic methods
    function createRest(rest, route) {
      var params = [methods, rest];
      if (route) {
        params.push(route);
      }
      return _.chain(methods).keys().reduce(function(memo, key) {
        memo[key] = methods[key].bind.apply(methods[key], params);
        return memo;
      }, {route: route || ''}).value();
    }

    var _interface = _.extend({polling: polling}, createRest(auth), {noAuth: createRest(noAuth)}),
      endpoints = [
        {name: 'login', route: 'authenticate', noAuth: true},
        {name: 'resetInit', route: 'account/reset_password/init', noAuth: true},
        {name: 'resetFinish', route: 'account/reset_password/finish', noAuth: true},
        {name: 'account', route: 'account'},
        {name: 'institutions', route: 'institutions/'},
        {name: 'institutionTypes', route: 'institutionTypes/'},
        {name: 'institutionStatus', route: 'institutionStatuss/'},
        {name: 'researchArea', route: 'researchAreas/'},
        {name: 'searchInstitutions', route: '/_search/institutions/'},
        {name: 'personInstitution', route: 'personInstitutions/'},
        {name: 'persons', route: 'persons/'},
        {name: 'searchPersons', route: '/_search/persons/'},
        {name: 'searchPersonInstitutions', route: '/_search/personsByInstitution/'},
        {name: 'searchUsers', route: '/_search/users/'},
        {name: 'registerUser', route : 'register'},
        {name: 'users', route: 'users/'},
        {name: 'functions', route: 'functions/'},
        {name: 'positions', route: 'positions/'},
        {name: 'connectionsInstitutions', route: 'personsInstitutionByInstitution/'},
        {name: 'connectionsPersons', route: 'personsInstitutionByPerson/'},
        {name: 'connections', route: 'personInstitutions/'},
        {name: 'states', route: 'states/'},
        {name: 'cities', route: 'cities/'},
        {name: 'townShips', route: 'townShips/'},
        {name: 'categorys', route: 'categorys/'},
        {name: 'personStatus', route: 'personStatuss/'},
        {name: 'authorities', route: 'authorities'},
        {name: 'personCategorys', route: 'personCategorys/'},
        {name: 'personTypes', route: 'personTypes/'},
        {name: 'personPositions', route: 'personPositions/'},
        {name: 'activitys', route: 'activitys/'}
      ];

    // generate endpoint-specific rest interfaces
    _.each(endpoints, function(endpoint) {
      _interface[endpoint.name] = createRest(endpoint.noAuth ? noAuth : auth, endpoint.route);
    });

    return _.freeze(_interface);
  }]);
})();
