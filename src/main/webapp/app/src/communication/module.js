(function() {
  'use strict';

  /**
   * @ngdoc Overview
   * @name  module:emMinisCommunication
   * @description
   * The emMinisCommunication module handles the communication between the web application
   * and the backing server.  This includes RESTful operations, CRUD operations, etc.
   */
  angular.module('emMinisCommunication', ['restangular', 'ui.router']);
})();
