(function() {
  'use strict';
  /**
   * @ngdoc Overview
   * @name  module:emAdminUtility
   * @description
   * Utility module for miscelaneous functions
   */
  angular.module('emAdminUtility', []);
})();
