(function() {
  'use strict';

  /**
   * @ngdoc filter
   * @name emFullname
   * @module emAdminUtility
   *
   * @param {Object} [user] - user object to work on
   * @param {string} [user.first_name] - user's first name / given name
   * @param {string} [user.last_name] - user's last name / family name
   * @param {boolean} [startWithLast=false] - determines order of the name parts,
   * first-last if false, last-first if true
   * @param {string} [delimiter=' '] - string to use to concat the two parts of the full name
   *
   * @description
   * Simple transformer for first and last name with customizable order and delimiter.
   *
   * @returns {string}
   * User's fullname according to specified options or an empty string if user or his name parts not specified.
   */
  angular.module('emAdminUtility')
  .filter('emFullname', function() {
    return function(user, startWithLast, delimiter) {
      delimiter = delimiter || ' ';
      if (user) {
        var first = user.first_name,
            last = user.last_name;
        return _.compact(startWithLast ? [last, first] : [first, last]).join(delimiter) || '';
      } else {
        return '';
      }
    };
  });

})();
