(function() {
  'use strict';

  /**
   * @ngdoc filter
   * @name orderObjectBy
   * @module emAdminUtility
   *
   * @param {Object} items - a collection of items to sort
   * @param {string} {function} [field='index'] - name of field by which items should be sorted
   * @param {boolean} [reverse=false] - true for descending order, false for ascending order
   *
   * @description
   * Sorts object values by specified field, optionally in reverse order.
   *
   * @returns {Array}
   * An array of sorted `items`.
   */
  angular.module('emAdminUtility')
  .filter('orderObjectBy', function() {
    return function(items, field, reverse) {
      field = field || 'index';
      var filtered = _.values(items);
      if (filtered.length > 0) {
        if (typeof filtered[0][field] === 'string') {
          filtered = _.sortBy(filtered, function(item) { return item[field].toLowerCase(); });
        } else {
          filtered = _.sortBy(filtered, field);
        }
        if (reverse) { filtered.reverse(); }
      }
      return filtered;
    };
  });
})();
