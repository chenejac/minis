(function() {
  'use strict';

  /**
   * @ngdoc object
   * @name patterns
   * @module emAdminUtility
   * @description
   * Contains global validation patterns.
   */
  angular.module('emAdminUtility').constant('patterns', Object.freeze({

    _FAIL_: /(?!)/,

    password: /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,

    // jscs:disable
    // Regex to validate e-mails per https://en.wikipedia.org/wiki/Email_address
    // Does not allow quotes in the string, however.
    email: /^(\w|\.(?!\.)|[-!$&'()*+,;=:]|(%(?:\d+))){1,}@(?:(?:(?:[-\w]{1,}\.)+\w{2,6})|(\[(?:\d\d?\d?\.){3}(?:\d\d?\d?)\])|(\[IPv6:([\dA-Fa-f]{1,4}::?){1,7}[\dA-Fa-f]{1,4}\]))$/, // jshint ignore:line
    // jscs:enable

    phone: /^\d{10,15}$/, // needs to be improved

    // stricter ssn regexp:
    // http://rionscode.wordpress.com/2013/09/10/validating-social-security-numbers-through-regular-expressions/
    ssn: /^(?!219-09-9999|078-05-1120)(?!666|000|9\d{2})[0-9*]{3}-(?!00)[0-9*]{2}-(?!0{4})\d{4}$/,
    ein: /^\d{2}-\d{7}$/,

    zipcode: /^\d{5}$/,

    integer: /^\d+$/,
    decimal: /^\d+(?:\.\d{1,2})?$/,
    decimalPositive: /^(?!(?:0|0\.0|0\.00)$)\d+(?:\.\d{1,2})?$/,

    httpError: /^(?:4|5)\d{2}$/,

    money: /^(\d){1,5}(\.\d{2})?$/,

    percent: /^(100|(\d){1,2})(\.\d{2})?$/,
    percentLong: /^(100|(\d){1,2})(\.\d{1,4})?$/,

    accountNumber: /^[0-9]{1,17}$/,

    bankAccountNumber: /^[0-9]{3}-[0-9]{13}-[0-9]{2}$/,

    routingNumber: /^[0-9]{9}$/,

    alienUscisNumber: /^\d{7,9}$/,
    formI94Number: /^\d{11}$/,
    passportNumber: /^.{6,12}$/,

    jmbgNumber: /^\d{13}$/,

    timePin: /^\d{4}$/

  }));
})();
