(function() {
  'use strict';

  /**
   * @ngdoc service
   * @name collections
   * @module emShared
   *
   * @description
   * The `collections` service provides methods for easier manipulation with resources.
   */
  angular.module('emAdminUtility').factory('collections', [
    '$state', '$modal', '$log', 'rest', 'utils',
    function($state, $modal, $log, rest, utils) {

      var _methods = {

        /**
         * @ngdoc method
         * @name collections#initPaging
         *
         * @param {Object} scope - where to initialize the paging variables
         *
         * @description
         * Initializes paging variables on provided `scope`.
         */
        initPaging: function(scope) {
          scope.itemCount = 0;
          scope.itemsPerPage = 0;
          scope.currentPage = 1;
        },

        /**
         * @ngdoc method
         * @name collections#getList
         *
         * @param {string} collection - name of the resource according to `module:emShared.service:rest` endpoints
         * @param {Object} scope - the context where paging variables are defined and where the results will be saved
         * @param {Object} [params=null] - optional params to attach to the request
         *
         * @description
         * Loads page from `collection` endpoint based on paging variables on `scope`.
         * The result is saved to `scope[collection]`.
         * Updates paging variables when first page is loaded.
         *
         * @returns {Promise}
         */
        getList: function(collection, scope, params) {
          scope.loadingIsActive = true;
          if (!scope.currentPage || scope.currentPage < 0) {
            scope.currentPage = 1;
          }
          return rest[collection].get(null, _.extend({pageNum: scope.currentPage}, params))
            .then(function(response) {
              if (scope.currentPage === 1) {
                scope.itemsPerPage = response.content.length;
                scope.itemCount = response.totalElements;
              }
              scope.loadingIsActive = false;
              return scope[collection] = response.content;
            }, function(err) {
              $log.error(err);
            });
        },

        /**
         * @ngdoc method
         * @name collections#getAll
         *
         * @param {Object} scope - the context where the results will be saved
         * @param {string} name - name of the resource according to `module:emShared.service:rest` endpoints
         * @param {Object} [params=null] - optional params to attach to the request
         * @param {boolean} [asArray=false] - determines how the results are saved
         *
         * @description
         * Loads all available data from `name` endpoint.
         * The result is saved to `scope[name]` either as an array or as an object indexed by `id` field.
         *
         * @returns {Promise}
         */

        getAll: function(scope, collection, params, asArray) {
          return rest[collection].all(params)
            .then(function(results) {
              scope[collection] = asArray ? results : _.indexBy(results, 'id');
              return scope[collection];
            });
        },

        /**
         * @ngdoc method
         * @name collections#seeDetails
         *
         * @param {Object|string} state - a state to move to
         * @param {number|string} id - the value of `id` param of the new state
         *
         * @description
         * Redirects to specified state.
         */
        seeDetails: function(state, id) {
          $state.go(state, {id: id});
        },

        /**
         * @ngdoc method
         * @name collections#customDelete
         *
         * @param {string} title - title of the dialog
         * @param {string} message - message of the dialog
         * @param {Function} callback - function to perform when dialog is confirmed
         *
         * @description
         * Displays confirmation dialog with _Cancel_ and _Yes Delete_ buttons.
         * When user confirms the dialog by clicking _Yes Delete_ the provided `callback` is called.
         */
        customDelete: function(title, message, callback) {
          var confirmCallback = utils.ensureValidFn(callback, _.noop);
          $modal.open({
            templateUrl: 'src/utility/remove-modal.html',
            controller: ['$scope', function($scope) {
              $scope.title = title;
              $scope.message = message;
              $scope.ok = function() {
                callback();
                $scope.$dismiss();
              };

              $scope.cancel = function() {
                $scope.$dismiss();
              };
            }],
            size: 'md'
          });
        },

        /**
         * @ngdoc method
         * @name collections#deleteItem
         *
         * @param {string} collection - name of the resource according to `module:emShared.service:rest` endpoints
         * @param {string|number} id - id of the item to delete
         * @param {string} title - title of the dialog
         * @param {string} message - message of the dialog
         * @param {Function} callback - function to perform when dialog is confirmed
         *
         * @description
         * Displays confirmation dialog with _Cancel_ and _Yes Delete_ buttons.
         * When user confirms the dialog by clicking _Yes Delete_
         * a request is send to `collection` endpoint to delete item with id `id`.
         * `callback` is called if the item was successfully deleted.
         */
        deleteItem: function(collection, id, title, message) {
          this.customDelete(title, message, function() {
            rest[collection].remove(id);
          });
        }

      };

      return _methods;

    }
  ]);
})();
