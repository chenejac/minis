(function() {
  'use strict';

  /**
   * @ngdoc service
   * @name utils
   * @module emShared
   *
   * @description
   * The `utils` service bundles together a variety of helper functions.
   */
  angular.module('emAdminUtility').factory('utils',
  ['$q', '$filter', 'config', 'pinesNotifications', '$translate',
  function($q, $filter, config, pinesNotifications, $translate) {

    var baseNotifyMsgOptions = {
      history: false,
      width: '450px'
    };

    var noticeMsgOptions = _.extend({}, baseNotifyMsgOptions, {
      title: $translate.instant('WARNING'),
      type: 'notice',
      icon: 'fa fa-exclamation',
      cornerclass: 'alert-warning'
    });

    var successMsgOptions = _.extend({}, baseNotifyMsgOptions, {
      title: $translate.instant('SUCCESS'),
      type: 'success'
    });

    var errorMsgOptions = _.extend({}, baseNotifyMsgOptions, {
      title: $translate.instant('ERROR'),
      type: 'error'
    });

    var infoMsgOptions = _.extend({}, baseNotifyMsgOptions, {
      title: $translate.instant('NOTIFICATION'),
      type: 'info'
    });

    function doNotify(defaults, msg, options) {
      if (_.isArray(msg)) {
        msg = (msg.length > 1 ? '<p>' + msg.join('</p><p>') + '</p>' : msg[0]);
      }
      pinesNotifications.notify(_.extend({}, defaults, options, {text: msg}));
    }

    /**
     * @ngdoc method
     * @name  utils#convertToUtc
     * @param  {Date} incDate - The localized date to convert to a UTC date.
     * @return {Date} Return a UTC date to account for timezones, etc.
     * @description
     * **INTERNAL**
     *
     * Converts an incoming (localized) date value to a UTC based date which
     * removes the difficulties when choosing dates in a timezone which is behind
     * GMT.
     *
     * ### Pre-conditions
     * incDate must not be null.
     */
    function convertToUtc(incDate) {
      return incDate;
    }

    var _utils = {
      /**
       * @ngdoc method
       * @name utils#noticeMsg
       *
       * @param {string|string[]} msg - message(s) to display on the notification
       * @param {Object} [options] - options further modifying the notification's behaviour
       *
       * @description
       * Creates a _notice_ level toaster notification.
       */
      noticeMsg: doNotify.bind(null, noticeMsgOptions),

      /**
       * @ngdoc method
       * @name utils#successMsg
       *
       * @param {string|string[]} msg - message(s) to display on the notification
       * @param {Object} [options] - options further modifying the notification's behaviour
       *
       * @description
       * Creates a _success_ level toaster notification.
       */
      successMsg: doNotify.bind(null, successMsgOptions),

      /**
       * @ngdoc method
       * @name utils#errorMsg
       *
       * @param {string|string[]} msg - message(s) to display on the notification
       * @param {Object} [options] - options further modifying the notification's behaviour
       *
       * @description
       * Creates an _error_ level toaster notification.
       */
      errorMsg: doNotify.bind(null, errorMsgOptions),

      /**
       * @ngdoc method
       * @name utils#infoMsg
       *
       * @param {string|string[]} msg - message(s) to display on the notification
       * @param {Object} [options] - options further modifying the notification's behaviour
       *
       * @description
       * Creates an _info_ level toaster notification.
       */
      infoMsg: doNotify.bind(null, infoMsgOptions),

      /**
       * @ngdoc method
       * @name utils#stringToDate
       *
       * @param {string} value - string value to convert to date
       *
       * @description
       * Converts `value` into date.
       *
       * @returns {Date|null} null if `value` is falsy, otherwise a date
       */
      stringToDate: function(value) {
        return value ? convertToUtc(new Date(value)) : null;
      },

      /**
       * @ngdoc method
       * @name utils#dateToString
       *
       * @param {Date} value - date value to convert
       * @param {string} [format='yyyy-MM-dd'] - format to use for string representation
       *
       * @description
       * Converts date to string according to specified format.
       * Uses `module:ng.filter:date` for the transformation.
       *
       * @returns {string}
       */
      dateToString: function(value, format) {
        var val = value;
        if (val) {
          val = convertToUtc(_.isDate(value) ? value : new Date(value));
        }
        return $filter('date')(val, format || 'yyyy-MM-dd');
      },

      /**
       * @ngdoc method
       * @name utils#isValueMissing
       *
       * @param {*} value - value to check
       *
       * @description
       * Numbers, dates, non-empty collections and string are considered as not missing.
       * null, undefined, functions, empty collections and string are considered missing.
       *
       * @returns {boolean}
       */
      isValueMissing: function(value) {
        return _.isEmpty(value) && !_.isNumber(value) && !_.isDate(value) && !_.isBoolean(value);
      },

      /**
       * @ngdoc method
       * @name utils#anyInner
       *
       * @param {Array|Object} collection - an array or object to iterate through
       *
       * @description
       * Application of lodash's `_.any` to collection of collections, e.g. checking values of 1-level nested objects.
       *
       * @returns {boolean}
       */
      anyInner: function(collection) {
        return _.any(collection, function(field) {
          return _.any(field);
        });
      },

      /**
       * @ngdoc method
       * @name utils#capitaliseFirstLetter
       *
       * @param {string|*} value
       *
       * @description
       * Capitalizes first letter of `value` if it is string and returns it.
       * Otherwise returns the original `value`.
       *
       * @returns {string|*}
       */
      capitaliseFirstLetter: function(value) {
        return _.isString(value) ? value.charAt(0).toUpperCase() + value.slice(1) : value;
      },

      /**
       * @ngdoc method
       * @name utils#page
       *
       * @param {Array|*} collection - a collection to get page from
       * @param {number} [current=1] - the page number to get, index is 1-based
       * @param {number} [size] - maximum number of items on a page
       *
       * @description
       * Returns a subset of `collection` that corresponds to a specified page.
       *
       * Default value of size is taken from `module:emShared.object:config#defaultPageSize`.
       *
       * @returns {Array|*}
       * A specified _page_ (subset) of `collection` if it is an array. The original value otherwise.
       */
      // returns selected page of specified size from an array
      page: function(collection, current, size) {
        current = current || 1; // 1-based: first page === 1, second page === 2, etc
        size = size || config.defaultPageSize;
        var start = (current - 1) * size;
        return _.isArray(collection) ? collection.slice(start, start + size) : collection;
      },

      /**
       * @ngdoc method
       * @name utils#elapsedHoursMinutes
       *
       * @param {number} minutes - total number of minutes to convert
       *
       * @description
       * Converts number of minutes into separate hours and minutes and joins them into a string together their units.
       *
       * @returns {string} string representation of number of hours and minutes
       */
      elapsedHoursMinutes: function(minutes) {
        var hrs = Math.floor(minutes / 60), mins = Math.floor(minutes % 60),
          result = [];

        if (hrs > 0) {
          result.push(hrs + (hrs > 1 ? ' hrs' : ' hr'));
        }
        if (mins > 0) {
          result.push(mins + (mins > 1 ? ' mins' : ' min'));
        }
        return result.join(' ');
      },

      /**
       * @ngdoc method
       * @name utils#elapsedHoursMinutesSeconds
       *
       * @param {number} seconds - total number of seconds to convert
       *
       * @description
       * Converts number of seconds into separate hours, minutes and seconds
       * and joins them into a string together with their units.
       *
       * @returns {string} string representation of number of hours and minutes
       */
      elapsedHoursMinutesSeconds: function(seconds) {
        var hrs = Math.floor(seconds / 3600),
          mins = Math.floor(seconds / 60) - hrs * 60,
          sec = Math.floor(seconds % 60),
          result = [];

        if (hrs > 0) {
          result.push(hrs + (hrs > 1 ? ' hrs' : ' hr'));
        }

        if (mins > 0) {
          result.push(mins + (mins > 1 ? ' mins' : ' min'));
        }

        if (sec > 0) {
          result.push(sec + (sec > 1 ? ' seconds' : ' sec'));
        }
        return result.join(' ');
      },

      /**
       * @ngdoc method
       * @name utils#dateToUTCString
       *
       * @param {Date|string} date - a date to convert
       *
       * @description
       * Uses _moment_ to convert `date` into its string representation in UTC.
       *
       * @returns {string} string representation of date in format `YYYY-MM-DDTHH:mm:ssZ`
       */
      dateToUTCString: function(date) {
        return moment(date).utc().format('YYYY-MM-DDTHH:mm:ss') + 'Z';
      },

      /**
       * @ngdoc method
       * @name utils#propertyToId
       *
       * @param {Array|Object} base - an array or object on which to transform its property
       * @param {string} prop - name of the property to transform
       *
       * @description
       * Transforms specified property from whole nested object into just its `id` field.
       * If `base` is an array then transforms its items.
       * Sets the property to null it it is falsy.
       */
      propertyToId: function(base, prop) {
        if (_.isArray(base)) {
          _.each(base, function(item) { this.propertyToId(item, prop); }, this);
        } else {
          base[prop] = base[prop] ? base[prop].id : null;
        }
      },

      /**
       * @ngdoc method
       * @name utils#getProperty
       *
       * @param {Object} o - object to work on
       * @param {string} prop - property to check
       * @param {*} defaultValue - value to return if property does not exist
       *
       * @description
       * Check whether object `o` has specified property `prop`.
       * If it does then its value is returned. Otherwise the `defaultValue` is returned.
       *
       * @returns {*}
       */
      getProperty: function(o, prop, defaultValue) {
        return _.has(o, prop) ? o[prop] : defaultValue;
      },

      /**
       * @ngdoc method
       * @name utils#sharePropertyById
       *
       * @param {Array} items - collection to go through
       * @param {string} prop - property name
       *
       * @description
       * Modifies item array so items share references to specified property for duplicate values (by id).
       *
       * @returns {Array}
       */
      sharePropertyById: function(items, prop) {
        var propById = _.chain(items).pluck(prop).indexBy('id').value();

        _.each(items, function(item) {
          item[prop] = propById[item[prop].id];
        });

        return items;
      },

      /**
       * @ngdoc method
       * @name utils#ensureValidFn
       *
       * @param {Function} fn - function to check
       * @param {Function} defaultFn - default function to return in case `fn` does not pass
       *
       * @description
       * Check whether `fn` is a function and returns it if it passes. Otherwise returns `defaultFn`.
       *
       * @returns {Function}
       */
      ensureValidFn: function(fn, defaultFn) {
        return _.isFunction(fn) ? fn : defaultFn;
      },

      /**
       * @ngdoc method
       * @name utils#serverErrors
       *
       * @param {Object} data - error result data structure
       * @param {Object} [paths={}] - optional translation object
       * @param {string} [path=''] - current path (used for recursion)
       *
       * @description
       * Transforms server result into array of error strings for display.
       *
       * #### Structure of `paths`:
       * Key-value pairs of error path and its related config, label and messages overwrite.
       * ```javascript
       * {
       *   // path to error message in error object separated by '/'
       *   <errorPath>: {
       *     // label to display in error message instead of the default one (or ''/null for none)
       *     "label": string | null,
       *     // key-value pairs of starts of messages to match and the message to replace it with
       *     "messages": {
       *       <messageStartWith>: string
       *     }
       *   }
       * }
       * ```
       *
       * #### Example of `paths` object:
       * ```javascript
       * {
       *   "foo": {
       *     "label": null
       *   },
       *   "bar_42": {
       *     "label": "bar",
       *     "messages": {
       *       "Critical Error": "Don't ever do that again!",
       *       "Operation cancelled by user": "Be more patient"
       *     }
       *   }
       * }
       * ```
       *
       * @returns {Array}
       */
      serverErrors: function(data, paths, path) {
        if (data) {
          paths = paths || {};

          return _.chain(data).keys().reduce(function(memo, key) {
            var error = data[key];
            if (typeof error === 'string') {
              var translate = paths[path], label, msg;
              if (translate) {
                label = translate.label;
                var mkey = _.chain(translate.messages || {})
                  .keys()
                  .find(function(key) { return new RegExp('^' + key).test(error); })
                  .value();
                msg = mkey ? translate.messages[mkey] : error;
              } else {
                label = path;
                msg = error;
              }
              memo.push((label ? label + ': ' : '') + msg);
            } else {
              memo = memo.concat(this.serverErrors(error, paths, (path ? path + '/' : '') + key));
            }
            return memo;
          }, [], this).value();
        }
        return [];
      },

      /**
       * @ngdoc method
       * @name utils#tautology
       *
       * @description
       * Always returns `true`.
       *
       * @returns {boolean}
       */
      tautology: function() { return true; },

      /**
       * @ngdoc method
       * @name utils#contradiction
       *
       * @description
       * Always returns `false`.
       *
       * @returns {boolean}
       */
      contradiction: function() { return false; },

      /**
       * @ngdoc method
       * @name utils#dataURItoBlob
       *
       * @description
       * Transforms dataURI into blob.
       *
       * Based on http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
       *
       * @returns {Blob}
       */
      dataURItoBlob: function(dataURI) {
        /* global unescape */
        var data = dataURI.split(',');

        // convert base64/URLEncoded data component to raw binary data held in a string
        var byteString = (data[0].indexOf('base64') >= 0) ? atob(data[1]) : unescape(data[1]);

        // separate out the mime component
        var mimeString = data[0].split(':')[1].split(';')[0];

        // write the bytes of the string to a typed array
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
          ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ia], {type: mimeString});
      },

      prepareSearchTokens: function(query) {
        var tokens = _.reject(query.split(/ +/), function(value) {
          return value === ' ';
        });
        var last = tokens[tokens.length - 1];
        if (tokens.length > 1) {
          return '+' + tokens.slice(0, tokens.length - 1).join(' +') + ' +(' + last + '* ' + last + ')';
        } else {
          return '+(' + last + '* ' + last + ')';
        }
      }

    };

    return _utils;

  }]);
})();
