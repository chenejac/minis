'use strict';

/**
 * @ngdoc service
 * @name profile
 * @module emShared
 *
 * @description
 * Keeps users profile and logged in flag, handles updates and logging in/out.
 */
angular.module('emAdminUtility').factory('profile', [
  '$rootScope', '$q', 'session', 'rest', 'config',
  function($rootScope, $q, session, rest, config) {

    var profile, publicProfile;

    function updatePublicProfile() {
      publicProfile = _.freeze(angular.copy(profile));
      $rootScope.$broadcast('profileChanged', publicProfile);
      return publicProfile;
    }

    function resetProfile() {
      profile = {loggedIn: false, user: {}, institution: null, person: null};
      return updatePublicProfile();
    }
    resetProfile();

    function loadAccount() {
      return rest.account.get()
        .then(function(res) {
          profile.user = transformUser(res);
          return res;
        });
    }

    function loadPerson(id) {
      if (id) {
        return rest.persons.get(id)
          .then(function(res) {
            profile.person = res;
          });
      } else {
        return $q.when();
      }
    }

    function loadInstitution(id) {
      if (id) {
        return rest.institutions.get(id)
          .then(function(res) {
            profile.institution = res;
          });
      } else {
        return $q.when();
      }
    }

    function transformUser(inc) {
      var res = angular.copy(inc);
      res.full_name = res.firstName + ' ' + res.lastName;
      res.loggedIn = true;
      _.each(res.authorities, function(authority) {
        switch(authority) {
          case config.roleConstants.ADMIN:
            res.isAdmin = true;
            break;
          case config.roleConstants.INSTITUTION_ADMIN:
            res.isInstitutionAdmin = true;
            break;
          case config.roleConstants.USER:
            res.isUser = true;
            break;
          case config.roleConstants.RESEARCHER:
            res.isResearcher = true;
            break;
        }
      });

      return res;
    }

    var profileServ = {};

    profileServ.update = function(reload) {
      var ls = session.get();
      if (ls.loggedIn) {
        if (!profile.loggedIn || reload) {
          return loadAccount()
            .then(function(res) {
              profile.loggedIn = true;
              session.start(ls.token, res.id, res.personId, res.institutionId);
              return $q.all([loadInstitution(res.institutionId), loadPerson(res.personId)])
                .then(updatePublicProfile);
            });
        }
      } else {
        if (profile.loggedIn || reload) {
          return $q.when(resetProfile());
        }
      }
      return $q.when(publicProfile);
    };

    profileServ.get = function() {
      return publicProfile;
    };


    profileServ.logIn = function(username, password) {
      var data = 'username=' + encodeURIComponent(username) +
                '&password=' + encodeURIComponent(password);
      return rest.login.post(data, null, null, {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
      }).then(function(res) {
        session.start(res.token, res.id, res.personId, res.institutionId);
        return loadAccount()
          .then(function(res) {
            profile.loggedIn = true;
            return $q.all([loadInstitution(res.institutionId), loadPerson(res.personId)])
                .then(updatePublicProfile);
          });        
      })
      .then(function() {
        $rootScope.$broadcast('loggedIn');
        return publicProfile;
      });
    };

    profileServ.setCurrentInstitution = function(institutionId) {
      var ls = session.get();
      profile.institution = institutionId;
      session.start(ls.token, ls.account, ls.person, institutionId);
      return $q.when(updatePublicProfile());
    };

    profileServ.logOut = function() {
      session.end();
      resetProfile();
      $rootScope.$broadcast('loggedOut');
    };

    profileServ.hasRoleAccess = function(state) {
      if(!state || !state.data) {
        return false;
      }
      var roles = state.data.userRoles;
      return _.contains(roles, 'all') || (profile.loggedIn && _.some(roles, function(role) {
        return _.includes(profile.user.authorities, role);
      }));
    };

    return _.freeze(profileServ);
  }
]);
