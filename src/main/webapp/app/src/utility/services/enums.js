(function() {
  'use strict';

  angular.module('emAdminUtility')
  .factory('enums', ['config', '$translate', function(config, $translate) {

    // globally used enumerations
    var arrays = {
      // payment methods distribution types
      distributionTypes: [
        {name: '% Percentages', label: 'Percent', value: 'percent'},
        {name: '$ Dollars', label: 'Amount', value: 'dollars'}
      ],

      authorities: [
        {name: $translate.instant('ADMINISTRATOR'), value: config.roleConstants.ADMIN},
        {name: $translate.instant('ADMIN_INSTITUTION'), value: config.roleConstants.INSTITUTION_ADMIN},
        {name: 'Istraživač', value: config.roleConstants.RESEARCHER},
        {name: 'Korisnik', value: config.roleConstants.USER}
      ]

    }, enums = {};

    // generate 'ByValue' objects
    _.chain(arrays).keys().each(function(key) {
      enums[key] = arrays[key];
      enums[key + 'ByValue'] = _.chain(arrays[key]).map(function(item, idx) {
        return _.extend(item, {index: idx});
      }).indexBy('value').value();
    });

    return _.freeze(enums);
  }]);
})();
