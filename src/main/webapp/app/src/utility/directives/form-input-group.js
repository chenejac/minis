'use strict';

angular.module('emAdminUtility').directive('emFormInputGroup', [ '$translate',
  function($translate) {
    return {
      restrict: 'E',
      transclude: true,
      replace: true,
      require: '^form',
      // attributes:
      //  title: name of group (show at left-hand side)
      //  form: name of the form (default: 'form')
      //  validate: boolean value which specifies the form is being validated
      //   ie. error messages should appear if any errors occurred (default: 'submitted')
      //  errors: array of objects describing and validation errors
      //   that may occur in the group. Takes the form:
      //   [{<inputName>: [{<errorType>: <errorMessage>}]}]
      //   inputName: name of form input that is validated
      //   errorType: form error property name corresponding to validator (eg. 'required', 'pattern', etc.)
      //   errorMessage: message to display when an error is encountered
      scope: {title:'@', type:'@', width: '@', validate:'=', errors:'=', offset:'@', labelWidth:'='},
      templateUrl: function(element, attrs) {
        return 'src/utility/directives/form-input-group-' + (attrs.type || 'horizontal') + '.html';
      },
      compile: function(element, attrs) {

        // set defaults for optional parameters
        var defaults = {
          type: 'horizontal',
          width: '9',
          labelWidth: '3',
          validate: 'submitted',
          errors: '[]',
          offset: 0
        };

        _.chain(defaults).keys().each(function(key) {
          if (!attrs[key]) { attrs[key] = defaults[key]; }
        });

        // return link function
        return function(scope, element, attrs, form) {

          // transforms input attribute 'errors' into something more easy to work with internally
          function calcValidations() {
            scope.validations =  _.reduce(scope.errors, function(memo, error) {
              var ekey = _.keys(error)[0];
              return memo.concat(_.reduce(error[ekey], function(memo, validation) {
                var error = _.keys(validation)[0];
                return memo.concat({name: ekey, error: error, message: $translate.instant(validation[error])});
              }, []));
            }, []);
          }
          calcValidations();

          scope.$watch('errors', calcValidations);

          scope.hasError = function(v) {
            if (form) {
              var field = form[v.name];
              return field && field.$error[v.error] && (field.$dirty || scope.validate);
            }
            return false;
          };

          scope.hasAnyErrors = function() {
            return scope.validate && form && _.any(scope.errors, function(error) {
              return form[_.keys(error)[0]].$invalid;
            });
          };
        };
      }
    };
  }
]);
