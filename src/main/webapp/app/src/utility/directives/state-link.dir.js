(function() {
  'use strict';

  var mod = angular.module('emAdminUtility');

  mod.controller('MinisNavController',
    ['$scope', '$state', 'profile',
    function($scope, $state, profile) {
    var self = this;
    var children = {};

    self.initDir = function(stateName) {
      self.state = $state.get(stateName);
      self.isVisible = profile.hasRoleAccess(self.state);
      if (self.parentNav) {
        self.parentNav.registerChild(self.state);
      }
    };

    self.intercept = function($event) {
      if (self.state.abstract) {
        $event.preventDefault();
        $state.go(self.state.data.firstDescendant);
      }
    };

    $scope.$on('profileChanged', function() {
      self.isVisible = profile.hasRoleAccess(self.state);
    });
  }]);

  mod.directive('mnsNavLink', ['$compile', 'ngShowDirective', '$interval', function($compile, ngShowDirective, $interval) {
    var ngShow = ngShowDirective[0];

    return {
      restrict: 'E',
      terminal: ngShow.terminal,
      priority: ngShow.priority - 1,
      transclude: true,
      replace: true,
      scope: {
        title: '@',
        icon: '@',
        mnsSref: '@'
      },
      controller: 'MinisNavController',
      controllerAs: 'navCtrl',
      templateUrl: function(element, attrs) {
        if (_.has(attrs, 'mnsTreeview')) {
          return 'src/utility/directives/state-link-treeview.html';
        } else {
          return 'src/utility/directives/state-link.html';
        }
      },
      compile: function() {
        return {
          pre: function(scope, element, attrs, ctrl, transclude) {
            var stateName = scope.mnsSref;
            scope.navCtrl.initDir(stateName);
            attrs.ngShow = function() {
              return scope.navCtrl.isVisible;
            }

            ngShow.link.apply(ngShow, [scope, element, attrs, ctrl, transclude]);
          }
        }
      }
    }
  }]);
})();
