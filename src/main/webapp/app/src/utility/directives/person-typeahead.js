'use strict';

angular.module('emAdminUtility').directive('emPersonTypeahead', [
  '$timeout', '$log', 'rest', 'utils',
  function($timeout, $log, rest, utils) {
    return {
      templateUrl: function() {
        return 'src/utility/directives/person-typeahead.html';
      },
      restrict: 'E',
      replace: true,
      scope: {
        value: '=',         // search content ('', undefined, or selected company object)
        message: '=?',      // optional, message content, changes based on status
        query: '=',         // search query    
        filter: '&?',       // optional, filtering of found results
        firstnamestring: '@',
        onSelect: '&?',     // optional, action to perform when company is selected
        onReset: '&?',      // optional, action to perform when selected company is reset
        customTemplate: '@?' // optional, if present use the custom template html file.
      },
      controller: ['$scope', function($scope) {
        $scope.loading = false;
        $scope.previousValue = null;
        $scope.minLength = 3;
        $scope.delay = 20;
      }],
      link: function(scope, element, attrs) {
        var onSelect = attrs.onSelect ? scope.onSelect : _.noop,
        onReset = attrs.onReset ? scope.onReset : _.noop,
        filter = attrs.filter ? scope.filter : function() { return true; };

        scope.search = function(query) {
          scope.message = '';
          scope.query= query;
          if (_.size(query) < scope.minLength) {
            if (!_.isEmpty(query)) {
              scope.message = 'Search query is too short';
            }
            scope.loading = false;
            return [];
          } else {
            query = utils.prepareSearchTokens(scope.firstnamestring + ' ' + query);
            return rest.searchPersons.get(query)
            .then(function(persons) {
              var filteredPersons = _.filter(persons, function(person) {
                return filter({person: person});
              });
              scope.message = filteredPersons.length + ' results found';
              return filteredPersons;
            }, function(err) {
              $log.error(err);
              scope.message = 'There was an error, try again';
              return [];
            })
            ['finally'](function() {
              scope.loading = false;
            });
          }
        };

        scope.reset = function() {
          if (scope.previousValue && !scope.value) {
            $timeout(function() {
              onReset();
            });
          }
          if (scope.value === '') {
            scope.message = '';
          }
          scope.previousValue = scope.value;
        };

        scope.select = function() {
          scope.previousValue = scope.value;
          scope.message = '';
          $timeout(function() {
            onSelect({person: scope.value});
          });
        };
      }
    };
  }
  ]);
