(function() {
  'use strict';

  angular.module('emAdminUtility').directive('minValidate', function() {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, element, attrs, ngModel) {
        var validators = scope.$eval(attrs.minValidate);
        var useNgValidators = _.has(attrs, 'useNgValidators');

        function validate(value) {
          var valid = true;
          _.each(validators, function(validator) {
            if (valid) {
              valid = validator.evaluator(value);
              ngModel.$setValidity(validator.name, valid);
              return;
            }
            ngModel.$setValidity(validator.name, true);
          });

          return value;
        }

        if (useNgValidators) {
          //Use angular validators instead of parsers and formatters
          _.each(validators, function(validator) {
            ngModel.$validators[validator.name] = validator.evaluator;
          });
        } else {
          //For DOM -> model validation
          ngModel.$parsers.unshift(validate);

          //For model -> DOM validation
          ngModel.$formatters.unshift(validate);
        }
      }
    };
  });
})();
