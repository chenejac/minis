'use strict';

angular.module('emAdminUtility').directive('emPersonSearchRegistration', [
  '$timeout', '$log', 'rest', 'utils', '$translate',
  function($timeout, $log, rest, utils, $translate) {
    return {
      templateUrl: function() {
        return 'src/utility/directives/person-search-registration.html';
      },
      restrict: 'E',
      replace: true,
      scope: {
        value: '=',         // search content ('', undefined, or selected company object)
        usrform: '=?',
        newPerson : '=?',
        message: '=?',      // optional, message content, changes based on status
        placeholder: '@?',  // optional, placeholder to show in the field
        filter: '&?',       // optional, filtering of found results
        onSelect: '&?',     // optional, action to perform when company is selected
        onReset: '&?',      // optional, action to perform when selected company is reset
        customTemplate: '@?', // optional, if present use the custom template html file.
        searchFields: '=?',
        endpoint: '@'
      },
      controller: ['$scope', function($scope) {
        $scope.loading = false;
        $scope.previousValue = null;
        $scope.minLength = 2;
        $scope.delay = 20;
      }],
      link: function(scope, element, attrs) {
        var prepareTokens = 'prepareTokens' in attrs;
        scope.endpoint = scope.endpoint || 'searchPersons';
        var onSelect = attrs.onSelect ? scope.onSelect : _.noop,
        onReset = attrs.onReset ? scope.onReset : _.noop,
        filter = attrs.filter ? scope.filter : function() { return true; };

        scope.search = function(query) {
          scope.message = '';
          if (_.size(query) < scope.minLength) {
            if (!_.isEmpty(query)) {
              scope.message = 'Search query is too short';
            }
            scope.loading = false;
            return [];
          } else {
            return rest[scope.endpoint].get(
              prepareTokens ? utils.prepareSearchTokens(query) : query, 
              scope.searchFields ? {fields: scope.searchFields} : undefined)
            .then(function(persons) {
              var filteredPersons = _.filter(persons, function(person) {
                return filter({person: person});
              });
              scope.message = filteredPersons.length + ' results found';
              var newPersonCommand = {id: -1, dateOfBirth: $translate.instant('NEW_PERSON_USER_REGISTRATION')};
              filteredPersons.unshift(newPersonCommand);
              return filteredPersons;
            }, function(err) {
              $log.error(err);
              scope.message = 'There was an error, try again';
              return [];
            })
            ['finally'](function() {
              scope.loading = false;
            });
          }
        };

        scope.reset = function() {
          if (scope.previousValue && !scope.value) {
            $timeout(function() {
              onReset();
            });
          }
          if (scope.value === '') {
            scope.message = '';
          }
          scope.previousValue = scope.value;
        };

        scope.select = function() {
          if(scope.value.id==-1){
            scope.usrform=true;
            scope.newPerson={};
          } else scope.usrform=false;

          scope.previousValue = scope.value;
          scope.message = '';
          $timeout(function() {
            onSelect({person: scope.value});
          });
          
        };

        scope.savePerson = function(form){
          scope['submittedBasic']= true;
          if (form.$valid) {
            rest.persons.save(scope.newPerson)
            .then(function(response) {
              scope.value=response;
              scope.previousValue = scope.value;
              scope.message = '';
              $timeout(function() {
                onSelect({person: scope.value});
                scope.usrform=false;
                scope.newPerson={};
              });
              utils.successMsg($translate.instant('PERSON_SAVE_SUCCESS'));
            }, function(error) {
              utils.errorMsg($translate.instant('ERROR'));
            });
          }
        };



      }
    };
  }
  ]);
