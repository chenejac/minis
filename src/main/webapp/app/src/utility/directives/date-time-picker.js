'use strict';

angular.module('emAdminUtility').directive('emDateTimePicker',
[function() {
  return {
    restrict: 'E',
    require: 'ngModel',
    // attributes:
    //  disabledFn: optional function to disable dates
    //  options: date picker options + extra property time
    //   specifies whether or not to allow setting the time
    scope: {
      disabledFn:'&',
      options:'=?',
      ngRequired:'=?',
      ngDisabled:'=?',
      ngChanged:'=?'
    },
    templateUrl: 'src/utility/directives/date-time-picker.html',
    controller: ['$scope', function($scope) {
      $scope.dateOptions = _.extend({startingDay: 0, time: false, showButtonBar: true, datepickerAppendToBody: false},
        $scope.options);
    }],
    link: function(scope, element, attr, ngModel) {

      var data = scope.data = {};

      scope.$watch(
        function() { return ngModel.$modelValue; },
        function() {
          data.date = ngModel.$modelValue;
        }
      );

      scope.$watch(
        'data.date',
        function() {
          if (
            (!ngModel.$modelValue && data.date) ||
            (ngModel.$modelValue && !data.date) ||
            (
              ngModel.$modelValue && data.date &&
              ngModel.$modelValue.getTime() !== data.date.getTime()
            )
          ) {
            ngModel.$setViewValue(data.date);
          }
        }
      );

      scope.dateDisabled = function(date, mode) {
        return scope.disabledFn ? scope.disabledFn({date: date, mode: mode}) : false;
      };

      scope.picker = function($event, state) {
        $event.preventDefault();
        $event.stopPropagation();
        scope.opened = state;
      };

      scope.togglePicker = function($event) {
        scope.picker($event, !scope.opened);
      };
    }
  };
}]);
