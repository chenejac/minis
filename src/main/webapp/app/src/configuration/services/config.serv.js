(function() {
  'use strict';

  var mod = angular.module('emMinisConfiguration');

  mod.factory('config', ['HOST', 'API_PATH', function(HOST, API_PATH) {
    return _.freeze({
      api: {
        host: HOST,
        path: API_PATH
      },
      pollingDelay: 5000,
      pollingStatus: {
        success: 'SUCCESS',
        pending: 'PENDING'
      },
      roleConstants: {
        ADMIN: 'ROLE_ADMIN',
        INSTITUTION_ADMIN: 'ROLE_INSTITUTION_ADMIN',
        USER: 'ROLE_USER',
        RESEARCHER: 'ROLE_RESEARCHER'
      },
      debug: false
    });
  }]);
})();
