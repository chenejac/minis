(function() {
  'use strict'

  angular
    .module('theme')
    .directive('fixHeight', ['$window', '$interval', '$location', function ($window, $interval, $location) {
      return {
        restrict: 'A',
        scope: true,
        link: function (scope, element, attr) {
          var sidebar = $(".sidebar");
          var heightInterval;
          scope.docHeight = $(sidebar).height();
          scope.setHeight = function () {
            var neg = $('.main-header').outerHeight() + $('.main-footer').outerHeight();
            var window_height = $(window).height();
            sidebar = $(".sidebar");
            if ($("body").hasClass("fixed")) {
              $(".content-wrapper, .right-side").css('min-height', window_height - $('.main-footer').outerHeight());
            } else {
              if (window_height >= sidebar.height()) {
                $(".content-wrapper, .right-side").css('min-height', window_height - neg);
              } else {
                $(".content-wrapper, .right-side").css('min-height', sidebar.height());
              }
            }
          };
          scope.$on('$viewContentLoaded', function(event){
            scope.setHeight();
          });
          $(window).on('resize', function () {
            scope.setHeight();
          });
          $(window).load(function() {
            scope.setHeight();            
           });
          var resetHeight = function () {
            scope.docHeight = $(sidebar).height();
          };
          if (!heightInterval) {
            heightInterval = $interval(resetHeight, 1000);
          }
          element.on('$destroy', function() {
            $interval.cancel(heightInterval);
          });
          
        }
      };
    }]).directive('loginColor', ['$state', function($state) {
      return {
        restrict: 'A',
        scope: true,
        link: function(scope) {          
          scope.$on('$viewContentLoaded', function(event){
            var state = $state.current.name
            if ( state === 'login' || state === 'forgotPassword'
              || state === 'finishResetPassword') {
              $('body').addClass('login-page');
            } else {
              $('body').removeClass('login-page');
            }
          });
        }
      }
    }]).directive('toggleSidebar', [function() {
      return {
        restrict: 'A',
        link: function(scope, element, attr) {
          var toggleSidebar = function() {
            $('body').toggleClass('sidebar-collapse');
            $('#companySearch').toggleClass('sidebar-form');
          };
          element.bind('click', toggleSidebar);
        }
      }
    }]).directive('icheck', ['$timeout', '$parse', function($timeout, $parse) {
      return {
          require: '?ngModel',
          link: function($scope, element, $attrs, ngModel) {
              return $timeout(function() {
                  var parentLabel = element.parent('label');
                  if (parentLabel.length)
                    parentLabel.addClass('icheck-label');
                  var value;
                  value = $attrs['value'];

                  $scope.$watch($attrs['ngModel'], function(newValue){
                      $(element).iCheck('update');
                  });

                  $scope.$watch($attrs['ngDisabled'], function(newValue){
                      $(element).iCheck('update');
                  });

                  return $(element).iCheck({
                      checkboxClass: 'icheckbox_minimal-blue',
                      radioClass: 'iradio_minimal-blue'

                  }).on('ifChanged', function(event) {
                      if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
                          $scope.$apply(function() {
                              return ngModel.$setViewValue(event.target.checked);
                          });
                      }
                      if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
                          return $scope.$apply(function() {
                              return ngModel.$setViewValue(value);
                          });
                      }
                  });
              });
          }
      };
  }]);
    /////////////////////////////////////////////////////////////////////////////////
    // Jako brlja nav menu i primorava navigaciju pomocu klika, sto nam ne odgovara.
    /////////////////////////////////////////////////////////////////////////////////
    /*.directive('treeview', [function() {
      return {
        restrict: 'A',
        scope: true,
        link: function (scope, element, attr) {
          var _this = this;
          var animationSpeed = 200;
          var menu = '.sidebar';
          $(document).on('click', menu + ' li a', function (e) {
            var $this = $(this);
            var checkElement = $this.next();

            //Check if the next element is a menu and is visible
            if ((checkElement.is('.treeview-menu')) && (checkElement.is(':visible'))) {
              //Close the menu
              checkElement.slideUp(animationSpeed, function () {
                checkElement.removeClass('menu-open');
                //_this.layout.fix();
              });
              checkElement.parent("li").removeClass("active");
            }
            //If the menu is not visible
            else if ((checkElement.is('.treeview-menu')) && (!checkElement.is(':visible'))) {
              //Get the parent menu
              var parent = $this.parents('ul').first();
              //Close all open menus within the parent
              var ul = parent.find('ul:visible').slideUp(animationSpeed);
              //Remove the menu-open class from the parent
              ul.removeClass('menu-open');
              //Get the parent li
              var parent_li = $this.parent("li");

              //Open the target menu and add the menu-open class
              checkElement.slideDown(animationSpeed, function () {
                //Add the class active to the parent li
                checkElement.addClass('menu-open');
                parent.find('li.active').removeClass('active');
                parent_li.addClass('active');
                //Fix the layout in case the sidebar stretches over the height of the window
                
              });
            }
            //if this isn't a link, prevent the page from being redirected
            if (checkElement.is('.treeview-menu')) {
              e.preventDefault();
            }
          });
        }
      }
    }]);*/
})();
