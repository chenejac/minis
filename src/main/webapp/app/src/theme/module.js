(function() {
  'use strict';
  /**
   * @ngdoc Overview
   * @name  module:emPlugins
   * @description
   * A module to bridge jQuery plugins with angular structures.
   */
  angular.module('theme', []);
})();
