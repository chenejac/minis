(function() {
  'use strict';

  var mod = angular.module('emMinisAdministration');

  mod.controller('FunctionController',
  ['$log', '$scope', 'collections', 'utils', 'rest',
  function($log, $scope, collections, utils, rest) {
    var self = this;
    this.functions = [];
    collections.initPaging(self);
    collections.getList('functions', self);

    this.getList = function() {
      return collections.getList('functions', self);
    };

    this.addFunction = function() {
      var func  = {
        _edit: true,
        edit: {} 
      };
      self.functions.push(func);
    };

    this.editFunction = function(func) {
      func.edit = angular.copy(func);
      func._edit = true;
    };
    this.cancelFunction = function(func, idx) {
      if (!func.id) {
        self.functions.splice(idx,1);
      }
      func._edit= false;
    };

    this.saveFunction = function(func, form) {
      func._submitted = true;
      if (form.$valid) {
        func._edit= false;
        var data = func.edit;
        func.name = func.edit.name
        func.description = func.edit.description;
        rest.functions.save(data)
        .then(function(response) {
          func = response;
          utils.successMsg('Uspešno ste sačuvli funkciju!');
        });
      }
    };
    
  }]);
})();
