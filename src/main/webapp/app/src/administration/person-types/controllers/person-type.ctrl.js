(function() {
  'use strict';

  var mod = angular.module('emMinisAdministration');

  mod.controller('PersonTypeController',
  ['$log', '$scope', 'collections', 'utils', 'rest',
  function($log, $scope, collections, utils, rest) {
    var self = this;
    this.personTypes = [];
    collections.initPaging(self);
    collections.getList('personTypes', self);

    this.getList = function() {
      return collections.getList('personTypes', self);
    };

    this.addPersonType = function() {
      var personType  = {
        _edit: true,
        edit: {} 
      };
      self.personTypes.push(personType);
    };

    this.editPersonType = function(personType) {
      personType.edit = angular.copy(personType);
      personType._edit = true;
    };
    this.cancelPersonType = function(personType, idx) {
      if (!personType.id) {
        self.personTypes.splice(idx,1);
      }
      personType._edit= false;
    };

    this.savePersonType = function(personType, form) {
      personType._submitted = true;
      if (form.$valid) {
        personType._edit= false;
        var data = personType.edit;
        personType.name = personType.edit.name
        personType.description = personType.edit.description;
        rest.personTypes.save(data)
        .then(function(response) {
          personType = response;
          utils.successMsg('Uspešno ste sačuvali tip istraživača!');
        });
      }
    };
    
  }]);
})();
