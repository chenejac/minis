(function() {
  'use strict';

  var mod = angular.module('emMinisAdministration');

  mod.controller('PositionController',
  ['$log', '$scope', 'collections', 'utils', 'rest',
  function($log, $scope, collections, utils, rest) {
    var self = this;
    this.positions = [];
    collections.initPaging(self);
    collections.getList('positions', self);

    this.getList = function() {
      return collections.getList('positions', self);
    };

    this.addPosition = function() {
      var position  = {
        _edit: true,
        edit: {} 
      };
      self.positions.push(position);
    };

    this.editPosition = function(position) {
      position.edit = angular.copy(position);
      position._edit = true;
    };
    this.cancel = function(position, idx) {
      if (!position.id) {
        self.positions.splice(idx,1);
      }
      position._edit= false;
    };

    this.savePosition = function(position, form) {
      position._submitted = true;
      if (form.$valid) {
        position._edit= false;
        var data = position.edit;
        position.name = position.edit.name
        position.description = position.edit.description;
        rest.positions.save(data)
        .then(function(response) {
          position = response;
          utils.successMsg('Uspešno ste sačuvali poziciju!');
        });
      }
    };
    
  }]);
})();
