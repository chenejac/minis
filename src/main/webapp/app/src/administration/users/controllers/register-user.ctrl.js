(function() {
  'use strict';

  var mod = angular.module('emMinisAdministration');

  mod.controller('RegisterUserController',
  ['$log', '$scope', 'collections', 'utils', 'rest', '$state', 'patterns', 'enums', 'config', '$translate',
  function($log, $scope, collections, utils, rest, $state, patterns, enums, config, $translate) {
    var self = this;
    this.userAccount = {};
    this.userAccount.authorities = config.roleConstants.USER;
    this.resetAccount = {};
    $scope.patterns = patterns;

    $scope.loadingCounter = 0;

    self.register = function (form) {
      self.submittedBasic = true;
      //form.passwordConfirm.$validate();
      if(form.$valid) {
        if((self.userAccount.institutionId==null || self.userAccount.institutionId==undefined) && self.userAccount.authorities == 'ROLE_INSTITUTION_ADMIN') return;
        var dataObj = angular.copy(self.userAccount);
        dataObj.authorities = [dataObj.authorities];

        if(self.userAccount.authorities == 'ROLE_INSTITUTION_ADMIN'){
          dataObj.institution = self.institutionsMap[dataObj.institutionId];
          dataObj.personId = $scope.search.value.id;
          dataObj.person = $scope.search.value;
        }
        else{
          dataObj.institution = null;
          dataObj.person = null;
          $scope.search.value = null;
          $scope.search.error = false;
        }

        dataObj.login = dataObj.email;
        dataObj.password="test";
        rest.registerUser.save(dataObj).then(function() {
          utils.successMsg($translate.instant('REGISTRATION_SUCCESSFUL'));
          $state.go('users');
        }, function(error) {
          utils.errorMsg(error.data);
        });
      }
    };

    self.pwMatch = function(passwordConfirm) {
      return passwordConfirm === self.userAccount.password;
    };

    self.pwChanged = function(form) {
      form.passwordConfirm.$validate();
    };

    self.reset = function(form) {
      // self.userAccount = {};
      // self.submittedBasic = false;
      // form.$setPristine();
      $state.go("users");
    };

    function loadInstitutions() {
      rest.institutions.all()
      .then(function(results) {
        self.institutions = results;
        self.institutionsMap = _.indexBy(results, 'id');
        $scope.loadingCounter += 1;
      });
    }

    function loadPersons() {
      // rest.persons.all()
      // .then(function(results) {
      //   self.persons = results;
      // });
    }

    function loadAuthorities() {
      self.authorities = ['ROLE_ADMIN','ROLE_INSTITUTION_ADMIN'];
      
      self.authorities = _.map(self.authorities, function(authority) {
        $scope.loadingCounter += 1;
        return enums.authoritiesByValue[authority]
      });

      // rest.authorities.get()
      // .then(function(result) {
      //   self.authorities = _.map(result.authorities, function(authority) {
      //     return enums.authoritiesByValue[authority]
      //   });
      // });
    }

    $scope.search = {};
    this.selectPerson = function(person) {
      if($scope.search.value!=null && $scope.search.value!=undefined){
        self.userAccount.firstName = person.firstName;
        self.userAccount.lastName = person.lastName;
        self.userAccount.email = person.email;
        self.userAccount.created_by = 'admin';
        self.userAccount.person = person;
        $scope.search.value=person;
      }
    };
    
    loadInstitutions();
    loadPersons();
    loadAuthorities();
  }]);
})();
