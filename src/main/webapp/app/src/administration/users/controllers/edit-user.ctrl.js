(function() {
  'use strict';

  var mod = angular.module('emMinisAdministration');

  mod.controller('EditUserController',
  ['$log', '$scope', 'collections', 'utils', 'rest', '$stateParams', '$state', 'patterns', 'config', 'enums',
  function($log, $scope, collections, utils, rest, $stateParams, $state, patterns, config, enums) {
    var self = this;
    var firstEdit = true;
    this.userAccount = {};
    this.resetAccount = {};
    $scope.patterns = patterns;
    $scope.loadingCounter = 0;

    self.register = function (form) {
      self.submittedBasic = true;
      
      if(form.$valid) {
        if((self.userAccount.institutionId==null || self.userAccount.institutionId==undefined) && self.userAccount.authorities == 'ROLE_INSTITUTION_ADMIN') return;
        var dataObj = angular.copy(self.userAccount);
        dataObj.authorities = [dataObj.authorities];
        dataObj.login = dataObj.email;

        if(self.userAccount.authorities == 'ROLE_INSTITUTION_ADMIN'){
          dataObj.institution = self.institutionsMap[dataObj.institutionId];
          dataObj.personId = $scope.search.value.id;
          dataObj.person = $scope.search.value;
        }
        else{
          dataObj.institution = null;
          dataObj.person = null;
          $scope.search.value = null;
          $scope.search.error = false;
        }

        rest.users.save(_.omit(dataObj, 'password', 'confirmPassword')).then(function(){
          utils.successMsg('Uspešno izmenjeni podaci.');
          
        }, function(error) {
          utils.errorMsg(error.data);
        });
      }
    };

    self.pwMatch = function(confirmPassword) {
      return confirmPassword === self.userAccount.password;
    };

    self.pwChanged = function(form) {
      if (firstEdit) {
        self.userAccount.password = '';
        form.confirmPassword.$validate();
        self.userAccount.confirmPassword = '';
        firstEdit = false;
      } else {
        form.confirmPassword.$validate();
      }
    };

    self.reset = function(form) {
      // self.userAccount = angular.copy(self.resetAccount);
      // self.submittedBasic = false;
      // form.$setPristine();
      $state.go("users");
    };

    function loadInstitutions() {
      rest.institutions.all()
      .then(function(results) {
        self.institutions = results;
        self.institutionsMap = _.indexBy(results, 'id');
        $scope.loadingCounter += 1;
      });
    }

    function loadPersons() {
      // rest.persons.all()
      // .then(function(results) {
      //   self.persons = results;
      //   $scope.loadingCounter += 1;
      // });
    }

    function getMainAuthority(userAuthorities) {
      return _.find(userAuthorities, function(auth) {
        return auth !== config.roleConstants.USER;
      });
    }

    function loadUser() {
      rest.users.get($stateParams.id).then(function(result) {
        if(result.login==undefined || result.login==null || result.login=='') result.login=result.email;
        if(result.email==undefined || result.email==null || result.email=='') result.email=result.login;
        result.authorities = getMainAuthority(result.authorities) || config.roleConstants.USER;
        self.userAccount = result;
        self.resetAccount = angular.copy(self.userAccount);
        if (result.personId!=null && result.personId!=undefined)
        {
          rest.persons.get(result.personId).then(function(response) {
            $scope.search.value=response;
            $scope.loadingCounter += 1;
          });
        }
      });
    }

    function loadAuthorities() {
      self.authorities = ['ROLE_ADMIN','ROLE_INSTITUTION_ADMIN'];
      
      self.authorities = _.map(self.authorities, function(authority) {
        $scope.loadingCounter += 1;
        return enums.authoritiesByValue[authority]
      });
      // rest.authorities.get()
      // .then(function(result) {
      //   self.authorities = _.map(result.authorities, function(authority) {
      //     $scope.loadingCounter += 1;
      //     return enums.authoritiesByValue[authority]
      //   });
      // });
    };

    $scope.search = {};
    this.selectPerson = function(person) {
      if($scope.search!=null && $scope.search!=undefined){
        self.userAccount.firstName = person.firstName;
        self.userAccount.lastName = person.lastName;
        self.userAccount.email = person.email;
        self.userAccount.created_by = 'admin';
        self.userAccount.person = person;

      }
    };
    
    loadInstitutions();
    loadPersons();
    loadAuthorities();
    loadUser();
  }]);
})();
