(function() {
  'use strict';

  var mod = angular.module('emMinisAdministration');

  mod.controller('MinisUserListController', 
    ['$scope', '$state', 'collections', function($scope, $state, collections) {
    var self = this;
    this.sortCriteria = "login";
    this.sortOrder = "asc";
    
    collections.initPaging(self);
    collections.getList('users', self, {
      sortBy: self.sortCriteria,
      orderBy: self.sortOrder
    });

    this.userSearchFields = [
      'firstName',
      'lastName',
      'person.firstName',
      'person.lastName',
      'login'
    ];
    
    this.getList = function() {
      return collections.getList('users', self, {
        sortBy: self.sortCriteria,
        orderBy: self.sortOrder
      });
    };
    this.edit = function(id) {
      $state.go('editUser', {id: id});
    };

    this.selectUser = function(user) {
      //setInstitution(institution.id);
      $state.go('editUser', {id: user.id});
      $scope.search.value = null;
      $scope.search.error = false;
    };


    this.changeOrder = function(attr){
      if(attr == this.sortCriteria){
        if(this.sortOrder=='asc') this.sortOrder='desc';
        else this.sortOrder = 'asc';
      }
      else
      {
        this.sortCriteria = attr;
        this.sortOrder = "asc";
      }

      collections.initPaging(self);
      collections.getList('users', self, {
        sortBy: self.sortCriteria,
        orderBy: self.sortOrder
      });
    };





  }]);
})();
