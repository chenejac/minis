(function() {
  'use strict';

  var mod = angular.module('emMinisAdministration');

  mod.controller('InstitutionTypesController',
  ['$log', '$scope', 'collections', 'utils', 'rest',
  function($log, $scope, collections, utils, rest) {
    var self = this;
    this.institutionTypes = [];
    collections.initPaging(self);
    collections.getList('institutionTypes', self);

    this.getList = function() {
      return collections.getList('institutionTypes', self);
    };

    this.addType = function() {
      var type  = {
        _edit: true,
        edit: {} 
      };
      self.institutionTypes.push(type);
    };

    this.editType = function(type) {
      type.edit = angular.copy(type);
      type._edit = true;
    };
    this.cancel = function(type, idx) {
      if (!type.id) {
        self.institutionTypes.splice(idx,1);
      }
      type._edit= false;
    };

    this.saveType = function(type, form) {
      type._edit= false;
      type._submitted = true;
      if (form.$valid) {
        type._edit= false;
        var data = type.edit;
        type.name = type.edit.name
        type.description = type.edit.description;
        console.log(type);
        rest.institutionTypes.save(data)
        .then(function(response) {
          type = response;
          utils.successMsg('Uspešno ste sačuvali tip!');
        });
      }
    };
    
  }]);
})();
