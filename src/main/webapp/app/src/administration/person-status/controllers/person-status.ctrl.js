(function() {
  'use strict';

  var mod = angular.module('emMinisAdministration');

  mod.controller('PersonStatusController',
  ['$log', '$scope', 'collections', 'utils', 'rest',
  function($log, $scope, collections, utils, rest) {
    var self = this;
    this.personStatus = [];
    collections.initPaging(self);
    collections.getList('personStatus', self);

    this.getList = function() {
      return collections.getList('personStatus', self);
    };

    this.addStatus = function() {
      var status  = {
        _edit: true,
        edit: {} 
      };
      self.personStatus.push(status);
    };

    this.editStatus = function(status) {
      status.edit = angular.copy(status);
      status._edit = true;
    };
    this.cancel = function(status, idx) {
      if (!status.id) {
        self.personStatus.splice(idx,1);
      }
      status._edit= false;
    };

    this.saveStatus = function(status, form) {
      status._submitted = true;
      if (form.$valid) {
        status._edit= false;
        var data = status.edit;
        status.name = status.edit.name
        status.description = status.edit.description;
        rest.personStatus.save(data)
        .then(function(response) {
          status = response;
          utils.successMsg('Uspešno ste sačuvali status!');
        });
      }
    };
    
  }]);
})();
