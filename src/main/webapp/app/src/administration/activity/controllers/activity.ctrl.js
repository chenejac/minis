(function() {
  'use strict';

  var mod = angular.module('emMinisAdministration');

  mod.controller('ActivityController',
  ['$log', '$scope', 'collections', 'utils', 'rest',
  function($log, $scope, collections, utils, rest) {
    var self = this;
    this.activitys = [];
    collections.initPaging(self);
    collections.getList('activitys', self);

    this.getList = function() {
      return collections.getList('activitys', self);
    };

    this.addActivity = function() {
      var activity  = {
        _edit: true,
        edit: {} 
      };
      self.activitys.push(activity);
    };

    this.editActivity = function(activity) {
      activity.edit = angular.copy(activity);
      activity._edit = true;
    };
    this.cancelActivity = function(activity, idx) {
      if (!activity.id) {
        self.activitys.splice(idx,1);
      }
      activity._edit= false;
    };

    this.saveActivity = function(activity, form) {
      activity._submitted = true;
      if (form.$valid) {
        activity._edit= false;
        var data = activity.edit;
        activity.name = activity.edit.name;
        activity.code = activity.edit.code;
        activity.description = activity.edit.description;
        rest.activitys.save(data)
        .then(function(response) {
          activity = response;
          utils.successMsg('Uspešno ste sačuvali delatnost!');
        });
      }
    };
    
  }]);
})();
