(function() {
  'use strict';

  var mod = angular.module('emMinisAdministration');

  mod.controller('ResearchAreaController',
  ['$log', '$scope', 'collections', 'rest', 'utils',
  function($log, $scope, collections, rest, utils) {
    var self = this;
    this.researchArea = [];
    collections.initPaging(self);
    collections.getList('researchArea', self);

    this.getList = function() {
      return collections.getList('researchArea', self);
    };

    this.addArea = function() {
      var area  = {
        _edit: true,
        edit: {} 
      };
      self.researchArea.push(area);
    };
        
    this.editArea = function(area) {
      area.edit = angular.copy(area);
      area._edit = true;
    };

    this.deleteArea = function(id, index){
      collections.deleteItem('researchArea', id, 'Obrisi oblast istrazivanja', 'Da li ste sigurni?', function(index) {
        self.researchArea.splice(index,1);
      });
    };

    this.cancel = function(area, idx) {
      if (!area.id) {
        self.researchArea.splice(idx,1);
      }
      area._edit= false;
    };

    this.saveArea = function(area, form) {
      area._submitted = true;
      if (form.$valid) {
        area._edit= false;
        var data = area.edit;
        area.name = area.edit.name
        area.description = area.edit.description;        
        rest.researchArea.save(data)
        .then(function(response) {
          area = response;
          utils.successMsg('Uspešno ste sačuvali oblast istraživanja!');
        });
      }
    };
    
  }]);
})();
