(function() {
  'use strict';

  var mod = angular.module('emMinisAdministration');

  mod.controller('CategoryController',
  ['$log', '$scope', 'collections', 'utils', 'rest',
  function($log, $scope, collections, utils, rest) {
    var self = this;
    this.categorys = [];
    collections.initPaging(self);
    collections.getList('categorys', self);

    this.getList = function() {
      return collections.getList('categorys', self);
    };

    this.addCategory = function() {
      var category  = {
        _edit: true,
        edit: {} 
      };
      self.categorys.push(category);
    };

    this.editCategory = function(category) {
      category.edit = angular.copy(category);
      category._edit = true;
    };
    this.cancelCategory = function(category, idx) {
      if (!category.id) {
        self.categorys.splice(idx,1);
      }
      category._edit= false;
    };

    this.saveCategory = function(category, form) {
      category._submitted = true;
      if (form.$valid) {
        category._edit= false;
        var data = category.edit;
        category.name = category.edit.name
        category.description = category.edit.description;
        rest.categorys.save(data)
        .then(function(response) {
          category = response;
          utils.successMsg('Uspešno ste sačuvali kategoriju!');
        });
      }
    };
    
  }]);
})();
