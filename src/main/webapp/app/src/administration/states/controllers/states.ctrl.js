(function() {
  'use strict';

  var mod = angular.module('emMinisAdministration');

  mod.controller('StateController',
  ['$log', '$scope', 'collections', 'utils', 'rest',
  function($log, $scope, collections, utils, rest) {
    var self = this;
    this.states = [];
    collections.initPaging(self);
    collections.getList('states', self);

    this.getList = function() {
      return collections.getList('states', self);
    };

    this.addState = function() {
      var state  = {
        _edit: true,
        edit: {} 
      };
      self.states.push(state);
    };

    this.editState = function(state) {
      state.edit = angular.copy(state);
      state._edit = true;
    };
    this.cancelState = function(state, idx) {
      if (!state.id) {
        self.states.splice(idx,1);
      }
      state._edit= false;
    };

    this.saveState = function(state, form) {
      state._submitted = true;
      if (form.$valid) {
        state._edit= false;
        var data = state.edit;
        state.name = state.edit.name
        state.description = state.edit.description;
        rest.states.save(data)
        .then(function(response) {
          state = response;
          for(var i=0;i<self.states.length;i++) if(self.states[i].id==state.id) {self.states[i]=state; break;}
          utils.successMsg('Uspešno ste sačuvali državu!');
        });
      }
    };
    
  }]);
})();
