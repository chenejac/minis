(function() {
  'use strict';

  var mod = angular.module('emMinisAdministration');

  mod.controller('CitiesController',
  ['$log', '$scope', 'collections', 'utils', 'rest', 'patterns',
  function($log, $scope, collections, utils, rest, patterns) {
    var self = this;
    $scope.patterns = patterns;
    this.cities = [];
    collections.initPaging(self);
    collections.getList('cities', self);

    this.getList = function() {
      return collections.getList('cities', self);
    };
    collections.getAll(self, 'states');

    this.addCity = function() {
      var city  = {
        _edit: true,
        edit: {} 
      };
      self.cities.push(city);
    };

    this.editCity = function(city) {
      city.edit = angular.copy(city);
      city._edit = true;
      city.edit.state = self.states[city.edit.state.id];
    };
    this.cancelCity = function(city, idx) {
      if (!city.id) {
        self.cities.splice(idx,1);
      }
      city._edit= false;
    };

    this.saveCity = function(city, form) {
      city._submitted = true;
      if (form.$valid) {
        city._edit= false;
        var data = city.edit;
        city.name = city.edit.name;
        city.zipCode = city.edit.zipCode;
        city.description = city.edit.description;
        city.state = city.edit.state;
        rest.cities.save(data)
        .then(function(response) {
          city = response;
          city._submitted = false;
          utils.successMsg('Uspešno ste sačuvali grad!');
        });
      }
    };
    
  }]);
})();
