(function() {
  'use strict';

  var mod = angular.module('emMinisAdministration');

  mod.controller('TownShipController',
  ['$log', '$scope', 'collections', 'utils', 'rest',
  function($log, $scope, collections, utils, rest) {
    var self = this;
    this.townShips = [];
    collections.initPaging(self);
    collections.getList('townShips', self);

    this.getList = function() {
      return collections.getList('townShips', self);
    };
    collections.getAll(self, 'states');

    this.addTownShip = function() {
      var townShip  = {
        _edit: true,
        edit: {} 
      };
      self.townShips.push(townShip);
    };

    this.editTownShip = function(townShip) {
      townShip.edit = angular.copy(townShip);
      townShip._edit = true;
      townShip.edit.state = self.states[townShip.edit.state.id];
    };
    this.cancelTownShip = function(townShip, idx) {
      if (!townShip.id) {
        self.townShips.splice(idx,1);
      }
      townShip._edit= false;
    };

    this.saveTownShip = function(townShip, form) {
      townShip._submitted = true;
      if (form.$valid) {
        townShip._edit= false;
        var data = townShip.edit;
        townShip.name = townShip.edit.name;
        townShip.state = townShip.edit.state;
        townShip.description = townShip.edit.description;
        rest.townShips.save(data)
        .then(function(response) {
          townShip = response;
          utils.successMsg('Uspešno ste sačuvali opštinu!');
        });
      }
    };
    
  }]);
})();
