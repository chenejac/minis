(function() {
  'use strict';

  /**
   * @ngdoc Overview
   * @name  module:emMinisRouting
   * @description
   * The emMinisRouting module is responsible for setting up all of the routing in
   * the Employii Minis web application.
   */
  var mod = angular.module('emMinisRouting', ['ui.router', 'emMinisConfiguration']);

  mod.config(['$stateProvider', 'configProvider', function($stateProvider, configProvider) {
    var config = configProvider.$get();
    $stateProvider.state('main', {
      templateUrl: 'src/main.html',
      abstract: true,
      resolve: {
        Profile: ['profile', function(profile) { return profile.update(); }]
      },
      data: {
        userRoles: ['all']
      }
    });
    $stateProvider.state('login', {
      templateUrl: 'src/authentication/login.view.html',
      controller: 'LoginController',
      url: '/login',
      controllerAs: 'authCtrl',
      data: {
        userRoles: ['all']
      }
    });
    $stateProvider.state('forgotPassword', {
      templateUrl: 'src/authentication/forgot-password.view.html',
      controller: 'ForgotPasswordController',
      url: '/forgot-password',
      data: {
        userRoles: ['all']
      }
    });
    $stateProvider.state('finishResetPassword', {
      templateUrl: 'src/authentication/finish-reset-password.view.html',
      controller: 'FinishResetController',
      url: '/reset/finish?key',
      data: {
        userRoles: ['all']
      }
    });
    $stateProvider.state('changePassword', {
      templateUrl: 'src/authentication/change-password.view.html',
      url: '/changepassword',
      parent: 'main'
    });
    $stateProvider.state('institutions', {
      templateUrl: 'src/institutions/institutions-list.view.html',
      url: '/institutions',
      parent: 'main',
      data: {
        userRoles: [
          config.roleConstants.ADMIN
        ]
      }
    });
    $stateProvider.state('adminInstitution', {
      templateUrl: 'src/institutions/institution-details.view.html',
      url: '/admin-institution/:id',
      parent: 'main',
      controller: ['$scope', 'profile', function($scope, profile) {
        $scope.institutionId = profile.get().user.institutionId;
      }],
      data: {
        userRoles: [
          config.roleConstants.INSTITUTION_ADMIN
        ]
      }
    });
    $stateProvider.state('addInstitution', {
      templateUrl: 'src/institutions/institution-details.view.html',
      url: '/institution/:id',
      parent: 'main',
      data: {
        userRoles: [
          config.roleConstants.ADMIN
        ]
      }
    });

    $stateProvider.state('persons', {
      templateUrl: 'src/persons/persons-list.view.html',
      url: '/persons',
      parent: 'main',
      data: {
        userRoles: [
          config.roleConstants.ADMIN,
          config.roleConstants.INSTITUTION_ADMIN
        ]
      }
    });
    $stateProvider.state('addPerson', {
      templateUrl: 'src/persons/person-details.view.html',
      url: '/persons/:id',
      parent: 'main',
      data: {
        userRoles: [
          config.roleConstants.ADMIN,
          config.roleConstants.INSTITUTION_ADMIN
        ]
      }
    });
    $stateProvider.state('administration', {
      template: '<ui-view></ui-view>',
      url: '/administration',
      parent: 'main',
      abstract: true,
      data: {
        firstDescendant: 'institutionTypes',
        userRoles: [
          config.roleConstants.ADMIN
        ]
      }
    });
    $stateProvider.state('institutionTypes', {
      templateUrl: 'src/administration/institution-types/institution-types.view.html',
      url: '/institution-types',
      parent: 'administration',
      data: {
        userRoles: [
          config.roleConstants.ADMIN
        ]
      }
    });
    $stateProvider.state('institutionStatus', {
      templateUrl: 'src/administration/institution-status/institution-status.view.html',
      url: '/institution-status',
      parent: 'administration',
      data: {
        userRoles: [
          config.roleConstants.ADMIN
        ]
      }
    });
    $stateProvider.state('researchArea', {
      templateUrl: 'src/administration/research-area/research-area.view.html',
      url: '/research-area',
      parent: 'administration',
      data: {
        userRoles: [
          config.roleConstants.ADMIN,
          config.roleConstants.INSTITUTION_ADMIN
        ]
      }
    });
    $stateProvider.state('users', {
      templateUrl: 'src/administration/users/users-list.view.html',
      url: '/users',
      parent: 'administration',
      data: {
        userRoles: [
          config.roleConstants.ADMIN
        ]
      }
    });
    $stateProvider.state('function', {
      templateUrl: 'src/administration/function/function.view.html',
      url: '/function',
      parent: 'administration',
      data: {
        userRoles: [
          config.roleConstants.ADMIN
        ]
      }
    });
    $stateProvider.state('position', {
      templateUrl: 'src/administration/position/position.view.html',
      url: '/position',
      parent: 'administration',
      data: {
        userRoles: [
          config.roleConstants.ADMIN
        ]
      }
    });
    $stateProvider.state('states', {
      templateUrl: 'src/administration/states/states.view.html',
      url: '/states',
      parent: 'administration',
      data: {
        userRoles: [
          config.roleConstants.ADMIN
        ]
      }
    });
    $stateProvider.state('cities', {
      templateUrl: 'src/administration/cities/cities.view.html',
      url: '/cities',
      parent: 'administration',
      data: {
        userRoles: [
          config.roleConstants.ADMIN
        ]
      }
    });
    $stateProvider.state('activitys', {
      templateUrl: 'src/administration/activity/activity.view.html',
      url: '/activitys',
      parent: 'administration',
      data: {
        userRoles: [
          config.roleConstants.ADMIN
        ]
      }
    });
     $stateProvider.state('townShips', {
      templateUrl: 'src/administration/townShips/townShips.view.html',
      url: '/townShips',
      parent: 'administration',
      data: {
        userRoles: [
          config.roleConstants.ADMIN
        ]
      }
    });
    $stateProvider.state('registerUser', {
      templateUrl: 'src/administration/users/register-user.view.html',
      url: '/users/register',
      parent: 'administration',
      data: {
        userRoles: [
          config.roleConstants.ADMIN
        ]
      }
    });
    $stateProvider.state('editUser', {
      templateUrl: 'src/administration/users/edit-user.view.html',
      url: '/users/:id/edit',
      parent: 'administration',
      data: {
        userRoles: [
          config.roleConstants.ADMIN
        ]
      }
    });
    $stateProvider.state('categories', {
      templateUrl: 'src/administration/category/category.view.html',
      url: '/categories',
      parent: 'administration',
      data: {
        userRoles: [
          config.roleConstants.ADMIN
        ]
      }
    });
    $stateProvider.state('personStatus', {
      templateUrl: 'src/administration/person-status/person-status.view.html',
      url: '/personStatus',
      parent: 'administration',
      data: {
        userRoles: [
          config.roleConstants.ADMIN
        ]
      }
    });
    $stateProvider.state('personTypes', {
      templateUrl: 'src/administration/person-types/person-type.view.html',
      url: '/personTypes',
      parent: 'administration',
      data: {
        userRoles: [
          config.roleConstants.ADMIN
        ]
      }
    });

  }]);
})();
