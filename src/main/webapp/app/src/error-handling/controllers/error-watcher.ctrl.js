(function() {
  'use strict';

  var mod = angular.module('emErrorHandling');
  /**
   * @ngdoc Object
   * @name  module:emErrorHandling.ErrorWatchingController
   * @param  {$rootScope.Scope} $rootScope - The root scope for the application
   * @param  {$rootScope.Scope} $scope     - The scope for this controller.
   * @param  {errorManager} errorManager)  - The errorManager service to use to clear errors.
   * @description
   * The ErrorWatchingController is intended to be used as an extension to Employii AngularJS
   * controllers in order to watch for server errors.
   *
   * ## Extending
   * To extend the other AngularJS controller functionality to include ErrorWatchingController
   * functionality:
   * ```js
   *   [controller declaration] {
   *     var _this = this;
   *     angular.extend(this, $controller('ErrorWatchingController',
   *       {$scope: $scope, $rootScope: $rootScope, errorManager: errorManager}));
   *   }
   * ```
   *
   * ## Calling
   * To call the ErrorWatchingController functionality, prefix the method with _this:
   * ```js
   *   _this.watchCall(promise, route);
   * ```
   */
  mod.controller('ErrorWatchingController', ['$rootScope', '$scope', 'errorManager',
  function($rootScope, $scope, errorManager) {
    var _this = this;
    _this.unbindFunctions = {};

    $scope.serverError = false;
    $scope.customErrorList = {};

    /**
     * @ngdoc function
     * @name errorListener
     * @methodOf module:emErrorHandling.ErrorWatchingController
     * @param  {Event} event      - The event triggering the listener call.
     * @param  {Object} errorData - The error data which will be used for displaying the error message.
     */
    this.errorListener = function(event, errorData) {
      $scope.serverError = errorData;
      if (!errorData) {
        $scope.customErrorList = {};
        _this.unbindFunctions[event.name]();
        delete _this.unbindFunctions[event.name];
      }
    };

    /**
     * @ngdoc function
     * @name  watchCall
     * @methodOf module:emErrorHandling.watchCall
     * @param  {Promise} promise - The promise to watch for an error or success result from.
     * @param  {string} route    - The route name which will be used for the event key.
     * @return {Promise}         - The original promise with a custom then attached.
     * @description
     * If the promise receives a success resolution, the errors will be cleared and the
     * event listener will be unbound from the root scope.
     */
    this.watchCall = function(promise, route) {
      var key = route + 'ServerError';
      if (!_this.unbindFunctions[key]) {
        _this.unbindFunctions[key] = $rootScope.$on(key, _this.errorListener);
      }
      return promise.then(function(success) {
        errorManager.clearErrors(route);
        return success;
      });
    };

    /**
     * @ngdoc function
     * @name  receivedErrors
     * @methodOf module:emErrorHandling.ErrorWatchingController
     * @param  {Array} errors - The list of errors.
     * @description
     * Sets the errors holder for problems caused by user entered data to be _errors_.
     *
     * Contrary to the _errorListener_ method above, this method takes only the error
     * values from the result object regardless of the return value from the server.
     */
    this.receivedErrors = function(errors) {
      $scope.dataErrors = errors ? _.uniq(errors) : errors;
    };

    /**
     * @ngdoc function
     * @name  addToReceivedErrors
     * @methodOf module:emErrorHandling.ErrorWatchingController
     * @param {Array} newErrors - The new errors to add to the list.
     * @description
     * Adds to the current list of errors a new set of warning items.
     *
     * Contrary to the _receivedErrors_ method above, this method does not replace
     * the contents of the errors array.  It appends the new results to it for display.
     */
    this.addToReceivedErrors = function(newErrors) {
      if ($scope.dataErrors && _.isArray($scope.dataErrors)) {
        for (var idx in newErrors) {
          $scope.dataErrors.push(newErrors[idx]);
        }
      } else {
        $scope.dataErrors = newErrors;
      }
    };

    /**
     * @ngdoc function
     * @name  receivedWarnings
     * @methodOf module:emErrorHandling.ErrorWatchingController
     * @param  {Array} warnings - The list of warnings.
     * @description
     * Sets the warnings holder for problems caused by user entered data to be _warnings.
     *
     * Contrary to the _errorListener_ method above, this method takes only the warning
     * values from the result object regardless of the return value from the server.
     */
    this.receivedWarnings = function(warnings) {
      $scope.dataWarnings = warnings;
    };

    /**
     * @ngdoc function
     * @name  addToReceivedWarnings
     * @methodOf module:emErrorHandling.ErrorWatchingController
     * @param {Array} newWarnings - The new warnings to add to the list.
     * @description
     * Adds to the current list of warnings a new set of warning items.
     *
     * Contrary to the _receivedWarnings_ method above, this method does not replace
     * the contents of the warning array.  It appends the new results to it for display.
     */
    this.addToReceivedWarnings = function(newWarnings) {
      if ($scope.dataWarnings && _.isArray($scope.dataWarnings)) {
        for (var idx in newWarnings) {
          $scope.dataWarnings.push(newWarnings[idx]);
        }
      } else {
        $scope.dataWarnings = newWarnings;
      }
    };
  }]);
})();
