(function() {
  'use strict';

  /**
   * @ngdoc overview
   * @name module:emErrorHandling
   * @description
   *
   * The em-error-handling module provides error handling capabilities and hooks for the
   * remainder of the web application.  These capabilities include various services,
   * controllers, directives, etc. to present the user with pertinent information when
   * an error occurs and/or to direct information to the Employii Sentry.
   *
   * ## Provided Services
   *
   * 1. errorManager: A general configuration and management service for error loggging / tracking.
   * 1. responseHandler: A service to handle HTTP error responses from Restangular.
   *
   * ## Provided Controllers
   *
   * To be created...
   *
   * ## Provided Directives
   *
   * To be created...
   */
  angular.module('emErrorHandling', ['ngRaven']);
})();
