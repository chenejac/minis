(function() {
  'use strict';
  var mod = angular.module('emErrorHandling');

  /**
   * @ngdoc directive
   * @name  module:emErrorHandling.emServerErrorDisplay
   * @description
   * The em-server-error-display directive provides the error display div for
   * server errors to the user.
   */
  mod.directive('emServerErrorDisplay', function() {
    return {
      restrict: 'E',
      templateUrl: 'src/error-handling/directives/server-error-display.dir.html',
      scope: {
        serverError: '=',
        title: '@',
        dismissable: '=?'
      }
    };
  });

  mod.directive('emCustomServerErrorDisplay', function() {
    return {
      restrict: 'E',
      templateUrl: 'src/error-handling/directives/custom-server-error-display.dir.html',
      transclude: true,
      scope: {
        serverError: '=',
        title: '@',
        dismissable: '=?'
      }
    };
  });

  /**
   * @ngdoc directive
   * @name  module:emErrorHandling.emDataErrorDisplay
   * @description
   * The em-data-error-display directive provides the error display div for
   * errors caused by data issues to the user.
   */
  mod.directive('emDataErrorDisplay', function() {
    return {
      restrict: 'E',
      templateUrl: 'src/error-handling/directives/data-error-display.dir.html',
      scope: {
        errors: '=',
        warnings: '=',
        errorTitle: '@errorTitle',
        warningTitle: '@warningTitle',
        dismissable: '=?'
      }
    };
  });
})();
