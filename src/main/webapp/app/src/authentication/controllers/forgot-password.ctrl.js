(function() {
  'use strict';

  var mod = angular.module('emMinisAuthentication');

  mod.controller('ForgotPasswordController',
  ['$log', '$scope', 'rest', '$state', 'patterns', 'errorManager', '$controller', '$rootScope', 'utils',
  function($log, $scope, rest, $state, patterns, errorManager, $controller, $rootScope, utils) {
    var _this = this;
    angular.extend(this, $controller('ErrorWatchingController',
    {$scope: $scope, $rootScope: $rootScope, errorManager: errorManager}));

    $scope.data={};
    $scope.pattern = patterns;

    $scope.resetPassword = function(form){
      $scope['submitted']= true;
      if (form.$valid) {
        var data = 'mail=' + encodeURIComponent($scope.data.email);
        return rest.resetInit.post(data, null, null, {
          'Content-Type': 'text/plain;charset=UTF-8'
        }).then(function(response) {
            $state.go('login');
            utils.successMsg('Vaša lozinka će biti poslata na email');
          }, function(error) {
            utils.errorMsg('Greška, uneta lozinka ne postoji u bazi!');
          });
      }

    }


  }]);
})();
