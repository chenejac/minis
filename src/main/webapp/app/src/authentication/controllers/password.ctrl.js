(function() {
  'use strict';

  var mod = angular.module('emMinisAuthentication');

  mod.controller('ChangePasswordController',
  ['$log', '$scope', 'rest', '$state', 'profile', 'session', 'errorManager', '$controller', '$rootScope', 'utils', '$translate',
  function($log, $scope, rest, $state, profile, session, errorManager, $controller, $rootScope, utils, $translate) {
    var _this = this;
    angular.extend(this, $controller('ErrorWatchingController',
    {$scope: $scope, $rootScope: $rootScope, errorManager: errorManager}));

    $scope.data={};
    $scope.data.showErrorText = false;

    this.resetError = function(){
      $scope.data.showErrorText = false;
    }


    this.changePassword = function(form){
      $scope['submitted' +form.$name]= true;
      if (form.$valid) {

        if($scope.data.newPasswordRepeat==$scope.data.newPassword){
          $scope.data.showErrorText = false;
        }else{
          $scope.data.showErrorText = true;
          return;
        }


        if($scope.data.newPasswordRepeat==$scope.data.newPassword){
          rest.post('account/change_password', {"oldPassword":$scope.data.oldPassword, "newPassword":$scope.data.newPassword})
          .then(function(response) {
            if(response.status=='OK'){
              utils.successMsg($translate.instant('PASSWORD_CHANGE_SUCCESS'));
            }
          }, function(error) {
            console.log(error);
            if(error.data.status=='NEW_BAD_FORMED'){
              utils.errorMsg($translate.instant('NEW_PASSWORD_BAD_FORMED'));
            }
            else if(error.data.status=='OLD_BAD_FORMED'){
              utils.errorMsg($translate.instant('OLD_PASSWORD_BAD_FORMED'));
            }
            
          });
        }
      }

    }


  }]);
})();
