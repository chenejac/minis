(function() {
  'use strict';

  var mod = angular.module('emMinisAuthentication');

  mod.controller('LoginController',
  ['$log', '$scope', 'rest', '$state', 'profile', 'session', 'errorManager', '$controller', '$rootScope',
  function($log, $scope, rest, $state, profile, session, errorManager, $controller, $rootScope) {
    var _this = this;
    angular.extend(this, $controller('ErrorWatchingController',
    {$scope: $scope, $rootScope: $rootScope, errorManager: errorManager}));

    $scope.login = {user: null, password: null, rememberMe:false};

    this.login = function() {
      var loginData = {
        username: $scope.login.user,
        password: $scope.login.password
      };
      _this.watchCall(profile.logIn(loginData.username, loginData.password), rest.login.route)
      .then(goToDashboard, function() {
        _this.addToReceivedErrors({data: 'Pogrešno korisničko ime ili lozinka!'});
      });
    };

    this.cancelLogin = function() {
      $scope.login.user = null;
      $scope.login.password = null;
      $scope.serverError = null;
      $scope.dataErrors = null;
    };

    // redirect to dashboard if user is already logged in

    function goToDashboard(p) {
      if (p.loggedIn) {
        if(p.user.isAdmin) {
          $state.go('institutions');
        } else if(p.user.isInstitutionAdmin) {
          $state.go('adminInstitution');
        }
      }
    }
    goToDashboard(profile.get());
  }]);
})();
