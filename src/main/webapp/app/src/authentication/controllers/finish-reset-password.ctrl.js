(function() {
  'use strict';

  var mod = angular.module('emMinisAuthentication');

  mod.controller('FinishResetController',
  ['$log', '$scope', 'rest', '$state', 'patterns', 'errorManager', '$controller',
  '$rootScope', 'utils', '$stateParams',
  function($log, $scope, rest, $state, patterns, errorManager, $controller,
    $rootScope, utils, $stateParams) {
    var _this = this;
    angular.extend(this, $controller('ErrorWatchingController',
    {$scope: $scope, $rootScope: $rootScope, errorManager: errorManager}));
    $scope.data={};

    $scope.resetPassword = function(form){
      $scope['submitted']= true;
      if (form.$valid) {
        var data = {
          key: $stateParams.key,
          newPassword: $scope.data.newPassord
        };
        return rest.resetFinish.post(data).then(function(response) {
            $state.go('login');
            utils.successMsg('Uspešno ste promenili lozinku');
          }, function(error) {
            utils.errorMsg('Greška, pokušajte ponovo. ');
          });
      }

    }


  }]);
})();
