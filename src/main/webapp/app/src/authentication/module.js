(function() {
  'use strict';

  /**
   * @ngdoc Overview
   * @name  module:emMinisAuthentication
   * @description
   * The emMinisAuthentication module contains the services, controllers and logic
   * necessary to authenticate into the Employii Minis web application.
   */
  angular.module('emMinisAuthentication', []);
})();
