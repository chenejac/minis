(function() {
  'use strict';

  var mod = angular.module('emMinisInstitutions');

  mod.controller('AddInstitutionsController',
  ['$log', '$scope', 'collections', '$stateParams', '$state', 'rest', 'utils', 'patterns', 'profile', '$filter', '$translate',
  function($log, $scope, collections, $stateParams, $state, rest, utils, patterns, profile, $filter, $translate) {
    var self = this;
    collections.initPaging(self);
    this.institutionTypes = [];
    this.institutionStatus = [];
    this.researchArea = [];
    this.activitys = [];
    this.connectionsInstitutions = [];
    $scope.data = {}; 
    $scope.search = {}; 
    $scope.connection = {};
    $scope.data.researchAreas = [];
    $scope.data.activities = [];
    this.supetInstitutions = [];
    this.supetInstitutions2 = [];
    this.states = [];
    this.cities = [];
    this.townShips = [];
    $scope.institutionId = $scope.institutionId || $stateParams.id;
    $scope.patterns = patterns;
    this.type = {};
    this.status = {};
    this.state = {};
    this.city = {};
    this.townShip = {};
    $scope.tabsTitle = $translate.instant('ADDING_NEW_INSTITUTION_TITLE');

    $scope.loadingCounter = 0;

    this.ownershipStructures = [
      {id:'1', name: $translate.instant('OWNERSHIP_STRUCTURE_STATE')}, 
      {id:'2', name: $translate.instant('OWNERSHIP_STRUCTURE_PRIVATE')},
      {id:'3', name: $translate.instant('OWNERSHIP_STRUCTURE_MIX')}
      ];  

    $scope.isAdmin = profile.get().user.isAdmin;  
    $scope.months = _.range(13);
    
    
    $scope.activeData = function(connection, data) {
      if (connection) {
        if (!utils.isValueMissing(connection.function)) {
          return 'function' === data;
        }
        if (!utils.isValueMissing(connection.position)) {
          return 'position' === data;
        }
        if (!utils.isValueMissing(connection.employmentPercentage) || !utils.isValueMissing(connection.maxMonthEngagement)) {
          return 'employment' === data;
        }
      }

      return true;
    };
    
    this.loadInstituionsConnections = function (){
      return collections.getList('connectionsInstitutions', self, {institution: $scope.institutionId});
    };

    function loadInstitutionTypes() {
      rest.institutionTypes.all()
      .then(function(results) {
        if ($scope.isAdmin) {
          results.push({name: $translate.instant('ADD_NEW_F'),id: -1});
        }
        self.institutionTypes = _.indexBy(results, 'id');
        $scope.loadingCounter+=1;
      });
    }

    function loadStates() {
      rest.states.all()
      .then(function(results) {
        results.push({name: $translate.instant('ADD_NEW_F'),id: -1});
        self.states = _.indexBy(results, 'id');
        $scope.loadingCounter+=1;
      });
    }
    function loadCities() {
      rest.cities.all()
      .then(function(results) { 
        results.push({name: $translate.instant('ADD_NEW_S'),id: -1});
        self.cities = _.indexBy(results, 'id');
        $scope.loadingCounter+=1;
      });
    }
    function loadTownShips() {
      rest.townShips.all()
      .then(function(results) { 
        results.push({name: $translate.instant('ADD_NEW_F'),id: -1});
        self.townShips = _.indexBy(results, 'id');
        $scope.loadingCounter+=1;
      });
    }

    function loadSupetInstitution() {
      rest.institutions.get('all')
      .then(function(results) {
          if ($scope.institutionId) {
            self.supetInstitutions = [];
            self.supetInstitutions2= [];
            for(var i=0;i<results.length;i++){
              if(results[i].id!=$scope.institutionId){
                self.supetInstitutions.push(results[i]);
                self.supetInstitutions2.push(results[i]);
              }
            }
            self.supetInstitutions2 = _.indexBy(self.supetInstitutions2, 'id');
          }
          else{
            self.supetInstitutions = results;
            self.supetInstitutions2 = _.indexBy(results, 'id');
          }
          $scope.loadingCounter+=1;
      });
    }

    function loadInstitutionStatus() {
      rest.institutionStatus.all()
      .then(function(results) {
        if ($scope.isAdmin) {
          results.push({name: $translate.instant('ADD_NEW_F'),id: -1});
        }    
        self.institutionStatus = _.indexBy(results, 'id');
        $scope.loadingCounter+=1;
      });
    }
    function loadResearchArea() {
      rest.researchArea.all()
      .then(function(results) {
        self.researchArea = results;
        $scope.loadingCounter+=1;
       });
    }

    function loadActivities() {
      rest.activitys.all()
      .then(function(results) {
        self.activitys = results;
        $scope.loadingCounter+=1;
       });
    }

    function loadInstituionsConnections(){
      collections.initPaging(self);
      collections.getList('connectionsInstitutions', self, {institution: $scope.institutionId});
    }

    function loadPersons() {
      rest.persons.all()
      .then(function(results) {
       self.persons = results;
      });
    }

    function loadPositions() {
      rest.positions.all()
      .then(function(results) {
       self.positions = results;
       $scope.loadingCounter+=1;
      });
    }

    function loadFunctions() {
      rest.functions.all()
      .then(function(results) {
       self.functions = results;
       $scope.loadingCounter+=1;
      });
    }

    //loadPersons();
    loadPositions();
    loadFunctions();    
    loadResearchArea();
    loadInstitutionStatus();
    loadInstitutionTypes();
    loadSupetInstitution();
    loadActivities();
    loadStates();
    loadCities();
    loadTownShips();

    if ($scope.institutionId) {
      rest.institutions.get($scope.institutionId)
      .then(function(response) {
        $scope.tabsTitle = $translate.instant('EDITING_INSTITUTION_DATA_TITLE')+" - "+response.name;
        response.accreditationDate = utils.stringToDate(response.accreditationDate);
        response.date = utils.stringToDate(response.date);
        $scope.data = response;
        $scope.data.researchAreas = _.map(response.researchAreas, function(area){ return JSON.stringify(area)});
        $scope.data.activities = _.map(response.activities, function(area){ return JSON.stringify(area)});
        $scope.data.type = response.type ? response.type.id : null;
        $scope.data.city = response.city ? response.city.id : null;
        $scope.data.townShip = response.townShip ? response.townShip.id : null;
        $scope.data.state = response.state ? response.state.id : null; 
        $scope.data.institutionStatus = response.institutionStatus ? response.institutionStatus.id : null; 
        $scope.data.supetInstitution = response.supetInstitution ? response.supetInstitution.id : null;      
      });
      loadInstituionsConnections();
    }


    this.selectPerson = function(person) {
      $scope.connection.person = person.id;
      $scope.search.error = false;
    };

    this.saveType = function(type, form) {
      type._submitted = true;
      if (form.$valid) {
        rest.institutionTypes.save(type)
        .then(function(response) {
          $scope.data.type = response.id;
          self.institutionTypes[response.id] =  response;       
        });
      }
    };

    this.saveState = function(state, form) {
      state._submitted = true;
      if (form.$valid) {
        rest.states.save(state)
        .then(function(response) {
          $scope.data.state = response.id;
          self.states[response.id] =  response;       
        });
      }
    };

    this.saveCity = function(city, form) {
      city._submitted = true;
      city.state = this.states[$scope.data.state];
      if (form.$valid) {
        rest.cities.save(city)
        .then(function(response) {
          $scope.data.city = response.id;
          self.cities[response.id] =  response;       
        });
      }
    };

    this.saveTownShip = function(townShip, form) {
      townShip._submitted = true;
      townShip.state = this.states[$scope.data.state];
      if (form.$valid) {
        rest.townShips.save(townShip)
        .then(function(response) {
          $scope.data.townShip = response.id;
          self.townShips[response.id] =  response;       
        });
      }
    };

    this.saveStatus = function(status, form) {
      status._submitted = true;
      if (form.$valid) {
        rest.institutionStatus.save(status)
        .then(function(response) {
          $scope.data.institutionStatus = response.id;
          self.institutionStatus[response.id] =  response;       
        });
      }
    };

    this.saveInstitution = function(form) {
      $scope['submitted' +form.$name]= true;
      if (form.$valid) {
        $scope.data.researchAreas = _.map($scope.data.researchAreas, function(area){ return JSON.parse(area);});
        $scope.data.activities = _.map($scope.data.activities, function(area){ return JSON.parse(area);});
        $scope.data.type = self.institutionTypes[$scope.data.type];
        if(this.states[$scope.data.state].citiesRegistered){
          $scope.data.city = self.cities[$scope.data.city];
          $scope.data.place = null;
          $scope.data.townShip = self.townShips[$scope.data.townShip];
          $scope.data.townShipText = null;
        }
        else{
          $scope.data.city=null;
          $scope.data.townShip=null;
        }
          
        $scope.data.state = self.states[$scope.data.state];
        $scope.data.institutionStatus = self.institutionStatus[$scope.data.institutionStatus];
        $scope.data.supetInstitution = self.supetInstitutions2[$scope.data.supetInstitution];

        rest.institutions.save($scope.data)
        .then(function(response) {
          $scope.institutionId = response.id;
          response.accreditationDate = utils.stringToDate(response.accreditationDate);
          response.date = utils.stringToDate(response.date);
          $scope.data = response;
          $scope.data.researchAreas = _.map(response.researchAreas, function(area){ return JSON.stringify(area)});
          $scope.data.activities = _.map(response.activities, function(area){ return JSON.stringify(area)});
          $scope.data.type = response.type? response.type.id : null;
          $scope.data.city = response.city? response.city.id : null;
          $scope.data.townShip = response.townShip? response.townShip.id : null;
          $scope.data.state = response.state ? response.state.id : null;
          $scope.data.institutionStatus = response.institutionStatus ? response.institutionStatus.id : null;
          $scope.data.supetInstitution = response.supetInstitution ? response.supetInstitution.id : null;
          //$state.go('addInstitution', {id: reponse.id});
          utils.successMsg($translate.instant('INSTITUTION_SAVE_SUCCESS'));
        }, function(error) {
          utils.errorMsg($translate.instant('ERROR'));
        });
      }
    };
    /******************************************************************
    **************** Person Institution Connection*********************
    *******************************************************************/
    this.savePersonConnection = function(form) {
      $scope.submittedConnection = true;
      if (form.$valid) {
        $scope.connection.institution = {id: $scope.data.id};
        $scope.connection.person = {id: $scope.connection.person};
        $scope.connection.function = $scope.connection.function? {id: $scope.connection.function} : null;
        $scope.connection.position = $scope.connection.position? {id: $scope.connection.position} : null;
        rest.connections.save($scope.connection)
        .then(function(response) {
          utils.successMsg($translate.instant('ENGAGEMENT_SAVED'));
          loadInstituionsConnections();
          $scope.showForm = false;
          $scope.connection = {};
          $scope.search = {};
          $scope.submittedConnection = false;
          form.$setPristine();
        }, function(error) {
          utils.errorMsg($translate.instant('ERROR'));
        });
      }
    };

    this.addConnection = function() {
      $scope.showForm = true;
    };

    this.editConnection = function(connection) {
      $scope.connection = angular.copy(connection);
      $scope.search.value = connection.person.firstName + ' ' + connection.person.lastName;
      $scope.connection.person = $scope.connection.person.id;
      $scope.connection.function = $scope.connection.function? $scope.connection.function.id : null;
      $scope.connection.position = $scope.connection.position? $scope.connection.position.id: null;
      $scope.connection.startDate = utils.stringToDate($scope.connection.startDate);
      $scope.connection.endDate = utils.stringToDate($scope.connection.endDate);
      $scope.showForm = true;
    };


    $scope.reset = function(form){
      $state.go("institutions");
    }

    this.cancel = function(form) {
      $scope.showForm = false;
      $scope.connection = {};
      form.$setPristine();
    };

    this.deleteConnection = function(id) {
      rest.personInstitution.remove(id).then(function() {
        utils.successMsg($translate.instant('ENGAGEMENT_DELETED'));
        loadInstituionsConnections();
      })
    };

  }]);
})();
