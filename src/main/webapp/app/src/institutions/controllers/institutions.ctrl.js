(function() {
  'use strict';

  var mod = angular.module('emMinisInstitutions');

  mod.controller('InstitutionsController',
  ['$log', '$scope', 'collections', '$state', 'profile', 'session', '$http',
  function($log, $scope, collections, $state, profile, session, $http) {
    var self = this;
    var institution = profile.get().institution;
    this.migrated = true;
    this.changed = true;
    this.approved = true;

    this.sortCriteria = "name";
    this.sortOrder = "asc";
    self.loadingIsActiveDownload = false;

   /* if (institution) {
      $state.go('addInstitution', {id: institution});
    }*/
    
    collections.initPaging(self);
    collections.getList('institutions', self, {
      migrated: self.migrated,
      changed: self.changed,
      approved: self.approved,
      sortBy: self.sortCriteria,
      orderBy: self.sortOrder
    });
    
    function setInstitution(id) {
      profile.setCurrentInstitution(id)
      .then(function(){
        $state.go('addInstitution', {id: id});
      });
    }
    this.getList = function() {
      return collections.getList('institutions', self, {
          migrated: self.migrated,
          changed: self.changed,
          approved: self.approved,
          sortBy: self.sortCriteria,
          orderBy: self.sortOrder
        });
    };
    this.edit = function(id) {
      setInstitution(id);
    };

    this.check = function(status) {
      self.getList();
    }

    this.selectInstitution = function(institution) {
      setInstitution(institution.id);
      $scope.search.value = null;
      $scope.search.error = false;
    };

    this.changeOrder = function(attr){
      if(attr == this.sortCriteria){
        if(this.sortOrder=='asc') this.sortOrder='desc';
        else this.sortOrder = 'asc';
      }
      else
      {
        this.sortCriteria = attr;
        this.sortOrder = "asc";
      }

      collections.initPaging(self);
      collections.getList('institutions', self, {
        migrated: self.migrated,
        changed: self.changed,
        approved: self.approved,
        sortBy: self.sortCriteria,
        orderBy: self.sortOrder
      });
    };

    this.exportExcel = function(){
      self.loadingIsActiveDownload = true;
      var parameters = {
        'migrated': self.migrated,
        'changed': self.changed,
        'approved': self.approved,
        'sortBy': self.sortCriteria,
        'orderBy': self.sortOrder
      };
      $http({
          url: 'api/institutions/xls',
          method: "GET",
          params: parameters,
          headers: {
             'Content-type': 'application/json',
             'x-auth-token': session.get().token
          },
          responseType: 'arraybuffer'
      }).success(function (data, status, headers, config) {
          var a = document.createElement("a");
          document.body.appendChild(a);
          a.style = "display: none";
          var blob = new Blob([data], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"});
          var url = window.URL.createObjectURL(blob);
          a.href = url;
          a.download = 'institutions.xls';
          a.click();
          window.URL.revokeObjectURL(url);
          self.loadingIsActiveDownload=false;
      }).error(function (data, status, headers, config) {
        self.loadingIsActiveDownload=false;
      });

    };
    
  }]);
})();
