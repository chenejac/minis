(function() {
  'use strict';

  var mod = angular.module('emMinisPersons');

  mod.controller('ModalInstanceCtrl',
  ['$log', '$scope', 'rest', 'utils', '$translate', '$modalInstance', 'connection', 'functions', 'positions',
  function($log, $scope, rest, utils, $translate, $modalInstance, connection, functions, positions) {
    $scope.connection = connection;

    $scope.positions = positions;
    $scope.positions_map = _.indexBy(positions, 'id');

    $scope.functions = functions;

    $scope.save = function() {
      var form = $scope.personFrom;
      $scope.submittedConnection = true;
      if (form.$valid) {
        $scope.connection.person = {id: connection.person};
        $scope.connection.institution = {id: connection.institution};
        $scope.connection.function = $scope.connection.function ? {id: $scope.connection.function} : null;
        $scope.connection.position = $scope.connection.position ? {id: $scope.connection.position} : null;
        console.log($scope.connection);
        rest.connections.save($scope.connection)
        .then(function() {
          utils.successMsg($translate.instant('ENGAGEMENT_SAVED'));
          form.$setPristine();
        }, function() {
          utils.errorMsg($translate.instant('ERROR'));
        }).finally(function() {
          $modalInstance.close();
        });
      }

    };

    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  }]);
})();
