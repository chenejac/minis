(function() {
  'use strict';

  var mod = angular.module('emMinisPersons');

  mod.controller('AddPersonController',
  ['$log', '$scope', 'collections', '$stateParams', '$state', 'rest', 'utils', 'patterns', 'profile',
  '$translate', '$q', '$modal',
  function($log, $scope, collections, $stateParams, $state, rest, utils, patterns, profile, $translate, $q, $modal) {
    var self = this;
    var user = profile.get().user;
    collections.initPaging(self);
    $scope.connection = {};
    $scope.data = {};
    $scope.dataBackup = {};
    $scope.personId = $stateParams.id;
    this.personStatuses = [];
    this.personTypes = [];
    this.states = [];
    this.cities = [];
    this.category = [];
    this.institutions = [];
    $scope.data.categories = [];
    $scope.categoriesForPerson = [];

    $scope.data.personPositions = [];
    $scope.positionsForPerson = [];
    $scope.isAdmin = profile.get().user.isAdmin;
    $scope.isInstitutionAdmin = profile.get().user.isInstitutionAdmin;
    $scope.tabsTitle = $translate.instant('ADDING_NEW_PERSON_TITLE');
    $scope.displayTypeaheadOnName = false;
    $scope.scopeForms = {};

    $scope.disableConnections = true;
    $scope.patterns = patterns;
    this.type = {};
    this.status = {};
    this.genders = [
      {id:'1', name: $translate.instant('MALE')},
      {id:'2', name: $translate.instant('FEMALE')}
    ];
    $scope.months = _.range(13);

    $scope.loadingCounter = 0;



    this.loadPersonConnections = function() {
      if ($scope.personId) {
        return collections.getList('connectionsPersons', self, {person: $scope.personId});
      }
    };

    loadPersonStatus();
    loadPersonType();
    loadStates();
    loadCities();
    loadCategory();
    loadAllInstitutions();
    loadPositions();
    loadFunctions();

    $scope.jmbgFieldDisabled=false;

    if ($scope.personId) {
      rest.persons.get($scope.personId)
      .then(function(response) {
        response.dateOfBirth = utils.stringToDate(response.dateOfBirth);
        $scope.tabsTitle = $translate.instant('EDITING_PERSON_DATA_TITLE') + " - " + response.firstName + " " + response.lastName;
        $scope.data = response;
        //disable jmbg field if user is not admin and this field exists
        if(profile.get().user.isInstitutionAdmin && response.jmbg!==null && response.jmbg!==undefined){
          $scope.jmbgFieldDisabled = true;
        }
        if ($scope.data.personStatus != null) $scope.data.personStatus = $scope.data.personStatus.id;
        if ($scope.data.type != null) $scope.data.type = $scope.data.type.id;
        loadCategoryStatuses();
        loadPersonPosition();
      });
      loadPersonConnections();
    } else {
      $scope.displayTypeaheadOnName = true;
      //if(profile.get().user.isInstitutionAdmin) $scope.displayTypeaheadOnName=true; //pri unosu novog za admina institucije
    }

    $scope.activeData = function(connection, data) {
      if (connection) {
        if (!utils.isValueMissing(connection.function)) {
          return 'function' === data;
        }
        if (!utils.isValueMissing(connection.position)) {
          return 'position' === data;
        }
        if (!utils.isValueMissing(connection.employmentPercentage) || !utils.isValueMissing(connection.maxMonthEngagement)) {
          return 'employment' === data;
        }
      }

      return true;
    };

    function loadCategoryStatuses() {
      rest.personCategorys.get('' + $scope.personId + '/for_person').then(function(response2) {
          $scope.categoriesForPerson = [];
          //eliminate all terminated - endDate not null
          for (var i = 0; i < response2.length; i++)
            if (response2[i].endDate == null)
              $scope.categoriesForPerson.push(response2[i]);

          $scope.data.categories = [];
          for (var i = 0; i < response2.length; i++)
            if (response2[i].endDate == null)
            $scope.data.categories.push(response2[i].category);
          $scope.data.categories = _.map($scope.data.categories, function(area) { return JSON.stringify(area)});
          $scope.loadingCounter += 1;
        });
    }

    function loadPersonPosition() {
      rest.personPositions.get('' + $scope.personId + '/for_person').then(function(response2) {
        $scope.data.previousPositionId = null;
        $scope.data.previousPosition = null;
        //eliminate all terminated - endDate not null
        for (var i = 0; i < response2.length; i++)
          if (response2[i].endDate == null) {
            $scope.data.previousPositionId = response2[i].position.id;
            $scope.data.previousPosition = response2[i];
            $scope.data.personPositions = response2[i].position.id;
          }
        $scope.loadingCounter += 1;
      });
    }

    $scope.$on('connectionLoaded', function() {
      if ($scope.isInstitutionAdmin && $scope.connectionLoaded && $scope.personId) {
        console.log(self.connectionsPersons);
        console.log($scope.connectionLoaded);
        console.log('dsadsadsadsdsad');
        var engaged = _.find(self.connectionsPersons, function(con) {
                return con.institution.id === user.institutionId;
              });
        if (!engaged) {
          var connection = {};
          connection.person = $scope.personId;
          connection.institution = user.institutionId;
          var modalInstance = $modal.open({
                  templateUrl: 'src/persons/modal-connections.html',
                  controller: 'ModalInstanceCtrl',
                  resolve: {
                    connection: function() {
                      return connection;
                    },
                    functions: function() {
                      return self.functions;
                    },
                    positions: function() {
                      return self.positions;
                    }
                  }
                });

          modalInstance.result.then(function() {
            self.loadPersonConnections();
          }, function() {
                    $log.info('Modal dismissed at: ' + new Date());
                  });
        }
      }
    });
    function loadPersonConnections() {
      if ($scope.personId) {
        collections.initPaging(self);
        collections.getList('connectionsPersons', self, {person: $scope.personId})
        .then(function(connections) {
          self.connectionsPersons = connections;
          $scope.connectionLoaded = true;
          $scope.$emit('connectionLoaded');
        });
      }
    }

    function loadPersonStatus() {
      rest.personStatus.all()
      .then(function(results) {
        if (profile.get().user.isAdmin)
          results.unshift({name: $translate.instant('ADD_NEW_M'), id: -1});
        self.personStatuses = results;
        self.personStatuses2 = _.indexBy(results, 'id');
        $scope.loadingCounter += 1;
      });
    }
    function loadPersonType() {
      rest.personTypes.all()
      .then(function(results) {
        self.personTypes = results;
        results.push({name: $translate.instant('ADD_NEW_M'), id: -1});
        self.personTypes2 = _.indexBy(results, 'id');
        $scope.loadingCounter += 1;
      });
    }
    function loadStates() {
      rest.states.all()
      .then(function(results) {
        self.states = results;
        $scope.loadingCounter += 1;
      });
    }

    function loadCities() {
      rest.cities.all()
      .then(function(results) {
        self.cities = results;
        $scope.loadingCounter += 1;
      });
    }

    function loadCategory() {
      rest.categorys.all()
      .then(function(results) {
        self.category = results;
        $scope.loadingCounter += 1;
      });
    }


    function loadAllInstitutions() {
      if (user.isAdmin) {
        rest.institutions.all()
        .then(function(results) {
          self.institutions = results;
          $scope.loadingCounter += 1;
        });
      } else {
        rest.institutions.get(user.institutionId)
        .then(function(result) {
          self.institutions = [result];
          $scope.loadingCounter += 1;
        });
      }
    }

    function loadPositions() {
      rest.positions.all()
      .then(function(results) {
        self.positions = results;
        self.positions_map = _.indexBy(results, 'id');
        $scope.loadingCounter += 1;
      });
    }

    function loadFunctions() {
      rest.functions.all()
      .then(function(results) {
        self.functions = results;
        $scope.loadingCounter += 1;
      });
    }


    this.savePersonStatus = function(personStatus, form) {
      personStatus._submitted = true;
      if (form.$valid) {
        rest.personStatus.save(personStatus)
        .then(function(response) {
          $scope.data.personStatus = response.id;
          self.personStatuses.splice(self.personStatuses.length - 1, 0, response);
          self.personStatuses2[response.id] =  response;
        });
      }
    };

    this.savePersonType = function(personType, form) {
      personType._submitted = true;
      if (form.$valid) {
        rest.personTypes.save(personType)
        .then(function(response) {
          $scope.data.type = response.id;
          self.personTypes.splice(self.personTypes.length - 1, 0, response);
          self.personTypes2[response.id] =  response;
        });
      }
    };

    this.canAddConnection = function() {
      return (user.isInstitutionAdmin || user.isAdmin);
    };

    this.canEditOrDelete = function(connection) {
      return user.isAdmin || user.institutionId === connection.institution.id;
    };

    this.canDelete = function(connection) {
      return user.isAdmin || ((user.institutionId === connection.institution.id) && utils.isValueMissing(connection.maxMonthEngagement));
    };

    this.isAdminInstitution = function() {
      if (user.isInstitutionAdmin && !user.isAdmin) {
        $scope.connection.institution = user.institutionId;
      }
      return user.isInstitutionAdmin && !user.isAdmin;
    };

    $scope.setFormScope = function(scope) {
      $scope.scopeForms = scope;
    }

    this.savePerson = function(form) {
      if ($scope.displayTypeaheadOnName) {
        $scope['submittedBasicName'] = true;
        if (!$scope.scopeForms.BasicName.$valid) {
          return;
        }

        // validate last name from the second form
        if ($scope.data.lastName != undefined && $scope.data.lastName != null) {
          if ($scope.data.lastName.length == 0) {
            alert($translate.instant('LAST_NAME_MANDATORY'));
            return;
          }
        }else {
          alert($translate.instant('LAST_NAME_MANDATORY'));
          return;
        }
      }

      $scope['submitted' + form.$name] = true;
      //rucna validacija datuma rodjenja
      if ('date' in form.$error) {
        if ('dateOfBirth' in form) {
          form['dateOfBirth'].$invalid = true;
          form['dateOfBirth'].$valid = false;
          form['dateOfBirth'].$error['date'] = true;
        }
      } else {
        if ('dateOfBirth' in form) {
          delete form['dateOfBirth'].$error['date'];
          form['dateOfBirth'].$invalid = false;
          form['dateOfBirth'].$valid = true;
        }
      }
      if (form.$valid) {
        $scope.dataBackup = angular.copy($scope.data);
        $scope.data.personStatus = self.personStatuses2[$scope.data.personStatus];
        $scope.data.type = self.personTypes2[$scope.data.type];
        $scope.data.categories = _.map($scope.data.categories, function(area) { return JSON.parse(area);});

        for (var i = 0; i < self.category.length; i++) {
          var exists = false;
          for (var j = 0; j < $scope.data.categories.length; j++) {
            if ($scope.data.categories[j].id == self.category[i].id) exists = true;
          }

          if (exists) { // is a category of this person -create if doesn't exist
            var existed = false;
            for (var k = 0; k < $scope.categoriesForPerson.length; k++)
              if ($scope.categoriesForPerson[k].category.id == self.category[i].id) existed = true;
            if (!existed) {
              var newCategory = {};
              newCategory.person = $scope.data;
              newCategory.category = self.category[i];
              newCategory.startDate = new Date();
              rest.personCategorys.save(newCategory).then(function(response) {});
            }
          } else { // is not a category of this person - delete it if it was
            var existed = false;
            var oldCategory = null;
            for (var k = 0; k < $scope.categoriesForPerson.length; k++)
              if ($scope.categoriesForPerson[k].category.id == self.category[i].id) {
                existed = true;
                oldCategory = $scope.categoriesForPerson[k];
                oldCategory.endDate = new Date();
              }
            if (existed) rest.personCategorys.save(oldCategory).then(function(response) {});
          }
        }

        if ($scope.data.previousPositionId == null) {
          if ($scope.data.personPositions != null) {
            var obj = {};
            obj.position = self.positions_map[$scope.data.personPositions];
            obj.person = $scope.data;
            obj.startDate = new Date();
            rest.personPositions.save(obj).then(function(response) {
              $scope.data.personPositions = response.position.id;
              $scope.data.previousPositionId = response.position.id;
              $scope.data.previousPosition = response.position;
            });
          }
        }else {
          if ($scope.data.personPositions == null) {
            $scope.data.previousPosition.endDate = new Date();
            rest.personPositions.save($scope.data.previousPosition).then(function(response) {
              $scope.data.personPositions = null;
              $scope.data.previousPositionId = null;
              $scope.data.previousPosition = null;
            });
          }else {
            if ($scope.data.previousPositionId != $scope.data.personPositions) {
              var obj = {};
              obj.position = self.positions_map[$scope.data.personPositions];
              obj.person = $scope.data;
              obj.startDate = new Date();
              $scope.data.previousPositionBackup = angular.copy($scope.data.previousPosition);
              rest.personPositions.save(obj).then(function(response) {
                $scope.data.personPositions = response.position.id;
                $scope.data.previousPositionId = response.position.id;
                $scope.data.previousPosition = response.position;
              });
              $scope.data.previousPositionBackup.endDate = new Date();
              rest.personPositions.save($scope.data.previousPositionBackup).then(function(response) {});
            }
          }
        }

        //if($scope.displayTypeaheadOnName){
        //  $scope.data.firstName = $scope.search.valueFirstName;
        //  $scope.data.lastName = $scope.search.valueLastName;
        //}

        $scope.data.categories = [];
        rest.persons.save($scope.data)
        .then(function(response) {
          $scope.personId = response.id;
          response.dateOfBirth = utils.stringToDate(response.dateOfBirth);
          $scope.data = response;
          if(profile.get().user.isInstitutionAdmin && response.jmbg){
            $scope.jmbgFieldDisabled = true;
          }
          $scope.data.personStatus = response.personStatus ? response.personStatus.id : null;
          $scope.data.type = response.type ? response.type.id : null;
          loadCategoryStatuses();
          loadPersonPosition();
          loadPersonConnections();
          utils.successMsg($translate.instant('PERSON_SAVE_SUCCESS'));
        }, function(error) {
          if (error.data.message.indexOf("jmbg_UNIQUE") > -1) {
            $scope.data.personStatus = null;
            utils.errorMsg($translate.instant('JMBG_UNIQUE'));
          } else{
            utils.errorMsg($translate.instant('ERROR'));
          }
            // restore dropdown values
          $scope.data = angular.copy($scope.dataBackup);
        });
      }
    };

    this.savePersonConnection = function(form) {
      $scope.submittedConnection = true;
      if (form.$valid) {
        $scope.connection.person = {id: $scope.data.id};
        $scope.connection.institution = {id: $scope.connection.institution};
        $scope.connection.function = $scope.connection.function ? {id: $scope.connection.function} : null;
        $scope.connection.position = $scope.connection.position ? {id: $scope.connection.position} : null;
        console.log($scope.connection);
        rest.connections.save($scope.connection)
        .then(function(response) {
          utils.successMsg($translate.instant('ENGAGEMENT_SAVED'));
          loadPersonConnections();
          $scope.showForm = false;
          form.$setPristine();
          $scope.submittedConnection = false;
          $scope.connection = {};
        }, function(error) {
          utils.errorMsg($translate.instant('ERROR'));
        });
      }
    };

    this.addConnection = function() {
      $scope.showForm = true;
    };

    this.editConnection = function(connection) {
      if (self.canEditOrDelete(connection)) {
        $scope.connection = angular.copy(connection);
        $scope.connection.institution = $scope.connection.institution.id;
        $scope.connection.function = $scope.connection.function ? $scope.connection.function.id : null;
        $scope.connection.position = $scope.connection.position ? $scope.connection.position.id : null;
        $scope.connection.startDate = utils.stringToDate($scope.connection.startDate);
        $scope.connection.endDate = utils.stringToDate($scope.connection.endDate);
        $scope.showForm = true;
      }
    };

    this.cancel = function(form) {
      $scope.showForm = false;
      $scope.connection = {};
      form.$setPristine();
    };

    $scope.reset = function() {
      $state.go("persons");
    }

    this.deleteConnection = function(id) {
      rest.personInstitution.remove(id).then(function() {
        utils.successMsg($translate.instant('ENGAGEMENT_DELETED'));
        loadPersonConnections();
      })
    };

    $scope.search = {};
    // PERSON NAME TYPEAHEAD
    this.selectPersonFirstName = function(person) {

      // $scope.data = {};
      // var elem = angular.copy($scope.search.valueFirstName);
      // $scope.search.valueFirstName = {};
      // $scope.search.valueLastName = {};
      // $scope.search.valueFirstName = elem.firstName;
      // $scope.search.valueLastName = elem.lastName;
      // $scope.data.firstName = elem.firstName;
      // $scope.data.lastName = elem.lastName;

      // $scope.personId = person.id;
      // person.dateOfBirth = utils.stringToDate(person.dateOfBirth);
      // $scope.tabsTitle = $translate.instant('EDITING_PERSON_DATA_TITLE') + " - " + person.firstName + " " + person.lastName;
      // $scope.data = person;
      // if ($scope.data.personStatus != null) $scope.data.personStatus = $scope.data.personStatus.id;
      // if ($scope.data.type != null) $scope.data.type = $scope.data.type.id;
      // loadCategoryStatuses();
      // loadPersonPosition();
      // loadPersonConnections();
    };

    this.selectPersonLastName = function(person) {

      $scope.data = {};
      var elem = angular.copy($scope.search.valueLastName);
      $scope.search.valueFirstName = {};
      $scope.search.valueLastName = {};
      $scope.search.valueFirstName = elem.firstName;
      $scope.search.valueLastName = elem.lastName;
      $scope.data.firstName = elem.firstName;
      $scope.data.lastName = elem.lastName;

      $scope.personId = person.id;
      person.dateOfBirth = utils.stringToDate(person.dateOfBirth);
      $scope.tabsTitle = $translate.instant('EDITING_PERSON_DATA_TITLE') + " - " + person.firstName + " " + person.lastName;
      $scope.data = person;
      if ($scope.data.personStatus != null) $scope.data.personStatus = $scope.data.personStatus.id;
      if ($scope.data.type != null) $scope.data.type = $scope.data.type.id;
      loadCategoryStatuses();
      loadPersonPosition();
      loadPersonConnections();
    };

  }]);
})();
