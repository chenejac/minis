(function() {
  'use strict';

  angular.module('emMinisStorage').factory('session',
  ['$rootScope', '$window',
  function($rootScope, $window) {
    var ls = $window.localStorage,
      timeLimit = 18000;

    return _.freeze({
      start: function(token, account, person, institution) {
        ls.setItem('data', JSON.stringify({
          loggedIn: true,
          account: account,
          person: person,
          institution: institution,
          token: token,
          timeStamp: Date.now()
        }));
      },

      get: function() {
        var data = JSON.parse(ls.getItem('data'));
        if (data && ((Date.now() - data.timeStamp) * 0.001) < timeLimit) {
          return data;
        } else {
          return {
            loggedIn: false,
            token: data ? data.token: null,
            account: data ? data.account: null,
            person: data ? data.person: null,
            institution: data ? data.institution: null,
            timeStamp: data ? data.timeStamp : 0};
        }
      },

      ping: function() {
        var data = this.get();
        if (data.loggedIn) {
          this.start(data.token);
        }
      },

      end: function() {
        ls.setItem('data', JSON.stringify({
          loggedIn: false,
          token: null,
          account: null,
          person: null,
          institution: null,
          timeStamp: 0
        }));
      }
    });
  }]);
})();
