package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.InstitutionKeywordsTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.InstitutionKeywordsTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.InstitutionKeywordsTranslationsSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the InstitutionKeywordsTranslationsResource REST controller.
 *
 * @see InstitutionKeywordsTranslationsResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class InstitutionKeywordsTranslationsResourceTest {

    private static final String DEFAULT_KEYWORDS = "AAAAA";
    private static final String UPDATED_KEYWORDS = "BBBBB";
    private static final String DEFAULT_TRANS_TYPE = "AAAAA";
    private static final String UPDATED_TRANS_TYPE = "BBBBB";

    @Inject
    private InstitutionKeywordsTranslationsRepository institutionKeywordsTranslationsRepository;

    @Inject
    private InstitutionKeywordsTranslationsSearchRepository institutionKeywordsTranslationsSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restInstitutionKeywordsTranslationsMockMvc;

    private InstitutionKeywordsTranslations institutionKeywordsTranslations;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        InstitutionKeywordsTranslationsResource institutionKeywordsTranslationsResource = new InstitutionKeywordsTranslationsResource();
        ReflectionTestUtils.setField(institutionKeywordsTranslationsResource, "institutionKeywordsTranslationsRepository", institutionKeywordsTranslationsRepository);
        ReflectionTestUtils.setField(institutionKeywordsTranslationsResource, "institutionKeywordsTranslationsSearchRepository", institutionKeywordsTranslationsSearchRepository);
        this.restInstitutionKeywordsTranslationsMockMvc = MockMvcBuilders.standaloneSetup(institutionKeywordsTranslationsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        institutionKeywordsTranslations = new InstitutionKeywordsTranslations();
        institutionKeywordsTranslations.setKeywords(DEFAULT_KEYWORDS);
        institutionKeywordsTranslations.setTransType(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void createInstitutionKeywordsTranslations() throws Exception {
        int databaseSizeBeforeCreate = institutionKeywordsTranslationsRepository.findAll().size();

        // Create the InstitutionKeywordsTranslations

        restInstitutionKeywordsTranslationsMockMvc.perform(post("/api/institutionKeywordsTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(institutionKeywordsTranslations)))
                .andExpect(status().isCreated());

        // Validate the InstitutionKeywordsTranslations in the database
        List<InstitutionKeywordsTranslations> institutionKeywordsTranslationss = institutionKeywordsTranslationsRepository.findAll();
        assertThat(institutionKeywordsTranslationss).hasSize(databaseSizeBeforeCreate + 1);
        InstitutionKeywordsTranslations testInstitutionKeywordsTranslations = institutionKeywordsTranslationss.get(institutionKeywordsTranslationss.size() - 1);
        assertThat(testInstitutionKeywordsTranslations.getKeywords()).isEqualTo(DEFAULT_KEYWORDS);
        assertThat(testInstitutionKeywordsTranslations.getTransType()).isEqualTo(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void getAllInstitutionKeywordsTranslationss() throws Exception {
        // Initialize the database
        institutionKeywordsTranslationsRepository.saveAndFlush(institutionKeywordsTranslations);

        // Get all the institutionKeywordsTranslationss
        restInstitutionKeywordsTranslationsMockMvc.perform(get("/api/institutionKeywordsTranslationss"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(institutionKeywordsTranslations.getId().intValue())))
                .andExpect(jsonPath("$.[*].keywords").value(hasItem(DEFAULT_KEYWORDS.toString())))
                .andExpect(jsonPath("$.[*].transType").value(hasItem(DEFAULT_TRANS_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getInstitutionKeywordsTranslations() throws Exception {
        // Initialize the database
        institutionKeywordsTranslationsRepository.saveAndFlush(institutionKeywordsTranslations);

        // Get the institutionKeywordsTranslations
        restInstitutionKeywordsTranslationsMockMvc.perform(get("/api/institutionKeywordsTranslationss/{id}", institutionKeywordsTranslations.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(institutionKeywordsTranslations.getId().intValue()))
            .andExpect(jsonPath("$.keywords").value(DEFAULT_KEYWORDS.toString()))
            .andExpect(jsonPath("$.transType").value(DEFAULT_TRANS_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingInstitutionKeywordsTranslations() throws Exception {
        // Get the institutionKeywordsTranslations
        restInstitutionKeywordsTranslationsMockMvc.perform(get("/api/institutionKeywordsTranslationss/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInstitutionKeywordsTranslations() throws Exception {
        // Initialize the database
        institutionKeywordsTranslationsRepository.saveAndFlush(institutionKeywordsTranslations);

		int databaseSizeBeforeUpdate = institutionKeywordsTranslationsRepository.findAll().size();

        // Update the institutionKeywordsTranslations
        institutionKeywordsTranslations.setKeywords(UPDATED_KEYWORDS);
        institutionKeywordsTranslations.setTransType(UPDATED_TRANS_TYPE);

        restInstitutionKeywordsTranslationsMockMvc.perform(put("/api/institutionKeywordsTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(institutionKeywordsTranslations)))
                .andExpect(status().isOk());

        // Validate the InstitutionKeywordsTranslations in the database
        List<InstitutionKeywordsTranslations> institutionKeywordsTranslationss = institutionKeywordsTranslationsRepository.findAll();
        assertThat(institutionKeywordsTranslationss).hasSize(databaseSizeBeforeUpdate);
        InstitutionKeywordsTranslations testInstitutionKeywordsTranslations = institutionKeywordsTranslationss.get(institutionKeywordsTranslationss.size() - 1);
        assertThat(testInstitutionKeywordsTranslations.getKeywords()).isEqualTo(UPDATED_KEYWORDS);
        assertThat(testInstitutionKeywordsTranslations.getTransType()).isEqualTo(UPDATED_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void deleteInstitutionKeywordsTranslations() throws Exception {
        // Initialize the database
        institutionKeywordsTranslationsRepository.saveAndFlush(institutionKeywordsTranslations);

		int databaseSizeBeforeDelete = institutionKeywordsTranslationsRepository.findAll().size();

        // Get the institutionKeywordsTranslations
        restInstitutionKeywordsTranslationsMockMvc.perform(delete("/api/institutionKeywordsTranslationss/{id}", institutionKeywordsTranslations.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<InstitutionKeywordsTranslations> institutionKeywordsTranslationss = institutionKeywordsTranslationsRepository.findAll();
        assertThat(institutionKeywordsTranslationss).hasSize(databaseSizeBeforeDelete - 1);
    }
}
