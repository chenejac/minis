package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.InstitutionStatus;
import rs.ac.uns.ftn.informatika.minis.repository.InstitutionStatusRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.InstitutionStatusSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the InstitutionStatusResource REST controller.
 *
 * @see InstitutionStatusResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class InstitutionStatusResourceTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    @Inject
    private InstitutionStatusRepository institutionStatusRepository;

    @Inject
    private InstitutionStatusSearchRepository institutionStatusSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restInstitutionStatusMockMvc;

    private InstitutionStatus institutionStatus;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        InstitutionStatusResource institutionStatusResource = new InstitutionStatusResource();
        ReflectionTestUtils.setField(institutionStatusResource, "institutionStatusRepository", institutionStatusRepository);
        ReflectionTestUtils.setField(institutionStatusResource, "institutionStatusSearchRepository", institutionStatusSearchRepository);
        this.restInstitutionStatusMockMvc = MockMvcBuilders.standaloneSetup(institutionStatusResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        institutionStatus = new InstitutionStatus();
        institutionStatus.setName(DEFAULT_NAME);
        institutionStatus.setDescription(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createInstitutionStatus() throws Exception {
        int databaseSizeBeforeCreate = institutionStatusRepository.findAll().size();

        // Create the InstitutionStatus

        restInstitutionStatusMockMvc.perform(post("/api/institutionStatuss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(institutionStatus)))
                .andExpect(status().isCreated());

        // Validate the InstitutionStatus in the database
        List<InstitutionStatus> institutionStatuss = institutionStatusRepository.findAll();
        assertThat(institutionStatuss).hasSize(databaseSizeBeforeCreate + 1);
        InstitutionStatus testInstitutionStatus = institutionStatuss.get(institutionStatuss.size() - 1);
        assertThat(testInstitutionStatus.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testInstitutionStatus.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllInstitutionStatuss() throws Exception {
        // Initialize the database
        institutionStatusRepository.saveAndFlush(institutionStatus);

        // Get all the institutionStatuss
        restInstitutionStatusMockMvc.perform(get("/api/institutionStatuss"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(institutionStatus.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getInstitutionStatus() throws Exception {
        // Initialize the database
        institutionStatusRepository.saveAndFlush(institutionStatus);

        // Get the institutionStatus
        restInstitutionStatusMockMvc.perform(get("/api/institutionStatuss/{id}", institutionStatus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(institutionStatus.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingInstitutionStatus() throws Exception {
        // Get the institutionStatus
        restInstitutionStatusMockMvc.perform(get("/api/institutionStatuss/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInstitutionStatus() throws Exception {
        // Initialize the database
        institutionStatusRepository.saveAndFlush(institutionStatus);

		int databaseSizeBeforeUpdate = institutionStatusRepository.findAll().size();

        // Update the institutionStatus
        institutionStatus.setName(UPDATED_NAME);
        institutionStatus.setDescription(UPDATED_DESCRIPTION);

        restInstitutionStatusMockMvc.perform(put("/api/institutionStatuss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(institutionStatus)))
                .andExpect(status().isOk());

        // Validate the InstitutionStatus in the database
        List<InstitutionStatus> institutionStatuss = institutionStatusRepository.findAll();
        assertThat(institutionStatuss).hasSize(databaseSizeBeforeUpdate);
        InstitutionStatus testInstitutionStatus = institutionStatuss.get(institutionStatuss.size() - 1);
        assertThat(testInstitutionStatus.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testInstitutionStatus.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void deleteInstitutionStatus() throws Exception {
        // Initialize the database
        institutionStatusRepository.saveAndFlush(institutionStatus);

		int databaseSizeBeforeDelete = institutionStatusRepository.findAll().size();

        // Get the institutionStatus
        restInstitutionStatusMockMvc.perform(delete("/api/institutionStatuss/{id}", institutionStatus.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<InstitutionStatus> institutionStatuss = institutionStatusRepository.findAll();
        assertThat(institutionStatuss).hasSize(databaseSizeBeforeDelete - 1);
    }
}
