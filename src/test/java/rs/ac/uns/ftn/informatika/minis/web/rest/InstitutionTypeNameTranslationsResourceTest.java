package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.InstitutionTypeNameTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.InstitutionTypeNameTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.InstitutionTypeNameTranslationsSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the InstitutionTypeNameTranslationsResource REST controller.
 *
 * @see InstitutionTypeNameTranslationsResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class InstitutionTypeNameTranslationsResourceTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_TRANS_TYPE = "AAAAA";
    private static final String UPDATED_TRANS_TYPE = "BBBBB";

    @Inject
    private InstitutionTypeNameTranslationsRepository institutionTypeNameTranslationsRepository;

    @Inject
    private InstitutionTypeNameTranslationsSearchRepository institutionTypeNameTranslationsSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restInstitutionTypeNameTranslationsMockMvc;

    private InstitutionTypeNameTranslations institutionTypeNameTranslations;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        InstitutionTypeNameTranslationsResource institutionTypeNameTranslationsResource = new InstitutionTypeNameTranslationsResource();
        ReflectionTestUtils.setField(institutionTypeNameTranslationsResource, "institutionTypeNameTranslationsRepository", institutionTypeNameTranslationsRepository);
        ReflectionTestUtils.setField(institutionTypeNameTranslationsResource, "institutionTypeNameTranslationsSearchRepository", institutionTypeNameTranslationsSearchRepository);
        this.restInstitutionTypeNameTranslationsMockMvc = MockMvcBuilders.standaloneSetup(institutionTypeNameTranslationsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        institutionTypeNameTranslations = new InstitutionTypeNameTranslations();
        institutionTypeNameTranslations.setName(DEFAULT_NAME);
        institutionTypeNameTranslations.setTransType(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void createInstitutionTypeNameTranslations() throws Exception {
        int databaseSizeBeforeCreate = institutionTypeNameTranslationsRepository.findAll().size();

        // Create the InstitutionTypeNameTranslations

        restInstitutionTypeNameTranslationsMockMvc.perform(post("/api/institutionTypeNameTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(institutionTypeNameTranslations)))
                .andExpect(status().isCreated());

        // Validate the InstitutionTypeNameTranslations in the database
        List<InstitutionTypeNameTranslations> institutionTypeNameTranslationss = institutionTypeNameTranslationsRepository.findAll();
        assertThat(institutionTypeNameTranslationss).hasSize(databaseSizeBeforeCreate + 1);
        InstitutionTypeNameTranslations testInstitutionTypeNameTranslations = institutionTypeNameTranslationss.get(institutionTypeNameTranslationss.size() - 1);
        assertThat(testInstitutionTypeNameTranslations.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testInstitutionTypeNameTranslations.getTransType()).isEqualTo(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void getAllInstitutionTypeNameTranslationss() throws Exception {
        // Initialize the database
        institutionTypeNameTranslationsRepository.saveAndFlush(institutionTypeNameTranslations);

        // Get all the institutionTypeNameTranslationss
        restInstitutionTypeNameTranslationsMockMvc.perform(get("/api/institutionTypeNameTranslationss"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(institutionTypeNameTranslations.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].transType").value(hasItem(DEFAULT_TRANS_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getInstitutionTypeNameTranslations() throws Exception {
        // Initialize the database
        institutionTypeNameTranslationsRepository.saveAndFlush(institutionTypeNameTranslations);

        // Get the institutionTypeNameTranslations
        restInstitutionTypeNameTranslationsMockMvc.perform(get("/api/institutionTypeNameTranslationss/{id}", institutionTypeNameTranslations.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(institutionTypeNameTranslations.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.transType").value(DEFAULT_TRANS_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingInstitutionTypeNameTranslations() throws Exception {
        // Get the institutionTypeNameTranslations
        restInstitutionTypeNameTranslationsMockMvc.perform(get("/api/institutionTypeNameTranslationss/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInstitutionTypeNameTranslations() throws Exception {
        // Initialize the database
        institutionTypeNameTranslationsRepository.saveAndFlush(institutionTypeNameTranslations);

		int databaseSizeBeforeUpdate = institutionTypeNameTranslationsRepository.findAll().size();

        // Update the institutionTypeNameTranslations
        institutionTypeNameTranslations.setName(UPDATED_NAME);
        institutionTypeNameTranslations.setTransType(UPDATED_TRANS_TYPE);

        restInstitutionTypeNameTranslationsMockMvc.perform(put("/api/institutionTypeNameTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(institutionTypeNameTranslations)))
                .andExpect(status().isOk());

        // Validate the InstitutionTypeNameTranslations in the database
        List<InstitutionTypeNameTranslations> institutionTypeNameTranslationss = institutionTypeNameTranslationsRepository.findAll();
        assertThat(institutionTypeNameTranslationss).hasSize(databaseSizeBeforeUpdate);
        InstitutionTypeNameTranslations testInstitutionTypeNameTranslations = institutionTypeNameTranslationss.get(institutionTypeNameTranslationss.size() - 1);
        assertThat(testInstitutionTypeNameTranslations.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testInstitutionTypeNameTranslations.getTransType()).isEqualTo(UPDATED_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void deleteInstitutionTypeNameTranslations() throws Exception {
        // Initialize the database
        institutionTypeNameTranslationsRepository.saveAndFlush(institutionTypeNameTranslations);

		int databaseSizeBeforeDelete = institutionTypeNameTranslationsRepository.findAll().size();

        // Get the institutionTypeNameTranslations
        restInstitutionTypeNameTranslationsMockMvc.perform(delete("/api/institutionTypeNameTranslationss/{id}", institutionTypeNameTranslations.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<InstitutionTypeNameTranslations> institutionTypeNameTranslationss = institutionTypeNameTranslationsRepository.findAll();
        assertThat(institutionTypeNameTranslationss).hasSize(databaseSizeBeforeDelete - 1);
    }
}
