package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.CategoryNameTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.CategoryNameTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.CategoryNameTranslationsSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the CategoryNameTranslationsResource REST controller.
 *
 * @see CategoryNameTranslationsResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class CategoryNameTranslationsResourceTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_TRANS_TYPE = "AAAAA";
    private static final String UPDATED_TRANS_TYPE = "BBBBB";

    @Inject
    private CategoryNameTranslationsRepository categoryNameTranslationsRepository;

    @Inject
    private CategoryNameTranslationsSearchRepository categoryNameTranslationsSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restCategoryNameTranslationsMockMvc;

    private CategoryNameTranslations categoryNameTranslations;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CategoryNameTranslationsResource categoryNameTranslationsResource = new CategoryNameTranslationsResource();
        ReflectionTestUtils.setField(categoryNameTranslationsResource, "categoryNameTranslationsRepository", categoryNameTranslationsRepository);
        ReflectionTestUtils.setField(categoryNameTranslationsResource, "categoryNameTranslationsSearchRepository", categoryNameTranslationsSearchRepository);
        this.restCategoryNameTranslationsMockMvc = MockMvcBuilders.standaloneSetup(categoryNameTranslationsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        categoryNameTranslations = new CategoryNameTranslations();
        categoryNameTranslations.setName(DEFAULT_NAME);
        categoryNameTranslations.setTransType(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void createCategoryNameTranslations() throws Exception {
        int databaseSizeBeforeCreate = categoryNameTranslationsRepository.findAll().size();

        // Create the CategoryNameTranslations

        restCategoryNameTranslationsMockMvc.perform(post("/api/categoryNameTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(categoryNameTranslations)))
                .andExpect(status().isCreated());

        // Validate the CategoryNameTranslations in the database
        List<CategoryNameTranslations> categoryNameTranslationss = categoryNameTranslationsRepository.findAll();
        assertThat(categoryNameTranslationss).hasSize(databaseSizeBeforeCreate + 1);
        CategoryNameTranslations testCategoryNameTranslations = categoryNameTranslationss.get(categoryNameTranslationss.size() - 1);
        assertThat(testCategoryNameTranslations.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCategoryNameTranslations.getTransType()).isEqualTo(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void getAllCategoryNameTranslationss() throws Exception {
        // Initialize the database
        categoryNameTranslationsRepository.saveAndFlush(categoryNameTranslations);

        // Get all the categoryNameTranslationss
        restCategoryNameTranslationsMockMvc.perform(get("/api/categoryNameTranslationss"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(categoryNameTranslations.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].transType").value(hasItem(DEFAULT_TRANS_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getCategoryNameTranslations() throws Exception {
        // Initialize the database
        categoryNameTranslationsRepository.saveAndFlush(categoryNameTranslations);

        // Get the categoryNameTranslations
        restCategoryNameTranslationsMockMvc.perform(get("/api/categoryNameTranslationss/{id}", categoryNameTranslations.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(categoryNameTranslations.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.transType").value(DEFAULT_TRANS_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCategoryNameTranslations() throws Exception {
        // Get the categoryNameTranslations
        restCategoryNameTranslationsMockMvc.perform(get("/api/categoryNameTranslationss/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCategoryNameTranslations() throws Exception {
        // Initialize the database
        categoryNameTranslationsRepository.saveAndFlush(categoryNameTranslations);

		int databaseSizeBeforeUpdate = categoryNameTranslationsRepository.findAll().size();

        // Update the categoryNameTranslations
        categoryNameTranslations.setName(UPDATED_NAME);
        categoryNameTranslations.setTransType(UPDATED_TRANS_TYPE);

        restCategoryNameTranslationsMockMvc.perform(put("/api/categoryNameTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(categoryNameTranslations)))
                .andExpect(status().isOk());

        // Validate the CategoryNameTranslations in the database
        List<CategoryNameTranslations> categoryNameTranslationss = categoryNameTranslationsRepository.findAll();
        assertThat(categoryNameTranslationss).hasSize(databaseSizeBeforeUpdate);
        CategoryNameTranslations testCategoryNameTranslations = categoryNameTranslationss.get(categoryNameTranslationss.size() - 1);
        assertThat(testCategoryNameTranslations.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCategoryNameTranslations.getTransType()).isEqualTo(UPDATED_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void deleteCategoryNameTranslations() throws Exception {
        // Initialize the database
        categoryNameTranslationsRepository.saveAndFlush(categoryNameTranslations);

		int databaseSizeBeforeDelete = categoryNameTranslationsRepository.findAll().size();

        // Get the categoryNameTranslations
        restCategoryNameTranslationsMockMvc.perform(delete("/api/categoryNameTranslationss/{id}", categoryNameTranslations.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<CategoryNameTranslations> categoryNameTranslationss = categoryNameTranslationsRepository.findAll();
        assertThat(categoryNameTranslationss).hasSize(databaseSizeBeforeDelete - 1);
    }
}
