package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.PersonTypeNameTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.PersonTypeNameTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PersonTypeNameTranslationsSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the PersonTypeNameTranslationsResource REST controller.
 *
 * @see PersonTypeNameTranslationsResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class PersonTypeNameTranslationsResourceTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_TRANS_TYPE = "AAAAA";
    private static final String UPDATED_TRANS_TYPE = "BBBBB";

    @Inject
    private PersonTypeNameTranslationsRepository personTypeNameTranslationsRepository;

    @Inject
    private PersonTypeNameTranslationsSearchRepository personTypeNameTranslationsSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPersonTypeNameTranslationsMockMvc;

    private PersonTypeNameTranslations personTypeNameTranslations;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PersonTypeNameTranslationsResource personTypeNameTranslationsResource = new PersonTypeNameTranslationsResource();
        ReflectionTestUtils.setField(personTypeNameTranslationsResource, "personTypeNameTranslationsRepository", personTypeNameTranslationsRepository);
        ReflectionTestUtils.setField(personTypeNameTranslationsResource, "personTypeNameTranslationsSearchRepository", personTypeNameTranslationsSearchRepository);
        this.restPersonTypeNameTranslationsMockMvc = MockMvcBuilders.standaloneSetup(personTypeNameTranslationsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        personTypeNameTranslations = new PersonTypeNameTranslations();
        personTypeNameTranslations.setName(DEFAULT_NAME);
        personTypeNameTranslations.setTransType(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void createPersonTypeNameTranslations() throws Exception {
        int databaseSizeBeforeCreate = personTypeNameTranslationsRepository.findAll().size();

        // Create the PersonTypeNameTranslations

        restPersonTypeNameTranslationsMockMvc.perform(post("/api/personTypeNameTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(personTypeNameTranslations)))
                .andExpect(status().isCreated());

        // Validate the PersonTypeNameTranslations in the database
        List<PersonTypeNameTranslations> personTypeNameTranslationss = personTypeNameTranslationsRepository.findAll();
        assertThat(personTypeNameTranslationss).hasSize(databaseSizeBeforeCreate + 1);
        PersonTypeNameTranslations testPersonTypeNameTranslations = personTypeNameTranslationss.get(personTypeNameTranslationss.size() - 1);
        assertThat(testPersonTypeNameTranslations.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPersonTypeNameTranslations.getTransType()).isEqualTo(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void getAllPersonTypeNameTranslationss() throws Exception {
        // Initialize the database
        personTypeNameTranslationsRepository.saveAndFlush(personTypeNameTranslations);

        // Get all the personTypeNameTranslationss
        restPersonTypeNameTranslationsMockMvc.perform(get("/api/personTypeNameTranslationss"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(personTypeNameTranslations.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].transType").value(hasItem(DEFAULT_TRANS_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getPersonTypeNameTranslations() throws Exception {
        // Initialize the database
        personTypeNameTranslationsRepository.saveAndFlush(personTypeNameTranslations);

        // Get the personTypeNameTranslations
        restPersonTypeNameTranslationsMockMvc.perform(get("/api/personTypeNameTranslationss/{id}", personTypeNameTranslations.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(personTypeNameTranslations.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.transType").value(DEFAULT_TRANS_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPersonTypeNameTranslations() throws Exception {
        // Get the personTypeNameTranslations
        restPersonTypeNameTranslationsMockMvc.perform(get("/api/personTypeNameTranslationss/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePersonTypeNameTranslations() throws Exception {
        // Initialize the database
        personTypeNameTranslationsRepository.saveAndFlush(personTypeNameTranslations);

		int databaseSizeBeforeUpdate = personTypeNameTranslationsRepository.findAll().size();

        // Update the personTypeNameTranslations
        personTypeNameTranslations.setName(UPDATED_NAME);
        personTypeNameTranslations.setTransType(UPDATED_TRANS_TYPE);

        restPersonTypeNameTranslationsMockMvc.perform(put("/api/personTypeNameTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(personTypeNameTranslations)))
                .andExpect(status().isOk());

        // Validate the PersonTypeNameTranslations in the database
        List<PersonTypeNameTranslations> personTypeNameTranslationss = personTypeNameTranslationsRepository.findAll();
        assertThat(personTypeNameTranslationss).hasSize(databaseSizeBeforeUpdate);
        PersonTypeNameTranslations testPersonTypeNameTranslations = personTypeNameTranslationss.get(personTypeNameTranslationss.size() - 1);
        assertThat(testPersonTypeNameTranslations.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPersonTypeNameTranslations.getTransType()).isEqualTo(UPDATED_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void deletePersonTypeNameTranslations() throws Exception {
        // Initialize the database
        personTypeNameTranslationsRepository.saveAndFlush(personTypeNameTranslations);

		int databaseSizeBeforeDelete = personTypeNameTranslationsRepository.findAll().size();

        // Get the personTypeNameTranslations
        restPersonTypeNameTranslationsMockMvc.perform(delete("/api/personTypeNameTranslationss/{id}", personTypeNameTranslations.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<PersonTypeNameTranslations> personTypeNameTranslationss = personTypeNameTranslationsRepository.findAll();
        assertThat(personTypeNameTranslationss).hasSize(databaseSizeBeforeDelete - 1);
    }
}
