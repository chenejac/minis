package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.ClassNameTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.ClassNameTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.ClassNameTranslationsSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ClassNameTranslationsResource REST controller.
 *
 * @see ClassNameTranslationsResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class ClassNameTranslationsResourceTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_TRANS_TYPE = "AAAAA";
    private static final String UPDATED_TRANS_TYPE = "BBBBB";

    @Inject
    private ClassNameTranslationsRepository classNameTranslationsRepository;

    @Inject
    private ClassNameTranslationsSearchRepository classNameTranslationsSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restClassNameTranslationsMockMvc;

    private ClassNameTranslations classNameTranslations;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ClassNameTranslationsResource classNameTranslationsResource = new ClassNameTranslationsResource();
        ReflectionTestUtils.setField(classNameTranslationsResource, "classNameTranslationsRepository", classNameTranslationsRepository);
        ReflectionTestUtils.setField(classNameTranslationsResource, "classNameTranslationsSearchRepository", classNameTranslationsSearchRepository);
        this.restClassNameTranslationsMockMvc = MockMvcBuilders.standaloneSetup(classNameTranslationsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        classNameTranslations = new ClassNameTranslations();
        classNameTranslations.setName(DEFAULT_NAME);
        classNameTranslations.setTransType(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void createClassNameTranslations() throws Exception {
        int databaseSizeBeforeCreate = classNameTranslationsRepository.findAll().size();

        // Create the ClassNameTranslations

        restClassNameTranslationsMockMvc.perform(post("/api/classNameTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(classNameTranslations)))
                .andExpect(status().isCreated());

        // Validate the ClassNameTranslations in the database
        List<ClassNameTranslations> classNameTranslationss = classNameTranslationsRepository.findAll();
        assertThat(classNameTranslationss).hasSize(databaseSizeBeforeCreate + 1);
        ClassNameTranslations testClassNameTranslations = classNameTranslationss.get(classNameTranslationss.size() - 1);
        assertThat(testClassNameTranslations.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testClassNameTranslations.getTransType()).isEqualTo(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void getAllClassNameTranslationss() throws Exception {
        // Initialize the database
        classNameTranslationsRepository.saveAndFlush(classNameTranslations);

        // Get all the classNameTranslationss
        restClassNameTranslationsMockMvc.perform(get("/api/classNameTranslationss"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(classNameTranslations.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].transType").value(hasItem(DEFAULT_TRANS_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getClassNameTranslations() throws Exception {
        // Initialize the database
        classNameTranslationsRepository.saveAndFlush(classNameTranslations);

        // Get the classNameTranslations
        restClassNameTranslationsMockMvc.perform(get("/api/classNameTranslationss/{id}", classNameTranslations.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(classNameTranslations.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.transType").value(DEFAULT_TRANS_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingClassNameTranslations() throws Exception {
        // Get the classNameTranslations
        restClassNameTranslationsMockMvc.perform(get("/api/classNameTranslationss/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClassNameTranslations() throws Exception {
        // Initialize the database
        classNameTranslationsRepository.saveAndFlush(classNameTranslations);

		int databaseSizeBeforeUpdate = classNameTranslationsRepository.findAll().size();

        // Update the classNameTranslations
        classNameTranslations.setName(UPDATED_NAME);
        classNameTranslations.setTransType(UPDATED_TRANS_TYPE);

        restClassNameTranslationsMockMvc.perform(put("/api/classNameTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(classNameTranslations)))
                .andExpect(status().isOk());

        // Validate the ClassNameTranslations in the database
        List<ClassNameTranslations> classNameTranslationss = classNameTranslationsRepository.findAll();
        assertThat(classNameTranslationss).hasSize(databaseSizeBeforeUpdate);
        ClassNameTranslations testClassNameTranslations = classNameTranslationss.get(classNameTranslationss.size() - 1);
        assertThat(testClassNameTranslations.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testClassNameTranslations.getTransType()).isEqualTo(UPDATED_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void deleteClassNameTranslations() throws Exception {
        // Initialize the database
        classNameTranslationsRepository.saveAndFlush(classNameTranslations);

		int databaseSizeBeforeDelete = classNameTranslationsRepository.findAll().size();

        // Get the classNameTranslations
        restClassNameTranslationsMockMvc.perform(delete("/api/classNameTranslationss/{id}", classNameTranslations.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ClassNameTranslations> classNameTranslationss = classNameTranslationsRepository.findAll();
        assertThat(classNameTranslationss).hasSize(databaseSizeBeforeDelete - 1);
    }
}
