package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.InstitutionFile;
import rs.ac.uns.ftn.informatika.minis.repository.InstitutionFileRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.InstitutionFileSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the InstitutionFileResource REST controller.
 *
 * @see InstitutionFileResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class InstitutionFileResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

    private static final String DEFAULT_FILE_NAME = "AAAAA";
    private static final String UPDATED_FILE_NAME = "BBBBB";
    private static final String DEFAULT_FILE_NAME_CLIENT = "AAAAA";
    private static final String UPDATED_FILE_NAME_CLIENT = "BBBBB";
    private static final String DEFAULT_MIME = "AAAAA";
    private static final String UPDATED_MIME = "BBBBB";

    private static final Long DEFAULT_LENGTH = 1L;
    private static final Long UPDATED_LENGTH = 2L;
    private static final String DEFAULT_UPLOADER = "AAAAA";
    private static final String UPDATED_UPLOADER = "BBBBB";

    private static final DateTime DEFAULT_DATE_MODIFIED = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_DATE_MODIFIED = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_DATE_MODIFIED_STR = dateTimeFormatter.print(DEFAULT_DATE_MODIFIED);
    private static final String DEFAULT_LICENSE = "AAAAA";
    private static final String UPDATED_LICENSE = "BBBBB";
    private static final String DEFAULT_TYPE = "AAAAA";
    private static final String UPDATED_TYPE = "BBBBB";
    private static final String DEFAULT_NOTE = "AAAAA";
    private static final String UPDATED_NOTE = "BBBBB";
    private static final String DEFAULT_STATUS = "AAAAA";
    private static final String UPDATED_STATUS = "BBBBB";

    @Inject
    private InstitutionFileRepository institutionFileRepository;

    @Inject
    private InstitutionFileSearchRepository institutionFileSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restInstitutionFileMockMvc;

    private InstitutionFile institutionFile;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        InstitutionFileResource institutionFileResource = new InstitutionFileResource();
        ReflectionTestUtils.setField(institutionFileResource, "institutionFileRepository", institutionFileRepository);
        ReflectionTestUtils.setField(institutionFileResource, "institutionFileSearchRepository", institutionFileSearchRepository);
        this.restInstitutionFileMockMvc = MockMvcBuilders.standaloneSetup(institutionFileResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        institutionFile = new InstitutionFile();
        institutionFile.setFileName(DEFAULT_FILE_NAME);
        institutionFile.setFileNameClient(DEFAULT_FILE_NAME_CLIENT);
        institutionFile.setMime(DEFAULT_MIME);
        institutionFile.setLength(DEFAULT_LENGTH);
        institutionFile.setUploader(DEFAULT_UPLOADER);
        institutionFile.setDateModified(DEFAULT_DATE_MODIFIED);
        institutionFile.setLicense(DEFAULT_LICENSE);
        institutionFile.setType(DEFAULT_TYPE);
        institutionFile.setNote(DEFAULT_NOTE);
        institutionFile.setStatus(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createInstitutionFile() throws Exception {
        int databaseSizeBeforeCreate = institutionFileRepository.findAll().size();

        // Create the InstitutionFile

        restInstitutionFileMockMvc.perform(post("/api/institutionFiles")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(institutionFile)))
                .andExpect(status().isCreated());

        // Validate the InstitutionFile in the database
        List<InstitutionFile> institutionFiles = institutionFileRepository.findAll();
        assertThat(institutionFiles).hasSize(databaseSizeBeforeCreate + 1);
        InstitutionFile testInstitutionFile = institutionFiles.get(institutionFiles.size() - 1);
        assertThat(testInstitutionFile.getFileName()).isEqualTo(DEFAULT_FILE_NAME);
        assertThat(testInstitutionFile.getFileNameClient()).isEqualTo(DEFAULT_FILE_NAME_CLIENT);
        assertThat(testInstitutionFile.getMime()).isEqualTo(DEFAULT_MIME);
        assertThat(testInstitutionFile.getLength()).isEqualTo(DEFAULT_LENGTH);
        assertThat(testInstitutionFile.getUploader()).isEqualTo(DEFAULT_UPLOADER);
        assertThat(testInstitutionFile.getDateModified().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_DATE_MODIFIED);
        assertThat(testInstitutionFile.getLicense()).isEqualTo(DEFAULT_LICENSE);
        assertThat(testInstitutionFile.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testInstitutionFile.getNote()).isEqualTo(DEFAULT_NOTE);
        assertThat(testInstitutionFile.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void getAllInstitutionFiles() throws Exception {
        // Initialize the database
        institutionFileRepository.saveAndFlush(institutionFile);

        // Get all the institutionFiles
        restInstitutionFileMockMvc.perform(get("/api/institutionFiles"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(institutionFile.getId().intValue())))
                .andExpect(jsonPath("$.[*].fileName").value(hasItem(DEFAULT_FILE_NAME.toString())))
                .andExpect(jsonPath("$.[*].fileNameClient").value(hasItem(DEFAULT_FILE_NAME_CLIENT.toString())))
                .andExpect(jsonPath("$.[*].mime").value(hasItem(DEFAULT_MIME.toString())))
                .andExpect(jsonPath("$.[*].length").value(hasItem(DEFAULT_LENGTH.intValue())))
                .andExpect(jsonPath("$.[*].uploader").value(hasItem(DEFAULT_UPLOADER.toString())))
                .andExpect(jsonPath("$.[*].dateModified").value(hasItem(DEFAULT_DATE_MODIFIED_STR)))
                .andExpect(jsonPath("$.[*].license").value(hasItem(DEFAULT_LICENSE.toString())))
                .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
                .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void getInstitutionFile() throws Exception {
        // Initialize the database
        institutionFileRepository.saveAndFlush(institutionFile);

        // Get the institutionFile
        restInstitutionFileMockMvc.perform(get("/api/institutionFiles/{id}", institutionFile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(institutionFile.getId().intValue()))
            .andExpect(jsonPath("$.fileName").value(DEFAULT_FILE_NAME.toString()))
            .andExpect(jsonPath("$.fileNameClient").value(DEFAULT_FILE_NAME_CLIENT.toString()))
            .andExpect(jsonPath("$.mime").value(DEFAULT_MIME.toString()))
            .andExpect(jsonPath("$.length").value(DEFAULT_LENGTH.intValue()))
            .andExpect(jsonPath("$.uploader").value(DEFAULT_UPLOADER.toString()))
            .andExpect(jsonPath("$.dateModified").value(DEFAULT_DATE_MODIFIED_STR))
            .andExpect(jsonPath("$.license").value(DEFAULT_LICENSE.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingInstitutionFile() throws Exception {
        // Get the institutionFile
        restInstitutionFileMockMvc.perform(get("/api/institutionFiles/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInstitutionFile() throws Exception {
        // Initialize the database
        institutionFileRepository.saveAndFlush(institutionFile);

		int databaseSizeBeforeUpdate = institutionFileRepository.findAll().size();

        // Update the institutionFile
        institutionFile.setFileName(UPDATED_FILE_NAME);
        institutionFile.setFileNameClient(UPDATED_FILE_NAME_CLIENT);
        institutionFile.setMime(UPDATED_MIME);
        institutionFile.setLength(UPDATED_LENGTH);
        institutionFile.setUploader(UPDATED_UPLOADER);
        institutionFile.setDateModified(UPDATED_DATE_MODIFIED);
        institutionFile.setLicense(UPDATED_LICENSE);
        institutionFile.setType(UPDATED_TYPE);
        institutionFile.setNote(UPDATED_NOTE);
        institutionFile.setStatus(UPDATED_STATUS);

        restInstitutionFileMockMvc.perform(put("/api/institutionFiles")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(institutionFile)))
                .andExpect(status().isOk());

        // Validate the InstitutionFile in the database
        List<InstitutionFile> institutionFiles = institutionFileRepository.findAll();
        assertThat(institutionFiles).hasSize(databaseSizeBeforeUpdate);
        InstitutionFile testInstitutionFile = institutionFiles.get(institutionFiles.size() - 1);
        assertThat(testInstitutionFile.getFileName()).isEqualTo(UPDATED_FILE_NAME);
        assertThat(testInstitutionFile.getFileNameClient()).isEqualTo(UPDATED_FILE_NAME_CLIENT);
        assertThat(testInstitutionFile.getMime()).isEqualTo(UPDATED_MIME);
        assertThat(testInstitutionFile.getLength()).isEqualTo(UPDATED_LENGTH);
        assertThat(testInstitutionFile.getUploader()).isEqualTo(UPDATED_UPLOADER);
        assertThat(testInstitutionFile.getDateModified().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_DATE_MODIFIED);
        assertThat(testInstitutionFile.getLicense()).isEqualTo(UPDATED_LICENSE);
        assertThat(testInstitutionFile.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testInstitutionFile.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testInstitutionFile.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void deleteInstitutionFile() throws Exception {
        // Initialize the database
        institutionFileRepository.saveAndFlush(institutionFile);

		int databaseSizeBeforeDelete = institutionFileRepository.findAll().size();

        // Get the institutionFile
        restInstitutionFileMockMvc.perform(delete("/api/institutionFiles/{id}", institutionFile.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<InstitutionFile> institutionFiles = institutionFileRepository.findAll();
        assertThat(institutionFiles).hasSize(databaseSizeBeforeDelete - 1);
    }
}
