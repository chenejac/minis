package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.FounderNameTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.FounderNameTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.FounderNameTranslationsSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the FounderNameTranslationsResource REST controller.
 *
 * @see FounderNameTranslationsResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class FounderNameTranslationsResourceTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_TRANS_TYPE = "AAAAA";
    private static final String UPDATED_TRANS_TYPE = "BBBBB";

    @Inject
    private FounderNameTranslationsRepository founderNameTranslationsRepository;

    @Inject
    private FounderNameTranslationsSearchRepository founderNameTranslationsSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restFounderNameTranslationsMockMvc;

    private FounderNameTranslations founderNameTranslations;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        FounderNameTranslationsResource founderNameTranslationsResource = new FounderNameTranslationsResource();
        ReflectionTestUtils.setField(founderNameTranslationsResource, "founderNameTranslationsRepository", founderNameTranslationsRepository);
        ReflectionTestUtils.setField(founderNameTranslationsResource, "founderNameTranslationsSearchRepository", founderNameTranslationsSearchRepository);
        this.restFounderNameTranslationsMockMvc = MockMvcBuilders.standaloneSetup(founderNameTranslationsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        founderNameTranslations = new FounderNameTranslations();
        founderNameTranslations.setName(DEFAULT_NAME);
        founderNameTranslations.setTransType(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void createFounderNameTranslations() throws Exception {
        int databaseSizeBeforeCreate = founderNameTranslationsRepository.findAll().size();

        // Create the FounderNameTranslations

        restFounderNameTranslationsMockMvc.perform(post("/api/founderNameTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(founderNameTranslations)))
                .andExpect(status().isCreated());

        // Validate the FounderNameTranslations in the database
        List<FounderNameTranslations> founderNameTranslationss = founderNameTranslationsRepository.findAll();
        assertThat(founderNameTranslationss).hasSize(databaseSizeBeforeCreate + 1);
        FounderNameTranslations testFounderNameTranslations = founderNameTranslationss.get(founderNameTranslationss.size() - 1);
        assertThat(testFounderNameTranslations.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testFounderNameTranslations.getTransType()).isEqualTo(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void getAllFounderNameTranslationss() throws Exception {
        // Initialize the database
        founderNameTranslationsRepository.saveAndFlush(founderNameTranslations);

        // Get all the founderNameTranslationss
        restFounderNameTranslationsMockMvc.perform(get("/api/founderNameTranslationss"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(founderNameTranslations.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].transType").value(hasItem(DEFAULT_TRANS_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getFounderNameTranslations() throws Exception {
        // Initialize the database
        founderNameTranslationsRepository.saveAndFlush(founderNameTranslations);

        // Get the founderNameTranslations
        restFounderNameTranslationsMockMvc.perform(get("/api/founderNameTranslationss/{id}", founderNameTranslations.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(founderNameTranslations.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.transType").value(DEFAULT_TRANS_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingFounderNameTranslations() throws Exception {
        // Get the founderNameTranslations
        restFounderNameTranslationsMockMvc.perform(get("/api/founderNameTranslationss/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFounderNameTranslations() throws Exception {
        // Initialize the database
        founderNameTranslationsRepository.saveAndFlush(founderNameTranslations);

		int databaseSizeBeforeUpdate = founderNameTranslationsRepository.findAll().size();

        // Update the founderNameTranslations
        founderNameTranslations.setName(UPDATED_NAME);
        founderNameTranslations.setTransType(UPDATED_TRANS_TYPE);

        restFounderNameTranslationsMockMvc.perform(put("/api/founderNameTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(founderNameTranslations)))
                .andExpect(status().isOk());

        // Validate the FounderNameTranslations in the database
        List<FounderNameTranslations> founderNameTranslationss = founderNameTranslationsRepository.findAll();
        assertThat(founderNameTranslationss).hasSize(databaseSizeBeforeUpdate);
        FounderNameTranslations testFounderNameTranslations = founderNameTranslationss.get(founderNameTranslationss.size() - 1);
        assertThat(testFounderNameTranslations.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testFounderNameTranslations.getTransType()).isEqualTo(UPDATED_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void deleteFounderNameTranslations() throws Exception {
        // Initialize the database
        founderNameTranslationsRepository.saveAndFlush(founderNameTranslations);

		int databaseSizeBeforeDelete = founderNameTranslationsRepository.findAll().size();

        // Get the founderNameTranslations
        restFounderNameTranslationsMockMvc.perform(delete("/api/founderNameTranslationss/{id}", founderNameTranslations.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<FounderNameTranslations> founderNameTranslationss = founderNameTranslationsRepository.findAll();
        assertThat(founderNameTranslationss).hasSize(databaseSizeBeforeDelete - 1);
    }
}
