package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.Person;
import rs.ac.uns.ftn.informatika.minis.repository.PersonRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PersonSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.joda.time.LocalDate;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the PersonResource REST controller.
 *
 * @see PersonResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class PersonResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

    private static final String DEFAULT_FIRST_NAME = "AAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBB";
    private static final String DEFAULT_LAST_NAME = "AAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBB";
    private static final String DEFAULT_MIDDLE_NAME = "AAAAA";
    private static final String UPDATED_MIDDLE_NAME = "BBBBB";

    private static final LocalDate DEFAULT_DATE_OF_BIRTH = new LocalDate(0L);
    private static final LocalDate UPDATED_DATE_OF_BIRTH = new LocalDate();
    private static final String DEFAULT_PLACE_OF_BIRTH = "AAAAA";
    private static final String UPDATED_PLACE_OF_BIRTH = "BBBBB";
    private static final String DEFAULT_STATE = "AAAAA";
    private static final String UPDATED_STATE = "BBBBB";
    private static final String DEFAULT_ADDRESS = "AAAAA";
    private static final String UPDATED_ADDRESS = "BBBBB";
    private static final String DEFAULT_CITY = "AAAAA";
    private static final String UPDATED_CITY = "BBBBB";
    private static final String DEFAULT_BIBLIOGRAPHY = "AAAAA";
    private static final String UPDATED_BIBLIOGRAPHY = "BBBBB";
    private static final String DEFAULT_KEYWORDS = "AAAAA";
    private static final String UPDATED_KEYWORDS = "BBBBB";
    private static final String DEFAULT_GENDER = "AAAAA";
    private static final String UPDATED_GENDER = "BBBBB";
    private static final String DEFAULT_URI = "AAAAA";
    private static final String UPDATED_URI = "BBBBB";
    private static final String DEFAULT_EMAIL = "AAAAA";
    private static final String UPDATED_EMAIL = "BBBBB";
    private static final String DEFAULT_ORCID = "AAAAA";
    private static final String UPDATED_ORCID = "BBBBB";
    private static final String DEFAULT_JMBG = "AAAAA";
    private static final String UPDATED_JMBG = "BBBBB";
    private static final String DEFAULT_PHONES = "AAAAA";
    private static final String UPDATED_PHONES = "BBBBB";
    private static final String DEFAULT_RESEARCH_AREAS = "AAAAA";
    private static final String UPDATED_RESEARCH_AREAS = "BBBBB";
    private static final String DEFAULT_MNTRN = "AAAAA";
    private static final String UPDATED_MNTRN = "BBBBB";
    private static final String DEFAULT_NOTE = "AAAAA";
    private static final String UPDATED_NOTE = "BBBBB";
    private static final String DEFAULT_CREATOR = "AAAAA";
    private static final String UPDATED_CREATOR = "BBBBB";
    private static final String DEFAULT_MODIFIER = "AAAAA";
    private static final String UPDATED_MODIFIER = "BBBBB";

    private static final DateTime DEFAULT_CREATION_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_CREATION_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_CREATION_DATE_STR = dateTimeFormatter.print(DEFAULT_CREATION_DATE);

    private static final DateTime DEFAULT_LAST_MODIFICATION_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_LAST_MODIFICATION_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_LAST_MODIFICATION_DATE_STR = dateTimeFormatter.print(DEFAULT_LAST_MODIFICATION_DATE);

    private static final Integer DEFAULT_RECORD_STATUS = 1;
    private static final Integer UPDATED_RECORD_STATUS = 2;

    private static final Boolean DEFAULT_ALREADY_REGISTERED = false;
    private static final Boolean UPDATED_ALREADY_REGISTERED = true;

    @Inject
    private PersonRepository personRepository;

    @Inject
    private PersonSearchRepository personSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPersonMockMvc;

    private Person person;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PersonResource personResource = new PersonResource();
        ReflectionTestUtils.setField(personResource, "personRepository", personRepository);
        ReflectionTestUtils.setField(personResource, "personSearchRepository", personSearchRepository);
        this.restPersonMockMvc = MockMvcBuilders.standaloneSetup(personResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        person = new Person();
        person.setFirstName(DEFAULT_FIRST_NAME);
        person.setLastName(DEFAULT_LAST_NAME);
        person.setMiddleName(DEFAULT_MIDDLE_NAME);
        person.setDateOfBirth(DEFAULT_DATE_OF_BIRTH);
        person.setPlaceOfBirth(DEFAULT_PLACE_OF_BIRTH);
        person.setState(DEFAULT_STATE);
        person.setAddress(DEFAULT_ADDRESS);
        person.setCity(DEFAULT_CITY);
        person.setBibliography(DEFAULT_BIBLIOGRAPHY);
        person.setKeywords(DEFAULT_KEYWORDS);
        person.setGender(DEFAULT_GENDER);
        person.setUri(DEFAULT_URI);
        person.setEmail(DEFAULT_EMAIL);
        person.setOrcid(DEFAULT_ORCID);
        person.setJmbg(DEFAULT_JMBG);
        person.setPhones(DEFAULT_PHONES);
        person.setResearchAreas(DEFAULT_RESEARCH_AREAS);
        person.setMntrn(DEFAULT_MNTRN);
        person.setNote(DEFAULT_NOTE);
        person.setCreator(DEFAULT_CREATOR);
        person.setModifier(DEFAULT_MODIFIER);
        person.setCreationDate(DEFAULT_CREATION_DATE);
        person.setLastModificationDate(DEFAULT_LAST_MODIFICATION_DATE);
        person.setRecordStatus(DEFAULT_RECORD_STATUS);
        person.setAlreadyRegistered(DEFAULT_ALREADY_REGISTERED);
    }

    @Test
    @Transactional
    public void createPerson() throws Exception {
        int databaseSizeBeforeCreate = personRepository.findAll().size();

        // Create the Person

        restPersonMockMvc.perform(post("/api/persons")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(person)))
                .andExpect(status().isCreated());

        // Validate the Person in the database
        List<Person> persons = personRepository.findAll();
        assertThat(persons).hasSize(databaseSizeBeforeCreate + 1);
        Person testPerson = persons.get(persons.size() - 1);
        assertThat(testPerson.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testPerson.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testPerson.getMiddleName()).isEqualTo(DEFAULT_MIDDLE_NAME);
        assertThat(testPerson.getDateOfBirth()).isEqualTo(DEFAULT_DATE_OF_BIRTH);
        assertThat(testPerson.getPlaceOfBirth()).isEqualTo(DEFAULT_PLACE_OF_BIRTH);
        assertThat(testPerson.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testPerson.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testPerson.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testPerson.getBibliography()).isEqualTo(DEFAULT_BIBLIOGRAPHY);
        assertThat(testPerson.getKeywords()).isEqualTo(DEFAULT_KEYWORDS);
        assertThat(testPerson.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testPerson.getUri()).isEqualTo(DEFAULT_URI);
        assertThat(testPerson.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testPerson.getOrcid()).isEqualTo(DEFAULT_ORCID);
        assertThat(testPerson.getJmbg()).isEqualTo(DEFAULT_JMBG);
        assertThat(testPerson.getPhones()).isEqualTo(DEFAULT_PHONES);
        assertThat(testPerson.getResearchAreas()).isEqualTo(DEFAULT_RESEARCH_AREAS);
        assertThat(testPerson.getMntrn()).isEqualTo(DEFAULT_MNTRN);
        assertThat(testPerson.getNote()).isEqualTo(DEFAULT_NOTE);
        assertThat(testPerson.getCreator()).isEqualTo(DEFAULT_CREATOR);
        assertThat(testPerson.getModifier()).isEqualTo(DEFAULT_MODIFIER);
        assertThat(testPerson.getCreationDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testPerson.getLastModificationDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_LAST_MODIFICATION_DATE);
        assertThat(testPerson.getRecordStatus()).isEqualTo(DEFAULT_RECORD_STATUS);
        assertThat(testPerson.getAlreadyRegistered()).isEqualTo(DEFAULT_ALREADY_REGISTERED);
    }

    @Test
    @Transactional
    public void getAllPersons() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the persons
        restPersonMockMvc.perform(get("/api/persons"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(person.getId().intValue())))
                .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME.toString())))
                .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME.toString())))
                .andExpect(jsonPath("$.[*].middleName").value(hasItem(DEFAULT_MIDDLE_NAME.toString())))
                .andExpect(jsonPath("$.[*].dateOfBirth").value(hasItem(DEFAULT_DATE_OF_BIRTH.toString())))
                .andExpect(jsonPath("$.[*].placeOfBirth").value(hasItem(DEFAULT_PLACE_OF_BIRTH.toString())))
                .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE.toString())))
                .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
                .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY.toString())))
                .andExpect(jsonPath("$.[*].bibliography").value(hasItem(DEFAULT_BIBLIOGRAPHY.toString())))
                .andExpect(jsonPath("$.[*].keywords").value(hasItem(DEFAULT_KEYWORDS.toString())))
                .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())))
                .andExpect(jsonPath("$.[*].uri").value(hasItem(DEFAULT_URI.toString())))
                .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
                .andExpect(jsonPath("$.[*].orcid").value(hasItem(DEFAULT_ORCID.toString())))
                .andExpect(jsonPath("$.[*].jmbg").value(hasItem(DEFAULT_JMBG.toString())))
                .andExpect(jsonPath("$.[*].phones").value(hasItem(DEFAULT_PHONES.toString())))
                .andExpect(jsonPath("$.[*].researchAreas").value(hasItem(DEFAULT_RESEARCH_AREAS.toString())))
                .andExpect(jsonPath("$.[*].mntrn").value(hasItem(DEFAULT_MNTRN.toString())))
                .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
                .andExpect(jsonPath("$.[*].creator").value(hasItem(DEFAULT_CREATOR.toString())))
                .andExpect(jsonPath("$.[*].modifier").value(hasItem(DEFAULT_MODIFIER.toString())))
                .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE_STR)))
                .andExpect(jsonPath("$.[*].lastModificationDate").value(hasItem(DEFAULT_LAST_MODIFICATION_DATE_STR)))
                .andExpect(jsonPath("$.[*].recordStatus").value(hasItem(DEFAULT_RECORD_STATUS)))
                .andExpect(jsonPath("$.[*].alreadyRegistered").value(hasItem(DEFAULT_ALREADY_REGISTERED.booleanValue())));
    }

    @Test
    @Transactional
    public void getPerson() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get the person
        restPersonMockMvc.perform(get("/api/persons/{id}", person.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(person.getId().intValue()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME.toString()))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME.toString()))
            .andExpect(jsonPath("$.middleName").value(DEFAULT_MIDDLE_NAME.toString()))
            .andExpect(jsonPath("$.dateOfBirth").value(DEFAULT_DATE_OF_BIRTH.toString()))
            .andExpect(jsonPath("$.placeOfBirth").value(DEFAULT_PLACE_OF_BIRTH.toString()))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE.toString()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY.toString()))
            .andExpect(jsonPath("$.bibliography").value(DEFAULT_BIBLIOGRAPHY.toString()))
            .andExpect(jsonPath("$.keywords").value(DEFAULT_KEYWORDS.toString()))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER.toString()))
            .andExpect(jsonPath("$.uri").value(DEFAULT_URI.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.orcid").value(DEFAULT_ORCID.toString()))
            .andExpect(jsonPath("$.jmbg").value(DEFAULT_JMBG.toString()))
            .andExpect(jsonPath("$.phones").value(DEFAULT_PHONES.toString()))
            .andExpect(jsonPath("$.researchAreas").value(DEFAULT_RESEARCH_AREAS.toString()))
            .andExpect(jsonPath("$.mntrn").value(DEFAULT_MNTRN.toString()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()))
            .andExpect(jsonPath("$.creator").value(DEFAULT_CREATOR.toString()))
            .andExpect(jsonPath("$.modifier").value(DEFAULT_MODIFIER.toString()))
            .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE_STR))
            .andExpect(jsonPath("$.lastModificationDate").value(DEFAULT_LAST_MODIFICATION_DATE_STR))
            .andExpect(jsonPath("$.recordStatus").value(DEFAULT_RECORD_STATUS))
            .andExpect(jsonPath("$.alreadyRegistered").value(DEFAULT_ALREADY_REGISTERED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingPerson() throws Exception {
        // Get the person
        restPersonMockMvc.perform(get("/api/persons/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePerson() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

		int databaseSizeBeforeUpdate = personRepository.findAll().size();

        // Update the person
        person.setFirstName(UPDATED_FIRST_NAME);
        person.setLastName(UPDATED_LAST_NAME);
        person.setMiddleName(UPDATED_MIDDLE_NAME);
        person.setDateOfBirth(UPDATED_DATE_OF_BIRTH);
        person.setPlaceOfBirth(UPDATED_PLACE_OF_BIRTH);
        person.setState(UPDATED_STATE);
        person.setAddress(UPDATED_ADDRESS);
        person.setCity(UPDATED_CITY);
        person.setBibliography(UPDATED_BIBLIOGRAPHY);
        person.setKeywords(UPDATED_KEYWORDS);
        person.setGender(UPDATED_GENDER);
        person.setUri(UPDATED_URI);
        person.setEmail(UPDATED_EMAIL);
        person.setOrcid(UPDATED_ORCID);
        person.setJmbg(UPDATED_JMBG);
        person.setPhones(UPDATED_PHONES);
        person.setResearchAreas(UPDATED_RESEARCH_AREAS);
        person.setMntrn(UPDATED_MNTRN);
        person.setNote(UPDATED_NOTE);
        person.setCreator(UPDATED_CREATOR);
        person.setModifier(UPDATED_MODIFIER);
        person.setCreationDate(UPDATED_CREATION_DATE);
        person.setLastModificationDate(UPDATED_LAST_MODIFICATION_DATE);
        person.setRecordStatus(UPDATED_RECORD_STATUS);
        person.setAlreadyRegistered(UPDATED_ALREADY_REGISTERED);

        restPersonMockMvc.perform(put("/api/persons")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(person)))
                .andExpect(status().isOk());

        // Validate the Person in the database
        List<Person> persons = personRepository.findAll();
        assertThat(persons).hasSize(databaseSizeBeforeUpdate);
        Person testPerson = persons.get(persons.size() - 1);
        assertThat(testPerson.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testPerson.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testPerson.getMiddleName()).isEqualTo(UPDATED_MIDDLE_NAME);
        assertThat(testPerson.getDateOfBirth()).isEqualTo(UPDATED_DATE_OF_BIRTH);
        assertThat(testPerson.getPlaceOfBirth()).isEqualTo(UPDATED_PLACE_OF_BIRTH);
        assertThat(testPerson.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testPerson.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testPerson.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testPerson.getBibliography()).isEqualTo(UPDATED_BIBLIOGRAPHY);
        assertThat(testPerson.getKeywords()).isEqualTo(UPDATED_KEYWORDS);
        assertThat(testPerson.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testPerson.getUri()).isEqualTo(UPDATED_URI);
        assertThat(testPerson.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testPerson.getOrcid()).isEqualTo(UPDATED_ORCID);
        assertThat(testPerson.getJmbg()).isEqualTo(UPDATED_JMBG);
        assertThat(testPerson.getPhones()).isEqualTo(UPDATED_PHONES);
        assertThat(testPerson.getResearchAreas()).isEqualTo(UPDATED_RESEARCH_AREAS);
        assertThat(testPerson.getMntrn()).isEqualTo(UPDATED_MNTRN);
        assertThat(testPerson.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testPerson.getCreator()).isEqualTo(UPDATED_CREATOR);
        assertThat(testPerson.getModifier()).isEqualTo(UPDATED_MODIFIER);
        assertThat(testPerson.getCreationDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testPerson.getLastModificationDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_LAST_MODIFICATION_DATE);
        assertThat(testPerson.getRecordStatus()).isEqualTo(UPDATED_RECORD_STATUS);
        assertThat(testPerson.getAlreadyRegistered()).isEqualTo(UPDATED_ALREADY_REGISTERED);
    }

    @Test
    @Transactional
    public void deletePerson() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

		int databaseSizeBeforeDelete = personRepository.findAll().size();

        // Get the person
        restPersonMockMvc.perform(delete("/api/persons/{id}", person.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Person> persons = personRepository.findAll();
        assertThat(persons).hasSize(databaseSizeBeforeDelete - 1);
    }
}
