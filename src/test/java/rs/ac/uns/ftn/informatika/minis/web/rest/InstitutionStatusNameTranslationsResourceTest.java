package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.InstitutionStatusNameTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.InstitutionStatusNameTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.InstitutionStatusNameTranslationsSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the InstitutionStatusNameTranslationsResource REST controller.
 *
 * @see InstitutionStatusNameTranslationsResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class InstitutionStatusNameTranslationsResourceTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_TRANS_TYPE = "AAAAA";
    private static final String UPDATED_TRANS_TYPE = "BBBBB";

    @Inject
    private InstitutionStatusNameTranslationsRepository institutionStatusNameTranslationsRepository;

    @Inject
    private InstitutionStatusNameTranslationsSearchRepository institutionStatusNameTranslationsSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restInstitutionStatusNameTranslationsMockMvc;

    private InstitutionStatusNameTranslations institutionStatusNameTranslations;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        InstitutionStatusNameTranslationsResource institutionStatusNameTranslationsResource = new InstitutionStatusNameTranslationsResource();
        ReflectionTestUtils.setField(institutionStatusNameTranslationsResource, "institutionStatusNameTranslationsRepository", institutionStatusNameTranslationsRepository);
        ReflectionTestUtils.setField(institutionStatusNameTranslationsResource, "institutionStatusNameTranslationsSearchRepository", institutionStatusNameTranslationsSearchRepository);
        this.restInstitutionStatusNameTranslationsMockMvc = MockMvcBuilders.standaloneSetup(institutionStatusNameTranslationsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        institutionStatusNameTranslations = new InstitutionStatusNameTranslations();
        institutionStatusNameTranslations.setName(DEFAULT_NAME);
        institutionStatusNameTranslations.setTransType(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void createInstitutionStatusNameTranslations() throws Exception {
        int databaseSizeBeforeCreate = institutionStatusNameTranslationsRepository.findAll().size();

        // Create the InstitutionStatusNameTranslations

        restInstitutionStatusNameTranslationsMockMvc.perform(post("/api/institutionStatusNameTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(institutionStatusNameTranslations)))
                .andExpect(status().isCreated());

        // Validate the InstitutionStatusNameTranslations in the database
        List<InstitutionStatusNameTranslations> institutionStatusNameTranslationss = institutionStatusNameTranslationsRepository.findAll();
        assertThat(institutionStatusNameTranslationss).hasSize(databaseSizeBeforeCreate + 1);
        InstitutionStatusNameTranslations testInstitutionStatusNameTranslations = institutionStatusNameTranslationss.get(institutionStatusNameTranslationss.size() - 1);
        assertThat(testInstitutionStatusNameTranslations.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testInstitutionStatusNameTranslations.getTransType()).isEqualTo(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void getAllInstitutionStatusNameTranslationss() throws Exception {
        // Initialize the database
        institutionStatusNameTranslationsRepository.saveAndFlush(institutionStatusNameTranslations);

        // Get all the institutionStatusNameTranslationss
        restInstitutionStatusNameTranslationsMockMvc.perform(get("/api/institutionStatusNameTranslationss"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(institutionStatusNameTranslations.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].transType").value(hasItem(DEFAULT_TRANS_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getInstitutionStatusNameTranslations() throws Exception {
        // Initialize the database
        institutionStatusNameTranslationsRepository.saveAndFlush(institutionStatusNameTranslations);

        // Get the institutionStatusNameTranslations
        restInstitutionStatusNameTranslationsMockMvc.perform(get("/api/institutionStatusNameTranslationss/{id}", institutionStatusNameTranslations.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(institutionStatusNameTranslations.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.transType").value(DEFAULT_TRANS_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingInstitutionStatusNameTranslations() throws Exception {
        // Get the institutionStatusNameTranslations
        restInstitutionStatusNameTranslationsMockMvc.perform(get("/api/institutionStatusNameTranslationss/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInstitutionStatusNameTranslations() throws Exception {
        // Initialize the database
        institutionStatusNameTranslationsRepository.saveAndFlush(institutionStatusNameTranslations);

		int databaseSizeBeforeUpdate = institutionStatusNameTranslationsRepository.findAll().size();

        // Update the institutionStatusNameTranslations
        institutionStatusNameTranslations.setName(UPDATED_NAME);
        institutionStatusNameTranslations.setTransType(UPDATED_TRANS_TYPE);

        restInstitutionStatusNameTranslationsMockMvc.perform(put("/api/institutionStatusNameTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(institutionStatusNameTranslations)))
                .andExpect(status().isOk());

        // Validate the InstitutionStatusNameTranslations in the database
        List<InstitutionStatusNameTranslations> institutionStatusNameTranslationss = institutionStatusNameTranslationsRepository.findAll();
        assertThat(institutionStatusNameTranslationss).hasSize(databaseSizeBeforeUpdate);
        InstitutionStatusNameTranslations testInstitutionStatusNameTranslations = institutionStatusNameTranslationss.get(institutionStatusNameTranslationss.size() - 1);
        assertThat(testInstitutionStatusNameTranslations.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testInstitutionStatusNameTranslations.getTransType()).isEqualTo(UPDATED_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void deleteInstitutionStatusNameTranslations() throws Exception {
        // Initialize the database
        institutionStatusNameTranslationsRepository.saveAndFlush(institutionStatusNameTranslations);

		int databaseSizeBeforeDelete = institutionStatusNameTranslationsRepository.findAll().size();

        // Get the institutionStatusNameTranslations
        restInstitutionStatusNameTranslationsMockMvc.perform(delete("/api/institutionStatusNameTranslationss/{id}", institutionStatusNameTranslations.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<InstitutionStatusNameTranslations> institutionStatusNameTranslationss = institutionStatusNameTranslationsRepository.findAll();
        assertThat(institutionStatusNameTranslationss).hasSize(databaseSizeBeforeDelete - 1);
    }
}
