package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.InstitutionNameTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.InstitutionNameTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.InstitutionNameTranslationsSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the InstitutionNameTranslationsResource REST controller.
 *
 * @see InstitutionNameTranslationsResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class InstitutionNameTranslationsResourceTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_TRANS_TYPE = "AAAAA";
    private static final String UPDATED_TRANS_TYPE = "BBBBB";

    @Inject
    private InstitutionNameTranslationsRepository institutionNameTranslationsRepository;

    @Inject
    private InstitutionNameTranslationsSearchRepository institutionNameTranslationsSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restInstitutionNameTranslationsMockMvc;

    private InstitutionNameTranslations institutionNameTranslations;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        InstitutionNameTranslationsResource institutionNameTranslationsResource = new InstitutionNameTranslationsResource();
        ReflectionTestUtils.setField(institutionNameTranslationsResource, "institutionNameTranslationsRepository", institutionNameTranslationsRepository);
        ReflectionTestUtils.setField(institutionNameTranslationsResource, "institutionNameTranslationsSearchRepository", institutionNameTranslationsSearchRepository);
        this.restInstitutionNameTranslationsMockMvc = MockMvcBuilders.standaloneSetup(institutionNameTranslationsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        institutionNameTranslations = new InstitutionNameTranslations();
        institutionNameTranslations.setName(DEFAULT_NAME);
        institutionNameTranslations.setTransType(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void createInstitutionNameTranslations() throws Exception {
        int databaseSizeBeforeCreate = institutionNameTranslationsRepository.findAll().size();

        // Create the InstitutionNameTranslations

        restInstitutionNameTranslationsMockMvc.perform(post("/api/institutionNameTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(institutionNameTranslations)))
                .andExpect(status().isCreated());

        // Validate the InstitutionNameTranslations in the database
        List<InstitutionNameTranslations> institutionNameTranslationss = institutionNameTranslationsRepository.findAll();
        assertThat(institutionNameTranslationss).hasSize(databaseSizeBeforeCreate + 1);
        InstitutionNameTranslations testInstitutionNameTranslations = institutionNameTranslationss.get(institutionNameTranslationss.size() - 1);
        assertThat(testInstitutionNameTranslations.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testInstitutionNameTranslations.getTransType()).isEqualTo(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void getAllInstitutionNameTranslationss() throws Exception {
        // Initialize the database
        institutionNameTranslationsRepository.saveAndFlush(institutionNameTranslations);

        // Get all the institutionNameTranslationss
        restInstitutionNameTranslationsMockMvc.perform(get("/api/institutionNameTranslationss"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(institutionNameTranslations.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].transType").value(hasItem(DEFAULT_TRANS_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getInstitutionNameTranslations() throws Exception {
        // Initialize the database
        institutionNameTranslationsRepository.saveAndFlush(institutionNameTranslations);

        // Get the institutionNameTranslations
        restInstitutionNameTranslationsMockMvc.perform(get("/api/institutionNameTranslationss/{id}", institutionNameTranslations.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(institutionNameTranslations.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.transType").value(DEFAULT_TRANS_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingInstitutionNameTranslations() throws Exception {
        // Get the institutionNameTranslations
        restInstitutionNameTranslationsMockMvc.perform(get("/api/institutionNameTranslationss/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInstitutionNameTranslations() throws Exception {
        // Initialize the database
        institutionNameTranslationsRepository.saveAndFlush(institutionNameTranslations);

		int databaseSizeBeforeUpdate = institutionNameTranslationsRepository.findAll().size();

        // Update the institutionNameTranslations
        institutionNameTranslations.setName(UPDATED_NAME);
        institutionNameTranslations.setTransType(UPDATED_TRANS_TYPE);

        restInstitutionNameTranslationsMockMvc.perform(put("/api/institutionNameTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(institutionNameTranslations)))
                .andExpect(status().isOk());

        // Validate the InstitutionNameTranslations in the database
        List<InstitutionNameTranslations> institutionNameTranslationss = institutionNameTranslationsRepository.findAll();
        assertThat(institutionNameTranslationss).hasSize(databaseSizeBeforeUpdate);
        InstitutionNameTranslations testInstitutionNameTranslations = institutionNameTranslationss.get(institutionNameTranslationss.size() - 1);
        assertThat(testInstitutionNameTranslations.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testInstitutionNameTranslations.getTransType()).isEqualTo(UPDATED_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void deleteInstitutionNameTranslations() throws Exception {
        // Initialize the database
        institutionNameTranslationsRepository.saveAndFlush(institutionNameTranslations);

		int databaseSizeBeforeDelete = institutionNameTranslationsRepository.findAll().size();

        // Get the institutionNameTranslations
        restInstitutionNameTranslationsMockMvc.perform(delete("/api/institutionNameTranslationss/{id}", institutionNameTranslations.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<InstitutionNameTranslations> institutionNameTranslationss = institutionNameTranslationsRepository.findAll();
        assertThat(institutionNameTranslationss).hasSize(databaseSizeBeforeDelete - 1);
    }
}
