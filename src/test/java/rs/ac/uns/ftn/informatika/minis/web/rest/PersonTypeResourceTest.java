package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.PersonType;
import rs.ac.uns.ftn.informatika.minis.repository.PersonTypeRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PersonTypeSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the PersonTypeResource REST controller.
 *
 * @see PersonTypeResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class PersonTypeResourceTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    @Inject
    private PersonTypeRepository personTypeRepository;

    @Inject
    private PersonTypeSearchRepository personTypeSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPersonTypeMockMvc;

    private PersonType personType;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PersonTypeResource personTypeResource = new PersonTypeResource();
        ReflectionTestUtils.setField(personTypeResource, "personTypeRepository", personTypeRepository);
        ReflectionTestUtils.setField(personTypeResource, "personTypeSearchRepository", personTypeSearchRepository);
        this.restPersonTypeMockMvc = MockMvcBuilders.standaloneSetup(personTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        personType = new PersonType();
        personType.setName(DEFAULT_NAME);
        personType.setDescription(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createPersonType() throws Exception {
        int databaseSizeBeforeCreate = personTypeRepository.findAll().size();

        // Create the PersonType

        restPersonTypeMockMvc.perform(post("/api/personTypes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(personType)))
                .andExpect(status().isCreated());

        // Validate the PersonType in the database
        List<PersonType> personTypes = personTypeRepository.findAll();
        assertThat(personTypes).hasSize(databaseSizeBeforeCreate + 1);
        PersonType testPersonType = personTypes.get(personTypes.size() - 1);
        assertThat(testPersonType.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPersonType.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllPersonTypes() throws Exception {
        // Initialize the database
        personTypeRepository.saveAndFlush(personType);

        // Get all the personTypes
        restPersonTypeMockMvc.perform(get("/api/personTypes"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(personType.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getPersonType() throws Exception {
        // Initialize the database
        personTypeRepository.saveAndFlush(personType);

        // Get the personType
        restPersonTypeMockMvc.perform(get("/api/personTypes/{id}", personType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(personType.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPersonType() throws Exception {
        // Get the personType
        restPersonTypeMockMvc.perform(get("/api/personTypes/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePersonType() throws Exception {
        // Initialize the database
        personTypeRepository.saveAndFlush(personType);

		int databaseSizeBeforeUpdate = personTypeRepository.findAll().size();

        // Update the personType
        personType.setName(UPDATED_NAME);
        personType.setDescription(UPDATED_DESCRIPTION);

        restPersonTypeMockMvc.perform(put("/api/personTypes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(personType)))
                .andExpect(status().isOk());

        // Validate the PersonType in the database
        List<PersonType> personTypes = personTypeRepository.findAll();
        assertThat(personTypes).hasSize(databaseSizeBeforeUpdate);
        PersonType testPersonType = personTypes.get(personTypes.size() - 1);
        assertThat(testPersonType.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPersonType.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void deletePersonType() throws Exception {
        // Initialize the database
        personTypeRepository.saveAndFlush(personType);

		int databaseSizeBeforeDelete = personTypeRepository.findAll().size();

        // Get the personType
        restPersonTypeMockMvc.perform(delete("/api/personTypes/{id}", personType.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<PersonType> personTypes = personTypeRepository.findAll();
        assertThat(personTypes).hasSize(databaseSizeBeforeDelete - 1);
    }
}
