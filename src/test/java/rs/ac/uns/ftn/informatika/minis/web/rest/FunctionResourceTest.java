package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.Function;
import rs.ac.uns.ftn.informatika.minis.repository.FunctionRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.FunctionSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the FunctionResource REST controller.
 *
 * @see FunctionResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class FunctionResourceTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    @Inject
    private FunctionRepository functionRepository;

    @Inject
    private FunctionSearchRepository functionSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restFunctionMockMvc;

    private Function function;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        FunctionResource functionResource = new FunctionResource();
        ReflectionTestUtils.setField(functionResource, "functionRepository", functionRepository);
        ReflectionTestUtils.setField(functionResource, "functionSearchRepository", functionSearchRepository);
        this.restFunctionMockMvc = MockMvcBuilders.standaloneSetup(functionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        function = new Function();
        function.setName(DEFAULT_NAME);
        function.setDescription(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createFunction() throws Exception {
        int databaseSizeBeforeCreate = functionRepository.findAll().size();

        // Create the Function

        restFunctionMockMvc.perform(post("/api/functions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(function)))
                .andExpect(status().isCreated());

        // Validate the Function in the database
        List<Function> functions = functionRepository.findAll();
        assertThat(functions).hasSize(databaseSizeBeforeCreate + 1);
        Function testFunction = functions.get(functions.size() - 1);
        assertThat(testFunction.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testFunction.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllFunctions() throws Exception {
        // Initialize the database
        functionRepository.saveAndFlush(function);

        // Get all the functions
        restFunctionMockMvc.perform(get("/api/functions"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(function.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getFunction() throws Exception {
        // Initialize the database
        functionRepository.saveAndFlush(function);

        // Get the function
        restFunctionMockMvc.perform(get("/api/functions/{id}", function.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(function.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingFunction() throws Exception {
        // Get the function
        restFunctionMockMvc.perform(get("/api/functions/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFunction() throws Exception {
        // Initialize the database
        functionRepository.saveAndFlush(function);

		int databaseSizeBeforeUpdate = functionRepository.findAll().size();

        // Update the function
        function.setName(UPDATED_NAME);
        function.setDescription(UPDATED_DESCRIPTION);

        restFunctionMockMvc.perform(put("/api/functions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(function)))
                .andExpect(status().isOk());

        // Validate the Function in the database
        List<Function> functions = functionRepository.findAll();
        assertThat(functions).hasSize(databaseSizeBeforeUpdate);
        Function testFunction = functions.get(functions.size() - 1);
        assertThat(testFunction.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testFunction.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void deleteFunction() throws Exception {
        // Initialize the database
        functionRepository.saveAndFlush(function);

		int databaseSizeBeforeDelete = functionRepository.findAll().size();

        // Get the function
        restFunctionMockMvc.perform(delete("/api/functions/{id}", function.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Function> functions = functionRepository.findAll();
        assertThat(functions).hasSize(databaseSizeBeforeDelete - 1);
    }
}
