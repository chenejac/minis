package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.PersonStatusNameTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.PersonStatusNameTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PersonStatusNameTranslationsSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the PersonStatusNameTranslationsResource REST controller.
 *
 * @see PersonStatusNameTranslationsResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class PersonStatusNameTranslationsResourceTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_TRANS_TYPE = "AAAAA";
    private static final String UPDATED_TRANS_TYPE = "BBBBB";

    @Inject
    private PersonStatusNameTranslationsRepository personStatusNameTranslationsRepository;

    @Inject
    private PersonStatusNameTranslationsSearchRepository personStatusNameTranslationsSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPersonStatusNameTranslationsMockMvc;

    private PersonStatusNameTranslations personStatusNameTranslations;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PersonStatusNameTranslationsResource personStatusNameTranslationsResource = new PersonStatusNameTranslationsResource();
        ReflectionTestUtils.setField(personStatusNameTranslationsResource, "personStatusNameTranslationsRepository", personStatusNameTranslationsRepository);
        ReflectionTestUtils.setField(personStatusNameTranslationsResource, "personStatusNameTranslationsSearchRepository", personStatusNameTranslationsSearchRepository);
        this.restPersonStatusNameTranslationsMockMvc = MockMvcBuilders.standaloneSetup(personStatusNameTranslationsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        personStatusNameTranslations = new PersonStatusNameTranslations();
        personStatusNameTranslations.setName(DEFAULT_NAME);
        personStatusNameTranslations.setTransType(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void createPersonStatusNameTranslations() throws Exception {
        int databaseSizeBeforeCreate = personStatusNameTranslationsRepository.findAll().size();

        // Create the PersonStatusNameTranslations

        restPersonStatusNameTranslationsMockMvc.perform(post("/api/personStatusNameTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(personStatusNameTranslations)))
                .andExpect(status().isCreated());

        // Validate the PersonStatusNameTranslations in the database
        List<PersonStatusNameTranslations> personStatusNameTranslationss = personStatusNameTranslationsRepository.findAll();
        assertThat(personStatusNameTranslationss).hasSize(databaseSizeBeforeCreate + 1);
        PersonStatusNameTranslations testPersonStatusNameTranslations = personStatusNameTranslationss.get(personStatusNameTranslationss.size() - 1);
        assertThat(testPersonStatusNameTranslations.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPersonStatusNameTranslations.getTransType()).isEqualTo(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void getAllPersonStatusNameTranslationss() throws Exception {
        // Initialize the database
        personStatusNameTranslationsRepository.saveAndFlush(personStatusNameTranslations);

        // Get all the personStatusNameTranslationss
        restPersonStatusNameTranslationsMockMvc.perform(get("/api/personStatusNameTranslationss"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(personStatusNameTranslations.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].transType").value(hasItem(DEFAULT_TRANS_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getPersonStatusNameTranslations() throws Exception {
        // Initialize the database
        personStatusNameTranslationsRepository.saveAndFlush(personStatusNameTranslations);

        // Get the personStatusNameTranslations
        restPersonStatusNameTranslationsMockMvc.perform(get("/api/personStatusNameTranslationss/{id}", personStatusNameTranslations.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(personStatusNameTranslations.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.transType").value(DEFAULT_TRANS_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPersonStatusNameTranslations() throws Exception {
        // Get the personStatusNameTranslations
        restPersonStatusNameTranslationsMockMvc.perform(get("/api/personStatusNameTranslationss/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePersonStatusNameTranslations() throws Exception {
        // Initialize the database
        personStatusNameTranslationsRepository.saveAndFlush(personStatusNameTranslations);

		int databaseSizeBeforeUpdate = personStatusNameTranslationsRepository.findAll().size();

        // Update the personStatusNameTranslations
        personStatusNameTranslations.setName(UPDATED_NAME);
        personStatusNameTranslations.setTransType(UPDATED_TRANS_TYPE);

        restPersonStatusNameTranslationsMockMvc.perform(put("/api/personStatusNameTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(personStatusNameTranslations)))
                .andExpect(status().isOk());

        // Validate the PersonStatusNameTranslations in the database
        List<PersonStatusNameTranslations> personStatusNameTranslationss = personStatusNameTranslationsRepository.findAll();
        assertThat(personStatusNameTranslationss).hasSize(databaseSizeBeforeUpdate);
        PersonStatusNameTranslations testPersonStatusNameTranslations = personStatusNameTranslationss.get(personStatusNameTranslationss.size() - 1);
        assertThat(testPersonStatusNameTranslations.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPersonStatusNameTranslations.getTransType()).isEqualTo(UPDATED_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void deletePersonStatusNameTranslations() throws Exception {
        // Initialize the database
        personStatusNameTranslationsRepository.saveAndFlush(personStatusNameTranslations);

		int databaseSizeBeforeDelete = personStatusNameTranslationsRepository.findAll().size();

        // Get the personStatusNameTranslations
        restPersonStatusNameTranslationsMockMvc.perform(delete("/api/personStatusNameTranslationss/{id}", personStatusNameTranslations.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<PersonStatusNameTranslations> personStatusNameTranslationss = personStatusNameTranslationsRepository.findAll();
        assertThat(personStatusNameTranslationss).hasSize(databaseSizeBeforeDelete - 1);
    }
}
