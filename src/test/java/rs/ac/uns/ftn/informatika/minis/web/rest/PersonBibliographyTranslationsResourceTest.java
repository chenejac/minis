package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.PersonBibliographyTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.PersonBibliographyTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PersonBibliographyTranslationsSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the PersonBibliographyTranslationsResource REST controller.
 *
 * @see PersonBibliographyTranslationsResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class PersonBibliographyTranslationsResourceTest {

    private static final String DEFAULT_BIBLIOGRAPHY = "AAAAA";
    private static final String UPDATED_BIBLIOGRAPHY = "BBBBB";
    private static final String DEFAULT_TRANS_TYPE = "AAAAA";
    private static final String UPDATED_TRANS_TYPE = "BBBBB";

    @Inject
    private PersonBibliographyTranslationsRepository personBibliographyTranslationsRepository;

    @Inject
    private PersonBibliographyTranslationsSearchRepository personBibliographyTranslationsSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPersonBibliographyTranslationsMockMvc;

    private PersonBibliographyTranslations personBibliographyTranslations;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PersonBibliographyTranslationsResource personBibliographyTranslationsResource = new PersonBibliographyTranslationsResource();
        ReflectionTestUtils.setField(personBibliographyTranslationsResource, "personBibliographyTranslationsRepository", personBibliographyTranslationsRepository);
        ReflectionTestUtils.setField(personBibliographyTranslationsResource, "personBibliographyTranslationsSearchRepository", personBibliographyTranslationsSearchRepository);
        this.restPersonBibliographyTranslationsMockMvc = MockMvcBuilders.standaloneSetup(personBibliographyTranslationsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        personBibliographyTranslations = new PersonBibliographyTranslations();
        personBibliographyTranslations.setBibliography(DEFAULT_BIBLIOGRAPHY);
        personBibliographyTranslations.setTransType(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void createPersonBibliographyTranslations() throws Exception {
        int databaseSizeBeforeCreate = personBibliographyTranslationsRepository.findAll().size();

        // Create the PersonBibliographyTranslations

        restPersonBibliographyTranslationsMockMvc.perform(post("/api/personBibliographyTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(personBibliographyTranslations)))
                .andExpect(status().isCreated());

        // Validate the PersonBibliographyTranslations in the database
        List<PersonBibliographyTranslations> personBibliographyTranslationss = personBibliographyTranslationsRepository.findAll();
        assertThat(personBibliographyTranslationss).hasSize(databaseSizeBeforeCreate + 1);
        PersonBibliographyTranslations testPersonBibliographyTranslations = personBibliographyTranslationss.get(personBibliographyTranslationss.size() - 1);
        assertThat(testPersonBibliographyTranslations.getBibliography()).isEqualTo(DEFAULT_BIBLIOGRAPHY);
        assertThat(testPersonBibliographyTranslations.getTransType()).isEqualTo(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void getAllPersonBibliographyTranslationss() throws Exception {
        // Initialize the database
        personBibliographyTranslationsRepository.saveAndFlush(personBibliographyTranslations);

        // Get all the personBibliographyTranslationss
        restPersonBibliographyTranslationsMockMvc.perform(get("/api/personBibliographyTranslationss"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(personBibliographyTranslations.getId().intValue())))
                .andExpect(jsonPath("$.[*].bibliography").value(hasItem(DEFAULT_BIBLIOGRAPHY.toString())))
                .andExpect(jsonPath("$.[*].transType").value(hasItem(DEFAULT_TRANS_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getPersonBibliographyTranslations() throws Exception {
        // Initialize the database
        personBibliographyTranslationsRepository.saveAndFlush(personBibliographyTranslations);

        // Get the personBibliographyTranslations
        restPersonBibliographyTranslationsMockMvc.perform(get("/api/personBibliographyTranslationss/{id}", personBibliographyTranslations.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(personBibliographyTranslations.getId().intValue()))
            .andExpect(jsonPath("$.bibliography").value(DEFAULT_BIBLIOGRAPHY.toString()))
            .andExpect(jsonPath("$.transType").value(DEFAULT_TRANS_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPersonBibliographyTranslations() throws Exception {
        // Get the personBibliographyTranslations
        restPersonBibliographyTranslationsMockMvc.perform(get("/api/personBibliographyTranslationss/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePersonBibliographyTranslations() throws Exception {
        // Initialize the database
        personBibliographyTranslationsRepository.saveAndFlush(personBibliographyTranslations);

		int databaseSizeBeforeUpdate = personBibliographyTranslationsRepository.findAll().size();

        // Update the personBibliographyTranslations
        personBibliographyTranslations.setBibliography(UPDATED_BIBLIOGRAPHY);
        personBibliographyTranslations.setTransType(UPDATED_TRANS_TYPE);

        restPersonBibliographyTranslationsMockMvc.perform(put("/api/personBibliographyTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(personBibliographyTranslations)))
                .andExpect(status().isOk());

        // Validate the PersonBibliographyTranslations in the database
        List<PersonBibliographyTranslations> personBibliographyTranslationss = personBibliographyTranslationsRepository.findAll();
        assertThat(personBibliographyTranslationss).hasSize(databaseSizeBeforeUpdate);
        PersonBibliographyTranslations testPersonBibliographyTranslations = personBibliographyTranslationss.get(personBibliographyTranslationss.size() - 1);
        assertThat(testPersonBibliographyTranslations.getBibliography()).isEqualTo(UPDATED_BIBLIOGRAPHY);
        assertThat(testPersonBibliographyTranslations.getTransType()).isEqualTo(UPDATED_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void deletePersonBibliographyTranslations() throws Exception {
        // Initialize the database
        personBibliographyTranslationsRepository.saveAndFlush(personBibliographyTranslations);

		int databaseSizeBeforeDelete = personBibliographyTranslationsRepository.findAll().size();

        // Get the personBibliographyTranslations
        restPersonBibliographyTranslationsMockMvc.perform(delete("/api/personBibliographyTranslationss/{id}", personBibliographyTranslations.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<PersonBibliographyTranslations> personBibliographyTranslationss = personBibliographyTranslationsRepository.findAll();
        assertThat(personBibliographyTranslationss).hasSize(databaseSizeBeforeDelete - 1);
    }
}
