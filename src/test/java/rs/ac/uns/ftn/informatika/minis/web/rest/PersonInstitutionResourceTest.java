package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.PersonInstitution;
import rs.ac.uns.ftn.informatika.minis.repository.PersonInstitutionRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PersonInstitutionSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the PersonInstitutionResource REST controller.
 *
 * @see PersonInstitutionResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class PersonInstitutionResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");


    private static final DateTime DEFAULT_START_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_START_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_START_DATE_STR = dateTimeFormatter.print(DEFAULT_START_DATE);

    private static final DateTime DEFAULT_END_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_END_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_END_DATE_STR = dateTimeFormatter.print(DEFAULT_END_DATE);
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";
    private static final String DEFAULT_RESEARCH_AREA = "AAAAA";
    private static final String UPDATED_RESEARCH_AREA = "BBBBB";

    @Inject
    private PersonInstitutionRepository personInstitutionRepository;

    @Inject
    private PersonInstitutionSearchRepository personInstitutionSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPersonInstitutionMockMvc;

    private PersonInstitution personInstitution;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PersonInstitutionResource personInstitutionResource = new PersonInstitutionResource();
        ReflectionTestUtils.setField(personInstitutionResource, "personInstitutionRepository", personInstitutionRepository);
        ReflectionTestUtils.setField(personInstitutionResource, "personInstitutionSearchRepository", personInstitutionSearchRepository);
        this.restPersonInstitutionMockMvc = MockMvcBuilders.standaloneSetup(personInstitutionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        personInstitution = new PersonInstitution();
        personInstitution.setStartDate(DEFAULT_START_DATE);
        personInstitution.setEndDate(DEFAULT_END_DATE);
        personInstitution.setDescription(DEFAULT_DESCRIPTION);
        personInstitution.setResearchArea(DEFAULT_RESEARCH_AREA);
    }

    @Test
    @Transactional
    public void createPersonInstitution() throws Exception {
        int databaseSizeBeforeCreate = personInstitutionRepository.findAll().size();

        // Create the PersonInstitution

        restPersonInstitutionMockMvc.perform(post("/api/personInstitutions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(personInstitution)))
                .andExpect(status().isCreated());

        // Validate the PersonInstitution in the database
        List<PersonInstitution> personInstitutions = personInstitutionRepository.findAll();
        assertThat(personInstitutions).hasSize(databaseSizeBeforeCreate + 1);
        PersonInstitution testPersonInstitution = personInstitutions.get(personInstitutions.size() - 1);
        assertThat(testPersonInstitution.getStartDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_START_DATE);
        assertThat(testPersonInstitution.getEndDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_END_DATE);
        assertThat(testPersonInstitution.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testPersonInstitution.getResearchArea()).isEqualTo(DEFAULT_RESEARCH_AREA);
    }

    @Test
    @Transactional
    public void getAllPersonInstitutions() throws Exception {
        // Initialize the database
        personInstitutionRepository.saveAndFlush(personInstitution);

        // Get all the personInstitutions
        restPersonInstitutionMockMvc.perform(get("/api/personInstitutions"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(personInstitution.getId().intValue())))
                .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE_STR)))
                .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE_STR)))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
                .andExpect(jsonPath("$.[*].researchArea").value(hasItem(DEFAULT_RESEARCH_AREA.toString())));
    }

    @Test
    @Transactional
    public void getPersonInstitution() throws Exception {
        // Initialize the database
        personInstitutionRepository.saveAndFlush(personInstitution);

        // Get the personInstitution
        restPersonInstitutionMockMvc.perform(get("/api/personInstitutions/{id}", personInstitution.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(personInstitution.getId().intValue()))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE_STR))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE_STR))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.researchArea").value(DEFAULT_RESEARCH_AREA.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPersonInstitution() throws Exception {
        // Get the personInstitution
        restPersonInstitutionMockMvc.perform(get("/api/personInstitutions/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePersonInstitution() throws Exception {
        // Initialize the database
        personInstitutionRepository.saveAndFlush(personInstitution);

		int databaseSizeBeforeUpdate = personInstitutionRepository.findAll().size();

        // Update the personInstitution
        personInstitution.setStartDate(UPDATED_START_DATE);
        personInstitution.setEndDate(UPDATED_END_DATE);
        personInstitution.setDescription(UPDATED_DESCRIPTION);
        personInstitution.setResearchArea(UPDATED_RESEARCH_AREA);

        restPersonInstitutionMockMvc.perform(put("/api/personInstitutions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(personInstitution)))
                .andExpect(status().isOk());

        // Validate the PersonInstitution in the database
        List<PersonInstitution> personInstitutions = personInstitutionRepository.findAll();
        assertThat(personInstitutions).hasSize(databaseSizeBeforeUpdate);
        PersonInstitution testPersonInstitution = personInstitutions.get(personInstitutions.size() - 1);
        assertThat(testPersonInstitution.getStartDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_START_DATE);
        assertThat(testPersonInstitution.getEndDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_END_DATE);
        assertThat(testPersonInstitution.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testPersonInstitution.getResearchArea()).isEqualTo(UPDATED_RESEARCH_AREA);
    }

    @Test
    @Transactional
    public void deletePersonInstitution() throws Exception {
        // Initialize the database
        personInstitutionRepository.saveAndFlush(personInstitution);

		int databaseSizeBeforeDelete = personInstitutionRepository.findAll().size();

        // Get the personInstitution
        restPersonInstitutionMockMvc.perform(delete("/api/personInstitutions/{id}", personInstitution.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<PersonInstitution> personInstitutions = personInstitutionRepository.findAll();
        assertThat(personInstitutions).hasSize(databaseSizeBeforeDelete - 1);
    }
}
