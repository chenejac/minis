package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.PersonCategory;
import rs.ac.uns.ftn.informatika.minis.repository.PersonCategoryRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PersonCategorySearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the PersonCategoryResource REST controller.
 *
 * @see PersonCategoryResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class PersonCategoryResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");


    private static final DateTime DEFAULT_START_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_START_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_START_DATE_STR = dateTimeFormatter.print(DEFAULT_START_DATE);

    private static final DateTime DEFAULT_END_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_END_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_END_DATE_STR = dateTimeFormatter.print(DEFAULT_END_DATE);

    @Inject
    private PersonCategoryRepository personCategoryRepository;

    @Inject
    private PersonCategorySearchRepository personCategorySearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPersonCategoryMockMvc;

    private PersonCategory personCategory;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PersonCategoryResource personCategoryResource = new PersonCategoryResource();
        ReflectionTestUtils.setField(personCategoryResource, "personCategoryRepository", personCategoryRepository);
        ReflectionTestUtils.setField(personCategoryResource, "personCategorySearchRepository", personCategorySearchRepository);
        this.restPersonCategoryMockMvc = MockMvcBuilders.standaloneSetup(personCategoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        personCategory = new PersonCategory();
        personCategory.setStartDate(DEFAULT_START_DATE);
        personCategory.setEndDate(DEFAULT_END_DATE);
    }

    @Test
    @Transactional
    public void createPersonCategory() throws Exception {
        int databaseSizeBeforeCreate = personCategoryRepository.findAll().size();

        // Create the PersonCategory

        restPersonCategoryMockMvc.perform(post("/api/personCategorys")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(personCategory)))
                .andExpect(status().isCreated());

        // Validate the PersonCategory in the database
        List<PersonCategory> personCategorys = personCategoryRepository.findAll();
        assertThat(personCategorys).hasSize(databaseSizeBeforeCreate + 1);
        PersonCategory testPersonCategory = personCategorys.get(personCategorys.size() - 1);
        assertThat(testPersonCategory.getStartDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_START_DATE);
        assertThat(testPersonCategory.getEndDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_END_DATE);
    }

    @Test
    @Transactional
    public void getAllPersonCategorys() throws Exception {
        // Initialize the database
        personCategoryRepository.saveAndFlush(personCategory);

        // Get all the personCategorys
        restPersonCategoryMockMvc.perform(get("/api/personCategorys"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(personCategory.getId().intValue())))
                .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE_STR)))
                .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE_STR)));
    }

    @Test
    @Transactional
    public void getPersonCategory() throws Exception {
        // Initialize the database
        personCategoryRepository.saveAndFlush(personCategory);

        // Get the personCategory
        restPersonCategoryMockMvc.perform(get("/api/personCategorys/{id}", personCategory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(personCategory.getId().intValue()))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE_STR))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE_STR));
    }

    @Test
    @Transactional
    public void getNonExistingPersonCategory() throws Exception {
        // Get the personCategory
        restPersonCategoryMockMvc.perform(get("/api/personCategorys/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePersonCategory() throws Exception {
        // Initialize the database
        personCategoryRepository.saveAndFlush(personCategory);

		int databaseSizeBeforeUpdate = personCategoryRepository.findAll().size();

        // Update the personCategory
        personCategory.setStartDate(UPDATED_START_DATE);
        personCategory.setEndDate(UPDATED_END_DATE);

        restPersonCategoryMockMvc.perform(put("/api/personCategorys")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(personCategory)))
                .andExpect(status().isOk());

        // Validate the PersonCategory in the database
        List<PersonCategory> personCategorys = personCategoryRepository.findAll();
        assertThat(personCategorys).hasSize(databaseSizeBeforeUpdate);
        PersonCategory testPersonCategory = personCategorys.get(personCategorys.size() - 1);
        assertThat(testPersonCategory.getStartDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_START_DATE);
        assertThat(testPersonCategory.getEndDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_END_DATE);
    }

    @Test
    @Transactional
    public void deletePersonCategory() throws Exception {
        // Initialize the database
        personCategoryRepository.saveAndFlush(personCategory);

		int databaseSizeBeforeDelete = personCategoryRepository.findAll().size();

        // Get the personCategory
        restPersonCategoryMockMvc.perform(delete("/api/personCategorys/{id}", personCategory.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<PersonCategory> personCategorys = personCategoryRepository.findAll();
        assertThat(personCategorys).hasSize(databaseSizeBeforeDelete - 1);
    }
}
