package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.ActivityNameTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.ActivityNameTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.ActivityNameTranslationsSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ActivityNameTranslationsResource REST controller.
 *
 * @see ActivityNameTranslationsResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class ActivityNameTranslationsResourceTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_TRANS_TYPE = "AAAAA";
    private static final String UPDATED_TRANS_TYPE = "BBBBB";

    @Inject
    private ActivityNameTranslationsRepository activityNameTranslationsRepository;

    @Inject
    private ActivityNameTranslationsSearchRepository activityNameTranslationsSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restActivityNameTranslationsMockMvc;

    private ActivityNameTranslations activityNameTranslations;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ActivityNameTranslationsResource activityNameTranslationsResource = new ActivityNameTranslationsResource();
        ReflectionTestUtils.setField(activityNameTranslationsResource, "activityNameTranslationsRepository", activityNameTranslationsRepository);
        ReflectionTestUtils.setField(activityNameTranslationsResource, "activityNameTranslationsSearchRepository", activityNameTranslationsSearchRepository);
        this.restActivityNameTranslationsMockMvc = MockMvcBuilders.standaloneSetup(activityNameTranslationsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        activityNameTranslations = new ActivityNameTranslations();
        activityNameTranslations.setName(DEFAULT_NAME);
        activityNameTranslations.setTransType(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void createActivityNameTranslations() throws Exception {
        int databaseSizeBeforeCreate = activityNameTranslationsRepository.findAll().size();

        // Create the ActivityNameTranslations

        restActivityNameTranslationsMockMvc.perform(post("/api/activityNameTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(activityNameTranslations)))
                .andExpect(status().isCreated());

        // Validate the ActivityNameTranslations in the database
        List<ActivityNameTranslations> activityNameTranslationss = activityNameTranslationsRepository.findAll();
        assertThat(activityNameTranslationss).hasSize(databaseSizeBeforeCreate + 1);
        ActivityNameTranslations testActivityNameTranslations = activityNameTranslationss.get(activityNameTranslationss.size() - 1);
        assertThat(testActivityNameTranslations.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testActivityNameTranslations.getTransType()).isEqualTo(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void getAllActivityNameTranslationss() throws Exception {
        // Initialize the database
        activityNameTranslationsRepository.saveAndFlush(activityNameTranslations);

        // Get all the activityNameTranslationss
        restActivityNameTranslationsMockMvc.perform(get("/api/activityNameTranslationss"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(activityNameTranslations.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].transType").value(hasItem(DEFAULT_TRANS_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getActivityNameTranslations() throws Exception {
        // Initialize the database
        activityNameTranslationsRepository.saveAndFlush(activityNameTranslations);

        // Get the activityNameTranslations
        restActivityNameTranslationsMockMvc.perform(get("/api/activityNameTranslationss/{id}", activityNameTranslations.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(activityNameTranslations.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.transType").value(DEFAULT_TRANS_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingActivityNameTranslations() throws Exception {
        // Get the activityNameTranslations
        restActivityNameTranslationsMockMvc.perform(get("/api/activityNameTranslationss/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateActivityNameTranslations() throws Exception {
        // Initialize the database
        activityNameTranslationsRepository.saveAndFlush(activityNameTranslations);

		int databaseSizeBeforeUpdate = activityNameTranslationsRepository.findAll().size();

        // Update the activityNameTranslations
        activityNameTranslations.setName(UPDATED_NAME);
        activityNameTranslations.setTransType(UPDATED_TRANS_TYPE);

        restActivityNameTranslationsMockMvc.perform(put("/api/activityNameTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(activityNameTranslations)))
                .andExpect(status().isOk());

        // Validate the ActivityNameTranslations in the database
        List<ActivityNameTranslations> activityNameTranslationss = activityNameTranslationsRepository.findAll();
        assertThat(activityNameTranslationss).hasSize(databaseSizeBeforeUpdate);
        ActivityNameTranslations testActivityNameTranslations = activityNameTranslationss.get(activityNameTranslationss.size() - 1);
        assertThat(testActivityNameTranslations.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testActivityNameTranslations.getTransType()).isEqualTo(UPDATED_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void deleteActivityNameTranslations() throws Exception {
        // Initialize the database
        activityNameTranslationsRepository.saveAndFlush(activityNameTranslations);

		int databaseSizeBeforeDelete = activityNameTranslationsRepository.findAll().size();

        // Get the activityNameTranslations
        restActivityNameTranslationsMockMvc.perform(delete("/api/activityNameTranslationss/{id}", activityNameTranslations.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ActivityNameTranslations> activityNameTranslationss = activityNameTranslationsRepository.findAll();
        assertThat(activityNameTranslationss).hasSize(databaseSizeBeforeDelete - 1);
    }
}
