package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.PersonName;
import rs.ac.uns.ftn.informatika.minis.repository.PersonNameRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PersonNameSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the PersonNameResource REST controller.
 *
 * @see PersonNameResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class PersonNameResourceTest {

    private static final String DEFAULT_FIRSTNAME = "AAAAA";
    private static final String UPDATED_FIRSTNAME = "BBBBB";
    private static final String DEFAULT_LASTNAME = "AAAAA";
    private static final String UPDATED_LASTNAME = "BBBBB";
    private static final String DEFAULT_MIDDLE_NAME = "AAAAA";
    private static final String UPDATED_MIDDLE_NAME = "BBBBB";

    @Inject
    private PersonNameRepository personNameRepository;

    @Inject
    private PersonNameSearchRepository personNameSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPersonNameMockMvc;

    private PersonName personName;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PersonNameResource personNameResource = new PersonNameResource();
        ReflectionTestUtils.setField(personNameResource, "personNameRepository", personNameRepository);
        ReflectionTestUtils.setField(personNameResource, "personNameSearchRepository", personNameSearchRepository);
        this.restPersonNameMockMvc = MockMvcBuilders.standaloneSetup(personNameResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        personName = new PersonName();
        personName.setFirstname(DEFAULT_FIRSTNAME);
        personName.setLastname(DEFAULT_LASTNAME);
        personName.setMiddleName(DEFAULT_MIDDLE_NAME);
    }

    @Test
    @Transactional
    public void createPersonName() throws Exception {
        int databaseSizeBeforeCreate = personNameRepository.findAll().size();

        // Create the PersonName

        restPersonNameMockMvc.perform(post("/api/personNames")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(personName)))
                .andExpect(status().isCreated());

        // Validate the PersonName in the database
        List<PersonName> personNames = personNameRepository.findAll();
        assertThat(personNames).hasSize(databaseSizeBeforeCreate + 1);
        PersonName testPersonName = personNames.get(personNames.size() - 1);
        assertThat(testPersonName.getFirstname()).isEqualTo(DEFAULT_FIRSTNAME);
        assertThat(testPersonName.getLastname()).isEqualTo(DEFAULT_LASTNAME);
        assertThat(testPersonName.getMiddleName()).isEqualTo(DEFAULT_MIDDLE_NAME);
    }

    @Test
    @Transactional
    public void getAllPersonNames() throws Exception {
        // Initialize the database
        personNameRepository.saveAndFlush(personName);

        // Get all the personNames
        restPersonNameMockMvc.perform(get("/api/personNames"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(personName.getId().intValue())))
                .andExpect(jsonPath("$.[*].firstname").value(hasItem(DEFAULT_FIRSTNAME.toString())))
                .andExpect(jsonPath("$.[*].lastname").value(hasItem(DEFAULT_LASTNAME.toString())))
                .andExpect(jsonPath("$.[*].middleName").value(hasItem(DEFAULT_MIDDLE_NAME.toString())));
    }

    @Test
    @Transactional
    public void getPersonName() throws Exception {
        // Initialize the database
        personNameRepository.saveAndFlush(personName);

        // Get the personName
        restPersonNameMockMvc.perform(get("/api/personNames/{id}", personName.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(personName.getId().intValue()))
            .andExpect(jsonPath("$.firstname").value(DEFAULT_FIRSTNAME.toString()))
            .andExpect(jsonPath("$.lastname").value(DEFAULT_LASTNAME.toString()))
            .andExpect(jsonPath("$.middleName").value(DEFAULT_MIDDLE_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPersonName() throws Exception {
        // Get the personName
        restPersonNameMockMvc.perform(get("/api/personNames/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePersonName() throws Exception {
        // Initialize the database
        personNameRepository.saveAndFlush(personName);

		int databaseSizeBeforeUpdate = personNameRepository.findAll().size();

        // Update the personName
        personName.setFirstname(UPDATED_FIRSTNAME);
        personName.setLastname(UPDATED_LASTNAME);
        personName.setMiddleName(UPDATED_MIDDLE_NAME);

        restPersonNameMockMvc.perform(put("/api/personNames")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(personName)))
                .andExpect(status().isOk());

        // Validate the PersonName in the database
        List<PersonName> personNames = personNameRepository.findAll();
        assertThat(personNames).hasSize(databaseSizeBeforeUpdate);
        PersonName testPersonName = personNames.get(personNames.size() - 1);
        assertThat(testPersonName.getFirstname()).isEqualTo(UPDATED_FIRSTNAME);
        assertThat(testPersonName.getLastname()).isEqualTo(UPDATED_LASTNAME);
        assertThat(testPersonName.getMiddleName()).isEqualTo(UPDATED_MIDDLE_NAME);
    }

    @Test
    @Transactional
    public void deletePersonName() throws Exception {
        // Initialize the database
        personNameRepository.saveAndFlush(personName);

		int databaseSizeBeforeDelete = personNameRepository.findAll().size();

        // Get the personName
        restPersonNameMockMvc.perform(delete("/api/personNames/{id}", personName.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<PersonName> personNames = personNameRepository.findAll();
        assertThat(personNames).hasSize(databaseSizeBeforeDelete - 1);
    }
}
