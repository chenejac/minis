package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.PositionNameTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.PositionNameTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PositionNameTranslationsSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the PositionNameTranslationsResource REST controller.
 *
 * @see PositionNameTranslationsResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class PositionNameTranslationsResourceTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_TRANS_TYPE = "AAAAA";
    private static final String UPDATED_TRANS_TYPE = "BBBBB";

    @Inject
    private PositionNameTranslationsRepository positionNameTranslationsRepository;

    @Inject
    private PositionNameTranslationsSearchRepository positionNameTranslationsSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPositionNameTranslationsMockMvc;

    private PositionNameTranslations positionNameTranslations;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PositionNameTranslationsResource positionNameTranslationsResource = new PositionNameTranslationsResource();
        ReflectionTestUtils.setField(positionNameTranslationsResource, "positionNameTranslationsRepository", positionNameTranslationsRepository);
        ReflectionTestUtils.setField(positionNameTranslationsResource, "positionNameTranslationsSearchRepository", positionNameTranslationsSearchRepository);
        this.restPositionNameTranslationsMockMvc = MockMvcBuilders.standaloneSetup(positionNameTranslationsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        positionNameTranslations = new PositionNameTranslations();
        positionNameTranslations.setName(DEFAULT_NAME);
        positionNameTranslations.setTransType(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void createPositionNameTranslations() throws Exception {
        int databaseSizeBeforeCreate = positionNameTranslationsRepository.findAll().size();

        // Create the PositionNameTranslations

        restPositionNameTranslationsMockMvc.perform(post("/api/positionNameTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(positionNameTranslations)))
                .andExpect(status().isCreated());

        // Validate the PositionNameTranslations in the database
        List<PositionNameTranslations> positionNameTranslationss = positionNameTranslationsRepository.findAll();
        assertThat(positionNameTranslationss).hasSize(databaseSizeBeforeCreate + 1);
        PositionNameTranslations testPositionNameTranslations = positionNameTranslationss.get(positionNameTranslationss.size() - 1);
        assertThat(testPositionNameTranslations.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPositionNameTranslations.getTransType()).isEqualTo(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void getAllPositionNameTranslationss() throws Exception {
        // Initialize the database
        positionNameTranslationsRepository.saveAndFlush(positionNameTranslations);

        // Get all the positionNameTranslationss
        restPositionNameTranslationsMockMvc.perform(get("/api/positionNameTranslationss"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(positionNameTranslations.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].transType").value(hasItem(DEFAULT_TRANS_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getPositionNameTranslations() throws Exception {
        // Initialize the database
        positionNameTranslationsRepository.saveAndFlush(positionNameTranslations);

        // Get the positionNameTranslations
        restPositionNameTranslationsMockMvc.perform(get("/api/positionNameTranslationss/{id}", positionNameTranslations.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(positionNameTranslations.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.transType").value(DEFAULT_TRANS_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPositionNameTranslations() throws Exception {
        // Get the positionNameTranslations
        restPositionNameTranslationsMockMvc.perform(get("/api/positionNameTranslationss/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePositionNameTranslations() throws Exception {
        // Initialize the database
        positionNameTranslationsRepository.saveAndFlush(positionNameTranslations);

		int databaseSizeBeforeUpdate = positionNameTranslationsRepository.findAll().size();

        // Update the positionNameTranslations
        positionNameTranslations.setName(UPDATED_NAME);
        positionNameTranslations.setTransType(UPDATED_TRANS_TYPE);

        restPositionNameTranslationsMockMvc.perform(put("/api/positionNameTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(positionNameTranslations)))
                .andExpect(status().isOk());

        // Validate the PositionNameTranslations in the database
        List<PositionNameTranslations> positionNameTranslationss = positionNameTranslationsRepository.findAll();
        assertThat(positionNameTranslationss).hasSize(databaseSizeBeforeUpdate);
        PositionNameTranslations testPositionNameTranslations = positionNameTranslationss.get(positionNameTranslationss.size() - 1);
        assertThat(testPositionNameTranslations.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPositionNameTranslations.getTransType()).isEqualTo(UPDATED_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void deletePositionNameTranslations() throws Exception {
        // Initialize the database
        positionNameTranslationsRepository.saveAndFlush(positionNameTranslations);

		int databaseSizeBeforeDelete = positionNameTranslationsRepository.findAll().size();

        // Get the positionNameTranslations
        restPositionNameTranslationsMockMvc.perform(delete("/api/positionNameTranslationss/{id}", positionNameTranslations.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<PositionNameTranslations> positionNameTranslationss = positionNameTranslationsRepository.findAll();
        assertThat(positionNameTranslationss).hasSize(databaseSizeBeforeDelete - 1);
    }
}
