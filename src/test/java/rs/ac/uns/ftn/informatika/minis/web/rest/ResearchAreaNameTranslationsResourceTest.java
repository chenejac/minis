package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.ResearchAreaNameTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.ResearchAreaNameTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.ResearchAreaNameTranslationsSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ResearchAreaNameTranslationsResource REST controller.
 *
 * @see ResearchAreaNameTranslationsResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class ResearchAreaNameTranslationsResourceTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_TRANS_TYPE = "AAAAA";
    private static final String UPDATED_TRANS_TYPE = "BBBBB";

    @Inject
    private ResearchAreaNameTranslationsRepository researchAreaNameTranslationsRepository;

    @Inject
    private ResearchAreaNameTranslationsSearchRepository researchAreaNameTranslationsSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restResearchAreaNameTranslationsMockMvc;

    private ResearchAreaNameTranslations researchAreaNameTranslations;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ResearchAreaNameTranslationsResource researchAreaNameTranslationsResource = new ResearchAreaNameTranslationsResource();
        ReflectionTestUtils.setField(researchAreaNameTranslationsResource, "researchAreaNameTranslationsRepository", researchAreaNameTranslationsRepository);
        ReflectionTestUtils.setField(researchAreaNameTranslationsResource, "researchAreaNameTranslationsSearchRepository", researchAreaNameTranslationsSearchRepository);
        this.restResearchAreaNameTranslationsMockMvc = MockMvcBuilders.standaloneSetup(researchAreaNameTranslationsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        researchAreaNameTranslations = new ResearchAreaNameTranslations();
        researchAreaNameTranslations.setName(DEFAULT_NAME);
        researchAreaNameTranslations.setTransType(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void createResearchAreaNameTranslations() throws Exception {
        int databaseSizeBeforeCreate = researchAreaNameTranslationsRepository.findAll().size();

        // Create the ResearchAreaNameTranslations

        restResearchAreaNameTranslationsMockMvc.perform(post("/api/researchAreaNameTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(researchAreaNameTranslations)))
                .andExpect(status().isCreated());

        // Validate the ResearchAreaNameTranslations in the database
        List<ResearchAreaNameTranslations> researchAreaNameTranslationss = researchAreaNameTranslationsRepository.findAll();
        assertThat(researchAreaNameTranslationss).hasSize(databaseSizeBeforeCreate + 1);
        ResearchAreaNameTranslations testResearchAreaNameTranslations = researchAreaNameTranslationss.get(researchAreaNameTranslationss.size() - 1);
        assertThat(testResearchAreaNameTranslations.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testResearchAreaNameTranslations.getTransType()).isEqualTo(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void getAllResearchAreaNameTranslationss() throws Exception {
        // Initialize the database
        researchAreaNameTranslationsRepository.saveAndFlush(researchAreaNameTranslations);

        // Get all the researchAreaNameTranslationss
        restResearchAreaNameTranslationsMockMvc.perform(get("/api/researchAreaNameTranslationss"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(researchAreaNameTranslations.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].transType").value(hasItem(DEFAULT_TRANS_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getResearchAreaNameTranslations() throws Exception {
        // Initialize the database
        researchAreaNameTranslationsRepository.saveAndFlush(researchAreaNameTranslations);

        // Get the researchAreaNameTranslations
        restResearchAreaNameTranslationsMockMvc.perform(get("/api/researchAreaNameTranslationss/{id}", researchAreaNameTranslations.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(researchAreaNameTranslations.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.transType").value(DEFAULT_TRANS_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingResearchAreaNameTranslations() throws Exception {
        // Get the researchAreaNameTranslations
        restResearchAreaNameTranslationsMockMvc.perform(get("/api/researchAreaNameTranslationss/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateResearchAreaNameTranslations() throws Exception {
        // Initialize the database
        researchAreaNameTranslationsRepository.saveAndFlush(researchAreaNameTranslations);

		int databaseSizeBeforeUpdate = researchAreaNameTranslationsRepository.findAll().size();

        // Update the researchAreaNameTranslations
        researchAreaNameTranslations.setName(UPDATED_NAME);
        researchAreaNameTranslations.setTransType(UPDATED_TRANS_TYPE);

        restResearchAreaNameTranslationsMockMvc.perform(put("/api/researchAreaNameTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(researchAreaNameTranslations)))
                .andExpect(status().isOk());

        // Validate the ResearchAreaNameTranslations in the database
        List<ResearchAreaNameTranslations> researchAreaNameTranslationss = researchAreaNameTranslationsRepository.findAll();
        assertThat(researchAreaNameTranslationss).hasSize(databaseSizeBeforeUpdate);
        ResearchAreaNameTranslations testResearchAreaNameTranslations = researchAreaNameTranslationss.get(researchAreaNameTranslationss.size() - 1);
        assertThat(testResearchAreaNameTranslations.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testResearchAreaNameTranslations.getTransType()).isEqualTo(UPDATED_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void deleteResearchAreaNameTranslations() throws Exception {
        // Initialize the database
        researchAreaNameTranslationsRepository.saveAndFlush(researchAreaNameTranslations);

		int databaseSizeBeforeDelete = researchAreaNameTranslationsRepository.findAll().size();

        // Get the researchAreaNameTranslations
        restResearchAreaNameTranslationsMockMvc.perform(delete("/api/researchAreaNameTranslationss/{id}", researchAreaNameTranslations.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ResearchAreaNameTranslations> researchAreaNameTranslationss = researchAreaNameTranslationsRepository.findAll();
        assertThat(researchAreaNameTranslationss).hasSize(databaseSizeBeforeDelete - 1);
    }
}
