package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.PersonFile;
import rs.ac.uns.ftn.informatika.minis.repository.PersonFileRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PersonFileSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the PersonFileResource REST controller.
 *
 * @see PersonFileResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class PersonFileResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

    private static final String DEFAULT_FILE_NAME = "AAAAA";
    private static final String UPDATED_FILE_NAME = "BBBBB";
    private static final String DEFAULT_FILE_NAME_CLIENT = "AAAAA";
    private static final String UPDATED_FILE_NAME_CLIENT = "BBBBB";
    private static final String DEFAULT_MIME = "AAAAA";
    private static final String UPDATED_MIME = "BBBBB";

    private static final Long DEFAULT_LENGTH = 1L;
    private static final Long UPDATED_LENGTH = 2L;
    private static final String DEFAULT_UPLOADER = "AAAAA";
    private static final String UPDATED_UPLOADER = "BBBBB";

    private static final DateTime DEFAULT_DATE_MODIFIED = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_DATE_MODIFIED = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_DATE_MODIFIED_STR = dateTimeFormatter.print(DEFAULT_DATE_MODIFIED);
    private static final String DEFAULT_LICENSE = "AAAAA";
    private static final String UPDATED_LICENSE = "BBBBB";
    private static final String DEFAULT_TYPE = "AAAAA";
    private static final String UPDATED_TYPE = "BBBBB";
    private static final String DEFAULT_NOTE = "AAAAA";
    private static final String UPDATED_NOTE = "BBBBB";
    private static final String DEFAULT_STATUS = "AAAAA";
    private static final String UPDATED_STATUS = "BBBBB";

    @Inject
    private PersonFileRepository personFileRepository;

    @Inject
    private PersonFileSearchRepository personFileSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPersonFileMockMvc;

    private PersonFile personFile;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PersonFileResource personFileResource = new PersonFileResource();
        ReflectionTestUtils.setField(personFileResource, "personFileRepository", personFileRepository);
        ReflectionTestUtils.setField(personFileResource, "personFileSearchRepository", personFileSearchRepository);
        this.restPersonFileMockMvc = MockMvcBuilders.standaloneSetup(personFileResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        personFile = new PersonFile();
        personFile.setFileName(DEFAULT_FILE_NAME);
        personFile.setFileNameClient(DEFAULT_FILE_NAME_CLIENT);
        personFile.setMime(DEFAULT_MIME);
        personFile.setLength(DEFAULT_LENGTH);
        personFile.setUploader(DEFAULT_UPLOADER);
        personFile.setDateModified(DEFAULT_DATE_MODIFIED);
        personFile.setLicense(DEFAULT_LICENSE);
        personFile.setType(DEFAULT_TYPE);
        personFile.setNote(DEFAULT_NOTE);
        personFile.setStatus(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createPersonFile() throws Exception {
        int databaseSizeBeforeCreate = personFileRepository.findAll().size();

        // Create the PersonFile

        restPersonFileMockMvc.perform(post("/api/personFiles")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(personFile)))
                .andExpect(status().isCreated());

        // Validate the PersonFile in the database
        List<PersonFile> personFiles = personFileRepository.findAll();
        assertThat(personFiles).hasSize(databaseSizeBeforeCreate + 1);
        PersonFile testPersonFile = personFiles.get(personFiles.size() - 1);
        assertThat(testPersonFile.getFileName()).isEqualTo(DEFAULT_FILE_NAME);
        assertThat(testPersonFile.getFileNameClient()).isEqualTo(DEFAULT_FILE_NAME_CLIENT);
        assertThat(testPersonFile.getMime()).isEqualTo(DEFAULT_MIME);
        assertThat(testPersonFile.getLength()).isEqualTo(DEFAULT_LENGTH);
        assertThat(testPersonFile.getUploader()).isEqualTo(DEFAULT_UPLOADER);
        assertThat(testPersonFile.getDateModified().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_DATE_MODIFIED);
        assertThat(testPersonFile.getLicense()).isEqualTo(DEFAULT_LICENSE);
        assertThat(testPersonFile.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testPersonFile.getNote()).isEqualTo(DEFAULT_NOTE);
        assertThat(testPersonFile.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void getAllPersonFiles() throws Exception {
        // Initialize the database
        personFileRepository.saveAndFlush(personFile);

        // Get all the personFiles
        restPersonFileMockMvc.perform(get("/api/personFiles"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(personFile.getId().intValue())))
                .andExpect(jsonPath("$.[*].fileName").value(hasItem(DEFAULT_FILE_NAME.toString())))
                .andExpect(jsonPath("$.[*].fileNameClient").value(hasItem(DEFAULT_FILE_NAME_CLIENT.toString())))
                .andExpect(jsonPath("$.[*].mime").value(hasItem(DEFAULT_MIME.toString())))
                .andExpect(jsonPath("$.[*].length").value(hasItem(DEFAULT_LENGTH.intValue())))
                .andExpect(jsonPath("$.[*].uploader").value(hasItem(DEFAULT_UPLOADER.toString())))
                .andExpect(jsonPath("$.[*].dateModified").value(hasItem(DEFAULT_DATE_MODIFIED_STR)))
                .andExpect(jsonPath("$.[*].license").value(hasItem(DEFAULT_LICENSE.toString())))
                .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
                .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void getPersonFile() throws Exception {
        // Initialize the database
        personFileRepository.saveAndFlush(personFile);

        // Get the personFile
        restPersonFileMockMvc.perform(get("/api/personFiles/{id}", personFile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(personFile.getId().intValue()))
            .andExpect(jsonPath("$.fileName").value(DEFAULT_FILE_NAME.toString()))
            .andExpect(jsonPath("$.fileNameClient").value(DEFAULT_FILE_NAME_CLIENT.toString()))
            .andExpect(jsonPath("$.mime").value(DEFAULT_MIME.toString()))
            .andExpect(jsonPath("$.length").value(DEFAULT_LENGTH.intValue()))
            .andExpect(jsonPath("$.uploader").value(DEFAULT_UPLOADER.toString()))
            .andExpect(jsonPath("$.dateModified").value(DEFAULT_DATE_MODIFIED_STR))
            .andExpect(jsonPath("$.license").value(DEFAULT_LICENSE.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPersonFile() throws Exception {
        // Get the personFile
        restPersonFileMockMvc.perform(get("/api/personFiles/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePersonFile() throws Exception {
        // Initialize the database
        personFileRepository.saveAndFlush(personFile);

		int databaseSizeBeforeUpdate = personFileRepository.findAll().size();

        // Update the personFile
        personFile.setFileName(UPDATED_FILE_NAME);
        personFile.setFileNameClient(UPDATED_FILE_NAME_CLIENT);
        personFile.setMime(UPDATED_MIME);
        personFile.setLength(UPDATED_LENGTH);
        personFile.setUploader(UPDATED_UPLOADER);
        personFile.setDateModified(UPDATED_DATE_MODIFIED);
        personFile.setLicense(UPDATED_LICENSE);
        personFile.setType(UPDATED_TYPE);
        personFile.setNote(UPDATED_NOTE);
        personFile.setStatus(UPDATED_STATUS);

        restPersonFileMockMvc.perform(put("/api/personFiles")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(personFile)))
                .andExpect(status().isOk());

        // Validate the PersonFile in the database
        List<PersonFile> personFiles = personFileRepository.findAll();
        assertThat(personFiles).hasSize(databaseSizeBeforeUpdate);
        PersonFile testPersonFile = personFiles.get(personFiles.size() - 1);
        assertThat(testPersonFile.getFileName()).isEqualTo(UPDATED_FILE_NAME);
        assertThat(testPersonFile.getFileNameClient()).isEqualTo(UPDATED_FILE_NAME_CLIENT);
        assertThat(testPersonFile.getMime()).isEqualTo(UPDATED_MIME);
        assertThat(testPersonFile.getLength()).isEqualTo(UPDATED_LENGTH);
        assertThat(testPersonFile.getUploader()).isEqualTo(UPDATED_UPLOADER);
        assertThat(testPersonFile.getDateModified().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_DATE_MODIFIED);
        assertThat(testPersonFile.getLicense()).isEqualTo(UPDATED_LICENSE);
        assertThat(testPersonFile.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testPersonFile.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testPersonFile.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void deletePersonFile() throws Exception {
        // Initialize the database
        personFileRepository.saveAndFlush(personFile);

		int databaseSizeBeforeDelete = personFileRepository.findAll().size();

        // Get the personFile
        restPersonFileMockMvc.perform(delete("/api/personFiles/{id}", personFile.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<PersonFile> personFiles = personFileRepository.findAll();
        assertThat(personFiles).hasSize(databaseSizeBeforeDelete - 1);
    }
}
