package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.Institution;
import rs.ac.uns.ftn.informatika.minis.repository.InstitutionRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.InstitutionSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.joda.time.LocalDate;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the InstitutionResource REST controller.
 *
 * @see InstitutionResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class InstitutionResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_ADDRESS = "AAAAA";
    private static final String UPDATED_ADDRESS = "BBBBB";
    private static final String DEFAULT_PLACE = "AAAAA";
    private static final String UPDATED_PLACE = "BBBBB";

    private static final Integer DEFAULT_POSTAL_CODE = 1;
    private static final Integer UPDATED_POSTAL_CODE = 2;
    private static final String DEFAULT_ACRO = "AAAAA";
    private static final String UPDATED_ACRO = "BBBBB";
    private static final String DEFAULT_KEYWORDS = "AAAAA";
    private static final String UPDATED_KEYWORDS = "BBBBB";
    private static final String DEFAULT_URI = "AAAAA";
    private static final String UPDATED_URI = "BBBBB";
    private static final String DEFAULT_EMAIL = "AAAAA";
    private static final String UPDATED_EMAIL = "BBBBB";
    private static final String DEFAULT_PHONE = "AAAAA";
    private static final String UPDATED_PHONE = "BBBBB";
    private static final String DEFAULT_PIB = "AAAAA";
    private static final String UPDATED_PIB = "BBBBB";
    private static final String DEFAULT_MATICNI_BROJ = "AAAAA";
    private static final String UPDATED_MATICNI_BROJ = "BBBBB";
    private static final String DEFAULT_ACCOUNT = "AAAAA";
    private static final String UPDATED_ACCOUNT = "BBBBB";

    private static final Integer DEFAULT_ECRISID = 1;
    private static final Integer UPDATED_ECRISID = 2;

    private static final Integer DEFAULT_NIO_ID = 1;
    private static final Integer UPDATED_NIO_ID = 2;
    private static final String DEFAULT_MNTR_ID = "AAAAA";
    private static final String UPDATED_MNTR_ID = "BBBBB";
    private static final String DEFAULT_ORCID = "AAAAA";
    private static final String UPDATED_ORCID = "BBBBB";
    private static final String DEFAULT_RESCRIPT_NUMBER = "AAAAA";
    private static final String UPDATED_RESCRIPT_NUMBER = "BBBBB";

    private static final LocalDate DEFAULT_DATE = new LocalDate(0L);
    private static final LocalDate UPDATED_DATE = new LocalDate();
    private static final String DEFAULT_FOUNDER = "AAAAA";
    private static final String UPDATED_FOUNDER = "BBBBB";
    private static final String DEFAULT_NOTE = "AAAAA";
    private static final String UPDATED_NOTE = "BBBBB";
    private static final String DEFAULT_ACCREDITATION_NUMBER = "AAAAA";
    private static final String UPDATED_ACCREDITATION_NUMBER = "BBBBB";

    private static final LocalDate DEFAULT_ACCREDITATION_DATE = new LocalDate(0L);
    private static final LocalDate UPDATED_ACCREDITATION_DATE = new LocalDate();
    private static final String DEFAULT_ACCREDITATION_NOTE = "AAAAA";
    private static final String UPDATED_ACCREDITATION_NOTE = "BBBBB";
    private static final String DEFAULT_CREATOR = "AAAAA";
    private static final String UPDATED_CREATOR = "BBBBB";
    private static final String DEFAULT_MODIFIER = "AAAAA";
    private static final String UPDATED_MODIFIER = "BBBBB";

    private static final DateTime DEFAULT_CREATION_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_CREATION_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_CREATION_DATE_STR = dateTimeFormatter.print(DEFAULT_CREATION_DATE);

    private static final DateTime DEFAULT_LAST_MODIFICATION_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_LAST_MODIFICATION_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_LAST_MODIFICATION_DATE_STR = dateTimeFormatter.print(DEFAULT_LAST_MODIFICATION_DATE);

    private static final Integer DEFAULT_RECORD_STATUS = 1;
    private static final Integer UPDATED_RECORD_STATUS = 2;

    @Inject
    private InstitutionRepository institutionRepository;

    @Inject
    private InstitutionSearchRepository institutionSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restInstitutionMockMvc;

    private Institution institution;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        InstitutionResource institutionResource = new InstitutionResource();
        ReflectionTestUtils.setField(institutionResource, "institutionRepository", institutionRepository);
        ReflectionTestUtils.setField(institutionResource, "institutionSearchRepository", institutionSearchRepository);
        this.restInstitutionMockMvc = MockMvcBuilders.standaloneSetup(institutionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        institution = new Institution();
        institution.setName(DEFAULT_NAME);
        institution.setAddress(DEFAULT_ADDRESS);
        institution.setPlace(DEFAULT_PLACE);
        institution.setPostalCode(DEFAULT_POSTAL_CODE);
        institution.setAcro(DEFAULT_ACRO);
        institution.setKeywords(DEFAULT_KEYWORDS);
        institution.setUri(DEFAULT_URI);
        institution.setEmail(DEFAULT_EMAIL);
        institution.setPhone(DEFAULT_PHONE);
        institution.setPib(DEFAULT_PIB);
        institution.setMaticniBroj(DEFAULT_MATICNI_BROJ);
        institution.setAccount(DEFAULT_ACCOUNT);
        institution.setEcrisid(DEFAULT_ECRISID);
        institution.setNioID(DEFAULT_NIO_ID);
        institution.setMntrID(DEFAULT_MNTR_ID);
        institution.setOrcid(DEFAULT_ORCID);
        institution.setRescriptNumber(DEFAULT_RESCRIPT_NUMBER);
        institution.setDate(DEFAULT_DATE);
        institution.setFounder(DEFAULT_FOUNDER);
        institution.setNote(DEFAULT_NOTE);
        institution.setAccreditationNumber(DEFAULT_ACCREDITATION_NUMBER);
        institution.setAccreditationDate(DEFAULT_ACCREDITATION_DATE);
        institution.setAccreditationNote(DEFAULT_ACCREDITATION_NOTE);
        institution.setCreator(DEFAULT_CREATOR);
        institution.setModifier(DEFAULT_MODIFIER);
        institution.setCreationDate(DEFAULT_CREATION_DATE);
        institution.setLastModificationDate(DEFAULT_LAST_MODIFICATION_DATE);
        institution.setRecordStatus(DEFAULT_RECORD_STATUS);
    }

    @Test
    @Transactional
    public void createInstitution() throws Exception {
        int databaseSizeBeforeCreate = institutionRepository.findAll().size();

        // Create the Institution

        restInstitutionMockMvc.perform(post("/api/institutions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(institution)))
                .andExpect(status().isCreated());

        // Validate the Institution in the database
        List<Institution> institutions = institutionRepository.findAll();
        assertThat(institutions).hasSize(databaseSizeBeforeCreate + 1);
        Institution testInstitution = institutions.get(institutions.size() - 1);
        assertThat(testInstitution.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testInstitution.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testInstitution.getPlace()).isEqualTo(DEFAULT_PLACE);
        assertThat(testInstitution.getPostalCode()).isEqualTo(DEFAULT_POSTAL_CODE);
        assertThat(testInstitution.getAcro()).isEqualTo(DEFAULT_ACRO);
        assertThat(testInstitution.getKeywords()).isEqualTo(DEFAULT_KEYWORDS);
        assertThat(testInstitution.getUri()).isEqualTo(DEFAULT_URI);
        assertThat(testInstitution.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testInstitution.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testInstitution.getPib()).isEqualTo(DEFAULT_PIB);
        assertThat(testInstitution.getMaticniBroj()).isEqualTo(DEFAULT_MATICNI_BROJ);
        assertThat(testInstitution.getAccount()).isEqualTo(DEFAULT_ACCOUNT);
        assertThat(testInstitution.getEcrisid()).isEqualTo(DEFAULT_ECRISID);
        assertThat(testInstitution.getNioID()).isEqualTo(DEFAULT_NIO_ID);
        assertThat(testInstitution.getMntrID()).isEqualTo(DEFAULT_MNTR_ID);
        assertThat(testInstitution.getOrcid()).isEqualTo(DEFAULT_ORCID);
        assertThat(testInstitution.getRescriptNumber()).isEqualTo(DEFAULT_RESCRIPT_NUMBER);
        assertThat(testInstitution.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testInstitution.getFounder()).isEqualTo(DEFAULT_FOUNDER);
        assertThat(testInstitution.getNote()).isEqualTo(DEFAULT_NOTE);
        assertThat(testInstitution.getAccreditationNumber()).isEqualTo(DEFAULT_ACCREDITATION_NUMBER);
        assertThat(testInstitution.getAccreditationDate()).isEqualTo(DEFAULT_ACCREDITATION_DATE);
        assertThat(testInstitution.getAccreditationNote()).isEqualTo(DEFAULT_ACCREDITATION_NOTE);
        assertThat(testInstitution.getCreator()).isEqualTo(DEFAULT_CREATOR);
        assertThat(testInstitution.getModifier()).isEqualTo(DEFAULT_MODIFIER);
        assertThat(testInstitution.getCreationDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testInstitution.getLastModificationDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_LAST_MODIFICATION_DATE);
        assertThat(testInstitution.getRecordStatus()).isEqualTo(DEFAULT_RECORD_STATUS);
    }

    @Test
    @Transactional
    public void getAllInstitutions() throws Exception {
        // Initialize the database
        institutionRepository.saveAndFlush(institution);

        // Get all the institutions
        restInstitutionMockMvc.perform(get("/api/institutions"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(institution.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
                .andExpect(jsonPath("$.[*].place").value(hasItem(DEFAULT_PLACE.toString())))
                .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE)))
                .andExpect(jsonPath("$.[*].acro").value(hasItem(DEFAULT_ACRO.toString())))
                .andExpect(jsonPath("$.[*].keywords").value(hasItem(DEFAULT_KEYWORDS.toString())))
                .andExpect(jsonPath("$.[*].uri").value(hasItem(DEFAULT_URI.toString())))
                .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
                .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())))
                .andExpect(jsonPath("$.[*].pib").value(hasItem(DEFAULT_PIB.toString())))
                .andExpect(jsonPath("$.[*].maticniBroj").value(hasItem(DEFAULT_MATICNI_BROJ.toString())))
                .andExpect(jsonPath("$.[*].account").value(hasItem(DEFAULT_ACCOUNT.toString())))
                .andExpect(jsonPath("$.[*].ecrisid").value(hasItem(DEFAULT_ECRISID)))
                .andExpect(jsonPath("$.[*].nioID").value(hasItem(DEFAULT_NIO_ID)))
                .andExpect(jsonPath("$.[*].mntrID").value(hasItem(DEFAULT_MNTR_ID.toString())))
                .andExpect(jsonPath("$.[*].orcid").value(hasItem(DEFAULT_ORCID.toString())))
                .andExpect(jsonPath("$.[*].rescriptNumber").value(hasItem(DEFAULT_RESCRIPT_NUMBER.toString())))
                .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
                .andExpect(jsonPath("$.[*].founder").value(hasItem(DEFAULT_FOUNDER.toString())))
                .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
                .andExpect(jsonPath("$.[*].accreditationNumber").value(hasItem(DEFAULT_ACCREDITATION_NUMBER.toString())))
                .andExpect(jsonPath("$.[*].accreditationDate").value(hasItem(DEFAULT_ACCREDITATION_DATE.toString())))
                .andExpect(jsonPath("$.[*].accreditationNote").value(hasItem(DEFAULT_ACCREDITATION_NOTE.toString())))
                .andExpect(jsonPath("$.[*].creator").value(hasItem(DEFAULT_CREATOR.toString())))
                .andExpect(jsonPath("$.[*].modifier").value(hasItem(DEFAULT_MODIFIER.toString())))
                .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE_STR)))
                .andExpect(jsonPath("$.[*].lastModificationDate").value(hasItem(DEFAULT_LAST_MODIFICATION_DATE_STR)))
                .andExpect(jsonPath("$.[*].recordStatus").value(hasItem(DEFAULT_RECORD_STATUS)));
    }

    @Test
    @Transactional
    public void getInstitution() throws Exception {
        // Initialize the database
        institutionRepository.saveAndFlush(institution);

        // Get the institution
        restInstitutionMockMvc.perform(get("/api/institutions/{id}", institution.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(institution.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.place").value(DEFAULT_PLACE.toString()))
            .andExpect(jsonPath("$.postalCode").value(DEFAULT_POSTAL_CODE))
            .andExpect(jsonPath("$.acro").value(DEFAULT_ACRO.toString()))
            .andExpect(jsonPath("$.keywords").value(DEFAULT_KEYWORDS.toString()))
            .andExpect(jsonPath("$.uri").value(DEFAULT_URI.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE.toString()))
            .andExpect(jsonPath("$.pib").value(DEFAULT_PIB.toString()))
            .andExpect(jsonPath("$.maticniBroj").value(DEFAULT_MATICNI_BROJ.toString()))
            .andExpect(jsonPath("$.account").value(DEFAULT_ACCOUNT.toString()))
            .andExpect(jsonPath("$.ecrisid").value(DEFAULT_ECRISID))
            .andExpect(jsonPath("$.nioID").value(DEFAULT_NIO_ID))
            .andExpect(jsonPath("$.mntrID").value(DEFAULT_MNTR_ID.toString()))
            .andExpect(jsonPath("$.orcid").value(DEFAULT_ORCID.toString()))
            .andExpect(jsonPath("$.rescriptNumber").value(DEFAULT_RESCRIPT_NUMBER.toString()))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
            .andExpect(jsonPath("$.founder").value(DEFAULT_FOUNDER.toString()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()))
            .andExpect(jsonPath("$.accreditationNumber").value(DEFAULT_ACCREDITATION_NUMBER.toString()))
            .andExpect(jsonPath("$.accreditationDate").value(DEFAULT_ACCREDITATION_DATE.toString()))
            .andExpect(jsonPath("$.accreditationNote").value(DEFAULT_ACCREDITATION_NOTE.toString()))
            .andExpect(jsonPath("$.creator").value(DEFAULT_CREATOR.toString()))
            .andExpect(jsonPath("$.modifier").value(DEFAULT_MODIFIER.toString()))
            .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE_STR))
            .andExpect(jsonPath("$.lastModificationDate").value(DEFAULT_LAST_MODIFICATION_DATE_STR))
            .andExpect(jsonPath("$.recordStatus").value(DEFAULT_RECORD_STATUS));
    }

    @Test
    @Transactional
    public void getNonExistingInstitution() throws Exception {
        // Get the institution
        restInstitutionMockMvc.perform(get("/api/institutions/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInstitution() throws Exception {
        // Initialize the database
        institutionRepository.saveAndFlush(institution);

		int databaseSizeBeforeUpdate = institutionRepository.findAll().size();

        // Update the institution
        institution.setName(UPDATED_NAME);
        institution.setAddress(UPDATED_ADDRESS);
        institution.setPlace(UPDATED_PLACE);
        institution.setPostalCode(UPDATED_POSTAL_CODE);
        institution.setAcro(UPDATED_ACRO);
        institution.setKeywords(UPDATED_KEYWORDS);
        institution.setUri(UPDATED_URI);
        institution.setEmail(UPDATED_EMAIL);
        institution.setPhone(UPDATED_PHONE);
        institution.setPib(UPDATED_PIB);
        institution.setMaticniBroj(UPDATED_MATICNI_BROJ);
        institution.setAccount(UPDATED_ACCOUNT);
        institution.setEcrisid(UPDATED_ECRISID);
        institution.setNioID(UPDATED_NIO_ID);
        institution.setMntrID(UPDATED_MNTR_ID);
        institution.setOrcid(UPDATED_ORCID);
        institution.setRescriptNumber(UPDATED_RESCRIPT_NUMBER);
        institution.setDate(UPDATED_DATE);
        institution.setFounder(UPDATED_FOUNDER);
        institution.setNote(UPDATED_NOTE);
        institution.setAccreditationNumber(UPDATED_ACCREDITATION_NUMBER);
        institution.setAccreditationDate(UPDATED_ACCREDITATION_DATE);
        institution.setAccreditationNote(UPDATED_ACCREDITATION_NOTE);
        institution.setCreator(UPDATED_CREATOR);
        institution.setModifier(UPDATED_MODIFIER);
        institution.setCreationDate(UPDATED_CREATION_DATE);
        institution.setLastModificationDate(UPDATED_LAST_MODIFICATION_DATE);
        institution.setRecordStatus(UPDATED_RECORD_STATUS);

        restInstitutionMockMvc.perform(put("/api/institutions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(institution)))
                .andExpect(status().isOk());

        // Validate the Institution in the database
        List<Institution> institutions = institutionRepository.findAll();
        assertThat(institutions).hasSize(databaseSizeBeforeUpdate);
        Institution testInstitution = institutions.get(institutions.size() - 1);
        assertThat(testInstitution.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testInstitution.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testInstitution.getPlace()).isEqualTo(UPDATED_PLACE);
        assertThat(testInstitution.getPostalCode()).isEqualTo(UPDATED_POSTAL_CODE);
        assertThat(testInstitution.getAcro()).isEqualTo(UPDATED_ACRO);
        assertThat(testInstitution.getKeywords()).isEqualTo(UPDATED_KEYWORDS);
        assertThat(testInstitution.getUri()).isEqualTo(UPDATED_URI);
        assertThat(testInstitution.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testInstitution.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testInstitution.getPib()).isEqualTo(UPDATED_PIB);
        assertThat(testInstitution.getMaticniBroj()).isEqualTo(UPDATED_MATICNI_BROJ);
        assertThat(testInstitution.getAccount()).isEqualTo(UPDATED_ACCOUNT);
        assertThat(testInstitution.getEcrisid()).isEqualTo(UPDATED_ECRISID);
        assertThat(testInstitution.getNioID()).isEqualTo(UPDATED_NIO_ID);
        assertThat(testInstitution.getMntrID()).isEqualTo(UPDATED_MNTR_ID);
        assertThat(testInstitution.getOrcid()).isEqualTo(UPDATED_ORCID);
        assertThat(testInstitution.getRescriptNumber()).isEqualTo(UPDATED_RESCRIPT_NUMBER);
        assertThat(testInstitution.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testInstitution.getFounder()).isEqualTo(UPDATED_FOUNDER);
        assertThat(testInstitution.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testInstitution.getAccreditationNumber()).isEqualTo(UPDATED_ACCREDITATION_NUMBER);
        assertThat(testInstitution.getAccreditationDate()).isEqualTo(UPDATED_ACCREDITATION_DATE);
        assertThat(testInstitution.getAccreditationNote()).isEqualTo(UPDATED_ACCREDITATION_NOTE);
        assertThat(testInstitution.getCreator()).isEqualTo(UPDATED_CREATOR);
        assertThat(testInstitution.getModifier()).isEqualTo(UPDATED_MODIFIER);
        assertThat(testInstitution.getCreationDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testInstitution.getLastModificationDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_LAST_MODIFICATION_DATE);
        assertThat(testInstitution.getRecordStatus()).isEqualTo(UPDATED_RECORD_STATUS);
    }

    @Test
    @Transactional
    public void deleteInstitution() throws Exception {
        // Initialize the database
        institutionRepository.saveAndFlush(institution);

		int databaseSizeBeforeDelete = institutionRepository.findAll().size();

        // Get the institution
        restInstitutionMockMvc.perform(delete("/api/institutions/{id}", institution.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Institution> institutions = institutionRepository.findAll();
        assertThat(institutions).hasSize(databaseSizeBeforeDelete - 1);
    }
}
