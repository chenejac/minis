package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.Scheme;
import rs.ac.uns.ftn.informatika.minis.repository.SchemeRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.SchemeSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the SchemeResource REST controller.
 *
 * @see SchemeResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class SchemeResourceTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    @Inject
    private SchemeRepository schemeRepository;

    @Inject
    private SchemeSearchRepository schemeSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restSchemeMockMvc;

    private Scheme scheme;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SchemeResource schemeResource = new SchemeResource();
        ReflectionTestUtils.setField(schemeResource, "schemeRepository", schemeRepository);
        ReflectionTestUtils.setField(schemeResource, "schemeSearchRepository", schemeSearchRepository);
        this.restSchemeMockMvc = MockMvcBuilders.standaloneSetup(schemeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        scheme = new Scheme();
        scheme.setName(DEFAULT_NAME);
        scheme.setDescription(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createScheme() throws Exception {
        int databaseSizeBeforeCreate = schemeRepository.findAll().size();

        // Create the Scheme

        restSchemeMockMvc.perform(post("/api/schemes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(scheme)))
                .andExpect(status().isCreated());

        // Validate the Scheme in the database
        List<Scheme> schemes = schemeRepository.findAll();
        assertThat(schemes).hasSize(databaseSizeBeforeCreate + 1);
        Scheme testScheme = schemes.get(schemes.size() - 1);
        assertThat(testScheme.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testScheme.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllSchemes() throws Exception {
        // Initialize the database
        schemeRepository.saveAndFlush(scheme);

        // Get all the schemes
        restSchemeMockMvc.perform(get("/api/schemes"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(scheme.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getScheme() throws Exception {
        // Initialize the database
        schemeRepository.saveAndFlush(scheme);

        // Get the scheme
        restSchemeMockMvc.perform(get("/api/schemes/{id}", scheme.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(scheme.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingScheme() throws Exception {
        // Get the scheme
        restSchemeMockMvc.perform(get("/api/schemes/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateScheme() throws Exception {
        // Initialize the database
        schemeRepository.saveAndFlush(scheme);

		int databaseSizeBeforeUpdate = schemeRepository.findAll().size();

        // Update the scheme
        scheme.setName(UPDATED_NAME);
        scheme.setDescription(UPDATED_DESCRIPTION);

        restSchemeMockMvc.perform(put("/api/schemes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(scheme)))
                .andExpect(status().isOk());

        // Validate the Scheme in the database
        List<Scheme> schemes = schemeRepository.findAll();
        assertThat(schemes).hasSize(databaseSizeBeforeUpdate);
        Scheme testScheme = schemes.get(schemes.size() - 1);
        assertThat(testScheme.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testScheme.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void deleteScheme() throws Exception {
        // Initialize the database
        schemeRepository.saveAndFlush(scheme);

		int databaseSizeBeforeDelete = schemeRepository.findAll().size();

        // Get the scheme
        restSchemeMockMvc.perform(delete("/api/schemes/{id}", scheme.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Scheme> schemes = schemeRepository.findAll();
        assertThat(schemes).hasSize(databaseSizeBeforeDelete - 1);
    }
}
