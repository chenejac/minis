package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.PersonStatus;
import rs.ac.uns.ftn.informatika.minis.repository.PersonStatusRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PersonStatusSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the PersonStatusResource REST controller.
 *
 * @see PersonStatusResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class PersonStatusResourceTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    @Inject
    private PersonStatusRepository personStatusRepository;

    @Inject
    private PersonStatusSearchRepository personStatusSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPersonStatusMockMvc;

    private PersonStatus personStatus;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PersonStatusResource personStatusResource = new PersonStatusResource();
        ReflectionTestUtils.setField(personStatusResource, "personStatusRepository", personStatusRepository);
        ReflectionTestUtils.setField(personStatusResource, "personStatusSearchRepository", personStatusSearchRepository);
        this.restPersonStatusMockMvc = MockMvcBuilders.standaloneSetup(personStatusResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        personStatus = new PersonStatus();
        personStatus.setName(DEFAULT_NAME);
        personStatus.setDescription(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createPersonStatus() throws Exception {
        int databaseSizeBeforeCreate = personStatusRepository.findAll().size();

        // Create the PersonStatus

        restPersonStatusMockMvc.perform(post("/api/personStatuss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(personStatus)))
                .andExpect(status().isCreated());

        // Validate the PersonStatus in the database
        List<PersonStatus> personStatuss = personStatusRepository.findAll();
        assertThat(personStatuss).hasSize(databaseSizeBeforeCreate + 1);
        PersonStatus testPersonStatus = personStatuss.get(personStatuss.size() - 1);
        assertThat(testPersonStatus.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPersonStatus.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllPersonStatuss() throws Exception {
        // Initialize the database
        personStatusRepository.saveAndFlush(personStatus);

        // Get all the personStatuss
        restPersonStatusMockMvc.perform(get("/api/personStatuss"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(personStatus.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getPersonStatus() throws Exception {
        // Initialize the database
        personStatusRepository.saveAndFlush(personStatus);

        // Get the personStatus
        restPersonStatusMockMvc.perform(get("/api/personStatuss/{id}", personStatus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(personStatus.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPersonStatus() throws Exception {
        // Get the personStatus
        restPersonStatusMockMvc.perform(get("/api/personStatuss/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePersonStatus() throws Exception {
        // Initialize the database
        personStatusRepository.saveAndFlush(personStatus);

		int databaseSizeBeforeUpdate = personStatusRepository.findAll().size();

        // Update the personStatus
        personStatus.setName(UPDATED_NAME);
        personStatus.setDescription(UPDATED_DESCRIPTION);

        restPersonStatusMockMvc.perform(put("/api/personStatuss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(personStatus)))
                .andExpect(status().isOk());

        // Validate the PersonStatus in the database
        List<PersonStatus> personStatuss = personStatusRepository.findAll();
        assertThat(personStatuss).hasSize(databaseSizeBeforeUpdate);
        PersonStatus testPersonStatus = personStatuss.get(personStatuss.size() - 1);
        assertThat(testPersonStatus.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPersonStatus.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void deletePersonStatus() throws Exception {
        // Initialize the database
        personStatusRepository.saveAndFlush(personStatus);

		int databaseSizeBeforeDelete = personStatusRepository.findAll().size();

        // Get the personStatus
        restPersonStatusMockMvc.perform(delete("/api/personStatuss/{id}", personStatus.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<PersonStatus> personStatuss = personStatusRepository.findAll();
        assertThat(personStatuss).hasSize(databaseSizeBeforeDelete - 1);
    }
}
