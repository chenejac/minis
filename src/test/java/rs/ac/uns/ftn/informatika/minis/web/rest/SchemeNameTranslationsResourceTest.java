package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.SchemeNameTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.SchemeNameTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.SchemeNameTranslationsSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the SchemeNameTranslationsResource REST controller.
 *
 * @see SchemeNameTranslationsResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class SchemeNameTranslationsResourceTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_TRANS_TYPE = "AAAAA";
    private static final String UPDATED_TRANS_TYPE = "BBBBB";

    @Inject
    private SchemeNameTranslationsRepository schemeNameTranslationsRepository;

    @Inject
    private SchemeNameTranslationsSearchRepository schemeNameTranslationsSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restSchemeNameTranslationsMockMvc;

    private SchemeNameTranslations schemeNameTranslations;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SchemeNameTranslationsResource schemeNameTranslationsResource = new SchemeNameTranslationsResource();
        ReflectionTestUtils.setField(schemeNameTranslationsResource, "schemeNameTranslationsRepository", schemeNameTranslationsRepository);
        ReflectionTestUtils.setField(schemeNameTranslationsResource, "schemeNameTranslationsSearchRepository", schemeNameTranslationsSearchRepository);
        this.restSchemeNameTranslationsMockMvc = MockMvcBuilders.standaloneSetup(schemeNameTranslationsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        schemeNameTranslations = new SchemeNameTranslations();
        schemeNameTranslations.setName(DEFAULT_NAME);
        schemeNameTranslations.setTransType(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void createSchemeNameTranslations() throws Exception {
        int databaseSizeBeforeCreate = schemeNameTranslationsRepository.findAll().size();

        // Create the SchemeNameTranslations

        restSchemeNameTranslationsMockMvc.perform(post("/api/schemeNameTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(schemeNameTranslations)))
                .andExpect(status().isCreated());

        // Validate the SchemeNameTranslations in the database
        List<SchemeNameTranslations> schemeNameTranslationss = schemeNameTranslationsRepository.findAll();
        assertThat(schemeNameTranslationss).hasSize(databaseSizeBeforeCreate + 1);
        SchemeNameTranslations testSchemeNameTranslations = schemeNameTranslationss.get(schemeNameTranslationss.size() - 1);
        assertThat(testSchemeNameTranslations.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testSchemeNameTranslations.getTransType()).isEqualTo(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void getAllSchemeNameTranslationss() throws Exception {
        // Initialize the database
        schemeNameTranslationsRepository.saveAndFlush(schemeNameTranslations);

        // Get all the schemeNameTranslationss
        restSchemeNameTranslationsMockMvc.perform(get("/api/schemeNameTranslationss"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(schemeNameTranslations.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].transType").value(hasItem(DEFAULT_TRANS_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getSchemeNameTranslations() throws Exception {
        // Initialize the database
        schemeNameTranslationsRepository.saveAndFlush(schemeNameTranslations);

        // Get the schemeNameTranslations
        restSchemeNameTranslationsMockMvc.perform(get("/api/schemeNameTranslationss/{id}", schemeNameTranslations.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(schemeNameTranslations.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.transType").value(DEFAULT_TRANS_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSchemeNameTranslations() throws Exception {
        // Get the schemeNameTranslations
        restSchemeNameTranslationsMockMvc.perform(get("/api/schemeNameTranslationss/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSchemeNameTranslations() throws Exception {
        // Initialize the database
        schemeNameTranslationsRepository.saveAndFlush(schemeNameTranslations);

		int databaseSizeBeforeUpdate = schemeNameTranslationsRepository.findAll().size();

        // Update the schemeNameTranslations
        schemeNameTranslations.setName(UPDATED_NAME);
        schemeNameTranslations.setTransType(UPDATED_TRANS_TYPE);

        restSchemeNameTranslationsMockMvc.perform(put("/api/schemeNameTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(schemeNameTranslations)))
                .andExpect(status().isOk());

        // Validate the SchemeNameTranslations in the database
        List<SchemeNameTranslations> schemeNameTranslationss = schemeNameTranslationsRepository.findAll();
        assertThat(schemeNameTranslationss).hasSize(databaseSizeBeforeUpdate);
        SchemeNameTranslations testSchemeNameTranslations = schemeNameTranslationss.get(schemeNameTranslationss.size() - 1);
        assertThat(testSchemeNameTranslations.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testSchemeNameTranslations.getTransType()).isEqualTo(UPDATED_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void deleteSchemeNameTranslations() throws Exception {
        // Initialize the database
        schemeNameTranslationsRepository.saveAndFlush(schemeNameTranslations);

		int databaseSizeBeforeDelete = schemeNameTranslationsRepository.findAll().size();

        // Get the schemeNameTranslations
        restSchemeNameTranslationsMockMvc.perform(delete("/api/schemeNameTranslationss/{id}", schemeNameTranslations.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<SchemeNameTranslations> schemeNameTranslationss = schemeNameTranslationsRepository.findAll();
        assertThat(schemeNameTranslationss).hasSize(databaseSizeBeforeDelete - 1);
    }
}
