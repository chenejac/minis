package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.PersonKeywordsTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.PersonKeywordsTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PersonKeywordsTranslationsSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the PersonKeywordsTranslationsResource REST controller.
 *
 * @see PersonKeywordsTranslationsResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class PersonKeywordsTranslationsResourceTest {

    private static final String DEFAULT_KEYWORDS = "AAAAA";
    private static final String UPDATED_KEYWORDS = "BBBBB";
    private static final String DEFAULT_TRANS_TYPE = "AAAAA";
    private static final String UPDATED_TRANS_TYPE = "BBBBB";

    @Inject
    private PersonKeywordsTranslationsRepository personKeywordsTranslationsRepository;

    @Inject
    private PersonKeywordsTranslationsSearchRepository personKeywordsTranslationsSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPersonKeywordsTranslationsMockMvc;

    private PersonKeywordsTranslations personKeywordsTranslations;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PersonKeywordsTranslationsResource personKeywordsTranslationsResource = new PersonKeywordsTranslationsResource();
        ReflectionTestUtils.setField(personKeywordsTranslationsResource, "personKeywordsTranslationsRepository", personKeywordsTranslationsRepository);
        ReflectionTestUtils.setField(personKeywordsTranslationsResource, "personKeywordsTranslationsSearchRepository", personKeywordsTranslationsSearchRepository);
        this.restPersonKeywordsTranslationsMockMvc = MockMvcBuilders.standaloneSetup(personKeywordsTranslationsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        personKeywordsTranslations = new PersonKeywordsTranslations();
        personKeywordsTranslations.setKeywords(DEFAULT_KEYWORDS);
        personKeywordsTranslations.setTransType(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void createPersonKeywordsTranslations() throws Exception {
        int databaseSizeBeforeCreate = personKeywordsTranslationsRepository.findAll().size();

        // Create the PersonKeywordsTranslations

        restPersonKeywordsTranslationsMockMvc.perform(post("/api/personKeywordsTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(personKeywordsTranslations)))
                .andExpect(status().isCreated());

        // Validate the PersonKeywordsTranslations in the database
        List<PersonKeywordsTranslations> personKeywordsTranslationss = personKeywordsTranslationsRepository.findAll();
        assertThat(personKeywordsTranslationss).hasSize(databaseSizeBeforeCreate + 1);
        PersonKeywordsTranslations testPersonKeywordsTranslations = personKeywordsTranslationss.get(personKeywordsTranslationss.size() - 1);
        assertThat(testPersonKeywordsTranslations.getKeywords()).isEqualTo(DEFAULT_KEYWORDS);
        assertThat(testPersonKeywordsTranslations.getTransType()).isEqualTo(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void getAllPersonKeywordsTranslationss() throws Exception {
        // Initialize the database
        personKeywordsTranslationsRepository.saveAndFlush(personKeywordsTranslations);

        // Get all the personKeywordsTranslationss
        restPersonKeywordsTranslationsMockMvc.perform(get("/api/personKeywordsTranslationss"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(personKeywordsTranslations.getId().intValue())))
                .andExpect(jsonPath("$.[*].keywords").value(hasItem(DEFAULT_KEYWORDS.toString())))
                .andExpect(jsonPath("$.[*].transType").value(hasItem(DEFAULT_TRANS_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getPersonKeywordsTranslations() throws Exception {
        // Initialize the database
        personKeywordsTranslationsRepository.saveAndFlush(personKeywordsTranslations);

        // Get the personKeywordsTranslations
        restPersonKeywordsTranslationsMockMvc.perform(get("/api/personKeywordsTranslationss/{id}", personKeywordsTranslations.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(personKeywordsTranslations.getId().intValue()))
            .andExpect(jsonPath("$.keywords").value(DEFAULT_KEYWORDS.toString()))
            .andExpect(jsonPath("$.transType").value(DEFAULT_TRANS_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPersonKeywordsTranslations() throws Exception {
        // Get the personKeywordsTranslations
        restPersonKeywordsTranslationsMockMvc.perform(get("/api/personKeywordsTranslationss/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePersonKeywordsTranslations() throws Exception {
        // Initialize the database
        personKeywordsTranslationsRepository.saveAndFlush(personKeywordsTranslations);

		int databaseSizeBeforeUpdate = personKeywordsTranslationsRepository.findAll().size();

        // Update the personKeywordsTranslations
        personKeywordsTranslations.setKeywords(UPDATED_KEYWORDS);
        personKeywordsTranslations.setTransType(UPDATED_TRANS_TYPE);

        restPersonKeywordsTranslationsMockMvc.perform(put("/api/personKeywordsTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(personKeywordsTranslations)))
                .andExpect(status().isOk());

        // Validate the PersonKeywordsTranslations in the database
        List<PersonKeywordsTranslations> personKeywordsTranslationss = personKeywordsTranslationsRepository.findAll();
        assertThat(personKeywordsTranslationss).hasSize(databaseSizeBeforeUpdate);
        PersonKeywordsTranslations testPersonKeywordsTranslations = personKeywordsTranslationss.get(personKeywordsTranslationss.size() - 1);
        assertThat(testPersonKeywordsTranslations.getKeywords()).isEqualTo(UPDATED_KEYWORDS);
        assertThat(testPersonKeywordsTranslations.getTransType()).isEqualTo(UPDATED_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void deletePersonKeywordsTranslations() throws Exception {
        // Initialize the database
        personKeywordsTranslationsRepository.saveAndFlush(personKeywordsTranslations);

		int databaseSizeBeforeDelete = personKeywordsTranslationsRepository.findAll().size();

        // Get the personKeywordsTranslations
        restPersonKeywordsTranslationsMockMvc.perform(delete("/api/personKeywordsTranslationss/{id}", personKeywordsTranslations.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<PersonKeywordsTranslations> personKeywordsTranslationss = personKeywordsTranslationsRepository.findAll();
        assertThat(personKeywordsTranslationss).hasSize(databaseSizeBeforeDelete - 1);
    }
}
