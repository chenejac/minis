package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.UserAccount;
import rs.ac.uns.ftn.informatika.minis.repository.UserAccountRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.UserAccountSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the UserAccountResource REST controller.
 *
 * @see UserAccountResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class UserAccountResourceTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_EMAIL = "AAAAA";
    private static final String UPDATED_EMAIL = "BBBBB";
    private static final String DEFAULT_USERPASSWORD = "AAAAA";
    private static final String UPDATED_USERPASSWORD = "BBBBB";
    private static final String DEFAULT_NOTE = "AAAAA";
    private static final String UPDATED_NOTE = "BBBBB";
    private static final String DEFAULT_ACTIVATION_CODE = "AAAAA";
    private static final String UPDATED_ACTIVATION_CODE = "BBBBB";

    @Inject
    private UserAccountRepository userAccountRepository;

    @Inject
    private UserAccountSearchRepository userAccountSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restUserAccountMockMvc;

    private UserAccount userAccount;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        UserAccountResource userAccountResource = new UserAccountResource();
        ReflectionTestUtils.setField(userAccountResource, "userAccountRepository", userAccountRepository);
        ReflectionTestUtils.setField(userAccountResource, "userAccountSearchRepository", userAccountSearchRepository);
        this.restUserAccountMockMvc = MockMvcBuilders.standaloneSetup(userAccountResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        userAccount = new UserAccount();
        userAccount.setName(DEFAULT_NAME);
        userAccount.setEmail(DEFAULT_EMAIL);
        userAccount.setUserpassword(DEFAULT_USERPASSWORD);
        userAccount.setNote(DEFAULT_NOTE);
        userAccount.setActivationCode(DEFAULT_ACTIVATION_CODE);
    }

    @Test
    @Transactional
    public void createUserAccount() throws Exception {
        int databaseSizeBeforeCreate = userAccountRepository.findAll().size();

        // Create the UserAccount

        restUserAccountMockMvc.perform(post("/api/userAccounts")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userAccount)))
                .andExpect(status().isCreated());

        // Validate the UserAccount in the database
        List<UserAccount> userAccounts = userAccountRepository.findAll();
        assertThat(userAccounts).hasSize(databaseSizeBeforeCreate + 1);
        UserAccount testUserAccount = userAccounts.get(userAccounts.size() - 1);
        assertThat(testUserAccount.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testUserAccount.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testUserAccount.getUserpassword()).isEqualTo(DEFAULT_USERPASSWORD);
        assertThat(testUserAccount.getNote()).isEqualTo(DEFAULT_NOTE);
        assertThat(testUserAccount.getActivationCode()).isEqualTo(DEFAULT_ACTIVATION_CODE);
    }

    @Test
    @Transactional
    public void getAllUserAccounts() throws Exception {
        // Initialize the database
        userAccountRepository.saveAndFlush(userAccount);

        // Get all the userAccounts
        restUserAccountMockMvc.perform(get("/api/userAccounts"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(userAccount.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
                .andExpect(jsonPath("$.[*].userpassword").value(hasItem(DEFAULT_USERPASSWORD.toString())))
                .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
                .andExpect(jsonPath("$.[*].activationCode").value(hasItem(DEFAULT_ACTIVATION_CODE.toString())));
    }

    @Test
    @Transactional
    public void getUserAccount() throws Exception {
        // Initialize the database
        userAccountRepository.saveAndFlush(userAccount);

        // Get the userAccount
        restUserAccountMockMvc.perform(get("/api/userAccounts/{id}", userAccount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(userAccount.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.userpassword").value(DEFAULT_USERPASSWORD.toString()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()))
            .andExpect(jsonPath("$.activationCode").value(DEFAULT_ACTIVATION_CODE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingUserAccount() throws Exception {
        // Get the userAccount
        restUserAccountMockMvc.perform(get("/api/userAccounts/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserAccount() throws Exception {
        // Initialize the database
        userAccountRepository.saveAndFlush(userAccount);

		int databaseSizeBeforeUpdate = userAccountRepository.findAll().size();

        // Update the userAccount
        userAccount.setName(UPDATED_NAME);
        userAccount.setEmail(UPDATED_EMAIL);
        userAccount.setUserpassword(UPDATED_USERPASSWORD);
        userAccount.setNote(UPDATED_NOTE);
        userAccount.setActivationCode(UPDATED_ACTIVATION_CODE);

        restUserAccountMockMvc.perform(put("/api/userAccounts")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userAccount)))
                .andExpect(status().isOk());

        // Validate the UserAccount in the database
        List<UserAccount> userAccounts = userAccountRepository.findAll();
        assertThat(userAccounts).hasSize(databaseSizeBeforeUpdate);
        UserAccount testUserAccount = userAccounts.get(userAccounts.size() - 1);
        assertThat(testUserAccount.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testUserAccount.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testUserAccount.getUserpassword()).isEqualTo(UPDATED_USERPASSWORD);
        assertThat(testUserAccount.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testUserAccount.getActivationCode()).isEqualTo(UPDATED_ACTIVATION_CODE);
    }

    @Test
    @Transactional
    public void deleteUserAccount() throws Exception {
        // Initialize the database
        userAccountRepository.saveAndFlush(userAccount);

		int databaseSizeBeforeDelete = userAccountRepository.findAll().size();

        // Get the userAccount
        restUserAccountMockMvc.perform(delete("/api/userAccounts/{id}", userAccount.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<UserAccount> userAccounts = userAccountRepository.findAll();
        assertThat(userAccounts).hasSize(databaseSizeBeforeDelete - 1);
    }
}
