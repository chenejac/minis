package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.Privilege;
import rs.ac.uns.ftn.informatika.minis.repository.PrivilegeRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.PrivilegeSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the PrivilegeResource REST controller.
 *
 * @see PrivilegeResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class PrivilegeResourceTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";
    private static final String DEFAULT_NOTE = "AAAAA";
    private static final String UPDATED_NOTE = "BBBBB";
    private static final String DEFAULT_ENTITY = "AAAAA";
    private static final String UPDATED_ENTITY = "BBBBB";
    private static final String DEFAULT_ACTION = "AAAAA";
    private static final String UPDATED_ACTION = "BBBBB";
    private static final String DEFAULT_FORM = "AAAAA";
    private static final String UPDATED_FORM = "BBBBB";

    @Inject
    private PrivilegeRepository privilegeRepository;

    @Inject
    private PrivilegeSearchRepository privilegeSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPrivilegeMockMvc;

    private Privilege privilege;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PrivilegeResource privilegeResource = new PrivilegeResource();
        ReflectionTestUtils.setField(privilegeResource, "privilegeRepository", privilegeRepository);
        ReflectionTestUtils.setField(privilegeResource, "privilegeSearchRepository", privilegeSearchRepository);
        this.restPrivilegeMockMvc = MockMvcBuilders.standaloneSetup(privilegeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        privilege = new Privilege();
        privilege.setName(DEFAULT_NAME);
        privilege.setDescription(DEFAULT_DESCRIPTION);
        privilege.setNote(DEFAULT_NOTE);
        privilege.setEntity(DEFAULT_ENTITY);
        privilege.setAction(DEFAULT_ACTION);
        privilege.setForm(DEFAULT_FORM);
    }

    @Test
    @Transactional
    public void createPrivilege() throws Exception {
        int databaseSizeBeforeCreate = privilegeRepository.findAll().size();

        // Create the Privilege

        restPrivilegeMockMvc.perform(post("/api/privileges")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(privilege)))
                .andExpect(status().isCreated());

        // Validate the Privilege in the database
        List<Privilege> privileges = privilegeRepository.findAll();
        assertThat(privileges).hasSize(databaseSizeBeforeCreate + 1);
        Privilege testPrivilege = privileges.get(privileges.size() - 1);
        assertThat(testPrivilege.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPrivilege.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testPrivilege.getNote()).isEqualTo(DEFAULT_NOTE);
        assertThat(testPrivilege.getEntity()).isEqualTo(DEFAULT_ENTITY);
        assertThat(testPrivilege.getAction()).isEqualTo(DEFAULT_ACTION);
        assertThat(testPrivilege.getForm()).isEqualTo(DEFAULT_FORM);
    }

    @Test
    @Transactional
    public void getAllPrivileges() throws Exception {
        // Initialize the database
        privilegeRepository.saveAndFlush(privilege);

        // Get all the privileges
        restPrivilegeMockMvc.perform(get("/api/privileges"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(privilege.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
                .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
                .andExpect(jsonPath("$.[*].entity").value(hasItem(DEFAULT_ENTITY.toString())))
                .andExpect(jsonPath("$.[*].action").value(hasItem(DEFAULT_ACTION.toString())))
                .andExpect(jsonPath("$.[*].form").value(hasItem(DEFAULT_FORM.toString())));
    }

    @Test
    @Transactional
    public void getPrivilege() throws Exception {
        // Initialize the database
        privilegeRepository.saveAndFlush(privilege);

        // Get the privilege
        restPrivilegeMockMvc.perform(get("/api/privileges/{id}", privilege.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(privilege.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()))
            .andExpect(jsonPath("$.entity").value(DEFAULT_ENTITY.toString()))
            .andExpect(jsonPath("$.action").value(DEFAULT_ACTION.toString()))
            .andExpect(jsonPath("$.form").value(DEFAULT_FORM.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPrivilege() throws Exception {
        // Get the privilege
        restPrivilegeMockMvc.perform(get("/api/privileges/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePrivilege() throws Exception {
        // Initialize the database
        privilegeRepository.saveAndFlush(privilege);

		int databaseSizeBeforeUpdate = privilegeRepository.findAll().size();

        // Update the privilege
        privilege.setName(UPDATED_NAME);
        privilege.setDescription(UPDATED_DESCRIPTION);
        privilege.setNote(UPDATED_NOTE);
        privilege.setEntity(UPDATED_ENTITY);
        privilege.setAction(UPDATED_ACTION);
        privilege.setForm(UPDATED_FORM);

        restPrivilegeMockMvc.perform(put("/api/privileges")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(privilege)))
                .andExpect(status().isOk());

        // Validate the Privilege in the database
        List<Privilege> privileges = privilegeRepository.findAll();
        assertThat(privileges).hasSize(databaseSizeBeforeUpdate);
        Privilege testPrivilege = privileges.get(privileges.size() - 1);
        assertThat(testPrivilege.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPrivilege.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testPrivilege.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testPrivilege.getEntity()).isEqualTo(UPDATED_ENTITY);
        assertThat(testPrivilege.getAction()).isEqualTo(UPDATED_ACTION);
        assertThat(testPrivilege.getForm()).isEqualTo(UPDATED_FORM);
    }

    @Test
    @Transactional
    public void deletePrivilege() throws Exception {
        // Initialize the database
        privilegeRepository.saveAndFlush(privilege);

		int databaseSizeBeforeDelete = privilegeRepository.findAll().size();

        // Get the privilege
        restPrivilegeMockMvc.perform(delete("/api/privileges/{id}", privilege.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Privilege> privileges = privilegeRepository.findAll();
        assertThat(privileges).hasSize(databaseSizeBeforeDelete - 1);
    }
}
