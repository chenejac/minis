package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.TitleInstitution;
import rs.ac.uns.ftn.informatika.minis.repository.TitleInstitutionRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.TitleInstitutionSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the TitleInstitutionResource REST controller.
 *
 * @see TitleInstitutionResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class TitleInstitutionResourceTest {

    private static final String DEFAULT_TITLE = "AAAAA";
    private static final String UPDATED_TITLE = "BBBBB";

    private static final Integer DEFAULT_YEAR = 1;
    private static final Integer UPDATED_YEAR = 2;

    @Inject
    private TitleInstitutionRepository titleInstitutionRepository;

    @Inject
    private TitleInstitutionSearchRepository titleInstitutionSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restTitleInstitutionMockMvc;

    private TitleInstitution titleInstitution;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TitleInstitutionResource titleInstitutionResource = new TitleInstitutionResource();
        ReflectionTestUtils.setField(titleInstitutionResource, "titleInstitutionRepository", titleInstitutionRepository);
        ReflectionTestUtils.setField(titleInstitutionResource, "titleInstitutionSearchRepository", titleInstitutionSearchRepository);
        this.restTitleInstitutionMockMvc = MockMvcBuilders.standaloneSetup(titleInstitutionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        titleInstitution = new TitleInstitution();
        titleInstitution.setTitle(DEFAULT_TITLE);
        titleInstitution.setYear(DEFAULT_YEAR);
    }

    @Test
    @Transactional
    public void createTitleInstitution() throws Exception {
        int databaseSizeBeforeCreate = titleInstitutionRepository.findAll().size();

        // Create the TitleInstitution

        restTitleInstitutionMockMvc.perform(post("/api/titleInstitutions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(titleInstitution)))
                .andExpect(status().isCreated());

        // Validate the TitleInstitution in the database
        List<TitleInstitution> titleInstitutions = titleInstitutionRepository.findAll();
        assertThat(titleInstitutions).hasSize(databaseSizeBeforeCreate + 1);
        TitleInstitution testTitleInstitution = titleInstitutions.get(titleInstitutions.size() - 1);
        assertThat(testTitleInstitution.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testTitleInstitution.getYear()).isEqualTo(DEFAULT_YEAR);
    }

    @Test
    @Transactional
    public void getAllTitleInstitutions() throws Exception {
        // Initialize the database
        titleInstitutionRepository.saveAndFlush(titleInstitution);

        // Get all the titleInstitutions
        restTitleInstitutionMockMvc.perform(get("/api/titleInstitutions"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(titleInstitution.getId().intValue())))
                .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
                .andExpect(jsonPath("$.[*].year").value(hasItem(DEFAULT_YEAR)));
    }

    @Test
    @Transactional
    public void getTitleInstitution() throws Exception {
        // Initialize the database
        titleInstitutionRepository.saveAndFlush(titleInstitution);

        // Get the titleInstitution
        restTitleInstitutionMockMvc.perform(get("/api/titleInstitutions/{id}", titleInstitution.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(titleInstitution.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.year").value(DEFAULT_YEAR));
    }

    @Test
    @Transactional
    public void getNonExistingTitleInstitution() throws Exception {
        // Get the titleInstitution
        restTitleInstitutionMockMvc.perform(get("/api/titleInstitutions/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTitleInstitution() throws Exception {
        // Initialize the database
        titleInstitutionRepository.saveAndFlush(titleInstitution);

		int databaseSizeBeforeUpdate = titleInstitutionRepository.findAll().size();

        // Update the titleInstitution
        titleInstitution.setTitle(UPDATED_TITLE);
        titleInstitution.setYear(UPDATED_YEAR);

        restTitleInstitutionMockMvc.perform(put("/api/titleInstitutions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(titleInstitution)))
                .andExpect(status().isOk());

        // Validate the TitleInstitution in the database
        List<TitleInstitution> titleInstitutions = titleInstitutionRepository.findAll();
        assertThat(titleInstitutions).hasSize(databaseSizeBeforeUpdate);
        TitleInstitution testTitleInstitution = titleInstitutions.get(titleInstitutions.size() - 1);
        assertThat(testTitleInstitution.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testTitleInstitution.getYear()).isEqualTo(UPDATED_YEAR);
    }

    @Test
    @Transactional
    public void deleteTitleInstitution() throws Exception {
        // Initialize the database
        titleInstitutionRepository.saveAndFlush(titleInstitution);

		int databaseSizeBeforeDelete = titleInstitutionRepository.findAll().size();

        // Get the titleInstitution
        restTitleInstitutionMockMvc.perform(delete("/api/titleInstitutions/{id}", titleInstitution.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<TitleInstitution> titleInstitutions = titleInstitutionRepository.findAll();
        assertThat(titleInstitutions).hasSize(databaseSizeBeforeDelete - 1);
    }
}
