package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.FunctionNameTranslations;
import rs.ac.uns.ftn.informatika.minis.repository.FunctionNameTranslationsRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.FunctionNameTranslationsSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the FunctionNameTranslationsResource REST controller.
 *
 * @see FunctionNameTranslationsResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class FunctionNameTranslationsResourceTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_TRANS_TYPE = "AAAAA";
    private static final String UPDATED_TRANS_TYPE = "BBBBB";

    @Inject
    private FunctionNameTranslationsRepository functionNameTranslationsRepository;

    @Inject
    private FunctionNameTranslationsSearchRepository functionNameTranslationsSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restFunctionNameTranslationsMockMvc;

    private FunctionNameTranslations functionNameTranslations;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        FunctionNameTranslationsResource functionNameTranslationsResource = new FunctionNameTranslationsResource();
        ReflectionTestUtils.setField(functionNameTranslationsResource, "functionNameTranslationsRepository", functionNameTranslationsRepository);
        ReflectionTestUtils.setField(functionNameTranslationsResource, "functionNameTranslationsSearchRepository", functionNameTranslationsSearchRepository);
        this.restFunctionNameTranslationsMockMvc = MockMvcBuilders.standaloneSetup(functionNameTranslationsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        functionNameTranslations = new FunctionNameTranslations();
        functionNameTranslations.setName(DEFAULT_NAME);
        functionNameTranslations.setTransType(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void createFunctionNameTranslations() throws Exception {
        int databaseSizeBeforeCreate = functionNameTranslationsRepository.findAll().size();

        // Create the FunctionNameTranslations

        restFunctionNameTranslationsMockMvc.perform(post("/api/functionNameTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(functionNameTranslations)))
                .andExpect(status().isCreated());

        // Validate the FunctionNameTranslations in the database
        List<FunctionNameTranslations> functionNameTranslationss = functionNameTranslationsRepository.findAll();
        assertThat(functionNameTranslationss).hasSize(databaseSizeBeforeCreate + 1);
        FunctionNameTranslations testFunctionNameTranslations = functionNameTranslationss.get(functionNameTranslationss.size() - 1);
        assertThat(testFunctionNameTranslations.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testFunctionNameTranslations.getTransType()).isEqualTo(DEFAULT_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void getAllFunctionNameTranslationss() throws Exception {
        // Initialize the database
        functionNameTranslationsRepository.saveAndFlush(functionNameTranslations);

        // Get all the functionNameTranslationss
        restFunctionNameTranslationsMockMvc.perform(get("/api/functionNameTranslationss"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(functionNameTranslations.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].transType").value(hasItem(DEFAULT_TRANS_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getFunctionNameTranslations() throws Exception {
        // Initialize the database
        functionNameTranslationsRepository.saveAndFlush(functionNameTranslations);

        // Get the functionNameTranslations
        restFunctionNameTranslationsMockMvc.perform(get("/api/functionNameTranslationss/{id}", functionNameTranslations.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(functionNameTranslations.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.transType").value(DEFAULT_TRANS_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingFunctionNameTranslations() throws Exception {
        // Get the functionNameTranslations
        restFunctionNameTranslationsMockMvc.perform(get("/api/functionNameTranslationss/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFunctionNameTranslations() throws Exception {
        // Initialize the database
        functionNameTranslationsRepository.saveAndFlush(functionNameTranslations);

		int databaseSizeBeforeUpdate = functionNameTranslationsRepository.findAll().size();

        // Update the functionNameTranslations
        functionNameTranslations.setName(UPDATED_NAME);
        functionNameTranslations.setTransType(UPDATED_TRANS_TYPE);

        restFunctionNameTranslationsMockMvc.perform(put("/api/functionNameTranslationss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(functionNameTranslations)))
                .andExpect(status().isOk());

        // Validate the FunctionNameTranslations in the database
        List<FunctionNameTranslations> functionNameTranslationss = functionNameTranslationsRepository.findAll();
        assertThat(functionNameTranslationss).hasSize(databaseSizeBeforeUpdate);
        FunctionNameTranslations testFunctionNameTranslations = functionNameTranslationss.get(functionNameTranslationss.size() - 1);
        assertThat(testFunctionNameTranslations.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testFunctionNameTranslations.getTransType()).isEqualTo(UPDATED_TRANS_TYPE);
    }

    @Test
    @Transactional
    public void deleteFunctionNameTranslations() throws Exception {
        // Initialize the database
        functionNameTranslationsRepository.saveAndFlush(functionNameTranslations);

		int databaseSizeBeforeDelete = functionNameTranslationsRepository.findAll().size();

        // Get the functionNameTranslations
        restFunctionNameTranslationsMockMvc.perform(delete("/api/functionNameTranslationss/{id}", functionNameTranslations.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<FunctionNameTranslations> functionNameTranslationss = functionNameTranslationsRepository.findAll();
        assertThat(functionNameTranslationss).hasSize(databaseSizeBeforeDelete - 1);
    }
}
