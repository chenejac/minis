package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.InstitutionType;
import rs.ac.uns.ftn.informatika.minis.repository.InstitutionTypeRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.InstitutionTypeSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the InstitutionTypeResource REST controller.
 *
 * @see InstitutionTypeResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class InstitutionTypeResourceTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    @Inject
    private InstitutionTypeRepository institutionTypeRepository;

    @Inject
    private InstitutionTypeSearchRepository institutionTypeSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restInstitutionTypeMockMvc;

    private InstitutionType institutionType;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        InstitutionTypeResource institutionTypeResource = new InstitutionTypeResource();
        ReflectionTestUtils.setField(institutionTypeResource, "institutionTypeRepository", institutionTypeRepository);
        ReflectionTestUtils.setField(institutionTypeResource, "institutionTypeSearchRepository", institutionTypeSearchRepository);
        this.restInstitutionTypeMockMvc = MockMvcBuilders.standaloneSetup(institutionTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        institutionType = new InstitutionType();
        institutionType.setName(DEFAULT_NAME);
        institutionType.setDescription(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createInstitutionType() throws Exception {
        int databaseSizeBeforeCreate = institutionTypeRepository.findAll().size();

        // Create the InstitutionType

        restInstitutionTypeMockMvc.perform(post("/api/institutionTypes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(institutionType)))
                .andExpect(status().isCreated());

        // Validate the InstitutionType in the database
        List<InstitutionType> institutionTypes = institutionTypeRepository.findAll();
        assertThat(institutionTypes).hasSize(databaseSizeBeforeCreate + 1);
        InstitutionType testInstitutionType = institutionTypes.get(institutionTypes.size() - 1);
        assertThat(testInstitutionType.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testInstitutionType.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllInstitutionTypes() throws Exception {
        // Initialize the database
        institutionTypeRepository.saveAndFlush(institutionType);

        // Get all the institutionTypes
        restInstitutionTypeMockMvc.perform(get("/api/institutionTypes"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(institutionType.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getInstitutionType() throws Exception {
        // Initialize the database
        institutionTypeRepository.saveAndFlush(institutionType);

        // Get the institutionType
        restInstitutionTypeMockMvc.perform(get("/api/institutionTypes/{id}", institutionType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(institutionType.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingInstitutionType() throws Exception {
        // Get the institutionType
        restInstitutionTypeMockMvc.perform(get("/api/institutionTypes/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInstitutionType() throws Exception {
        // Initialize the database
        institutionTypeRepository.saveAndFlush(institutionType);

		int databaseSizeBeforeUpdate = institutionTypeRepository.findAll().size();

        // Update the institutionType
        institutionType.setName(UPDATED_NAME);
        institutionType.setDescription(UPDATED_DESCRIPTION);

        restInstitutionTypeMockMvc.perform(put("/api/institutionTypes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(institutionType)))
                .andExpect(status().isOk());

        // Validate the InstitutionType in the database
        List<InstitutionType> institutionTypes = institutionTypeRepository.findAll();
        assertThat(institutionTypes).hasSize(databaseSizeBeforeUpdate);
        InstitutionType testInstitutionType = institutionTypes.get(institutionTypes.size() - 1);
        assertThat(testInstitutionType.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testInstitutionType.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void deleteInstitutionType() throws Exception {
        // Initialize the database
        institutionTypeRepository.saveAndFlush(institutionType);

		int databaseSizeBeforeDelete = institutionTypeRepository.findAll().size();

        // Get the institutionType
        restInstitutionTypeMockMvc.perform(delete("/api/institutionTypes/{id}", institutionType.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<InstitutionType> institutionTypes = institutionTypeRepository.findAll();
        assertThat(institutionTypes).hasSize(databaseSizeBeforeDelete - 1);
    }
}
