package rs.ac.uns.ftn.informatika.minis.web.rest;

import rs.ac.uns.ftn.informatika.minis.Application;
import rs.ac.uns.ftn.informatika.minis.domain.ResearchArea;
import rs.ac.uns.ftn.informatika.minis.repository.ResearchAreaRepository;
import rs.ac.uns.ftn.informatika.minis.repository.search.ResearchAreaSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ResearchAreaResource REST controller.
 *
 * @see ResearchAreaResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class ResearchAreaResourceTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    @Inject
    private ResearchAreaRepository researchAreaRepository;

    @Inject
    private ResearchAreaSearchRepository researchAreaSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restResearchAreaMockMvc;

    private ResearchArea researchArea;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ResearchAreaResource researchAreaResource = new ResearchAreaResource();
        ReflectionTestUtils.setField(researchAreaResource, "researchAreaRepository", researchAreaRepository);
        ReflectionTestUtils.setField(researchAreaResource, "researchAreaSearchRepository", researchAreaSearchRepository);
        this.restResearchAreaMockMvc = MockMvcBuilders.standaloneSetup(researchAreaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        researchArea = new ResearchArea();
        researchArea.setName(DEFAULT_NAME);
        researchArea.setDescription(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createResearchArea() throws Exception {
        int databaseSizeBeforeCreate = researchAreaRepository.findAll().size();

        // Create the ResearchArea

        restResearchAreaMockMvc.perform(post("/api/researchAreas")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(researchArea)))
                .andExpect(status().isCreated());

        // Validate the ResearchArea in the database
        List<ResearchArea> researchAreas = researchAreaRepository.findAll();
        assertThat(researchAreas).hasSize(databaseSizeBeforeCreate + 1);
        ResearchArea testResearchArea = researchAreas.get(researchAreas.size() - 1);
        assertThat(testResearchArea.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testResearchArea.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllResearchAreas() throws Exception {
        // Initialize the database
        researchAreaRepository.saveAndFlush(researchArea);

        // Get all the researchAreas
        restResearchAreaMockMvc.perform(get("/api/researchAreas"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(researchArea.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getResearchArea() throws Exception {
        // Initialize the database
        researchAreaRepository.saveAndFlush(researchArea);

        // Get the researchArea
        restResearchAreaMockMvc.perform(get("/api/researchAreas/{id}", researchArea.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(researchArea.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingResearchArea() throws Exception {
        // Get the researchArea
        restResearchAreaMockMvc.perform(get("/api/researchAreas/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateResearchArea() throws Exception {
        // Initialize the database
        researchAreaRepository.saveAndFlush(researchArea);

		int databaseSizeBeforeUpdate = researchAreaRepository.findAll().size();

        // Update the researchArea
        researchArea.setName(UPDATED_NAME);
        researchArea.setDescription(UPDATED_DESCRIPTION);

        restResearchAreaMockMvc.perform(put("/api/researchAreas")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(researchArea)))
                .andExpect(status().isOk());

        // Validate the ResearchArea in the database
        List<ResearchArea> researchAreas = researchAreaRepository.findAll();
        assertThat(researchAreas).hasSize(databaseSizeBeforeUpdate);
        ResearchArea testResearchArea = researchAreas.get(researchAreas.size() - 1);
        assertThat(testResearchArea.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testResearchArea.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void deleteResearchArea() throws Exception {
        // Initialize the database
        researchAreaRepository.saveAndFlush(researchArea);

		int databaseSizeBeforeDelete = researchAreaRepository.findAll().size();

        // Get the researchArea
        restResearchAreaMockMvc.perform(delete("/api/researchAreas/{id}", researchArea.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ResearchArea> researchAreas = researchAreaRepository.findAll();
        assertThat(researchAreas).hasSize(databaseSizeBeforeDelete - 1);
    }
}
